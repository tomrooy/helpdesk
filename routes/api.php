<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('pisah', function(){
    $unitkerja = "Inspektorat Jenderal - Sekretariat Inspektorat Jenderal - Koordinator Hubungan Masyarakat Dan Sistem Informasi Pengawasan - Subkoordinator Pengelolaan Teknologi Informasi";
    $unitkerja=  (explode("-",$unitkerja));
    return end($unitkerja);
});