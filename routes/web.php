<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\GroupController;
use App\Http\Controllers\Admin\RoleController;

use App\Http\Controllers\Front\HelpdeskController;
use App\Http\Controllers\Front\AttendanceController;
use App\Http\Controllers\Front\ZoomRequestController;
use App\Http\Controllers\Front\DailyPresentController;
use App\Http\Controllers\Front\ApelPresentController;
use App\Http\Controllers\Front\BmnRequestController;
use App\Http\Controllers\Front\NologinController;
use Illuminate\Support\Facades\Http;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('sync-simpeg', [UserController::class, 'syncSimpeg']);
Route::get('add-url/{id}', [AttendanceController::class, 'addurl']);

Route::get('/',function(){
    $zoom           =   \App\Models\Zoom::year(2023)->get();
    $zoomaccount    =   \App\Models\ZoomAccount::all();
    return view('welcome',[
        'zoom'          =>  $zoom,
        'zoomaccount'   =>  $zoomaccount
    ]);
})->name('front.helpdeskalpha.index');

Route::get('layanan-it',[NologinController::class, 'index'])->name('nologin.index');


Route::get('form-permintaan-zoom',[NologinController::class, 'viewRequestZoom'])->name('nologin.view.requestZoom');
Route::post('form-permintaan-zoom/simpan',[NologinController::class, 'storeRequestZoom'])->name('nologin.store.requestZoom');

Route::get('form-bantuan-layanan-ti',[NologinController::class, 'viewHelpdesk'])->name('nologin.view.Helpdesk');
Route::post('form-bantuan-layanan-ti/simpan',[NologinController::class, 'storeHelpdesk'])->name('nologin.store.Helpdesk');



Route::get('presensi-harian-sip',[DailyPresentController::class,'index'])->name('daily.present.dashboard');
Route::get('presensi-harian',[DailyPresentController::class,'getData'])->name('getData.daily.present');
Route::post('presensi-harian-sip/save',[DailyPresentController::class,'store'])->name('daily.present.store');
Route::get('presensi-harian-sip/rekap',[DailyPresentController::class,'rekap'])->name('daily.present.rekap');
Route::post('presensi-harian-sip/rekap/save',[DailyPresentController::class,'rekap_store'])->name('daily.present.rekap.store');


Route::get('presensi-virtual',[AttendanceController::class,'dashboard'])->name('attendance.dashboard');
Route::get('presensi-rekap/{id}/excel',[AttendanceController::class, 'reportExcelAttendance'])->name('report_excel.attendance');
Route::get('presensi-rekap/{id}/pdf',[AttendanceController::class, 'reportAttendance_pdf'])->name('report_pdf.attendance');
Route::get('presensi-rekap/{id}',[AttendanceController::class, 'reportAttendance'])->name('report.attendance');
Route::post('presensi-rekap/personal',[AttendanceController::class, 'reportAttendancePersonal'])->name('attendance.print.personal');
Route::post('presensi-rekap/group',[AttendanceController::class, 'reportAttendancegroup'])->name('attendance.print.group');
Route::get('presensi-virtual/{id}',[AttendanceController::class, 'index'])->name('attendance.index');
Route::get('presensi-virtual/manual/{id}',[AttendanceController::class, 'index2'])->name('attendance.index2');
Route::post('presensi-virtual',[AttendanceController::class, 'store'])->name('attendance.store');




Route::get('presensi-apel/{id}',[ApelPresentController::class , 'index'])->name('apel.form');
Route::post('presensi-apel/store',[ApelPresentController::class , 'store'])->name('apel.store');

Route::get('presensi-manual/{id}',[AttendanceController::class,'apelManual'])->name('apel.manual');
Route::post('presensi-manual/store',[AttendanceController::class,'store_apelManual'])->name('apel.manual.simpan');

Route::post('absensi-cekdata', [AttendanceController::class, 'seacrhSimpeg'])->name('search.simpeg');
Route::get('absensi-cek', [AttendanceController::class, 'syncUser'])->name('search.user');
Route::get('search-nip',[AttendanceController::class, 'searchNip'])->name('search_nip');



//Route::get('/helpdesk',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'create'])->name('front.helpdeskalpha.index');
Route::get('/getsubcategory/{id}',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'getSubcategory'])->name('getsubcategory');
Route::post('/helpdesk/simpan',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'store'])->name('front.helpdeskalpha.store');
Route::get('/reload-captcha', [App\Http\Controllers\Front\HelpdeskAlphaController::class, 'reloadCaptcha']);
Route::get('/search-name',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'searchName'])->name('search_name');
Route::get('/search-ticket',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'searchTicket'])->name('search_ticket');
Route::get('/get_ticket/{id}',[App\Http\Controllers\Front\HelpdeskController::class, 'getTicket'])->name('get_ticket');
Route::get('/export_ticket/{id}',[App\Http\Controllers\Front\HelpdeskController::class, 'exportTicket'])->name('export_ticket');
Route::get('/personal/gethelpdesk',[App\Http\Controllers\Front\HelpdeskController::class, 'getHelpdesk'])->name('front.helpdesk.data');

Route::get('e-ekspedisi/input',[App\Http\Controllers\Front\EkspedisiController::class, 'create'])->name('front.ekspedisi.create');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard',[HomeController::class, 'index'])->name('home');
    Route::get('/profile/{id}',[UserController::class, 'profile'])->name('profile');
    Route::get('/user/change-password',[UserController::class, 'viewChangePassword'])->name('user.change.password.view');
    Route::put('/user/change-password/loading',[UserController::class, 'actionChangePassword'])->name('user.change.password.process');
    Route::get('faq',[UserController::class, 'actionFaq'])->name('user.faq');

    
    Route::group(['prefix' => 'admin', 'middleware'=> 'role:Super_Admin'],function () {
        Route::post('/user/import/process',[UserController::class,'importProcess'])->name('user.import.process');
        Route::post('/user/import/process/loading',[UserController::class,'importProcessAlpha'])->name('user.import.process_alpha');
        Route::get('/user/import/',[UserController::class,'import'])->name('user.import');
        Route::get('/user', [UserController::class, 'index'])->name('user.index');

        Route::get('/golongan',[GroupController::class, 'index'])->name('group.index');
        Route::post('/golongan/simpan',[GroupController::class, 'store'])->name('group.store');
        Route::get('/golongan/{id}/edit',[GroupController::class,'edit'])->name('group.edit');
        Route::post('/golongan/update',[GroupController::class,'update'])->name('group.update');
        Route::post('/golongan/{id}/delete',[GroupController::class,'destroy'])->name('group.delete');

        route::get('/home/chart/helpdesk',[HomeController::class,'helpdeskChart'])->name('helpdesk.chart');
        route::get('/home/export/report',[HomeController::class,'report'])->name('helpdesk.report');
        route::post('/home/export/report/process',[HomeController::class,'processReport'])->name('helpdesk.report.process');

        route::get('/roles',[RoleController::class,'index'])->name('role.index');
        route::post('/roles/simpan',[RoleController::class,'store'])->name('role.store');
        route::delete('/roles/{id}/delete',[RoleController::class,'destroy'])->name('role.destroy');

        Route::post('/role/permission', [RoleController::class,'addPermission'])->name('role.add_permission');
        Route::get('/role/role-permission', [RoleController::class,'rolePermission'])->name('role.roles_permission');
        Route::put('/role/permission/{role}', [RoleController::class,'setRolePermission'])->name('role.setRolePermission');
        Route::delete('/role/permission/{id}/delete', [RoleController::class,'deletePermission'])->name('role.setpermission.destroy');
    });


    Route::group(['prefix' => 'personal', 'middleware'=> 'role:User|Kepegawaian|User_Bmn'],function () {
        Route::get('/helpdesk',[HelpdeskController::class, 'index'])->name('front.helpdesk.index');
        Route::get('/helpdesk/detail/{id}',[HelpdeskController::class, 'detail'])->name('front.helpdesk.detail');
        Route::get('/helpdesk/create',[HelpdeskController::class, 'create'])->name('front.helpdesk.create');
        Route::post('/helpdesk/simpan',[HelpdeskController::class, 'store'])->name('front.helpdesk.store');


        Route::get('/zoom-request',[ZoomRequestController::class, 'index'])->name('front.zoom_request.index');
        Route::get('/zoom-request/create',[ZoomRequestController::class, 'create'])->name('front.zoom_request.create');
        Route::get('/zoom-request/{id}/detail',[ZoomRequestController::class, 'show'])->name('front.zoom_request.show');
        Route::post('/zoom-request/simpan',[ZoomRequestController::class, 'store'])->name('front.zoom_request.store');
    });

    Route::group(['prefix' => 'user/bmn-request', 'middleware'=> 'role:User_Bmn|Super_Admin'],function () {

        Route::get('/',[BmnRequestController::class, 'index'])->name('front.bmn_request.index');
        Route::get('/get-data',[BmnRequestController::class, 'getData'])->name('front.bmn_request.getdata');
        Route::get('/get-data-bmn/{id}',[BmnRequestController::class, 'getDataBmn'])->name('front.bmn_request.getdata_bmn');
        Route::get('/calendar',[BmnRequestController::class, 'calendar'])->name('front.bmn_request.calendar');
        Route::get('/create',[BmnRequestController::class, 'create'])->name('front.bmn_request.create');
        Route::post('/store',[BmnRequestController::class, 'store'])->name('front.bmn_request.store');
        


    });

});
