@extends('layouts.sbadmin.main')
@section('title','Data Disposisi Masuk')
@section('content')
<div class="wrapper">
    @push('main-menu')Disposisi @endpush
    @push('url-main-menu') {{route('module.letter.index')}} @endpush
    @push('sub-menu')Index @endpush       
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data Disposisi
        </div>
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
               
            </div>
            <div class="card-header rounded bg-primary text-white">
                Diposisi untuk Surat tentang {{$data->title}}
            </div><br>
            <a href="{{route('module.disposition.create',$data->id)}}" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</a>
            <br><br>
            <div class="table-responsive">
                <table id="letter-table" class="table table-bordered">
                    <thead class="bg-success text-white rounded">
                        <tr>
                            <th>#</th>
                            <th>Tujuan Disposisi</th>
                            <th>Isi Disposisi</th>
                            <th>Sifat Instruksi</th>
                            <th width="300px" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($disposition as $item)
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@include('layouts.sbadmin.partial.indexjs')
@include('asamurat::disposition.javascript')

@endsection