@extends('layouts.sbadmin.main')
@section('title','Tambah data Disposisi')
@push('main-menu')Disposisi @endpush
@push('url-main-menu') {{route('module.letter.index')}} @endpush
@push('sub-menu')Create @endpush
@section('content')
    <div class="card">
        <div class="card-header bg-success">
            Disposisi Surat
        </div>
        <div class="card-body">
            <div class="card-header rounded bg-primary text-white">
                Diposisi untuk Surat tentang : {{$data->title}}
            </div><br>
            <h2>Form Input Data Disposisi</h2><hr>
            <form method="POST" action="{{route('module.disposition.store')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Tujuan Disposisi 1</b></label>
                            <select name="disposisi1" class="form-control {{$errors->first('disposisi1') ? "is-invalid" : ""}}">
                                <option value=null>--Kepada Yth--</option>
                                <option value="Subbag Pengelolaan Teknologi Informasi" @if(old('disposisi1') == 'Subbag Pengelolaan Teknologi Informasi') {{'selected'}} @endif>Subbag Pengelolaan Teknologi Informasi</option>
                                <option value="Subbag Pengelolaan dan Analisis 1"  @if(old('disposisi1') == 'Subbag Pengelolaan dan Analisis 1') {{'selected'}} @endif>Subbag Pengelolaan dan Analisis 1</option>
                                <option value="Subbag Pengelolaan dan Analisis 2"  @if(old('disposisi1') == 'Subbag Pengelolaan dan Analisis 2') {{'selected'}} @endif>Subbag Pengelolaan dan Analisis 2</option>
                                <option value="Subbag Layanan Pengaduan"  @if(old('disposisi1') == 'Subbag Layanan Pengaduan') {{'selected'}} @endif>Subbag Layanan Pengaduan</option>
                            </select>
                            <div class="invalid-feedback">
                                {{$errors->first('disposisi1')}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label><b>Tujuan Disposisi 2</b></label>
                            <select name="disposisi2" class="form-control {{$errors->first('disposisi1') ? "is-invalid" : ""}}">
                                <option value=null>--Kepada Yth--</option>
                                <option value="Subbag Pengelolaan Teknologi Informasi" @if(old('disposisi2') == 'Subbag Pengelolaan Teknologi Informasi') {{'selected'}} @endif>Subbag Pengelolaan Teknologi Informasi</option>
                                <option value="Subbag Pengelolaan dan Analisis 1"  @if(old('disposisi2') == 'Subbag Pengelolaan dan Analisis 1') {{'selected'}} @endif>Subbag Pengelolaan dan Analisis 1</option>
                                <option value="Subbag Pengelolaan dan Analisis 2"  @if(old('disposisi2') == 'Subbag Pengelolaan dan Analisis 2') {{'selected'}} @endif>Subbag Pengelolaan dan Analisis 2</option>
                                <option value="Subbag Layanan Pengaduan"  @if(old('disposisi2') == 'Subbag Layanan Pengaduan') {{'selected'}} @endif>Subbag Layanan Pengaduan</option>
                            </select>
                            <div class="invalid-feedback">
                                {{$errors->first('disposisi2')}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label><b>Tujuan Disposisi 3</b></label>
                            <select name="disposisi3" class="form-control {{$errors->first('disposisi3') ? "is-invalid" : ""}}">
                                <option value=null>--Kepada Yth--</option>
                                <option value="Subbag Pengelolaan Teknologi Informasi" @if(old('disposisi3') == 'Subbag Pengelolaan Teknologi Informasi') {{'selected'}} @endif>Subbag Pengelolaan Teknologi Informasi</option>
                                <option value="Subbag Pengelolaan dan Analisis 1"  @if(old('disposisi3') == 'Subbag Pengelolaan dan Analisis 1') {{'selected'}} @endif>Subbag Pengelolaan dan Analisis 1</option>
                                <option value="Subbag Pengelolaan dan Analisis 2"  @if(old('disposisi3') == 'Subbag Pengelolaan dan Analisis 2') {{'selected'}} @endif>Subbag Pengelolaan dan Analisis 2</option>
                                <option value="Subbag Layanan Pengaduan"  @if(old('disposisi3') == 'Subbag Layanan Pengaduan') {{'selected'}} @endif>Subbag Layanan Pengaduan</option>
                            </select>
                            <div class="invalid-feedback">
                                {{$errors->first('disposisi3')}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label><b>Tujuan Disposisi 4</b></label>
                            <select name="disposisi4" class="form-control {{$errors->first('disposisi4') ? "is-invalid" : ""}}">
                                <option value=null>--Kepada Yth--</option>
                                <option value="Subbag Pengelolaan Teknologi Informasi" @if(old('disposisi4') == 'Subbag Pengelolaan Teknologi Informasi') {{'selected'}} @endif>Subbag Pengelolaan Teknologi Informasi</option>
                                <option value="Subbag Pengelolaan dan Analisis 1"  @if(old('disposisi4') == 'Subbag Pengelolaan dan Analisis 1') {{'selected'}} @endif>Subbag Pengelolaan dan Analisis 1</option>
                                <option value="Subbag Pengelolaan dan Analisis 2"  @if(old('disposisi4') == 'Subbag Pengelolaan dan Analisis 2') {{'selected'}} @endif>Subbag Pengelolaan dan Analisis 2</option>
                                <option value="Subbag Layanan Pengaduan"  @if(old('disposisi4') == 'Subbag Layanan Pengaduan') {{'selected'}} @endif>Subbag Layanan Pengaduan</option>
                            </select>
                            <div class="invalid-feedback">
                                {{$errors->first('disposisi4')}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Isi Disposisi</label>
                            <textarea name="isidisposisi" class="form-control {{$errors->first('isidisposisi') ? "is-invalid" : ""}}" placeholder="isi Keterangan"></textarea>
                            <div class="invalid-feedback">
                                {{$errors->first('isidisposisi')}}
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Sifat</b></label>
                            <select name="sifat" class="form-control {{$errors->first('sifat') ? "is-invalid" : ""}}">
                                <option value=null>Pilih Sifat</option>
                                <option value="Sangat Rahasia" @if(old('sifat') == 'Sangat Rahasia') {{'selected'}} @endif>Sangat Rahasia</option>
                                <option value="Rahasia"  @if(old('sifat') == 'Rahasia') {{'selected'}} @endif>Rahasia</option>
                                <option value="Biasa"  @if(old('sifat') == 'Biasa') {{'selected'}} @endif>Biasa</option>
                            </select>
                            <div class="invalid-feedback">
                                {{$errors->first('disposisi1')}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label><b>Instruksi 1</b></label>
                            <select name="sifat" class="form-control {{$errors->first('sifat') ? "is-invalid" : ""}}">
                                <option value=null>--Instruksi 1--</option>
                                <option value="Sangat Rahasia" @if(old('sifat') == 'Sangat Rahasia') {{'selected'}} @endif>Sangat Rahasia</option>
                                <option value="Rahasia"  @if(old('sifat') == 'Rahasia') {{'selected'}} @endif>Rahasia</option>
                                <option value="Biasa"  @if(old('sifat') == 'Biasa') {{'selected'}} @endif>Biasa</option>
                            </select>
                            <div class="invalid-feedback">
                                {{$errors->first('disposisi1')}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label><b>Instruksi 2</b></label>
                            <select name="sifat" class="form-control {{$errors->first('sifat') ? "is-invalid" : ""}}">
                                <option value=null>--Instruksi 2--</option>
                                <option value="Sangat Rahasia" @if(old('sifat') == 'Sangat Rahasia') {{'selected'}} @endif>Sangat Rahasia</option>
                                <option value="Rahasia"  @if(old('sifat') == 'Rahasia') {{'selected'}} @endif>Rahasia</option>
                                <option value="Biasa"  @if(old('sifat') == 'Biasa') {{'selected'}} @endif>Biasa</option>
                            </select>
                            <div class="invalid-feedback">
                                {{$errors->first('disposisi1')}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control {{$errors->first('keterangan') ? "is-invalid" : ""}}" style="height: 8em" placeholder="isi Keterangan"></textarea>
                            <div class="invalid-feedback">
                                {{$errors->first('keterangan')}}
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="col text-center">
                    <button type="submit" id="btn-submit" class="btn bg-success btn-lg text-white"><i class="fas fa-save"></i> Simpan</button>
                    <button type="reset" class="btn btn-warning btn-lg "><i class="fas fa-recycle"></i> Reset</button>
                </div>
                
            </form>
        </div>
    </div>
@endsection