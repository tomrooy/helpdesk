@extends('layouts.sbadmin.main')
@section('title','Data Surat Masuk')
@section('content')
<div class="wrapper">
    @push('main-menu')Surat Masuk @endpush
    @push('url-main-menu') {{route('module.letter.index')}} @endpush
    @push('sub-menu')Index @endpush       
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data Surat Masuk
        </div>
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                <a href="{{route('module.letter.create')}}" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</a>
            </div>
           
            <div class="table-responsive">
                <table id="letter-table" class="table table-bordered">
                    <thead class="bg-gradient-secondary text-white">
                        <tr>
                            <th>#</th>
                            <th>Agenda/Kode</th>
                            <th>Isi Ringkas, File</th>
                            <th>Asal Surat</th>
                            <th>Nomor Tanggal</th>
                            <th>Inputer, Tanggal</th>
                            <th width="300px" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@include('layouts.sbadmin.partial.indexjs')
@include('asamurat::letter.javascript')

@endsection