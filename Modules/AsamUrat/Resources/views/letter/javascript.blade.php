<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>  
    $(document).ready(function() {
        //data jam dan tanggal
        @include('layouts.sbadmin.partial.clock')
        var dataTable = $('#letter-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('module.getletter') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'letter_type', name: 'letter_type'},
                {data: 'title', name: 'title'},
                {data: 'from', name: 'from'},
                {data: 'letter_date', name: 'letter_date'},
                {data: 'created_by', name: 'created_by'},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight : true,
            autoclose   : true,

        });
        $('#kode_klasifikasi').select2();
        $('#formSuratMasuk').submit(function(e){
            e.preventDefault();
            let formData    =   new FormData(this);
            swal({
                title: "Kirim?",
                text: "Apakah Yakin Menyimpan Surat Masuk!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e){
                if (e.value === true) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url:"{{ route('module.letter.store') }}",
                        type: 'POST',
                        data:formData,
                        contentType: false,
                        processData: false,
                        success:function(data) {
                            if(data.errors){
                                if(data.errors.nomor_agenda)
                                {
                                    $('#nomor_agenda-error' ).html( data.errors.nomor_agenda[0] ).addClass('invalid-feedback');
                                    $('#nomor_agenda').addClass('is-invalid')
                                }
                                if(data.errors.kode_klasifikasi)
                                {
                                    $('#kode_klasifikasi-error' ).html( data.errors.kode_klasifikasi[0] ).addClass('alert-danger');
                                    $('#kode_klasifikasi').addClass('is-invalid')
                                }
                                if(data.errors.asal_surat)
                                {
                                    $('#asal_surat-error' ).html( data.errors.asal_surat[0] ).addClass('alert-danger');
                                    $('#asal_surat').addClass('is-invalid')
                                }
                                if(data.errors.nomor_surat)
                                {
                                    $('#nomor_surat-error' ).html( data.errors.nomor_surat[0] ).addClass('alert-danger');
                                    $('#nomor_surat').addClass('is-invalid')
                                }
                                if(data.errors.isi_ringkas)
                                {
                                    $('#isi_ringkas-error' ).html( data.errors.isi_ringkas[0] ).addClass('alert-danger');
                                    $('#isi_ringkas').addClass('is-invalid')
                                }
                                if(data.errors.index_berkas)
                                {
                                    $('#index_berkas-error' ).html( data.errors.index_berkas[0] ).addClass('alert-danger');
                                    $('#index_berkas').addClass('is-invalid')
                                }
                                if(data.errors.tanggal_surat)
                                {
                                    $('#tanggal_surat-error' ).html( data.errors.tanggal_surat[0] ).addClass('alert-danger');
                                    $('#tanggal_surat').addClass('is-invalid')
                                }
                                if(data.errors.file_surat)
                                {
                                    $('#file_surat-error' ).html( data.errors.file_surat[0] ).addClass('alert-danger');
                                    $('#file_surat').addClass('is-invalid');
                                }
                                if(data.errors.instruksi_atasan)
                                {
                                    $('#instruksi_atasan-error' ).html( data.errors.instruksi_atasan[0] ).addClass('alert-danger');
                                    $('#instruksi_atasan').addClass('is-invalid')
                                }
                                
                                
                            }else
                            {
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 2000
                                 });
                                //$('#category_id').removeClass('is-invalid');
                                document.getElementById("formSuratMasuk").reset();
                                
                            }
                        }
                    });
                }
            });
            
        })
        $('#formEditSuratMasuk').submit(function(e){
            e.preventDefault();
            let formData    =   new FormData(this);
            swal({
                title: "Kirim?",
                text: "Apakah Yakin Menyimpan Surat Masuk!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e){
                if (e.value === true) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url:"{{ route('module.letter.update') }}",
                        type: 'POST',
                        data:formData,
                        contentType: false,
                        processData: false,
                        success:function(data) {
                            if(data.errors){
                                if(data.errors.nomor_agenda)
                                {
                                    $('#nomor_agenda-error' ).html( data.errors.nomor_agenda[0] ).addClass('invalid-feedback');
                                    $('#nomor_agenda').addClass('is-invalid')
                                }
                                if(data.errors.kode_klasifikasi)
                                {
                                    $('#kode_klasifikasi-error' ).html( data.errors.kode_klasifikasi[0] ).addClass('alert-danger');
                                    $('#kode_klasifikasi').addClass('is-invalid')
                                }
                                if(data.errors.asal_surat)
                                {
                                    $('#asal_surat-error' ).html( data.errors.asal_surat[0] ).addClass('alert-danger');
                                    $('#asal_surat').addClass('is-invalid')
                                }
                                if(data.errors.nomor_surat)
                                {
                                    $('#nomor_surat-error' ).html( data.errors.nomor_surat[0] ).addClass('alert-danger');
                                    $('#nomor_surat').addClass('is-invalid')
                                }
                                if(data.errors.isi_ringkas)
                                {
                                    $('#isi_ringkas-error' ).html( data.errors.isi_ringkas[0] ).addClass('alert-danger');
                                    $('#isi_ringkas').addClass('is-invalid')
                                }
                                if(data.errors.index_berkas)
                                {
                                    $('#index_berkas-error' ).html( data.errors.index_berkas[0] ).addClass('alert-danger');
                                    $('#index_berkas').addClass('is-invalid')
                                }
                                if(data.errors.tanggal_surat)
                                {
                                    $('#tanggal_surat-error' ).html( data.errors.tanggal_surat[0] ).addClass('alert-danger');
                                    $('#tanggal_surat').addClass('is-invalid')
                                }
                                if(data.errors.file_surat)
                                {
                                    $('#file_surat-error' ).html( data.errors.file_surat[0] ).addClass('alert-danger');
                                    $('#file_surat').addClass('is-invalid');
                                }
                                if(data.errors.instruksi_atasan)
                                {
                                    $('#instruksi_atasan-error' ).html( data.errors.instruksi_atasan[0] ).addClass('alert-danger');
                                    $('#instruksi_atasan').addClass('is-invalid')
                                }
                                
                                
                            }else
                            {
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 2000
                                 });
                                //$('#category_id').removeClass('is-invalid');
                                document.getElementById("formSuratMasuk").reset();
                                
                            }
                        }
                    });
                }
            });
            
        })
        $(document).on('click', '#deleteLetter', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: "Hapus?",
                text: "Apakah Yakin Menghapus data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {                    
                $.ajax({
                    type: 'POST',
                    url: 'surat/'+id+'/delete',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        },
                    dataType: 'JSON',
                    success: function (data) {
                        if(data.status == true)
                        {
                            swal("Done!", data.message, "success");
                            $('#letter-table').DataTable().ajax.reload();
                        }else
                        {
                            swal("Error!", data.message, "error");
                        }
                        
                    },
                    error : function(xhr, status, errorThrown)
                    {
                         //Here the status code can be retrieved like;
                        xhr.status;
                
                        //The message added to Response object in Controller can be retrieved as following.
                        xhr.responseText;
                    }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
        
    });
</script>