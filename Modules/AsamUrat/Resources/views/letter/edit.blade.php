@extends('layouts.sbadmin.main')
@section('title','Edit Surat Masuk')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="wrapper">
    @push('main-menu')Surat Masuk @endpush
    @push('url-main-menu') {{route('module.letter.index')}} @endpush
    @push('sub-menu')Edit @endpush       
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data Surat Masuk
        </div>
        <div class="card-body bg-content-image" style="max-height: 100%;">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <h2 class="col text-center">Form Edit Surat Masuk</h2><br>
            <form method="POST" id="formEditSuratMasuk" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label><b>No Agenda</b></label>
                                <input type="text" name="nomor_agenda" id="nomor_agenda" class="form-control" value="{{$letter->agenda_number}}">
                                <span id="nomor_agenda-error"></span>
                            </div>
                            <div class="col-md-6">
                                <label><b>Kode Klasifikasi</b></label>
                                <input type="hidden" name="id"  value="{{$letter->id}}">
                                <select name="kode_klasifikasi" id="kode_klasifikasi" class="form-control">
                                    
                                    <option value="">Pilih Klasifikasi Surat</option>
                                    @foreach ($kode_klasifikasi as $item)
                                        <option value="{{$item->letter_id}}" {{$item->letter_id == $letter->letter_type ? 'selected' : ''}}>{{$item->letter_id.'-'.$item->name}}</option>
                                    @endforeach
                                </select>
                                <span id="kode_klasifikasi-error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label><b>Asal Surat</b></label>
                                <input type="text" name="asal_surat"  id="asal_surat" class="form-control" value="{{$letter->from}}">
                                <span id="asal_surat-error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9">
                                <label><b>No Surat</b></label>
                                <input type="text" name="nomor_surat" id="nomor_surat" class="form-control" value="{{$letter->letter_number}}">
                                <span id="nomor_surat-error"></span>
                                <br>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9">
                                <label><b>Isi Ringkas</b></label>
                                <textarea type="text" name="isi_ringkas" id="isi_ringkas" class="form-control">{{$letter->title}}</textarea>
                                <span id="isi_ringkas-error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label><b>Index Berkas</b></label>
                                <input type="text" name="index_berkas" id="index_berkas" class="form-control" value="{{$letter->letter_index}}">
                                <span id="index_berkas-error"></span>
                            </div>
                            <div class="col-md-4">
                                <label><b>Tanggal Input</b></label>
                                <input disabled type="text" class="form-control" value={{\Carbon\Carbon::now()->format('Y-m-d')}}>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label><b>Tanggal Surat</b></label>
                                <input autocomplete="off" type="text" name="tanggal_surat" id="tanggal_surat" class="form-control datepicker" value="{{$letter->letter_date}}">
                                <span id="tanggal_surat-error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-7">
                                <label><b>File Surat</b></label>
                                <input type="file" name="file_surat" id="file_surat" class="form-control">
                                <div class="text-danger">*Ukuran Max: 10 MB. jenis File yang diterima hanya : .pdf</div>
                                {{$letter->file != null ? $letter->file : 'tidak ada file '}}
                                <span id="file_surat-error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9">
                                <label><b>Instruksi Atasan</b></label>
                                <textarea type="text" name="instruksi_atasan" id="instruksi_atasan" class="form-control">{{$letter->description}}</textarea>
                                <span id="instruksi_atasan-error"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col text-center">
                    <button type="submit" id="btn-submit" class="btn bg-success btn-lg text-white"><i class="fas fa-save"></i> Simpan</button>
                    <button type="reset" class="btn btn-warning btn-lg "><i class="fas fa-recycle"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
@include('layouts.sbadmin.partial.indexjs')
@include('asamurat::letter.javascript')

@endsection