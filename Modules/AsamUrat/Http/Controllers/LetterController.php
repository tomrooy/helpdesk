<?php

namespace Modules\AsamUrat\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Letter;
use App\Models\LetterType;
use DB;

class LetterController extends Controller
{
    
    Public function getLetter()
    {
        $data   = Letter::latest()->get();
        return \DataTables::of($data)
        ->addIndexColumn()
        ->editColumn('letter_type', function($data){
            $agenda =   $data->agenda_number;
            $code   =   $data->letter_type;
            return $agenda.'-'.$code;
        })
        ->editColumn('title', function($data){
            $title =   $data->title;
            $file   =   $data->file;
            return ''.$title.'<br><a class="btn btn-info" href="'.$file.'">'.$file.'</a>';
        })
        ->editColumn('letter_date', function($data){
            $agenda =   $data->letter_number;
            $code   =   \Carbon\Carbon::parse($data->letter_date)->format('d F Y');
            return ''.$agenda.'<br><b>'.$code.'</b>';
        })
        ->editColumn('created_by', function($data){
            $agenda =   $data->users->name;
            $code   =   \Carbon\Carbon::parse($data->created_at)->format('d F Y');
            return ''.$agenda.'<br><b>'.$code.'</b>';
        })
        ->addColumn('Actions', function ($data){
            $cetak  =   '<button type="button" class="btn btn-success btn-sm" id="getEditCategoryData" data-id="'.$data->id.'" ><i class="fas fa-print"></i> Cetak</button>';
            $disposisi  = '<a href="'.route('module.disposition.index', $data->id).'"  class="btn btn-info btn-sm" ><i class="fas fa-sort-amount-down-alt"></i> Disposisi</a>';
            $hapus = '<button type="button" class="btn btn-danger btn-sm" id="deleteLetter" data-id="'.$data->id.'" ><i class="fa fa-trash"></i> Delete</button>';
            $edit = '<a href="'.route('module.letter.edit', $data->id).'"  class="btn btn-warning btn-sm" ><i class="fas fa-edit"></i> Edit</a>';
            
            return $cetak.$disposisi.$edit.$hapus;
        })
        ->rawColumns(['Actions','title','created_by','letter_date'])
        ->make(true);
    }

    public function index()
    {
        return view('asamurat::letter.index');
    }
    public function create()
    {
        $id = IdGenerator::generate(['table' => 'letters','field' => 'agenda_number', 'length' => 4, 'prefix' =>'0']);
        $data   =   LetterType::all();
        return view('asamurat::letter.create',[
            'kode_klasifikasi'   =>  $data,
            'agenda'             =>  $id
        ]);
    }
    public function store(Request $request)
    {
        try{
            $message    =   new Letter;
            $messages    =   $message->messages();
            $validator = \Validator::make($request->all(),[
                'nomor_agenda'      =>  'required',
                'kode_klasifikasi'  =>  'required',
                'asal_surat'        =>  'required',
                'nomor_surat'       =>  'required',
                'isi_ringkas'       =>  'required',
                'index_berkas'      =>  'required',
                'tanggal_surat'     =>  'required',
                'file_surat'        =>  'nullable|mimes:pdf',
                'instruksi_atasan'  =>  'nullable',
            ], $messages);
            
            if($validator->passes())
            {
                Letter::create([
                    'letter_type'       =>  $request->kode_klasifikasi,
                    'agenda_number'     =>  $request->nomor_agenda,
                    'letter_index'      =>  $request->index_berkas,
                    'title'             =>  $request->isi_ringkas,
                    'from'              =>  $request->asal_surat,
                    'letter_number'     =>  $request->nomor_surat,
                    'letter_date'       =>  $request->tanggal_surat,
                    'file'              =>  'file.pdf',
                    'description'       =>  $request->instruksi_atasan,
                    'created_by'        =>  \Auth::User()->nip,
                ]);

            }else
            {
                return response()->json(['errors' => $validator->errors()]);
            }

        }catch (\Exception $e)
        {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function getAgenda()
    {
        $id = IdGenerator::generate(['table' => 'letters','field' => 'agenda_number', 'length' => 4, 'prefix' =>'0']);

        return $id;
    }
    public function edit($id)
    {
        $letter =   Letter::findOrFail($id);
        $data   =   LetterType::all();
        return view('asamurat::letter.edit',[
            'letter'    =>  $letter,
            'kode_klasifikasi'   =>  $data,
        ]);
    }
    public function update(Request $request)
    {
        try{
            $message        =   new Letter;
            $messages       =   $message->messages();
            $validator      = \Validator::make($request->all(),[
                'nomor_agenda'      =>  'required',
                'kode_klasifikasi'  =>  'required',
                'asal_surat'        =>  'required',
                'nomor_surat'       =>  'required',
                'isi_ringkas'       =>  'required',
                'index_berkas'      =>  'required',
                'tanggal_surat'     =>  'required',
                'file_surat'        =>  'nullable|mimes:pdf',
                'instruksi_atasan'  =>  'nullable',
            ], $messages);
            
            if($validator->passes())
            {
                $letter         =   Letter::findOrFail($request->id);
                $letter->update([
                    'letter_type'       =>  $request->kode_klasifikasi,
                    'agenda_number'     =>  $request->nomor_agenda,
                    'letter_index'      =>  $request->index_berkas,
                    'title'             =>  $request->isi_ringkas,
                    'from'              =>  $request->asal_surat,
                    'letter_number'     =>  $request->nomor_surat,
                    'letter_date'       =>  $request->tanggal_surat,
                    'file'              =>  'file.pdf',
                    'description'       =>  $request->instruksi_atasan,
                    'updated_by'        =>  \Auth::User()->nip,
                ]);
            }else
            {
                return response()->json(['errors' => $validator->errors()]);
            }

        }catch (\Exception $e)
        {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function destroy($id)
    {
        try{
            $data   =   Letter::findOrFail($id);
            $data->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
           
            $data->delete();
            
            return response()->json([
                'status'   =>  true,
                'message'  =>  $data->name.' delete'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
    }
  
}
