<?php

namespace Modules\AsamUrat\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Letter;
use App\Models\Disposition;

class DispositionController extends Controller
{
    
    public function index($id)
    {
        
        $data       =   Letter::FindOrfail($id);
        $disposition    =   Disposition::where('letter_id',$id)->get();
        return view('asamurat::disposition.index',[
            'disposition'     =>  $disposition,
            'data'            =>  $data,
        ]);
    }

   
    public function create($id)
    {
        $data       =   Letter::FindOrfail($id);
        return view('asamurat::disposition.create',[
            'data'  =>  $data
        ]);
    }

   
    public function store(Request $request)
    {
        \Validator::make($request->all(),[
            disposisi1,disposisi2,disposisi3,disposisi4,isidisposisi,sifat,instruksi1,instruksi2,keterangan
        ])->validate();
        
    }

    
    public function show($id)
    {
        return view('asamurat::show');
    }

    public function edit($id)
    {
        return view('asamurat::edit');
    }
    
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
