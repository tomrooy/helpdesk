<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'/admin/module','middleware' => 'auth'], function() {
    Route::get('/surat', 'LetterController@index')->name('module.letter.index');
    Route::get('/surat/create', 'LetterController@create')->name('module.letter.create');
    Route::post('/surat/simpan', 'LetterController@store')->name('module.letter.store');
    Route::get('/surat/{id}/edit', 'LetterController@edit')->name('module.letter.edit');
    Route::post('/surat/update', 'LetterController@update')->name('module.letter.update');
    Route::get('/getsurat','LetterController@getLetter')->name('module.getletter');
    Route::get('/getagenda','LetterController@getAgenda')->name('module.getAgenda');
    Route::get('/surat/{id}/disposisi','LetterController@disposisi')->name('module.letter.disposisi');
    Route::post('/surat/{id}/delete','LetterController@destroy')->name('module.letter.delete');


    Route::get('/disposisi/{id}/index', 'DispositionController@index')->name('module.disposition.index');
    Route::get('/dispossisi/{id}/create','DispositionController@create')->name('module.disposition.create');
    Route::post('/dispossisi/store','DispositionController@store')->name('module.disposition.store');
});
