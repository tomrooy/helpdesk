<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix'=>'admin/module/zoom/','middleware'=>'role:Super_Admin'],function() {
        Route::get('/', 'ZoomController@index')->name('module.zoom.index');
        Route::get('/cari','ZoomController@search')->name('module.zoom.search');
        Route::get('/cari/hasil}','ZoomController@searchProcess')->name('module.zoom.search_process');
        Route::get('/create','ZoomController@create')->name('module.zoom.create');
        Route::get('/{id}/show','ZoomController@show')->name('module.zoom.show');
        Route::get('/{id}/edit','ZoomController@edit')->name('module.zoom.edit');
        Route::put('/{id}/update','ZoomController@update')->name('module.zoom.update');
        Route::delete('/{id}/destroy','ZoomController@destroy')->name('module.zoom.destroy');
        Route::post('/record/action', 'ZoomController@actionRecordZoom')->name('module.zoom.action_record');
        Route::post('/simpan','ZoomController@store')->name('module.zoom.store');
        Route::get('/calendarview','ZoomController@calendarView')->name('module.zoom.calendar.view');
    
        Route::get('useraccount/getcolor/{username}','ZoomController@getColor')->name('module.zoom.get.color');
    
        Route::get('/zoom-request', 'ZoomRequestController@index')->name('module.zoom_request.index');
        Route::get('/zoom-request/proses/{id}', 'ZoomRequestController@processZoom')->name('module.zoom_request.process');
        Route::get('/zoom-request/detail/{id}', 'ZoomRequestController@detail')->name('module.zoom_request.detail');
        Route::put('/zoom-request/proses/{id}/action', 'ZoomRequestController@actionProcessZoom')->name('module.zoom_request.action_process');
        Route::get('/zoom-request/record/{id}', 'ZoomRequestController@RecordZoom')->name('module.zoom_request.record');
        Route::put('/zoom-request/record/{id}/action', 'ZoomRequestController@actionRecordZoom')->name('module.zoom_request.action_record');
        Route::delete('/zoom-request/{id}/destroy', 'ZoomRequestController@destroy')->name('module.zoom_request.destroy');
    
    });

});

