@extends('layouts.nice.main')
@section('title','Zoom Form Request Page')
@push('nav-helpdesk') show @endpush
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Form Proses Request Zoom @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Form Proses Permintaan Link ZOOM</strong></h2>
            </div>    
            <div class="card-body">
             <br>
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <form method="POST" action="{{route('module.zoom_request.action_process',$data->id)}}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <input type="hidden" name="colour" id="colour" value="{{old('colour')}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    Data Permintaan Zoom
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Pegawai</label>
                                                <input type="text" class="form-control" value="{{$data->created_by_users->name}}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Bagian / Wilayah</label>
                                                <input type="text" class="form-control" value="{{$data->request_by}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Kegiatan</label>
                                        <textarea class="form-control" disabled>{{$data->name}}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Kategori Kegiatan</label>
                                                <input type="text" class="form-control" value="{{$data->category}}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Jumlah Peserta</label>
                                                <input type="text" class="form-control" value="{{$data->participant}} Peserta" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Jadwal</label>
                                        <input type="text" class="form-control" value="{!! 'Tanggal '.\Carbon\Carbon::createFromFormat('Y-m-d',$data->date)->format('d M Y'). "   Jam ". $data->start.'-'.$data->end !!}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label>Request Link Absen</label>
                                        <input type="text" class="form-control" value="{{$data->presensi == 'on' ? 'Diajukan' : 'tidak diajukan'}}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    Form Proses Link Permintaan Zoom
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ID Zoom Meeting</label>
                                                <input type="number" name="zoom_id" value="{{old('zoom_id')}}" class="form-control {{$errors->first('zoom_id') ? 'is-invalid': ''}}" >
                                                <div class="invalid-feedback">
                                                    {{$errors->first('zoom_id')}}
                                                </div>
                                            </div>                                              
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="text" name="password" value="{{old('password')}}" class="form-control {{$errors->first('password') ? 'is-invalid': ''}}" >
                                                <div class="invalid-feedback">
                                                    {{$errors->first('password')}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Link ZOOM</label>
                                            <textarea autocomplete="off" type="text" class="form-control {{$errors->first('link') ? "is-invalid" : ""}} " name="link" placeholder="Input link">{{old('link')}}</textarea>
                                                <div class="invalid-feedback">
                                                    {{$errors->first('link')}}
                                                </div>
                                    </div>
                                    <div class="form-group">
                                        <label><b>Dibuat Pakai Akun</b></label>
                                        
                                        <select name="created_by_account" class="form-control {{$errors->first('created_by_account') ? "is-invalid" : ""}}">
                                            <option value="">--Pilih Akun --</option>
                                            @foreach($zoomaccount as $item)
                                                <option value="{{$item->username}}" @if(old('created_by_account') == '{{$item->name}}') {{'selected'}} @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">
                                            {{$errors->first('created_by_account')}}
                                        </div>
                                    </div>
                                   
                                    <div class="col text-center">
                                        <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Simpan</button>
                                        <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {        
        
        //$('select[name="created_by_account"]').on('change', function(){
        //    var username = $(this).val();
        //    var url =   '{{ route("module.zoom.get.color", ":id") }}';
        //    $.ajax({
        //        url : url.replace(':id', username),
        //        type : 'GET',
        //        dataType : 'Json',
        //        success : function(data){
        //            document.getElementById("colour").value = data;
        //        }
        //    });
        //});  
    });
</script>
@endsection