@extends('layouts.nice.main')
@section('title','Data Permintaan Zoom')
@push('nav-zoom') show @endpush
@section('css')
<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom Request @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Index @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Data Zoom</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <br>
                @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <a href="{{route('module.zoom.create')}}" class="btn btn-info"><span class="fas fa-plus-square"></span> Tambah Data</a>
                <br><br>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Agenda</th>
                                <th>Direquest Oleh</th>
                                <th>Jumlah Peserta</th>
                                <th>Tanggal dan Jam</th>
                                <th>Status</th>
                                <th>Category</th>
                                <th width="250" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;    
                            @endphp
                            @foreach ($zoom_request as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{!!$item->request_by.'<br>'.$item->created_by_users->name!!}</td>
                                <td>{{$item->participant}}</td>
                                <td>{!! 'Tanggal '.\Carbon\Carbon::createFromFormat('Y-m-d',$item->date)->format('d M Y'). "<br>Jam ". $item->start.'-'.$item->end !!}</td>
                                <td><h4><span class="badge bg-info">{{$item->status}}</span></h4></td>
                                <td>{{$item->category}}</td>
                                <td>
                                    <a href="{{route('module.zoom_request.detail', $item->id)}}" class="btn btn-success btn-sm"><span class="fas fa-eye"></span> Detail</a>
                                    <a href="#" class="btn btn-warning btn-sm"><span class="fas fa-edit"></span> Edit</a>
                                    <a href="javascript:void(0)" onclick="$(this).find('form').submit()" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Hapus
                                        <form method="POST" action={{route('module.zoom_request.destroy',$item->id) }}  onsubmit="return confirm('Delete this Zoom temporarily?')">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </a><br><br>
                                    @if($item->link == null)
                                        <a href="{{route('module.zoom_request.process',$item->id)}}" class="btn btn-info btn-sm"><span class="fas fa-cogs"></span> Input Link</a>
                                    @endif
                                    @if($item->link_record == null)
                                        <a href="{{route('module.zoom_request.record',$item->id)}}" class="btn btn-success btn-sm"><span class="fas fa-video"></span> Input Record</a>
                                    @endif
                                </td>
                            </tr>
                            @php
                                $no++;
                            @endphp    
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {        
        $('#myTable').DataTable();
    });
</script>
@endsection