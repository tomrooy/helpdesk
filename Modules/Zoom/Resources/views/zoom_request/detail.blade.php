@extends('layouts.nice.main')
@section('title','Zoom Detail-Data-Page')
@push('nav-helpdesk') show @endpush
@section('css')
<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<style>
    .hide_zoom{
        display:none;
    }
</style>
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Detail @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Data {{$zoom->name}}</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-7">
                        <table class="table table-borderless">
                            <tr>
                                <td>Nama Agenda Rapat</td>
                                <td>{{$zoom->name}}</td>
                            </tr>
                            <tr>
                                <td>Diajukan Oleh</td>
                                <td>{{$zoom->request_by}}</td>
                            </tr>
                            <tr>
                                <td>Jenis Rapat</td>
                                <td>{{$zoom->category}}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Rapat</td>
                                <td>{{$zoom->date.' '.$zoom->start.' '.$zoom->end}}</td>
                            </tr>
                            <tr>
                                <td>Akun Zoom </td>
                                <td>{{$zoom->created_by_account}}</td>
                            </tr>
                            <tr>
                                <td>Contact Person</td>
                                <td>{{$zoom->created_by_users->handphone}}</td>
                            </tr>
                            <tr>
                                <td>File Nodin</td>
                                <td>{{$zoom->attachment}}</td>
                            </tr>
                            <tr>
                                <td>Keterangan Lain</td>
                                <td>{{$zoom->description}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <table class="table table-responsive" id="link_zoom">
                            <tr id="name_zoom" class="hide_zoom">
                                <td>Nama Agenda Rapat</td>
                                <td>{{$zoom->name}}</td>
                            </tr>
                            <tr id="tanggal_zoom" class="hide_zoom">
                                <td>Tanggal Rapat</td>
                                <td>{{$zoom->date.' '.$zoom->start.' '.$zoom->end}}</td>
                            </tr>
                            <tr>
                                <td>ID Zoom</td>
                                <td>{{$zoom->zoom_id}}</td>
                            </tr>
                            <tr>
                                <td>Password Zoom</td>
                                <td>{{$zoom->password}}</td>
                            </tr>
                            <tr>
                                <td>Link Zoom</td>
                                <td>{{$zoom->link}}</td>
                            </tr>
                            <tr>
                                <td>Link Record Zoom</td>
                                <td>{{$zoom->link_record}}</td>
                            </tr>
                            <tr>
                                <td>Password Record Zoom</td>
                                <td>{{$zoom->password_record}}</td>
                            </tr>
                            
                        </table>
                        <button onclick="copy('#link_zoom')" type="button" class="btn btn-info"><span class="fas fa-copy"></span> Copy Link</button>
                    </div>
                </div>
                
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {               
    });
    function selectText(el){
            var doc = document;
            var element = document.getElementById(el);
            if (doc.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(element);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();        
                var range = document.createRange();
                range.selectNodeContents(element);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
        function copy(selector) {
            var x = document.getElementById("name_zoom");
            var y = document.getElementById("tanggal_zoom");
            x.style.display = "block";
            y.style.display = "block";
            var $temp = $("<div id='toCopy'>");
            $("body").append($temp);
            $temp.attr("contenteditable", true)
                .html($(selector).html());
            selectText("toCopy");
            document.execCommand("copy");
            $temp.remove();
            x.style.display = "none";
            x.style.display = "none";
            alert('sukses copy data');
        }
</script>
@endsection