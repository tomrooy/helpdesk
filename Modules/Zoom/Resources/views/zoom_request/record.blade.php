@extends('layouts.nice.main')
@section('title','Zoom Form Request Page')
@push('nav-helpdesk') show @endpush
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Form Proses Request Zoom @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Form Proses Input Link Record ZOOM</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <form method="POST" action="{{route('module.zoom_request.action_record',$data->id)}}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    Data Permintaan Zoom
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Pegawai</label>
                                                <input type="text" class="form-control" value="{{$data->created_by_users->name}}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Bagian / Wilayah</label>
                                                <input type="text" class="form-control" value="{{$data->request_by}}" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Kegiatan</label>
                                        <textarea class="form-control" disabled>{{$data->name}}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Kategori Kegiatan</label>
                                                <input type="text" class="form-control" value="{{$data->category}}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Jumlah Peserta</label>
                                                <input type="text" class="form-control" value="{{$data->participant}} Peserta" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Jadwal</label>
                                        <input type="text" class="form-control" value="{!! 'Tanggal '.\Carbon\Carbon::createFromFormat('Y-m-d',$data->date)->format('d M Y'). "   Jam ". $data->start.'-'.$data->end !!}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    Form Proses Record Permintaan Zoom
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ID Zoom Meeting</label>
                                                <input disabled type="number" name="zoom_id" value="{{$data->zoom_id}}" class="form-control {{$errors->first('zoom_id') ? 'is-invalid': ''}}" >
                                                <div class="invalid-feedback">
                                                    {{$errors->first('zoom_id')}}
                                                </div>
                                            </div>                                              
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input disabled type="text" name="password" value="{{$data->password}}" class="form-control {{$errors->first('password') ? 'is-invalid': ''}}" >
                                                <div class="invalid-feedback">
                                                    {{$errors->first('password')}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Link ZOOM</label>
                                            <textarea disabled autocomplete="off" type="text" class="form-control {{$errors->first('link') ? "is-invalid" : ""}} " name="link" placeholder="Input link">{{$data->link}}</textarea>
                                                <div class="invalid-feedback">
                                                    {{$errors->first('link')}}
                                                </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <label>Link  Record ZOOM</label>
                                            <textarea  autocomplete="off" type="text" class="form-control {{$errors->first('link_record') ? "is-invalid" : ""}} " name="link_record" placeholder="Input link Recording">{{old('link_record')}}</textarea>
                                                <div class="invalid-feedback">
                                                    {{$errors->first('link_record')}}
                                                </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Password Record</label>
                                                <input  type="text" name="password_record" value="{{old('password_record')}}" class="form-control {{$errors->first('password_record') ? 'is-invalid': ''}}" >
                                                <div class="invalid-feedback">
                                                    {{$errors->first('password_record')}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="col text-center">
                                        <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Simpan</button>
                                        <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {        
    });
</script>
@endsection