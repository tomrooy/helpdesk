<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title col text-center" id="exampleModalLabel"><b>Data Lengkap ZOOM</b></h4>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" style="overflow: hidden;">
              <form>
                  <div class="row mb-3">
                      <label for="name" class="col-sm-2 col-form-label">Nama Kegiatan</label>
                      <div class="col-sm-10">
                        <textarea type="text" class="form-control" id="title"></textarea>
                      </div>
                    </div>
                    <div class="row mb-3">
                      <label for="name" class="col-sm-2 col-form-label">Jadwal Kegiatan</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="schedule"></input>
                      </div>
                    </div>
                    <div class="row mb-3">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">ID dan Password</label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="zoom_id">
                      </div>
                      <div class="col-sm-2">
                          <input type="text" class="form-control" id="password">
                        </div>
                        <div class="col-sm-5">
                          <button id="something" class="btn btn-info"><i class="fas fa-video"></i> Start Meeting</button>
                        </div>
                    </div>
                    <div class="row mb-3">
                      <label for="name" class="col-sm-2 col-form-label">Link ZOOM</label>
                      <div class="col-sm-10">
                        <textarea type="text" class="form-control" id="link"></textarea>
                      </div>
                    </div>
              </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary close-btn " data-bs-dismiss="modal">Close</button>
          </div>
                          
      </div>
  </div>
</div>