@extends('layouts.nice.main')
@section('title','Zoom Data Edit-Page')
@push('nav-zoom') show @endpush
@section('css')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<style>
    .select2{
        width:100%!important;
        
        } 
</style>
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Edit @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Form Input Zoom Data</strong></h2>
            </div> 
            <div class="card-body">
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <br>
                <form method="POST" action="{{route('module.zoom.update',$zoom->id)}}" enctype="multipart/form-data" class="row g-3">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-floating">
                                <input type="text" name="name" value="{{$zoom->name}}" class="form-control {{$errors->first('name') ? 'is-invalid': ''}}" placeholder="Silahkan Isi Nama Agenda Meeting">
                                <label>Nama Agenda Meeting</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('name')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating">
                                <select name="created_by_account" class="form-control {{$errors->first('created_by_account') ? "is-invalid" : ""}}">
                                    @foreach($zoomaccount as $item)
                                        <option value="{{$item->username}}" {{$item->username == $zoom->created_by_account ? 'selected' : '' }}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                <label><b>Dibuat Pakai Akun</b></label>
                                <div class="invalid-feedback">
                                    {{$errors->first('created_by_account')}}
                                </div>
                            </div>
                        </div>
                    </div>          
                
                        <div class="col-md-3">
                            <div class="form-floating">
                                <select name="request_by" class="form-control {{$errors->first('request_by') ? "is-invalid" : ""}}">
                                    @foreach($userlocation as $item)
                                        <option value="{{$item->location_id}}" {{$item->location_id == $zoom->request_by ? 'selected' : '' }}>{{$item->name}}</option>
                                    @endforeach                            
                                </select>
                                <label>Yang Ajukan Permintaan</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('request_by')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-floating">
                                <select name="category" class="form-control {{$errors->first('category') ? "is-invalid" : ""}}">
                                    <option value="Rapat Kordinasi" @if($zoom->category == 'Rapat Kordinasi') {{'selected'}} @endif>Rapat Kordinasi</option>
                                    <option value="Rapat Internal" @if($zoom->category == 'Rapat Internal') {{'selected'}} @endif>Rapat Internal</option>
                                    <option value="Acara Workshop" @if($zoom->category== 'Acara Workshop') {{'selected'}} @endif>Acara Workshop</option>
                                    <option value="Acara Gemar Belajar" @if($zoom->category == 'Acara Gemar Belajar') {{'selected'}} @endif>Acara Gemar Belajar</option>
                                </select>
                                <label>Kategory Meeting</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('category')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-floating">
                                <select name="subcategory" class="form-control {{$errors->first('subcategory') ? "is-invalid" : ""}}">
                                    <option value="itjen" @if($zoom->subcategory == 'itjen') {{'selected'}} @endif>Pegawai Itjen</option>
                                    <option value="kumham" @if($zoom->subcategory == 'kumham') {{'selected'}} @endif>Pegawai Kumham</option>
                                    <option value="umum" @if($zoom->subcategory== 'umum') {{'selected'}} @endif>Umum</option>
                                </select>
                                <label>Peserta Meeting</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('subcategory')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-floating">
                                <textarea autocomplete="off" type="text" class="form-control {{$errors->first('description') ? "is-invalid" : ""}} " name="description" placeholder="description">{{$zoom->description}}</textarea>
                                <label>Deskripsi ZOOM</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('description')}}
                                </div>
                            </div> 
                        </div>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input autocomplete="off" type="date" name="date" value="{{$zoom->date}}" class="datepicker form-control {{$errors->first('date') ? 'is-invalid': ''}}" >
                            <label>Tanggal Agenda Meeting</label>
                            <div class="invalid-feedback">
                                {{$errors->first('date')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-floating">
                            <select name="start" id="start" class="form-control {{$errors->first('start') ? 'is-invalid': ''}}">
                                @foreach($time as $item)
                                    <option value="{{$item->name}}" {{$item->name == $zoom->start ? 'selected' : '' }}>{{$item->name}}</option>
                                @endforeach
                            </select>
                            <label>Jam Mulai</label>
                            <div class="invalid-feedback">
                                test
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-floating">
                            <select name="end" id="end" class="form-control {{$errors->first('end') ? 'is-invalid': ''}}">
                                @foreach($time as $item)
                                    <option value="{{$item->name}}" {{$item->name == $zoom->end ? 'selected' : '' }}>{{$item->name}}</option>
                                @endforeach
                            </select>
                            <label>Jam Berakhir</label>
                            <div class="invalid-feedback">
                                {{$errors->first('end')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-floating">
                            <input type="file" name="attachment"  class="form-control {{$errors->first('attachment') ? 'is-invalid': ''}}" >
                            <label>File Nodin (opsional)</label>
                            <div class="invalid-feedback">
                                {{$errors->first('attachment')}}
                            </div>
                            *format yang diterima adalah PDF max : 1 MB
                        </div>
                    </div>                 
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input type="number" name="zoom_id" value="{{$zoom->zoom_id}}" class="form-control {{$errors->first('zoom_id') ? 'is-invalid': ''}}" >
                            <label>ID Zoom Meeting</label>
                            <div class="invalid-feedback">
                                {{$errors->first('zoom_id')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input type="text" name="password" value="{{$zoom->password}}" class="form-control {{$errors->first('password') ? 'is-invalid': ''}}" >
                            <label>Password</label>
                            <div class="invalid-feedback">
                                {{$errors->first('password')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating">
                            <input type="text" name="link" value="{{$zoom->link}}" class="form-control {{$errors->first('link') ? 'is-invalid': ''}}" >
                            <label>Link</label>
                            <div class="invalid-feedback">
                                {{$errors->first('link')}}
                            </div>
                        </div>
                    </div>
              
               
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input type="text" name="link_record" value="{{$zoom->link_record}}" class="form-control {{$errors->first('link') ? 'is-invalid': ''}}" >
                            <label>Link Record</label>
                            <div class="invalid-feedback">
                                {{$errors->first('link_record')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input type="text" name="password_record" value="{{$zoom->password_record}}" class="form-control {{$errors->first('password') ? 'is-invalid': ''}}" >
                            <label>Password</label>
                            <div class="invalid-feedback">
                                {{$errors->first('password_record')}}
                            </div>
                        </div>
                    </div>                    
                    <br>
                    <div class="col text-center">
                        <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Simpan</button>
                        <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
@include('layouts.nice.partial.indexjs')
@include('zoom::zoom.javascript')
@endsection