@extends('layouts.nice.main')
@section('title','Zoom Data Create-Page')
@push('nav-zoom') show @endpush
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style>
    .select2{
        width:100%!important;
        
        } 
</style>
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Create @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Form Input Zoom Data</strong></h2>
            </div>    
            <div class="card-body">
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <br>
                <form method="POST" action="{{route('module.zoom.store')}}" enctype="multipart/form-data" class="row g-3">
                    @csrf             
                    <div class="col-md-8">
                        <div class="form-floating">
                            <input type="text" name="name" value="{{old('name')}}" class="form-control {{$errors->first('name') ? 'is-invalid': ''}}" placeholder="Silahkan Isi Nama Agenda Meeting">
                            <label>Nama Agenda Meeting</label>
                            <div class="invalid-feedback">
                                {{$errors->first('name')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating">
                            <select name="created_by_account" class="form-control {{$errors->first('created_by_account') ? "is-invalid" : ""}}">
                                <option value="">--Pilih Akun --</option>
                                @foreach($zoomaccount as $item)
                                    <option value="{{$item->username}}" @if(old('created_by_account') == '{{$item->name}}') {{'selected'}} @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                            <label>Dibuat Pakai Akun</label>
                            <div class="invalid-feedback">
                                {{$errors->first('created_by_account')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <select name="request_by" class="form-control {{$errors->first('request_by') ? "is-invalid" : ""}}">
                                <option value="">--Diajukan Oleh--</option>
                                @foreach($userlocation as $item)
                                    <option value="{{$item->location_id}}" @if(old('request_by') == '{{$item->location_id}}') {{'selected'}} @endif>{{$item->name}}</option>
                                @endforeach                            
                            </select>
                            <label>Yang Ajukan Permintaan</label>
                            <div class="invalid-feedback">
                                {{$errors->first('request_by')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-floating">
                            <select name="category" class="form-control {{$errors->first('category') ? "is-invalid" : ""}}">
                                <option value="">--kategory meeting --</option>
                                <option value="Rapat Kordinasi" @if(old('category') == 'Rapat Kordinasi') {{'selected'}} @endif>Rapat Kordinasi</option>
                                <option value="Rapat Internal" @if(old('category') == 'Rapat Internal') {{'selected'}} @endif>Rapat Internal</option>
                                <option value="Acara Workshop" @if(old('category') == 'Acara Workshop') {{'selected'}} @endif>Acara Workshop</option>
                                <option value="Acara Gemar Belajar" @if(old('category') == 'Acara Gemar Belajar') {{'selected'}} @endif>Acara Gemar Belajar</option>
                            </select>
                            <label>Kategory Meeting</label>
                            <div class="invalid-feedback">
                                {{$errors->first('category')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-floating">
                            <select name="subcategory" class="form-control {{$errors->first('subcategory') ? "is-invalid" : ""}}">
                                <option value="">--Peserta meeting --</option>
                                <option value="itjen" @if(old('subcategory') == 'itjen') {{'selected'}} @endif>Pegawai Itjen</option>
                                <option value="kumham" @if(old('subcategory') == 'kumham') {{'selected'}} @endif>Pegawai Kumham</option>
                                <option value="umum" @if(old('umum') == 'umum') {{'selected'}} @endif>UMUM</option>
                            </select>
                            <label>Kategory Meeting</label>
                            <div class="invalid-feedback">
                                {{$errors->first('subcategory')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-floating">
                            <textarea autocomplete="off" type="text" class="form-control {{$errors->first('description') ? "is-invalid" : ""}} " name="description" placeholder="description">{{old('description')}}</textarea>
                            <label>Deskripsi ZOOM</label>        
                            <div class="invalid-feedback">
                                        {{$errors->first('description')}}
                                    </div>
                        </div> 
                    </div>                    
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input autocomplete="off" type="date" name="date" value="{{old('date')}}" class=" form-control {{$errors->first('date') ? 'is-invalid': ''}}" >
                            <label>Tanggal Agenda Meeting</label>
                            <div class="invalid-feedback">
                                {{$errors->first('date')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-floating">
                            <select name="start" id="start" class="form-control {{$errors->first('start') ? 'is-invalid': ''}}">
                                <option value="">Pilih Jam</option>
                                @foreach($time as $item)
                                    <option value="{{$item->name}}" @if(old('start') == '{{$item->name}}') 'selected' @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                            <label>Jam Mulai</label>
                            <div class="invalid-feedback">
                                {{$errors->first('date')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-floating">
                            <select name="end" id="end" class="form-control {{$errors->first('end') ? 'is-invalid': ''}}">
                                <option value="">Pilih Jam</option>
                                @foreach($time as $item)
                                    <option value="{{$item->name}}" @if(old('end') == '{{$item->name}}') 'selected' @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                            <label>Jam Berakhir</label>
                            <div class="invalid-feedback">
                                {{$errors->first('end')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-floating">
                            <input type="file" name="attachment"  class="form-control {{$errors->first('attachment') ? 'is-invalid': ''}}" >
                            <label>File Nodin (opsional)</label>
                            <div class="invalid-feedback">
                                {{$errors->first('attachment')}}
                            </div>
                            *format yang diterima adalah PDF max : 1 MB
                        </div>
                    </div>                
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input type="number" name="zoom_id" value="{{old('zoom_id')}}" class="form-control {{$errors->first('zoom_id') ? 'is-invalid': ''}}" placeholder="ID Zoom ">
                            <label>ID Zoom Meeting</label>
                            <div class="invalid-feedback">
                                {{$errors->first('zoom_id')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input type="text" name="password" value="{{old('password')}}" class="form-control {{$errors->first('password') ? 'is-invalid': ''}}" placeholder="Password" >
                            <label>Password</label>
                            <div class="invalid-feedback">
                                {{$errors->first('password')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating">
                            <input type="text" name="link" value="{{old('link')}}" class="form-control {{$errors->first('link') ? 'is-invalid': ''}}" placeholder="LINK ZOOM">
                            <label>Link</label>
                            <div class="invalid-feedback">
                                {{$errors->first('link')}}
                            </div>
                        </div>
                    </div>                    
                    <div>
                        *catatan : ID Password dan Link dikosongkan jika ingin membuat ID Meeting pertama kali (Coming Soon)
                    </div>
                    <div class="col-md-3">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" name="activity" id="flexSwitchCheckDefault">
                            <label class="form-check-label" for="flexSwitchCheckDefault">Buat Presensi Online Kegiatan</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-check form-switch" id="photo_upload" style="display: none">
                            <input class="form-check-input" type="checkbox"  name="photo_upload" id="flexSwitchCheckDefault">
                            <label class="form-check-label" for="flexSwitchCheckDefault">Upload Foto Mengikuti Kegiatan</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-check form-switch"  id="presensi" style="display: none" >
                            <input class="form-check-input" type="checkbox" name="presensi" id="flexSwitchCheckDefault">
                            <label class="form-check-label" for="flexSwitchCheckDefault">Rekap Pegawai Itjen Yang Tidak Hadir</label>
                        </div>
                    </div>
                    <div class="col text-center">
                        <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Simpan</button>
                        <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@include('layouts.nice.partial.indexjs')
@include('zoom::zoom.javascript')
@endsection