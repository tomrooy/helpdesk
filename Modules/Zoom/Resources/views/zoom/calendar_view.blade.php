@extends('layouts.nice.main')
@section('title','Calendar View')
@push('nav-zoom') show @endpush
@section('css')
<link href="{{asset('nice/calendar/fullcalendar.css')}}" rel='stylesheet' />
<style>
    @foreach ($zoomaccount as $item)
        .{{$item->username}}  
        {
            background-color: {{$item->color}};
            color: #ffffff;
            padding: 5px;
        }
    @endforeach
   
    
</style>
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Index @endpush   
        <div class="card">
            <div class="card-header">
                <div style="text-align: center"><h2><strong >Data ZOOM Perbulan</strong></h2></div>
            </div>    
            <div class="card-body">
                            
                <a href="{{route('module.zoom.create')}}" class="btn btn-info float-end"><span class="fas fa-plus-square"></span> Tambah Zoom Meeting</a>
               
                @include('zoom::zoom.calendar_modal')
                <table>
                    <tr>
                    @foreach ($zoomaccount as $item)
                        <td class="{{$item->username}}">{{$item->name}}</td>
                    @endforeach
                    </tr>
                </table>
                <br>
                <div id='calendar'></div>
                  
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="{{asset('nice/calendar/fullcalendar.min.js')}}"></script>
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {        
        var interval = setInterval(function() {
            var momentNow = moment();
            $('#date-part').html(momentNow.format('dddd')
                                .substring(0,9).toUpperCase()+' '+ momentNow.format('DD MMMM YYYY'));
            $('#time-part').html(momentNow.format('HH:mm:ss A'));
        }, 100);
        var today = new Date().toISOString().slice(0,10);
            console.log(today);
            $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,basicWeek,basicDay'
                    },
                    windowResize: function(arg) {
                        alert('The calendar has adjusted to a window resize. Current view: ' + arg.view.type);
                      },
                    handleWindowResize :true,
                    eventStartEditable :false,//fungsi untuk mindahkan event
                    defaultDate: today,
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    selectable: true,
                    multipleselect : false,
                    selectHelper: true,
                    aspectRatio:  3,
                    displayEventEnd: true,
                    select: function(start, end){
                      if(moment(start._d).add(1, 'days').format('YYYY-MM-DD')==moment(end._d).format('YYYY-MM-DD')){
                        //$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                        //$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                        //$('#ModalAdd').modal('show');
                       
                        
                      }
                    },
                    
                    //fungsi untuk menampilkan detail dari event yang di klik
                    eventRender: function(event, element) {
                        element.bind('dblclick', function() {
                            $('#ModalEdit #id').val(event.id);
                            $('#ModalEdit #title').val(event.name);
                            $('#ModalEdit #zoom_id').val(event.zoom_id); 
                            $('#ModalEdit #link').val(event.link);
                            $('#ModalEdit #password').val(event.password);
                            $('#ModalEdit #schedule').val(event.schedule);
                            $('#ModalEdit #color').val(event.color);
                            $('#something').click(function() { 
                                window.open(event.link,'_blank');
                            } );
                            //var deleteMsg = alert("Nama Kegiatan : "+ event.name +
                            //                    "\n ID : "+event.zoom_id+ 
                            //                    "\n Password : "+event.password+
                            //                    "\n Link : " +event.link);
                            $('#ModalEdit').modal('show');
                        });
                    },
                   
                    eventDrop: function(event, delta, revertFunc) { // si changement de position
        
                        edit(event);
        
                    },
                    eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
        
                        edit(event);
        
                    },
                    //parsing data zoom dari database
                    events: [
                            @foreach($zoom as $item)
                                {
                                    id:     '{{$item->id}}',
                                    title:  '{{$item->request_by}}',
                                    name : '{{$item->name}}',
                                    link : '{{$item->link}}',
                                    zoom_id: '{{$item->zoom_id}}',
                                    password: '{{$item->password}}',
                                    start:  '{{$item->date.' '.\Carbon\Carbon::createFromFormat('H:i', $item->start)->format('H:i:s')}}',
                                    end:    '{{$item->date.' '.\Carbon\Carbon::createFromFormat('H:i', $item->end)->format('H:i:s')}}',
                                    color:  '{{$item->colors->color}}',
                                    schedule : '{{$item->date.' '.$item->start.'-'.$item->end}}' 
                                },
                            @endforeach                      
                    ]
                });
                
                function edit(event){
                    start = event.start.format('YYYY-MM-DD HH:mm:ss');
                    if(event.end){
                        end = event.end.format('YYYY-MM-DD HH:mm:ss');
                    }else{
                        end = start;
                    }
                    
                    id =  event.id;
                    
                   
                    
                    $.ajax({
                      url: '../../admin/update_shift_time/',
                      dataType: 'json',
                      data: {
                            id : id,
                            start : start,
                            end : end,
                            nik   : event.nik,
                            title : event.title_id,
                            color : event.color,
                        },
                       success: function(rep) {
                        if(rep == 22){
                          alert('data tersimpan');
                          location.reload();
                        }else if(rep == 24)
                        {
                          alert('ada jadwal personal yang ganda dalam satu hari');
                          location.reload();
                        }else{
                          alert('data tidak berhasil disimpan. Coba Lagi');
                           
                        }
                      }
                      });
                }
    });
</script>    
@endsection
