@extends('layouts.nice.main')
@section('title','Zoom Main-Page')
@push('nav-zoom') show @endpush
@section('css')
<link href="{{asset('nice/assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Index @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Data Zoom</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <br>
                @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <a href="{{route('module.zoom.create')}}" class="btn btn-info"><span class="fas fa-plus-square"></span> Tambah Data</a>
                <br><br>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Agenda</th>
                                <th>Direquest Oleh</th>
                                <th>Tanggal</th>
                                <th>Link</th>
                                <th>ID/Password</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;    
                            @endphp
                            @foreach ($zoom as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->request_by}}</td>
                                <td>{!! 'Tanggal '.\Carbon\Carbon::createFromFormat('Y-m-d',$item->date)->format('d M Y'). "<br>Jam ". $item->start.'-'.$item->end !!}</td>
                                <td><a href="{{$item->link}}" class="btn btn-info">Klik Disini</a></td>
                                <td>{!!$item->zoom_id .'<br>'.$item->password!!}</td>
                                <td>
                                    <a href="{{route('module.zoom.show',$item->id)}}" class="btn btn-info btn-sm"><span class="bx bx-detail"></span></a>
                                    <a href="{{route('module.zoom.edit',$item->id)}}" class="btn btn-warning btn-sm"><span class="bx bxs-edit-alt"></span> </a>
                                    <button type="button" class="btn btn-success btn-sm" id="inputRecord" data-name_zoom="{{$item->name}}" data-bs-id="{{$item->id}}"><span class="bx bxs-video-recording"></span> </button>
                                    <a href="javascript:void(0)" onclick="$(this).find('form').submit()" class="btn btn-danger btn-sm"><i class="bx bx-eraser"></i>
                                        <form method="POST" action={{route('module.zoom.destroy',$item->id) }}  onsubmit="return confirm('Delete this Zoom temporarily?')">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </a>
                                </td>
                            </tr>
                            @php
                                $no++;
                            @endphp    
                            @endforeach
                        </tbody>
                    </table>
                    <div class="modal fade" id="formInputRecord" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Input Data Zoom</h5>
                                    <button class="close" type="button" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="overflow: hidden;">
                                    <form method="POST" action="{{route('module.zoom.action_record')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="id">Nama Kegiatan</label>
                                            <input type="hidden" id="idzoom" name="id">
                                            <textarea disabled autocomplete="off" type="text"  class="form-control" id="name_zoom" name="name_zoom" placeholder="Silahkan Isi Id"></textarea>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Link</label>
                                                <textarea required autocomplete="off" type="text" class="form-control {{$errors->first('link_record') ? "is-invalid" : ""}} " name="link_record" placeholder="Silahkan Isi Link">{{old('link')}}</textarea>
                                                
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Password</label>
                                            <input required autocomplete="off" type="text" class="form-control" name="password_record" placeholder="Silahkan Isi Password">
                                         
                                        </div>                                       
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> Simpan</button>
                                    <button type="reset" class="btn btn-warning"><span class="fa fa-recycle"></span> Reset</button>
                                </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="{{asset('nice/assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {        
        $('#myTable').DataTable();
        $('body').on('click', '#inputRecord', function(e) {
            e.preventDefault();
            $('.alert-danger').html('');
            $('.alert-danger').hide();
            document.getElementById('idzoom').value    =   $(this).data('id');
            document.getElementById('name_zoom').value    =   $(this).data('name_zoom');
            $('#formInputRecord').modal('show');
        });
        
    });
    
</script>
@endsection