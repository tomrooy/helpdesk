@extends('layouts.nice.main')
@section('title','Zoom Data Create-Page')
@push('nav-zoom') show @endpush
@section('css')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Pencarian @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Halaman Pencarian Rekap ZOOM Meeting</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <p>Silahkan Isi Form Untuk melakukan pencarian Data</p>
                    <form action="{{route('module.zoom.search_process')}}" method="GET" enctype="multipart/form-data" class="row g-3">
                    @csrf
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input autocomplete="off" type="date" name="date" value="{{old('date')}}" class="datepicker form-control {{$errors->first('date') ? 'is-invalid': ''}}" >
                                <label>Tanggal Agenda Meeting</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('date')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <select name="created_by_account" class="form-control {{$errors->first('created_by_account') ? "is-invalid" : ""}}">
                                    <option value="all">--Pilih Akun --</option>
                                    <option value="all">Semua Akun</option>
                                    <option value="Akun Utama" @if(old('created_by_account') == 'Akun Utama') {{'selected'}} @endif>Akun Utama</option>
                                    <option value="Akun Ari" @if(old('created_by_account') == 'Akun Ari') {{'selected'}} @endif>Akun Ari</option>
                                    <option value="Akun Rifaldy" @if(old('created_by_account') == 'Akun Rifaldy') {{'selected'}} @endif>Akun Rifaldy</option>
                                    <option value="Akun Tommy" @if(old('created_by_account') == 'Akun Tommy') {{'selected'}} @endif>Akun Tommy</option>
                                </select>
                                <label><b>Dibuat Pakai Akun</b></label>
                                <input type="hidden" name="color" id="color">
                                <div class="invalid-feedback">
                                    {{$errors->first('created_by_account')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <select name="category" class="form-control {{$errors->first('category') ? "is-invalid" : ""}}">
                                    <option value="all">--kategory meeting --</option>
                                    <option value="all">Semua Kategory</option>
                                    <option value="Rapat Kordinasi" @if(old('category') == 'Rapat Kordinasi') {{'selected'}} @endif>Rapat Kordinasi</option>
                                    <option value="Rapat Internal" @if(old('category') == 'Rapat Internal') {{'selected'}} @endif>Rapat Internal</option>
                                    <option value="Acara Workshop" @if(old('category') == 'Acara Workshop') {{'selected'}} @endif>Acara Workshop</option>
                                    <option value="Acara Gemar Belajar" @if(old('category') == 'Acara Gemar Belajar') {{'selected'}} @endif>Acara Gemar Belajar</option>
                                </select>
                                <label><b>Kategory Meeting</b></label>
                                <div class="invalid-feedback">
                                    {{$errors->first('category')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <select name="request_by" class="form-control {{$errors->first('request_by') ? "is-invalid" : ""}}">
                                    <option value="all">--Diajukan Oleh--</option>
                                    <option value="all">Semua Bagian</option>
                                    <option value="Inspektorat Wilayah I" @if(old('request_by') == 'Inspektorat Wilayah I') {{'selected'}} @endif>Inspektorat Wilayah I</option>
                                    <option value="Inspektorat Wilayah II" @if(old('request_by') == 'Inspektorat Wilayah II') {{'selected'}} @endif>Inspektorat Wilayah II</option>
                                    <option value="Inspektorat Wilayah III" @if(old('request_by') == 'Inspektorat Wilayah III') {{'selected'}} @endif>Inspektorat Wilayah III</option>
                                    <option value="Inspektorat Wilayah IV" @if(old('request_by') == 'Inspektorat Wilayah IV') {{'selected'}} @endif>Inspektorat Wilayah IV</option>
                                    <option value="Inspektorat Wilayah V" @if(old('request_by') == 'Inspektorat Wilayah V') {{'selected'}} @endif>Inspektorat Wilayah V</option>
                                    <option value="Inspektorat Wilayah VI" @if(old('request_by') == 'Inspektorat Wilayah VI') {{'selected'}} @endif>Inspektorat Wilayah VI</option>
                                    <option value="Bagian Kepegawaian" @if(old('request_by') == 'Bagian Kepegawaian') {{'selected'}} @endif>Bagian Kepegawaian</option>
                                    <option value="Bagian Keuangan" @if(old('request_by') == 'Bagian Keuangan') {{'selected'}} @endif>Bagian Keuangan</option>
                                    <option value="Bagian PHP" @if(old('request_by') == 'Bagian PHP') {{'selected'}} @endif>Bagian PHP</option>
                                    <option value="Bagian SIP" @if(old('request_by') == 'Bagian SIP') {{'selected'}} @endif>Bagian SIP</option>
                                    <option value="Bagian UMUM" @if(old('request_by') == 'Bagian UMUM') {{'selected'}} @endif>Bagian UMUM</option>                                    
                                </select>
                                <label><b>Yang Ajukan Permintaan</b></label>
                                <div class="invalid-feedback">
                                    {{$errors->first('request_by')}}
                                </div>
                            </div>
                        </div>        
                    <div class="col text-center">
                        <button type="submit" class="btn btn-info"><span class="fas fa-search"></span> Cari</button>
                        <button type="reset" class="btn btn-warning"><span class="fas fa-recycle"></span> Reset</button>
                    </div>
                </form>
                    @if($status == 1)
                    <h2>Berikut Hasil Pencarian Data : </h2>
                        <div class="table-responsive">
                            <table id="resultTable" class="table table-bordered">
                                <thead class="bg bg-info text-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Agenda</th>
                                        <th>Direquest Oleh</th>
                                        <th>Link</th>
                                        <td>ID</td>
                                        <th>Password</th>
                                        <th>Nodin</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;    
                                    @endphp
                                    @foreach ($zoom as $item)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->request_by}}</td>
                                        <td>{{$item->link}}</td>
                                        <td>{{$item->zoom_id}}</td>
                                        <td>{{$item->password}}</td>
                                        <td>
                                            @if($item->attachment != null)
                                                Nota Dinas Ada
                                            @else
                                                Nota Dinas Belum
                                            @endif
                                        </td>
                                    </tr>
                                    @php
                                        $no++;
                                    @endphp    
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
@include('layouts.nice.partial.indexjs')
@include('zoom::zoom.javascript')
@endsection