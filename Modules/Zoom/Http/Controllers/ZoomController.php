<?php

namespace Modules\Zoom\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\Models\Zoom;
use App\Models\UserLocation;
use App\Models\ZoomAccount;
use App\Models\Time;
use App\Models\Activity;
use Carbon\Carbon;
use Illuminate\Support\Str;
use SendWa;
use DB;


class ZoomController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $zoom       =   Zoom::latest()->get();
        return view('zoom::zoom.index',[
            'zoom'  =>  $zoom
        ]);
    }
    public function create()
    {
        $time   =  Time::all();
	    $zoomaccount	=	ZoomAccount::all();
        $userlocation   =   UserLocation::all();
        return view('zoom::zoom.create',[
            'userlocation'  =>  $userlocation,
            'time'          =>  $time ,
	        'zoomaccount'    =>  $zoomaccount
        ]);

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function getColor($username)
    {
        $color  =   ZoomAccount::where('username',$username)->pluck('color');
        return $color;
    }
    public function store(Request $request)
    {
        $message =  [
            'name.required'     => 'nama harus di isi'
        ];
        \Validator::make($request->all(),[
            'name'          =>  'required|max:200',
            'request_by'    =>  'required|max:50',
            'category'      =>  'required|max:50',
            'subcategory'   =>  'required|max:50',
            'date'          =>  'required|max:50',
            'start'         =>  'required|max:50',
            'end'           =>  'required|max:50',
            'zoom_id'       =>  'nullable|max:20',
            'password'      =>  'nullable|max:20',
            'link'          =>  'nullable',
            'created_by_account' => 'required',
            'attachment'    =>  'nullable|mimes:pdf,doc,docx'
        ],$message)->validate();
        try{
            $cek_data = Zoom::where('created_by_account', $request->created_by_account)->where('date', $request->date)->where('start',$request->start)->first();
            $cek_data1 = Zoom::where('created_by_account', $request->created_by_account)->where('date', $request->date)->where('start',$request->start)->get();
            if($request->hasFile('attachment'))
            {   
                $unique_name =  'NODIN-'.$request->request_by.'-'.$request->date;
                $path = Str::slug($unique_name);
                $photo = $request->file('attachment');
                $name_cv = $path.'.'.$photo->getClientOriginalExtension();
                $path = $photo->storeAs('public/uploads/zooms/', $name_cv);

            }else {
                $name_cv = null;
            }
            

            if(count($cek_data1) > 0)
            {
                return redirect()->back()->with('errors_message','Gagal Tambah Data.Sudah ada Kegiatan '.$cek_data->name.' di tanggal dan jam yang sama dengan akun yang sama');
            }
            //$meetings = \MacsiDigital\Zoom\Facades\Zoom::user()->find('itjenkumhamsip@gmail.com')->meetings()->create([    
            //    'topic'             => $request->name,
            //    'type'              => 2,
            //    'duration'          => 120,
            //    'start_time'        => new Carbon($request->date.' '.$request->start.':00'),
            //    'timezone'          => 'Asia/Jakarta',
            //    'password'          =>  $request->password,   
            //    ]
            //);
            $meetings   =   1;  
            if($meetings)
            {
                
                DB::beginTransaction();
                $zoom   =   new Zoom;
                $zoom->name             =   $request->name;
                $zoom->request_by       =   $request->request_by;
                $zoom->category         =   $request->category;
                $zoom->subcategory      =   $request->subcategory;
                $zoom->date             =   $request->date;
                $zoom->start            =   $request->start;
                $zoom->end              =   $request->end;
                $zoom->zoom_id          =   $request->zoom_id;
                $zoom->password         =   $request->password;
                $zoom->link             =   $request->link;
                $zoom->method           =   'Admin';
                $zoom->attachment       =   $name_cv;
                $zoom->created_by_account       =   $request->created_by_account;
                $zoom->created_by       =   \Auth::User()->nip;
                $zoom->description      =   $request->description;
                $zoom->save();

		$link = '';
                if($request->activity == 'on')
                {
                    $activity   =   new Activity;
                    $index_number               =    IdGenerator::generate(['table' => 'activities','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
                    $activity_id                =   'ITJ-K-'.$index_number;
                    //$url = null;
                    $url                        =   $activity->short_link($activity_id);
                    
                    $activity->index_number     = $index_number;
                    $activity->activity_id      = $activity_id;
                    $activity->name             = $request->name;
                    $activity->slug             = $request->name;
                    $activity->date             = $request->date;
                    $activity->start            = $request->start;
                    $activity->end              = $request->end;
                    $activity->category_id      = $request->subcategory;;
                    $activity->url              = $url;
                    $activity->location         = $request->request_by;
                    $activity->status           =   '0';
                    $activity->description      =   $request->description;
                    $activity->created_by       =   \Auth::User()->nip;
                    if($request->presensi == 'on')
                    {
                        $activity->alpha_check  = '1';
                    }else{
                        $activity->alpha_check  = '0';
                    }
                    if($request->photo_upload == 'on')
                    {
                        $activity->photo_upload = '1';
                    }else{
                        $activity->photo_upload = '0';
                    }
                    $activity->save();
$link = 
"
link absensi : $activity->url
link rekap   : https://ithelpdesk.itjenkumham.id/presensi-rekap/$activity->activity_id
";
                }
$text =
"Inspektorat Jenderal is inviting you to a scheduled Zoom meeting.
Topic : $zoom->name
Time : $zoom->date $zoom->start
Join Zoom Meeting
$zoom->link

Zoom ID  : $zoom->zoom_id
Password : $zoom->password
$link
Dibuat menggunakan akun $zoom->created_by_account
";
                SendWa::send_it($text);
                //$lastActivity = Spatie\Activitylog\Models\Activity::all()->last();
                DB::commit();


                return redirect()->route('module.zoom.index')->with('success_message','data Berhasil di tambah');
            }else
            {
                DB::rollback();
                return redirect()->route('module.zoom.index')->with('success_message','terjadi kesalahan');
            }    
        }catch (\Exception $e)
        {
            return redirect()->back()->with('errors_message',$e->getMessage());
        }
    }
    public function show($id)
    {
        $zoom   =   Zoom::findOrFail($id);
        return view('zoom::zoom.detail',[
            'zoom'      =>  $zoom
        ]);
    }
    public function search()
    {
        return view('zoom::zoom.search',[
            'status'    => 0
        ]);
    }
    public function searchProcess(Request $request)
    {
        $cek_data = Zoom::where('date', $request->date)
                        ->Where('created_by_account', $request->created_by_account)
                        ->Where('category',$request->category)
                        ->Where('request_by',$request->request_by)->get();
        if($request->request_by == 'all' && $request->category =='all' && $request->created_by_account == 'all')
        {
            $cek_data = Zoom::where('date', $request->date)->get();
        }
        if($request->request_by == 'all' && $request->category =='all' && $request->created_by_account != 'all')
        {
            $cek_data = Zoom::where('date', $request->date)->Where('created_by_account', $request->created_by_account)->get();
        }
        if($request->request_by == 'all' && $request->category !='all' && $request->created_by_account == 'all')
        {
            $cek_data = Zoom::where('date', $request->date)->Where('category',$request->category)->get();
        }
        if($request->request_by == 'all' && $request->category !='all' && $request->created_by_account != 'all')
        {
            $cek_data = Zoom::where('date', $request->date)->Where('category',$request->category)->Where('created_by_account', $request->created_by_account)->get();
        }
        if($request->request_by != 'all' && $request->category =='all' && $request->created_by_account != 'all')
        {
            $cek_data = Zoom::where('date', $request->date)->Where('request_by',$request->request_by)->Where('created_by_account', $request->created_by_account)->get();
        }
        if($request->request_by != 'all' && $request->category =='all' && $request->created_by_account == 'all')
        {
            $cek_data = Zoom::where('date', $request->date)->Where('request_by',$request->request_by)->get();
        }
        if($request->request_by != 'all' && $request->category !='all' && $request->created_by_account == 'all')
        {
            $cek_data = Zoom::where('date', $request->date)->Where('request_by',$request->request_by)->Where('category',$request->category)->get();
        }        
        
        return view('zoom::zoom.search',[
            'zoom'  =>  $cek_data,
            'status'=> 1
        ]);
        
    }
    public function calendarView()
    {
        $zoom           =   Zoom::all();
        $zoomaccount    =   ZoomAccount::all();
        return view('zoom::zoom.calendar_view',[
            'zoom'          =>  $zoom,
            'zoomaccount'   =>  $zoomaccount
        ]);
    }

    public function edit($id)
    {
        $zoom   =   Zoom::findOrFail($id);
        $time   =  Time::all();
        $zoomaccount    =   ZoomAccount::All();
        $userlocation   =   UserLocation::all();
        return view('zoom::zoom.edit',[
            'zoom'          =>  $zoom,
            'userlocation'  =>  $userlocation,
            'zoomaccount'   =>  $zoomaccount,
            'time'          =>  $time  
        ]);
    }

    public function update(Request $request, $id)
    {
        $zoom   =   Zoom::findOrFail($id);
        $message =  [
            'name.required'     => 'nama harus di isi'
        ];
        \Validator::make($request->all(),[
            'name'                  =>  'required|max:200',
            'request_by'            =>  'required|max:50',
            'category'              =>  'required|max:50',
            'subcategory'           =>  'required|max:50',
            'date'                  =>  'required|max:50',
            'start'                 =>  'required|max:50',
            'end'                   =>  'required|max:50',
            'zoom_id'               =>  'required|max:20',
            'password'              =>  'required|max:20',
            'link'                  =>  'required',
            'created_by_account'    =>  'required'
        ],$message)->validate();
        try{
            $zoom->name             =   $request->name;
            $zoom->request_by       =   $request->request_by;
            $zoom->category         =   $request->category;
            $zoom->subcategory      =   $request->subcategory;
            $zoom->date             =   $request->date;
            $zoom->start            =   $request->start;
            $zoom->end              =   $request->end;
            $zoom->zoom_id          =   $request->zoom_id;
            $zoom->password         =   $request->password;
            $zoom->link             =   $request->link;
            $zoom->password_record  =   $request->password_record;
            $zoom->link_record      =   $request->link_record;
            $zoom->created_by_account =   $request->created_by_account;
            $zoom->updated_by       =   \Auth::User()->nip;
            $zoom->description      =   $request->description;
            $zoom->save();

            return redirect()->route('module.zoom.index')->with('success_message','data Berhasil di tambah');

        }catch (\Exception $e)
        {
            return redirect()->back()->with('errors_message',$e->getMessage());
        }
    }
    public function actionRecordZoom(Request $request)
    {
        $zoom   =   Zoom::findOrFail($request->id);

        $zoom->link_record      =   $request->link_record;
        $zoom->password_record  =   $request->password_record;
        $zoom->save();
        return redirect()->back()->with('success_message','Link ZOOM Berhasil di tambah');
    }

    public function destroy($id)
    {
        $zoom   =   Zoom::findOrFail($id);
        try{
            $zoom->update([
                'deleted_by'    =>   \Auth::User()->nip
            ]);
            $zoom->delete();
            return redirect()->back()->with('errors_message','data Berhasil dihapus');

        }catch (\Exception $e)
        {
            return redirect()->back()->with('errors_message',$e->getMessage());
        }
    }
    public function cek()
    {
                $url            = 'https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/' . $nip . '.png';
                $fotoDb         = '';

                //cek 404
                $handle = curl_init($url);
                curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
                /* Get the HTML or whatever is linked in $url. */
                $response = curl_exec($handle);
                /* Check for 404 (file not found). */
                $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                
                if ($httpCode == 404) {
                    /* Handle 404 here. */
                    $url            = 'https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/' . $nip . '.jpeg';
                    $handle = curl_init($url);
                    curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
                    $response = curl_exec($handle);
                    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                    if ($httpCode == 404) {
                        $url            = 'https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/' . $nip . '.jpg';
                        $handle = curl_init($url);
                        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
                        $response = curl_exec($handle);
                        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                        if ($httpCode == 404) {
                            $fotoDb         = base_url() . '/dist/img/foto-profil/default.png';
                        } else {
                            $img            = $nip . '.jpg';
                        }
                    } else {
                        $img            = $nip . '.jpeg';
                    }
                } else {
                    $img            = $nip . '.png';
                }
                curl_close($handle);

                $password       = password_hash($this->request->getVar('pass'), PASSWORD_DEFAULT);
                $unitKerja      = explode(" - ", $this->request->getVar('unitKerja'));
                $unitKerja2     = '';
                $unitKerja3     = '';
                $unitKerja4     = '';
                for ($i = 0; $i < count($unitKerja); $i++) {
                    ${'unitKerja' . ($i + 1)} = $unitKerja[$i];
                }
                $role           = 5; //TWL
                $statusPass     = 2; //sudah mengubah password
                $statusAktif    = 1; //aktif
                $alasanNonaktif = 0;
                //kirim email pemberitahuan berhasil mendaftar
                $email          = $this->request->getVar('email');
                $nama           = $this->request->getVar('nama');
                $subject        = 'Selamat, Anda Berhasil Mendaftar LHKASN';
                $dataEmail   = [
                    'nama' => $nama
                ];
                $pesan          = view('email/pendaftaran', $dataEmail);
               
    }
    
}
