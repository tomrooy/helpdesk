<?php

namespace Modules\Zoom\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\ZoomRequest;
use App\Models\ZoomAccount;
use App\Models\Zoom;
use App\Models\Activity;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use DB;
use SendWa;


class ZoomRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data   =   ZoomRequest::latest()->get();
        return view('zoom::zoom_request.index',[
            'zoom_request'  =>  $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function processZoom($id)
    {
        $zoomaccount    =   ZoomAccount::All();
        $data   =   ZoomRequest::findOrFail($id);
        return view('zoom::zoom_request.process',[
            'data'          =>  $data,
            'zoomaccount'   =>  $zoomaccount
        ]);
    }
    public function detail($id)
    {
        $zoom   =   ZoomRequest::findOrFail($id);
        return view('zoom::zoom_request.detail',[
            'zoom'      =>  $zoom
        ]);
    }
    public function actionProcessZoom($id, Request $request)
    {
        
        $data   =   ZoomRequest::findOrFail($id);
        $messages   =   [
            'zoom_id.required'              =>  'tidak boleh kosong',
            'password.required'             =>  'tidak boleh kosong',
            'link.required'                 =>  'tidak boleh kosong',
            'created_by_account.required'   =>  'tidak boleh kosong'
        ];
        \Validator::make($request->all(),[
            'zoom_id'               =>  'required',
            'password'              =>  'required',
            'link'                  =>  'required',
            'created_by_account'    =>  'required',
        ],$messages)->validate();

        try{
            DB::beginTransaction();
            $data->zoom_id              =   $request->zoom_id;
            $data->password             =   $request->password;
            $data->link                 =   $request->link;
            $data->status               =   'Link Zoom Telah Dibuat';
            $data->updated_by           =   \Auth::User()->nip;
            $data->created_by_account   =   $request->created_by_account;
            $data->save();

            $cek_data = Zoom::where('created_by_account', $data->created_by_account)->where('date', $data->date)->where('start',$data->start)->first();
            $cek_data1 = Zoom::where('created_by_account', $data->created_by_account)->where('date', $data->date)->where('start',$data->start)->get();
            if(count($cek_data1) > 0)
            {
                return redirect()->back()->with('errors_message','Gagal Tambah Data.Sudah ada Kegiatan '.$cek_data->name.' di tanggal dan jam yang sama dengan akun yang sama');
            }

            $zoom   =   new Zoom;
            $zoom->name                 =   $data->name;
            $zoom->request_by           =   $data->request_by;
            $zoom->request_by_name      =   $data->created_by;
            $zoom->category             =   $data->category;
            $zoom->date                 =   $data->date;
            $zoom->start                =   $data->start;
            $zoom->end                  =   $data->end;
            $zoom->participant          =   $data->participant;
            $zoom->zoom_id              =   $data->zoom_id;
            $zoom->password             =   $data->password;
            $zoom->link                 =   $data->link;
            $zoom->created_by_account   =   $request->created_by_account;
            $zoom->method               =   'User';
            $zoom->created_by           =   \Auth::User()->nip;
            $zoom->description          =   $data->description;
            $zoom->save();

            $link = '';
            if($data->presensi == 'on')
                {
                    $activity   =   new Activity;
                    $index_number               =    IdGenerator::generate(['table' => 'activities','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
                    $activity_id                =   'ITJ-K-'.$index_number;
                    $url                        =   $activity->short_link($activity_id);

                    //return "test";
                    $activity->index_number     = $index_number;
                    $activity->activity_id      = $activity_id;
                    $activity->name             = $data->name;
                    $activity->slug             = $data->name;
                    $activity->date             = $data->date;
                    $activity->start            = $data->start;
                    $activity->end              = $data->end;
                    $activity->url              = $url;
                    $activity->location         = $data->request_by;
                    $activity->status           =   '0';
                    $activity->description      =   $data->description;
                    $activity->created_by       =   \Auth::User()->nip;
                    $activity->save();
                    $link = 
"
link absensi : $activity->url
link rekap   : https://ithelpdesk.itjenkumham.id/presensi-rekap/$activity->activity_id
";
                }
$text =
"Inspektorat Jenderal is inviting you to a scheduled Zoom meeting.
Topic : $zoom->name
Time : $zoom->date $zoom->start
Join Zoom Meeting
$zoom->link
                
Zoom ID  : $zoom->zoom_id
Password : $zoom->password

$link

Dibuat menggunakan akun $zoom->created_by_account
";
            $text2 = $text;
            $userphone = $data->user_wa;            
            SendWa::send_group($text,$text2,$userphone);            
            DB::commit();
            return redirect()->route('module.zoom_request.index')->with('success_message','data Berhasil di proses');
        }catch (\Exception $e)
        {
            DB::rollback();
            return redirect()->back()->with('errors_message',$e->getMessage());
        }
    }
    public function recordZoom($id)
    {
        $data   =   ZoomRequest::findOrFail($id);
        return view('zoom::zoom_request.record',[
            'data'  =>  $data
        ]);
    }
    public function actionRecordZoom($id, Request $request)
    {
        $data   =   ZoomRequest::findOrFail($id);
        $messages   =   [
            'password_record.required' =>  'tidak boleh kosong',
            'link_record.required'     =>  'tidak boleh kosong',
        ];
        \Validator::make($request->all(),[
           
            'password_record'  =>  'required',
            'link_record'      =>  'required'
        ],$messages)->validate();
        try{
            DB::beginTransaction();
            $data->password_record      =   $request->password_record;
            $data->link_record          =   $request->link_record;
            $data->status               =   'Record Zoom Telah Dibuat';
            $data->updated_by           =   \Auth::User()->nip;
            $data->save();

            //zoom database
            $zoom   =   Zoom::where('zoom_id',$data->zoom_id)->where('password',$data->password)->first();
            $zoom->password_record      =   $data->password_record;
            $zoom->link_record          =   $data->link_record;
            $zoom->save();
            DB::commit();
            return redirect()->route('module.zoom_request.index')->with('success_message','data Berhasil di proses');
        }catch (\Exception $e)
        {
            DB::rollback();
            return redirect()->back()->with('errors_message',$e->getMessage());
        }
    }
    public function create()
    {
        return view('zoom::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('zoom::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('zoom::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $zoom   =   ZoomRequest::findOrFail($id);
        try{
            $zoom->update([
                'deleted_by'    =>   \Auth::User()->nip
            ]);
            $zoom->delete();
            return redirect()->back()->with('errors_message','data Berhasil dihapus');

        }catch (\Exception $e)
        {
            return redirect()->back()->with('errors_message',$e->getMessage());
        }
    }
    public function send_wa($text)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => "6282113736864-1492728320",
            "message" => $text
        );

        $param_post = json_encode($data, JSON_PRETTY_PRINT);
        $post        = curl_init("https://app.alatwa.com/api/send/message/text");
        curl_setopt($post, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post, CURLOPT_POST, 1);
        curl_setopt($post, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($post);
        curl_close($post);
        echo $response;
    }
    public function send_wa_user($text,$userphone)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => $userphone,
            "message" => $text
        );

        $param_post = json_encode($data, JSON_PRETTY_PRINT);
        $post        = curl_init("https://app.alatwa.com/api/send/message/text");
        curl_setopt($post, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post, CURLOPT_POST, 1);
        curl_setopt($post, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($post);
        curl_close($post);
        echo $response;
    }
}
