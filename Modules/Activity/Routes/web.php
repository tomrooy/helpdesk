<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    Route::group(['prefix'=>'admin/module/presensi','middleware'=>'role:Super_Admin|Kepegawaian'], function() {

        Route::get('/data-kegiatan', 'ActivityController@index')->name('module.activity.index');
        Route::get('/data-kegiatan/get-data', 'ActivityController@getActivity')->name('module.activity.data');
        Route::get('/data-kegiatan/create', 'ActivityController@create')->name('module.activity.create');
        Route::post('/data-kegiatan/store', 'ActivityController@store')->name('module.activity.store');
        Route::get('/data-kegiatan/{id}/edit', 'ActivityController@edit')->name('module.activity.edit');
        Route::put('/data-kegiatan/{id}/update', 'ActivityController@update')->name('module.activity.update');
        Route::delete('/data-kegiatan/{id}/destroy', 'ActivityController@destroy')->name('module.activity.destroy');

        Route::get('/data-kegiatan/daftar-absen/{id}','AttendanceController@index')->name('module.activity.attendance');
        Route::delete('/data-kegiatan/daftar-absen/hapus/{id}','AttendanceController@destroy')->name('module.attendance.delete');
        Route::get('/data-kegiatan/attendance/download/{id}','AttendanceController@download')->name('module.activity.download');
        Route::POST('/data-kegiatan/on/off/{id}','ActivityController@switch')->name('module.activity.switch');

        
        Route::get('/data-kegiatan/kegiatan-harian', 'DailyPresentController@index')->name('module.daily_present.index');
        Route::get('/data-kegiatan/kegiatan-harian/{id}/edit', 'DailyPresentController@edit')->name('module.daily_present.edit');
        Route::post('/data-kegiatan/kegiatan-harian/update', 'DailyPresentController@update')->name('module.daily_present.update');


        Route::get('/data-kegiatan-apel', 'ApelActivityController@index')->name('module.apel.index');
        Route::get('/data-kegiatan-apel/{id}', 'ApelPresentController@index')->name('module.apel_present.index');
        Route::delete('/data-kegiatan/daftar-absen/{id}/hapus','ApelPresentController@destroy')->name('module.apel_present.delete');
        Route::get('/data-kegiatan-apel/print_qr/{id}', 'ApelPresentController@print')->name('module.apel_present.print');
        Route::get('/data-kegiatan-apel/rekap/{id}', 'ApelPresentController@rekap')->name('module.apel_present.rekap');
        Route::get('/data-kegiatan-apel/export_excel/{id}', 'ApelPresentController@export_excel')->name('module.apel_present.export_excel');
        Route::get('/data-kegiatan-apel/switch/{id}', 'ApelPresentController@switch')->name('module.apel_present.switch');
        Route::get('/data-kegiatan-apel/create', 'ApelActivityController@create')->name('module.apel.create');
        Route::post('/data-kegiatan-apel/store', 'ApelActivityController@store')->name('module.apel.store');

    });

});