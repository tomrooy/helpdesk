<?php

namespace Modules\Activity\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Attendance;
use App\Models\Activity;
use PDF;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($id)
    {
        $data   =   Attendance::where('activity_id', $id)->get();
        $name   =   Activity::where('activity_id', $id)->first(['activity_id','name']);
        return view('activity::attendance.index',[
            'attendance'    =>  $data,
            'name'          =>  $name
        ]);
    }

    public function download($id)
    {
        $name           =   Activity::where('activity_id', $id)->first(['date','name']);
        $attendance     =   Attendance::where('activity_id', $id)->get();
            
        $pdf = PDF::loadView('activity::attendance.pdf',[
            'attendance'   => $attendance,
            'title'         => $name->name,
            'date'          => $name->date
        ]);
     
        return $pdf->download('data_presensi.pdf');
    }

    
    public function destroy($id)
    {
        $data   =   Attendance::where('id', $id)->first();
        $data->delete();
        return redirect()->back()->with('success_message','data sukses dihapus');
    }
}
