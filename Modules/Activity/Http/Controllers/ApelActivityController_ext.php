<?php

namespace Modules\Activity\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use App\Models\ApelPresent;
use App\Models\ApelActivity;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use DB;
use QrCode;

class ApelActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data   =   ApelActivity::withCount('apel_presents')->orderBY('created_at','DESC')->get();
        return view('activity::apel_activity.index',[
            'apel_activity' =>  $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('activity::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try{
            $validator  =   \Validator::make($request->all(),[
                'date_'           =>      'required|max:255',
                'name_'          =>       'required|max:255',
              
            ]);
            if($validator->passes())
            {
                $data                       =   new ApelActivity;
                $index_number               =    IdGenerator::generate(['table' => 'apel_activities','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
                $id                         =   'apl-'.$index_number;
                //QrCode::size(500)->format('png')->generate('ItSolutionStuff.com', public_path('storage/uploads/apel/qr/'));

                
                DB::beginTransaction();
                $data                       =   new ApelActivity;
                $data->date                 =   $request->date_;
                $data->name                 =   $request->name_;
                $data->activity_id          =   $id;
                $data->index_number         =   $index_number;
                $data->status               =   1;
                $data->category_id          =   2;
                $data->save();

                //$url                        =   $data->short_link($data->id);
                $url                        =   Str::random(7);
                $data->qr_code              =   $data->id;
                $data->link                 =   $url;
                $data->save();
                DB::commit();
                return $data;
            }else{
                return response()->json(['errors' => $validator->errors()]);
            }

        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function change_url(Request $request)
    {
        $data               =   ApelActivity::where('id',$request->id)->first();

        $data->link          =   Str::random(7);
        $data->save();
        
        return "sukses";
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('activity::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('activity::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
