<?php

namespace Modules\Activity\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\DailyPresent;
use App\Models\DailyPresentStatus;

class DailyPresentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data       =   DailyPresent::latest()->get();
        $status     =   DailyPresentStatus::all();
        return view('activity::daily_present.index',[
            'data'      =>  $data,
            'status'    =>  $status
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('activity::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('activity::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $daily          =   DailyPresent::findOrFail($id);
        return response()->json([
            'daily'  =>  $daily,
            'status'  =>   true,
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request)
    {

        $daily      =   DailyPresent::findOrFail($request->id);
        $daily->status_id   =   $request->status_daily_;
        $daily->save();
        

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
