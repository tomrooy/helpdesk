<?php

namespace Modules\Activity\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Activity;
use App\Models\Time;
use App\Models\UserLocation;
use GuzzleHttp\Client;
use Haruncpi\LaravelIdGenerator\IdGenerator;


class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('activity::activity.index');
    }
    public function getActivity()
    {
        $activity   =   Activity::all();
        return \DataTables::of($activity)
            ->addIndexColumn()
            ->addColumn('jadwal', function($data){
          
                $date = \Carbon\Carbon::parse($data->date)->locale('id');
                $date->settings(['formatFunction' => 'translatedFormat']);
            
                $jadwal = $date->format('l, j F Y').'<br>Pukul '. $data->start. '-'. $data->end;
                return $jadwal;
            })
            ->addColumn('status', function($data){
                return  $data->status == '1'  ? '<span class="badge bg-primary">Aktif</span>' : '<span class="badge bg-danger">TIdak Aktif</span>';
            })
            ->addColumn('link', function($data){
                $presensi   = 'Presensi : <a target="_blank" href="'.$data->url.'">'.$data->url.'</a><br>';
                $rekap      = 'Rekap : <a target="_blank" href="https://ithelpdesk.itjenkumham.id/presensi-rekap/'.$data->activity_id.'"> https://ithelpdesk.itjenkumham.id/presensi-rekap/'.$data->activity_id.'</a>';
                return  $presensi.'<br>'.$rekap;
            })
            ->addColumn('action', function($data){
                $list       = '<a href="'.route('module.activity.attendance', $data->activity_id).'" class="btn btn-info btn-sm"><span class="bx bx-detail"></span></a>';
                //$switch     = '<a href="'.route('module.activity.switch', $data->id).'" class="btn btn-warning btn-sm"><span class="bi bi-toggle2-off"></span> </a><br>';
                $switch     = '<button type="button" class="btn btn-warning btn-sm" id="switchActivity" data-id="'.$data->id.'" ><span class="bi bi-toggle2-off"></span> </a><br>';
                $delete     = '<button type="button" class="btn btn-danger btn-sm" id="deleteActivity" data-id="'.$data->id.'" ><i class="bi bi-trash"></i> </button>' ;
                //$setting    = '<button type="button" class="btn btn-danger btn-sm" id="settingActivity" data-id="'.$data->id.'" ><i class="bx bxs-cog"></i> </button>' ;
                return $list.$switch.$delete;
            })
            //->addColumn('count', function($data){
            //    $count = $data->attendances->count();
           //     return "<b> $count Orang</b>" ;
           // })
            ->rawColumns(['jadwal','action','status','link'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */

    public function switch($id)
    {
        try{    
            $activity   =   Activity::findOrfail($id);
            if($activity->status != '0')
            {
                $activity->status   =   '0' ;
            }else{
                $activity->status   =   '1' ;
            }
            $activity->save();
           
            return response()->json([
                'status'   =>  true,
                'message'  =>  $activity->name.' diupdate'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
    }
    public function create()
    {
        $time           =  Time::all();
        $userlocation   =   UserLocation::all();
        return view('activity::activity.create',[
            'userlocation'  =>  $userlocation,
            'time'          =>  $time  
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try{
            \Validator::make($request->all(),[
                'name'          =>  'required',
                'request_by'    =>  'required',
                'category'      =>  'required',
                'subcategory'   =>  'required',
                'date'          =>  'required',
                'start'         =>  'required',
                'end'           =>  'required',
                'attachment'    =>  'nullable'
            ])->validate();

            if($request->hasFile('attachment'))
            {   
                $unique_name =  'NODIN-'.$request->request_by.'-'.$request->date;
                $path = Str::slug($unique_name);
                $file = $request->file('attachment');
                $name_cv = $path.'.'.$file->getClientOriginalExtension();
                $path = $file->storeAs('public/uploads/zooms/', $name_cv);

            }else {
                $name_cv = null;
            }
            $activity   =   new Activity;
            $index_number               =    IdGenerator::generate(['table' => 'activities','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
            $activity_id                =   'ITJ-K-'.$index_number;
            $url                        =   $activity->short_link($activity_id);
                    
            $activity->index_number     = $index_number;
            $activity->activity_id      = $activity_id;
            $activity->name             = $request->name;
            $activity->slug             = $request->name;
            $activity->date             = $request->date;
            $activity->start            = $request->start;
            $activity->end              = $request->end;
            $activity->category_id      = $request->subcategory;;
            $activity->url              = $url;
            $activity->location         = $request->request_by;
            $activity->status           =   '0';
            $activity->created_by       =   \Auth::User()->nip;
            if($request->presensi == 'on')
            {
                $activity->alpha_check  = '1';
            }else{
                $activity->alpha_check  = '0';
            }
            if($request->photo_upload == 'on')
            {
                $activity->photo_upload = '1';
            }else{
                $activity->photo_upload = '0';
            }
            $activity->save();

             return redirect()->route('module.activity.index')->with('success_message','Link telah sukses dibuat'); 

        }catch (\exception $e)
        {
            return redirect()->back()->with('errors_message',$e->getMessage());
        }
       
        

        return $request->all();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('activity::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('activity::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try{
            $data   =   Activity::findOrFail($id);
            $data->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
            $data->delete();
            return response()->json([
                'status'   =>  true,
                'message'  =>  $data->name.' delete'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
    }
}
