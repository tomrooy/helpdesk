<?php

namespace Modules\Activity\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ApelExport;
use Illuminate\Routing\Controller;
use App\Models\ApelPresent;
use App\Models\ApelActivity;
use App\Models\UserLocation;
use App\Models\User;

class ApelPresentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($id)
    {
        $present    =   ApelPresent::where('activity_id',$id)->get();
        $id         =   ApelActivity::where('activity_id',$id)->first();
        return view('activity::apel_present.index',[
            'present'   =>  $present,
            'id'        =>  $id->activity_id,
        ]);
    }
    public function rekap($id)
    {
        $bagian      =   UserLocation::withCount(['users' => function($q) use($id){
            $q->whereHas('apel_presents', function($k) use($id){
                $k->where('activity_id',$id);
            });
        
        }])->having('users_count','>',0)->get();
        
        $present    =   ApelPresent::where('activity_id',$id)->get();
        $data       =   ApelActivity::where('activity_id',$id)->first();
        $user       =   User::all();
         foreach($user as $item)
        {
            $cek     =   ApelPresent::where('activity_id', $id)->where('nip', $item->nip)->first();
            if(!$cek)
            {
                $not_present[] = [
                'name'                 =>   $item->name,
                'nip'                  =>   $item->nip,
                //$not_present->activity_id          =>   $item->activity_id,
                'location'             =>   $item->userLocations->name,

                //$not_present->title                =>   NULL,
                ];
               
            }
        }
        

        $not_present1 = json_encode($not_present);
        return view('activity::apel_present.report',[
            'present'   =>  $present,
            'data'      =>  $data,
             'not_present'=>  $not_present1,
             'bagian'               =>   $bagian,  
        ]);
    
    }
    public function print($id)
    {
        $data       =   ApelActivity::where('activity_id',$id)->first();
        return view('activity::apel_present.print_qr',[
            'data'      =>  $data,
           
        ]);
    }
    public function switch($id)
    {
        try{    
            $activity   =   ApelActivity::findOrfail($id);
            if($activity->status != '0')
            {
                $activity->status   =   '0' ;
            }else{
                $activity->status   =   '1' ;
            }
            $activity->save();
           
            
            return redirect()->back()->with('success_message','Kegiatan Berhasil Dirubah');
        }catch (\Exception $e)
        {
            return redirect()->back()->with('errors_message',$e->getMessage());
        }
    }
    public function export_excel($id)
    {
        $apel   = new ApelPresent;
        $data = $apel->getPeople($id);

        
       

        //$data       =   ApelPresent::with(['users' => function($q){
        //    $q->withCount('userLocations');
        //}])->where('activity_id',$id)->get();
        $apel_activity      =   ApelActivity::all();
        $no = 1;
        foreach($apel_activity as $item)
        {
            $activity_id    =   $item->activity_id;
            $data.$no       =   UserLocation::withCount(['users' => function($q) use($apel_activity){
                $q->whereHas('apel_presents', function($k) use($apel_activity){
                    $k->where('activity_id',$apel_activity);
                });
            }])->get();
            $no++;
        } 
    
       
        return $data;
        //return Excel::download(new ApelExport($id),'Apel_Report.xlsx');
    }
   

    public function destroy($id)
    {
        $data   =   ApelPresent::where('id', $id)->first();
        $data->delete();
        return redirect()->back()->with('success_message','data sukses dihapus');
    }
}
