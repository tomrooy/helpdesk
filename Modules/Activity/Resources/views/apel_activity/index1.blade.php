@extends('layouts.nice.main')
@section('title','Data Kegiatan')
@section('css')
<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@push('nav-activity')show @endpush
@push('main-menu')Kegiatan @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@section('content')
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                <h2><strong>Data Kegiatan Apel di Inspektorat Jenderal</strong></h2>
            </div>    
            <div class="card-body">
                @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif<br>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#apelModal">
                    Tambah Data
                </button>
                    <div class="modal fade" id="apelModal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title">Tambah Data Apel</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div>
                                        <form method="POST" id="createFormApel" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-floating mb-3" id="div_name_" >
                                                <input autocomplete="off" class="form-control" id="name_"  type="text" name="name_" />
                                                <label for="inputNama">Nama Apel </label>
                                                <strong id="name_-error"></strong>
                                            </div>
                                            <div class="form-floating mb-3" id="div_date" >
                                                <input autocomplete="off" class="form-control" id="date_"  type="date" name="date_"  />
                                                <label for="inputTanggal">Tanggal</label>
                                                <strong id="date_-error"></strong>
                                            </div>                                            
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div><!-- End Basic Modal-->
               
                <br><br>
                <div class="table-responsive table-body">
                    <table id="myTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Nama Kegiatan</th>
                                <th>Jadwal Kegiatan</th>
                                <th>Link</th>
                                <th>Peserta</th>
                                <th>Status</th>
                                <th>Qr</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;    
                            @endphp
                            @foreach ($apel_activity as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                @php
                                    $date = \Carbon\Carbon::parse($item->date)->locale('id');
                                    $date->settings(['formatFunction' => 'translatedFormat']);
                                @endphp
                                <td>{{$date->format('l, j F Y')}}</td>
                                <td>Presensi : <a target="_blank" href="{{$item->link}}">{{$item->link}}</a><br>
                                    Rekap : <a target="_blank" href="https://ithelpdesk.itjenkumham.id/presensi-apel/rekap/{{$item->activity_id}}"> https://ithelpdesk.itjenkumham.id/presensi-apel/rekap/{{$item->activity_id}}</a>
                                </td>
                                <td><b>{{$item->apel_presents_count}} Orang</b></td>
                                <td>{!! $item->status == '1'  ? '<span class="badge bg-primary">Aktif</span>' : '<span class="badge bg-danger">TIdak Aktif</span>' !!}</td>
                                <td>{!! QrCode::size(250)->generate($item->link); !!}</td>
                                <td>
                                    <a href="{{route('module.apel_present.switch',$item->id)}}" class="btn btn-warning">Switch</a>
                                    <a href="{{route('module.apel_present.index',$item->activity_id)}}" class="btn btn-success">List</a>
                                    <a href="{{route('module.apel_present.print',$item->activity_id)}}" class="btn btn-info">Print QR</a>
                                </td>
                            </tr>
                            @php
                                $no++;
                            @endphp    
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {        
        $('#myTable').DataTable();
        $('#createFormApel').submit(function(e){
            e.preventDefault();
            let formData = new FormData(this);
            Swal.fire({
            title: 'Apakah Sudah yakin data untuk disimpan?',
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText: 'Simpan',
            denyButtonText: `Jangan Simpan`,
            }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) 
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            }
                    });
                    $.ajax({
                        type: 'POST',
                        url:"{{ route('module.apel.store') }}",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if(data.errors) {
                                swal.fire({ 
                                icon: 'error', 
                                title: 'Mohon cek kembali form terlebih dahulu', 
                                showConfirmButton: true,
                                });
                                if(data.errors.nip_){
                                    $( '#name_-error' ).html( data.errors.name_[0] ).addClass('alert-danger');
                                    $( '#name_' ).addClass('is-invalid');
                                }
                                if(data.errors.name_){
                                    $( '#date_-error' ).html( data.errors.date_[0] ).addClass('alert-danger');
                                    $( '#date_' ).addClass('is-invalid');
                                }
                                
                                }else
                                {
                                    console.log(data);
                                    swal.fire({ 
                                        icon: 'success', 
                                        title: 'Data Berhasil Disimpan', 
                                        showConfirmButton: true,
                                        timer: 1500
                                    }).then(function() {
                                        $('#apelModal').hide();
                                        document.getElementById("createFormApel").reset();
                                        window.location.reload();

                                        });
                                }
                                },
                                error : function(xhr, status, errorThrown)
                                {
                                    //Here the status code can be retrieved like;
                                    xhr.status;
                            
                                    //The message added to Response object in Controller can be retrieved as following.
                                    xhr.responseText;
                                }
                            });
                } else if (result.isDenied) 
                {
                    Swal.fire('Tidak Ada yang Berubah', '', 'info')
                }
            });
        });
    });
</script>
@endsection