@extends('layouts.nice.main')
@section('title','Data Kegiatan')
@section('css')
    <link href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css" rel="stylesheet">
@endsection
@push('nav-activity')show @endpush
@push('main-menu')Kegiatan @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@section('content')
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                <h2><strong>Data Kegiatan di Inspektorat Jenderal</strong></h2>
            </div>    
            <div class="card-body">
                @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <a href="{{route('module.activity.create')}}" class="btn btn-info"><span class="fas fa-plus-square"></span> Tambah Data</a>
                <br><br>
                <div class="table-responsive table-body">
                    <table id="activity-table" class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ID Kegiatan</th>
                                <th>Nama Kegiatan</th>
                                <th>Jadwal Kegiatan</th>
                                <th>Link</th>
                                <!-- <th>Jumlah Peserta</th> -->
                                <th>Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="modal fade" id="edit_activity_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Pengaturan Konfigurasi Presensi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" name="activity" id="flexSwitchCheckfoto">
                                        <label class="form-check-label" for="flexSwitchCheckDefault">Fitur Upload Foto</label>
                                    </div>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" name="activity" id="flexSwitchCheckRekap" >
                                        <label class="form-check-label" for="flexSwitchCheckDefault">Fitur Rekap dengan Pegawai yang belum Presensi</label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-bs-dismiss="modal">Save</button>
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function() {        
        var dataTable = $('#activity-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 10,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('module.activity.data') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'activity_id', name: 'activity_id'},
                {data: 'name', name: 'name'},
                {data: 'jadwal', name: 'jadwal'},
                {data: 'link', name: 'link'},
                //{data: 'count', name: 'count'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action',orderable:false,serachable:false,sClass:'text-center'},
            ],
           
        });
        $('body').on('click', '#settingActivity', function(e) {
            e.preventDefault();
            var btn = document.getElementById("flexSwitchCheckRekap");
            // Setting new attributes
            btn.setAttribute("checked", "");
            $('#edit_activity_modal').modal('show');
        });
        $(document).on('click', '#deleteActivity', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var url_delete = '{{ route("module.activity.destroy", ":id") }}';
            url_delete = url_delete.replace(':id', id);
            swal({
                title: "Hapus?",
                text: "Apakah Yakin Menghapus data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {                    
                $.ajax({
                    type: 'DELETE',
                    url: url_delete,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        },
                    dataType: 'JSON',
                    success: function (data) {
                        if(data.status == true)
                        {
                            swal("Done!", data.message, "success");
                            $('#activity-table').DataTable().ajax.reload();
                        }else
                        {
                            swal("Error!", data.message, "error");
                        }
                        
                    },
                    error : function(xhr, status, errorThrown)
                    {
                         //Here the status code can be retrieved like;
                        xhr.status;
                
                        //The message added to Response object in Controller can be retrieved as following.
                        xhr.responseText;
                    }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
        $(document).on('click', '#switchActivity', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var url_switch = '{{ route("module.activity.switch", ":id") }}';
            url_switch = url_switch.replace(':id', id);
            swal({
                title: "Ganti Status?",
                text: "Apakah Yakin Mengedit status ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {                    
                $.ajax({
                    type: 'POST',
                    url: url_switch,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        },
                    dataType: 'JSON',
                    success: function (data) {
                        if(data.status == true)
                        {
                            swal("Done!", data.message, "success");
                            $('#activity-table').DataTable().ajax.reload();
                        }else
                        {
                            swal("Error!", data.message, "error");
                        }
                        
                    },
                    error : function(xhr, status, errorThrown)
                    {
                         //Here the status code can be retrieved like;
                        xhr.status;
                
                        //The message added to Response object in Controller can be retrieved as following.
                        xhr.responseText;
                    }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });


    });
    
    
</script>
@endsection