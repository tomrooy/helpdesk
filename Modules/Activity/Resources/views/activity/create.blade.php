@extends('layouts.nice.main')
@section('title','Tambah Data Kegiatan')
@section('css')

@endsection
@push('main-menu')Kegiatan @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Tambah @endpush
@section('content')
<div class="card">
    <div class="card-header">
        Form Pembuatan Link Absensi
    </div>
    <div class="card-body">
        @if(session('errors_message'))
            <div class="alert alert-danger btn-sm alert-small" >
                {{session('errors_message')}}
            </div> 
        @endif
        <br>
        <form method="POST" action="{{route('module.activity.store')}}" enctype="multipart/form-data" class="row g-3">
            @csrf             
            <div class="col-md-12">
                <div class="form-floating">
                    <input type="text" name="name" value="{{old('name')}}" class="form-control {{$errors->first('name') ? 'is-invalid': ''}}" placeholder="Silahkan Isi Nama Agenda Meeting">
                    <label>Nama Agenda Kegiatan</label>
                    <div class="invalid-feedback">
                        {{$errors->first('name')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <select name="request_by" class="form-control {{$errors->first('request_by') ? "is-invalid" : ""}}">
                        <option value="">--Diajukan Oleh--</option>
                        @foreach($userlocation as $item)
                            <option value="{{$item->location_id}}" @if(old('request_by') == '{{$item->location_id}}') {{'selected'}} @endif>{{$item->name}}</option>
                        @endforeach                            
                    </select>
                    <label>Yang Ajukan Permintaan</label>
                    <div class="invalid-feedback">
                        {{$errors->first('request_by')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <select name="category" class="form-control {{$errors->first('category') ? "is-invalid" : ""}}">
                        <option value="">--kategory meeting --</option>
                        <option value="Rapat Kordinasi" @if(old('category') == 'Rapat Kordinasi') {{'selected'}} @endif>Rapat Kordinasi</option>
                        <option value="Rapat Internal" @if(old('category') == 'Rapat Internal') {{'selected'}} @endif>Rapat Internal</option>
                        <option value="Acara Workshop" @if(old('category') == 'Acara Workshop') {{'selected'}} @endif>Acara Workshop</option>
                        <option value="Acara Gemar Belajar" @if(old('category') == 'Acara Gemar Belajar') {{'selected'}} @endif>Acara Gemar Belajar</option>
                    </select>
                    <label>Kategory Meeting</label>
                    <div class="invalid-feedback">
                        {{$errors->first('category')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <select name="subcategory" class="form-control {{$errors->first('subcategory') ? "is-invalid" : ""}}">
                        <option value="">--Peserta meeting --</option>
                        <option value="itjen" @if(old('subcategory') == 'itjen') {{'selected'}} @endif>Pegawai Itjen</option>
                        <option value="kumham" @if(old('subcategory') == 'kumham') {{'selected'}} @endif>Pegawai Kumham</option>
                        <option value="umum" @if(old('umum') == 'umum') {{'selected'}} @endif>UMUM</option>
                    </select>
                    <label>Kategory Meeting</label>
                    <div class="invalid-feedback">
                        {{$errors->first('subcategory')}}
                    </div>
                </div>
            </div>
                             
            <div class="col-md-4">
                <div class="form-floating">
                    <input autocomplete="off" type="date" name="date" value="{{old('date')}}" class=" form-control {{$errors->first('date') ? 'is-invalid': ''}}" >
                    <label>Tanggal Agenda Meeting</label>
                    <div class="invalid-feedback">
                        {{$errors->first('date')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <select name="start" id="start" class="form-control {{$errors->first('start') ? 'is-invalid': ''}}">
                        <option value="">Pilih Jam</option>
                        @foreach($time as $item)
                            <option value="{{$item->name}}" @if(old('start') == '{{$item->name}}') 'selected' @endif>{{$item->name}}</option>
                        @endforeach
                    </select>
                    <label>Jam Mulai</label>
                    <div class="invalid-feedback">
                        {{$errors->first('date')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <select name="end" id="end" class="form-control {{$errors->first('end') ? 'is-invalid': ''}}">
                        <option value="">Pilih Jam</option>
                        @foreach($time as $item)
                            <option value="{{$item->name}}" @if(old('end') == '{{$item->name}}') 'selected' @endif>{{$item->name}}</option>
                        @endforeach
                    </select>
                    <label>Jam Berakhir</label>
                    <div class="invalid-feedback">
                        {{$errors->first('end')}}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-floating">
                    <input type="file" name="attachment"  class="form-control {{$errors->first('attachment') ? 'is-invalid': ''}}" >
                    <label>File Nodin (opsional)</label>
                    <div class="invalid-feedback">
                        {{$errors->first('attachment')}}
                    </div>
                    *format yang diterima adalah PDF max : 1 MB
                </div>
            </div>                
            <div class="col text-center">
                <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Simpan</button>
                <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
            </div>
           
        </form>
    </div>
</div>
@endsection
@section('js')
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
@endsection