@extends('layouts.nice.main')
@section('title','Data Kegiatan')
@section('css')
<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@push('nav-activity')show @endpush
@push('main-menu')Kegiatan @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@section('content')
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                <h2><strong>Data Kegiatan di Inspektorat Jenderal</strong></h2>
            </div>    
            <div class="card-body">
                @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <a href="{{route('module.activity.create')}}" class="btn btn-info"><span class="fas fa-plus-square"></span> Tambah Data</a>
                <br><br>
                <div class="table-responsive table-body">
                    <table id="myTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ID Kegiatan</th>
                                <th>Nama Kegiatan</th>
                                <th>Jadwal Kegiatan</th>
                                <th>Link</th>
                                <th>Jumlah Peserta</th>
                                <th>Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;    
                            @endphp
                            @foreach ($activity as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->activity_id}}</td>
                                <td>{{$item->name}}</td>
                                @php
                                    $date = \Carbon\Carbon::parse($item->date)->locale('id');
                                    $date->settings(['formatFunction' => 'translatedFormat']);
                                @endphp
                                <td>{{$date->format('l, j F Y')}} {{ $item->start. '-'. $item->end }}</td>
                                <td>Presensi : <a target="_blank" href="{{$item->url}}">{{$item->url}}</a><br>
                                    Rekap : <a target="_blank" href="https://ithelpdesk.itjenkumham.id/presensi-rekap/{{$item->activity_id}}"> https://ithelpdesk.itjenkumham.id/presensi-rekap/{{$item->activity_id}}</a>
                                </td>
                                <td>{{$item->attendances_count}} Orang</td>
                                <td>{!! $item->status == '1'  ? '<span class="badge bg-primary">Aktif</span>' : '<span class="badge bg-danger">TIdak Aktif</span>' !!}</td>
                                <td>
                                    <a href="{{route('module.activity.attendance', $item->activity_id)}}" class="btn btn-info btn-sm"><span class="bx bx-detail"></span></a>
                                    <a href="{{route('module.activity.switch', $item->id)}}" class="btn btn-warning btn-sm"><span class="bi bi-toggle2-off"></span> </a>
                                    <a href="javascript:void(0)" onclick="$(this).find('form').submit()" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i>
                                        <form method="POST" action=""  onsubmit="return confirm('Apakah Yakin Menghapus Kegiatan Berikut?')">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </a>
                                    <button  id="edit_activity" data-rekap="{{$item->photo_upload}}" data-rekap="{{$item->alpha_check}}" data-id="{{$item->activity_id}}" href="#" class="btn btn-warning btn-sm"><span class="bx bxs-cog"></span> </button>
                                    <div class="modal fade" id="edit_activity_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Pengaturan Konfigurasi Presensi</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-check form-switch">
                                                        <input class="form-check-input" type="checkbox" name="activity" id="flexSwitchCheckfoto">
                                                        <label class="form-check-label" for="flexSwitchCheckDefault">Fitur Upload Foto</label>
                                                    </div>
                                                    <div class="form-check form-switch">
                                                        <input class="form-check-input" type="checkbox" name="activity" id="flexSwitchCheckRekap" >
                                                        <label class="form-check-label" for="flexSwitchCheckDefault">Fitur Rekap dengan Pegawai yang belum Presensi</label>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" data-bs-dismiss="modal">Save</button>
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @php
                                $no++;
                            @endphp    
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {        
        $('#myTable').DataTable();
        $('body').on('click', '#edit_activity', function(e) {
            e.preventDefault();
            var btn = document.getElementById("flexSwitchCheckRekap");
            // Setting new attributes
            btn.setAttribute("checked", "");
            $('#edit_activity_modal').modal('show');
        });
    });
    
    
</script>
@endsection