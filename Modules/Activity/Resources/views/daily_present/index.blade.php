@extends('layouts.nice.main')
@section('title','Data Kegiatan')
@section('css')
<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@push('main-menu')List Absensi @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@push('nav-activity')show @endpush
@section('content')
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                <h2>Data Daftar Hadir Harian Pada Substansi Humas dan SIP</h2>
            </div>    
            <div class="card-body">
                <br>
                @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <a href="#" class="btn btn-info"><span class="fas fa-plus-square"></span> Tambah Data</a>
                <br><br>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th>Nama/NIP</th>
                                <th>Jabatan</th>
                                <th>Status</th>                               
                                <th>Tanda Tangan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;    
                            @endphp
                            @foreach ($data as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->date}}</td>
                                <td>{!!$item->name .'<br>'. $item->nip!!}</td>
                                
                                <td>{{$item->title}}</td>
                                <td>{{$item->daily_present_status->name}}</td>
                                <td><img src="{{asset('storage/uploads/present_signature/'. $item->signature)}}" width="200px" height="100px"></td>
                                <td><button type="button" class="btn btn-success btn-sm" id="getEditData" data-id="{{$item->id}}" ><i class="bi bi-pencil"></i> Edit</button></td>
                            </tr>
                            @php
                                $no++;
                            @endphp    
                            @endforeach
                        </tbody>
                    </table>
                    <div class="modal" id="editPresentData">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Update Data Presensi Harian : <span id="nama_modal"></span></h4>
                                   
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                        <div>
                                            <form method="POST" id="updateFormPresent" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" id="id_daily" name="id">
                                                <div class="form-floating mb-3" id="div_date" >
                                                    <input autocomplete="off" class="form-control" id="date"  type="text" name="date" disabled  />
                                                    <label for="inputNoLhp">Tanggal </label>
                                                    <strong id="date-error"></strong>
                                                </div>
                                                <div class="form-floating mb-3" id="div_status_daily_">
                                                    <select class="form-select" id="status_daily_" aria-label="State" name="status_daily_">
                                                    <option value="">Pilih Status</option>
                                                    @foreach($status as $item)
                                                        <option value="{{$item->status_id}}">{{$item->name}}</option>
                                                    @endforeach
                                                    </select>
                                                    <label for="floatingSelect">Pilih Status</label>
                                                </div>
                                                
                                        </div>
                                </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success" >Update</button>
                                        <button type="button" class="btn btn-danger " id="modelClose">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {        
        $('#myTable').DataTable();
        $('body').on('click', '#getEditData', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            var url = '{{ route("module.daily_present.edit", ":id") }}';
            url = url.replace(':id', id)
            $.ajax({
                url: url,
                method: 'GET',
                success: function(data) {
                    if(data.status == true)
                    {
                        $('#nama_modal').html(data.daily.name);
                        //document.getElementById('id_riksus_').value    =   id;
                        document.getElementById('date').value    =   data.daily.date;
                        document.getElementById('id_daily').value    =   data.daily.id;
                        $('#editPresentData').show();
                    }else
                    {
                        swal("Error!", "Mohon Hubungi Pihak IT", "error");
                    }
                }
            })
            
        });
        $('#modelClose').on('click', function(){
            $('#editPresentData').hide();
            document.getElementById("updatePresent").reset();
        });
        $('#updateFormPresent').submit(function(e){
            e.preventDefault();
            let formData = new FormData(this);
            Swal.fire({
            title: 'Apakah Sudah yakin data untuk disimpan?',
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText: 'Simpan',
            denyButtonText: `Jangan Simpan`,
            }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) 
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            }
                    });
                    $.ajax({
                        type: 'POST',
                        url:"{{ route('module.daily_present.update') }}",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if(data.errors) {
                                swal.fire({ 
                                icon: 'error', 
                                title: 'Mohon cek kembali form terlebih dahulu', 
                                showConfirmButton: true,
                                });
                                if(data.errors.date){
                                    $( '#status-error' ).html( data.errors.status[0] ).addClass('alert-danger');
                                    $( '#status' ).addClass('is-invalid');
                                }
                                }else
                                {
                                    swal.fire({ 
                                        icon: 'success', 
                                        title: 'Data Berhasil Disimpan', 
                                        showConfirmButton: true,
                                        timer: 1500
                                    }).then(function() {
                                        window.location.reload();
                                        $('#editPresentData').hide();
                                        document.getElementById("updateFormPresent").reset();

                                        //$('#riksus-table').DataTable().ajax.reload();
                                        });
                                }
                                },
                                error : function(xhr, status, errorThrown)
                                {
                                    //Here the status code can be retrieved like;
                                    xhr.status;
                            
                                    //The message added to Response object in Controller can be retrieved as following.
                                    xhr.responseText;
                                }
                            });
                } else if (result.isDenied) 
                {
                    Swal.fire('Tidak Ada yang Berubah', '', 'info')
                }
            });
        });
    });
    
</script>
@endsection