<!DOCTYPE html>
<html>
<head>
    <title>Rekap Presensi Kegiatan Virtual</title>
</head>
<body>
    <h1>{{$title }}</h1>
    <p>{{ $date }}</p>
    <table>
        <thead> 
            <th>NO</th>
            <th>Nama</th>
            <th>NIP</th>
            <th>Unit Kerja</th>
            <th>Tanda Tangan</th>
        </thead>
        <tbody>
            @php 
                $no= 1;
         
           
            @endphp
                @foreach($attendance as $items)
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$items->name}}</td>
                        <td>{{$items->nip}}</td>
                        <td>{{strtok($items->location, '-')}}</td>
                        <td><img src="{{asset('storage/uploads/signature/'. $items->signature)}}" width="150px" height="75px"></td></td>
                        @php
                            $no++;
                        @endphp
                    </tr>
                @endforeach                    
        </tbody>
    </table>
</body>
</html>