@extends('layouts.nice.main')
@section('title','Data Kegiatan')
@section('css')
<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@push('main-menu')List Absensi @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@push('nav-activity')show @endpush
@section('content')
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                <h2>Data Absen Pada {{$name->name}}</h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <br>
                @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <a href="#" class="btn btn-info"><span class="fas fa-plus-square"></span> Tambah Data</a>
                <a target="_blank" href="{{route('report.attendance', $name->activity_id)}}" class="btn btn-success"><span class="bx bx-print"></span> Cetak Data Presensi</a>
                <a target="_blank" href="{{route('module.activity.download', $name->activity_id)}}" class="btn btn-info"><span class="fas fa-print"></span> Cetak PDF</a>
                <a href="{{route('report_excel.attendance', $name->activity_id)}}" class="btn btn-success"><span class="fas fa-excel"></span> Export Data Presensi</a>
                <br><br>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Nip</th>
                                <th>Unit Kerja</th>
                                <th>Jabatan</th>
                                <th>Tanda Tangan</th>
                                <th class="text-center" width="80px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;    
                            @endphp
                            @foreach ($attendance as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->nip}}</td>
                                <td>{{$item->location}}</td>
                                <td>{{$item->title}}</td>
                                <td><img src="{{asset('storage/uploads/signature/'. $item->signature)}}" width="200px" height="100px"></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="$(this).find('form').submit()" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i> Hapus
                                        <form method="POST" action="{{route('module.attendance.delete',$item->id)}}"  onsubmit="return confirm('Hapus data absen tersebut?')">
                                            @csrf @method('DELETE')
                                        </form>
                                    </a>
                                </td>
                            </tr>
                            @php
                                $no++;
                            @endphp    
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {        
        $('#myTable').DataTable();
    });
    
</script>
@endsection