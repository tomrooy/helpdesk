<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Absensi Virtual</title>
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css" rel="stylesheet"> 
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
            div.wrapper 
            {
                  background-color: #999;
                  border: 1px solid #333;
                  float: left;
                  margin: 10px;
                  width: 200px;
                  height: 250px;
            }
            
        </style>
    </head>
    <body class="bg-custom" onload="window.print()">
    <div class="card">
        <div class="card-header">
            Rekap Laporan Absen Yang hadir di Lapangan
        </div>
        <div class="card-body">
            @php
                $date = \Carbon\Carbon::parse($data->date)->locale('id');
                $date->settings(['formatFunction' => 'translatedFormat']);
            @endphp
            <h5>{{$data->name}}</h5> {{$date->format('l, j F Y')}} 
            <table class="table table-responsive-md">
                <thead>
                    <th>NO</th>
                    <th>Bagian/Inspektorat</th>
                    <th>Jumlah Pegawai yang hadir</th>
                </thead>
                <tbody>
                    @php 
                        $no1= 1;
                    @endphp
                    @foreach($bagian as $items)
                        <tr>
                            <td>{{$no1}}</td>
                            <td>{{$items->name}}</td>
                            <td>{{$items->users_count}}</td>
                        </tr>
                        @php
                            $no1++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
            <table class="table table-responsive-md">
                <thead> 
                    <th>NO</th>
                    <th width="300px">Nama</th>
                    <th>NIP</th>
                    <th width="300px">Unit Kerja</th>
                    <th width="300px">Jabatan</th>
                    <th>Tanda Tangan</th>
                </thead>
                <tbody>
                    @php 
                            $no= 1;
                        @endphp
                        @foreach($present as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->nip}}</td>
                                <td>{{$item->location}}</td>
                                <td>{{$item->title}}</td>
                                <td><img src="{{asset('storage/uploads/apel/signature/'. $item->signature)}}" width="200px" height="100px"></td>
                                @php
                                    $no++;
                                @endphp
                            </tr>
                        @endforeach                    
                </tbody>
            </table>
            <br>
                    
        </div>
    </div>
        
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script>
    const images = document.querySelectorAll('img');

        Array.from(images).forEach(image => {
          image.addEventListener('load', () => fitImage(image));
          
          if (image.complete && image.naturalWidth !== 0)
            fitImage(image);
        });

        function fitImage(image) {
          const aspectRatio = image.naturalWidth / image.naturalHeight;
          
          // If image is landscape
          if (aspectRatio > 1) {
            image.style.width = '50%';
            image.style.height = 'auto';
          }
          
          // If image is portrait
          else if (aspectRatio < 1) {
            image.style.width = 'auto';
            image.style.maxHeight = '50%';
          }
          
          // Otherwise, image is square
          else {
            image.style.maxWidth = '50%';
            image.style.height = 'auto';
          }
        }
</script>


    </body>
    </html>
