@extends('layouts.nice.main')
@section('title','Data Kegiatan')
@section('css')
<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@push('main-menu')List Absensi @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@push('nav-activity')show @endpush
@section('content')
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                <h2>Data Daftar Hadir Apel</h2>
            </div>    
            <div class="card-body">
                @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
                <br>
                <a href="#" class="btn btn-info"><span class="fas fa-plus-square"></span> Tambah Data</a>
                <a href="{{route('module.apel_present.rekap',$id)}}" class="btn btn-info"><span class="fas fa-print"></span> Print Data</a>
                <a href="{{route('module.apel_present.export_excel',$id)}}" class="btn btn-info"><span class="fas fa-print"></span> Export Data</a>
                <br><br>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tanggal</th>
                                <th>Nama/NIP</th>
                                <th>Jabatan</th>        
                                <th>Unit Kerja</th> 
                                <th>Tanda Tangan</th>
                                <th width="80px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;    
                            @endphp
                            @foreach ($present as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->date}}</td>
                                <td>{!!$item->name .'<br>'. $item->nip!!}</td>
                                <td>{{$item->title}}</td>
                                <td>{{$item->location}}</td>
                                <td><img src="{{asset('storage/uploads/apel/signature/'. $item->signature)}}" width="200px" height="100px"></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="$(this).find('form').submit()" class="btn btn-danger btn-sm"><i class="bi bi-trash"></i> Hapus<form method="POST" action="{{route('module.apel_present.delete',$item->id)}}"  onsubmit="return confirm('Hapus data absen tersebut?')">@csrf @method('DELETE')</form></a>
                                </td>
                            </tr>
                            @php
                                $no++;
                            @endphp    
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {        
        $('#myTable').DataTable({
            "columnDefs": [
            { "width": "5%", "targets": 1 }
            ]
        });
    });
    
</script>
@endsection