<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Absensi Virtual</title>
        <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
        </style>
    </head>
    <body class="bg-custom" >
        <div class="text-center">
            <h2 class="text-center">QR Absensi</h2>
            <input type="hidden" value="{{$data->id}}" id="apel_id">
            <img src="{{asset('kumham.png')}}" class="my-2" height="150px" weight="150px" class="img-fluid" alt="Responsive image">
            @php
            $date = \Carbon\Carbon::parse($data->date)->locale('id');
            $date->settings(['formatFunction' => 'translatedFormat']);
            @endphp
            <h4>{{$data->name}}</h4> {{$date->format('l, j F Y')}}</div>
            <br><br>
            <div class="text-center">
               {!! QrCode::size(800)->generate("https://ithelpdesk.itjenkumham.id/presensi-apel/".$data->link); !!}
            </div>
            <div id="carasingkat" class="text-center">
                
            </div>
        </div>
        <br><br>
        <div class="card-footer text-center py-3">
            <div class="text-muted">Tim Pengelola Teknologi Informasi, Inspektorat Jenderal  2022</div>
        </div>

<script src="{{asset('nice/assets/js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    var id_apel     =   document.getElementById("apel_id").value;
    var count = 60; // dalam detik
        function countDown() {
            if (count > 0) {
                    count--;
                    var waktu = count + 1;
                    $('#carasingkat').html('<h2>QR Ini Akan Otomatis Di Refresh  </h2> dalam ' + waktu + ' detik.');
                    setTimeout("countDown()", 1000);
                }else
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            }
                        });
                    $.ajax({ 
                        type: 'POST',
                        url:"{{ route('module.apel.change.link') }}",
                        data:{ 
                                "_token": "{{ csrf_token() }}",
                                "id": id_apel 
                            },
                        success: function(data) {
                            window.location.reload()
                            }, 
                            complete: function () { 
                            // clearTimeout(sTimeOut); 
                            } 
                        }); 
                }
        }
    countDown();

</script>
</body>
</html>
