<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Absensi Virtual</title>
        <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
        </style>
    </head>
    <body class="bg-custom" onload="window.print()" >
        <div class="text-center">
            <h2 class="text-center">QR Absensi</h2>
            <img src="{{asset('kumham.png')}}" class="my-2" height="150px" weight="150px" class="img-fluid" alt="Responsive image">
            @php
            $date = \Carbon\Carbon::parse($data->date)->locale('id');
            $date->settings(['formatFunction' => 'translatedFormat']);
            @endphp
            <h4>{{$data->name}}</h4> {{$date->format('l, j F Y')}}</div>
            <br><br>
            <div class="text-center">
               {!! QrCode::size(800)->generate($data->link); !!}
            </div>
        </div>
        <br><br>
        <div class="card-footer text-center py-3">
            <div class="text-muted">Tim Pengelola Teknologi Informasi, Inspektorat Jenderal  2022</div>
        </div>

<script src="{{asset('nice/assets/js/main.js')}}"></script>
</body>
</html>
