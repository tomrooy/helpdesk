@extends('layouts.nice.main')
@section('title','Index category')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
@push('nav-helpdesk')show @endpush
@push('main-menu')Category @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush    
<div class="wrapper">    
    <div class="card">
        <div class="card-header bg-primary text-white">
            DATA CATEGORY
        </div>
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            <button data-bs-toggle="modal" data-bs-target="#addCategory" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</button>
            <button type="button" id="delete_record" class="btn btn-danger"><span class="fa fa-trash"></span> Hapus Data</button>
            <br><br>
            <div class="table-responsive">
                <table id="category-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="30"> 
                              <input type="checkbox" name="checkAll" id="checkall" > 
                             </th>
                            <th>#</th>
                            <th width="200">Category ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th width="200" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            @include('helpdesk::category.modal')
        </div>
    </div>
</div>
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
@include('helpdesk::category.javascript')
@endsection