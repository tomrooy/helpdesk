@extends('layouts.nice.main')
@section('title','Subcategory Main-Page')
@push('nav-helpdesk')show @endpush
@section('content')
    <div class="wrapper">
    @push('main-menu')SubCategory @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Index @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Data SubCategory</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <br>
                @include('helpdesk::subcategory.modal')
                <button data-bs-toggle="modal" data-bs-target="#addSubCategory" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</button>
                <br><br>
                <div class="table-responsive">
                    <table id="subcategory-table" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Sub Category ID</th>
                                <th>Name</th>
                                <th>Category ID</th>
                                <th>Description</th>
                                <th width="150" class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
@include('helpdesk::subcategory.javascript')
@endsection