<div class="modal fade" id="addSubCategory">
    <div class="modal-dialog" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Jabatan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <form method="POST" id="formSubCategory" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="id">Category ID</label>
                        <select name="category" class="form-control" id="category">
                            @foreach($category as $item)
                            <option value="{{$item->category_id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <strong id="id-error"></strong>
                    </div>
                    <div class="form-group">
                        <label for="nama">Subcategory ID</label>
                        <input autocomplete="off" type="text" class="form-control" name="subcategory" placeholder="Silahkan Isi Nama">
                        <strong id="name-error"></strong>
                    </div>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input autocomplete="off" type="text" class="form-control" name="name" placeholder="Silahkan Isi Nama">
                        <strong id="name-error"></strong>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                            <textarea autocomplete="off" type="text" class="form-control {{$errors->first('description') ? "is-invalid" : ""}} " name="description" placeholder="Silahkan Isi deskripsi singkat">{{old('description')}}</textarea>
                            <strong id="description-error"></strong>       
                    </div>
                    
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> Simpan</button>
                <button type="reset" class="btn btn-warning"><span class="fa fa-recycle"></span> Reset</button>
            </div>
                </form>
        </div>
    </div>
</div>

<div class="modal" id="editSubCategoryData">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit SubCategory <span id="namaCategory"></span></h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div id="EditArticleModalBody">
                        <form method="POST" id="editFormSubCategory" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="id">Category ID</label>
                                <select name="category_id" class="form-control" id="formEditCategoryId">
                                </select>
                                <strong id="id-error"></strong>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id" id="idSubCategory">
                                <label for="id">SubCatagory ID</label>
                                <input autocomplete="off" type="text" class="form-control" id="formEditSubCategoryId" name="subcategory_id" placeholder="Silahkan Isi Id">
                                <strong id="id-error"></strong>
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input autocomplete="off" id="formEditNama" type="text" class="form-control" name="name" placeholder="Silahkan Isi Nama">
                                <strong id="name-error"></strong>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                    <textarea autocomplete="off" type="text" class="form-control {{$errors->first('description') ? "is-invalid" : ""}} " name="description" placeholder="Silanhkan isi Deskripsi singkat" id="formEditDescription"></textarea>
                                    <strong id="description-error"></strong>       
                            </div>
                    </div>
            </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" >Update</button>
                    <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>