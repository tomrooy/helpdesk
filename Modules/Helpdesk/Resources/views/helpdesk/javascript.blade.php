<script>  
    $(document).ready(function() {
        var dataTable = $('#helpdesk-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 10,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('module.helpdesk.data') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'ticket_number', name: 'ticket_number'},
                {data: 'date', name: 'date'},
                {data: 'title', name: 'title'},
                {data: 'request_by', name: 'request_by'},
                {data: 'handle_by', name: 'handle_by'},
                {data: 'status', name: 'status'},
                {data: 'checked', name : 'checked',orderable:false,serachable:false},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });
        $('#checkall').click(function(){
            if($(this).is(':checked')){
               $('.delete_check').prop('checked', true);
            }else{
               $('.delete_check').prop('checked', false);
            }
         });
         $(document).on('click', '#delete_record', function (e) {
            e.preventDefault();
            var deleteids_arr = [];
            // Read all checked checkboxes
            $("input:checkbox[class=delete_check]:checked").each(function () {
               deleteids_arr.push($(this).val());
            });
            if(deleteids_arr.length > 0){
                swal({
                    title: "Hapus?",
                    text: "Apakah Yakin Menghapus data yang terpilih ?",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak!",
                    reverseButtons: false
                }).then(function (e) {
                    if (e.value === true) { 
                        var helpdesk    = deleteids_arr;
                        $.ajax({
                                type: 'DELETE',
                                url: '{{ route('module.helpdesk.delete.all') }}',
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                data: {deleteids_arr: helpdesk},
                                success: function(data){
                                    if(data)
                                    {
                                        swal("Berhasil!", data.message, "success");
                                        $('#helpdesk-table').DataTable().ajax.reload();
                                    }else
                                    {
                                        swal("Error!", data.message, "error");
                                    }
                            }
                        });
                    }else
                    {
                        swal("Pemberitahuan!","Sukses dibatalkan", "success");
                    }
                });
             }else{
                 alert('pilih data');
             }
            
         });
        $(document).on('click', '#deleteHelpdesk', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: "Hapus?",
                text: "Apakah Yakin Menghapus data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {                    
                $.ajax({
                    type: 'POST',
                    url: 'helpdesk/'+id+'/delete',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        },
                    dataType: 'JSON',
                    success: function (data) {
                        if(data.status == true)
                        {
                            swal("Done!", data.message, "success");
                            $('#helpdesk-table').DataTable().ajax.reload();
                        }else
                        {
                            swal("Error!", data.message, "error");
                        }
                        
                    },
                    error : function(xhr, status, errorThrown)
                    {
                         //Here the status code can be retrieved like;
                        xhr.status;
                
                        //The message added to Response object in Controller can be retrieved as following.
                        xhr.responseText;
                    }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });

    });
</script>