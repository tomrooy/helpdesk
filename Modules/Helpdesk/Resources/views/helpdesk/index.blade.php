@extends('layouts.nice.main')
@section('title','Data Helpdesk')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@push('nav-helpdesk')show @endpush
@section('content')
<div class="wrapper">
    @push('main-menu')Helpdesk @endpush
    @push('url-main-menu') {{route('module.helpdesk.index')}} @endpush
    @push('sub-menu')Index @endpush       
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data Permintaan Helpdesk
        </div>
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            @if(session('success_message'))
                    <div class="alert alert-success btn-sm alert-small" >
                        {{session('success_message')}}
                    </div> 
                @endif
                @if(session('errors_message'))
                    <div class="alert alert-danger btn-sm alert-small" >
                        {{session('errors_message')}}
                    </div> 
                @endif
            <a href="{{route('module.helpdesk.create')}}" class="btn btn-info"><span class="fa fa-plus"></span> Input Helpdesk Admin</a>
            <button type="button" id="delete_record" class="btn btn-danger float-right"><span class="fa fa-trash"></span> Hapus Data</button>
            <br><br>
            <div class="table-responsive">
                <table id="helpdesk-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="30"> 
                               #
                            </th>
                            <th width="90">Ticket</th>
                            <th width="109">Tanggal</th>
                            <th>Permasalahan</th>
                            <th>Direquest Oleh</th>
                            <th>Dikerjakan Oleh</th>
                            <th>Status</th>
                            <th>Select <input type="checkbox" name="checkAll" id="checkall" > </th>
                            <th width="150" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
@include('helpdesk::helpdesk.javascript')

@endsection