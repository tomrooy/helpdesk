@extends('layouts.nice.main')
@section('title','Input Helpdesk Baru')
@section('css')
    <!-- <link href="{{asset('nice/vendor/wysiwyg/editor.css')}}" type="text/css" rel="stylesheet"/> -->
@endsection
@section('content')
@push('main-menu')Helpdesk @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Input Data @endpush   
<div class="card shadow mb-12">
    <div class="card-header py-3 bg-primary">
        <h6 class="m-0 font-weight-bold text-light">Form Layanan Helpedesk IT</h6>
    </div>      
    <div class="card-body">
        <br>
        <form method="POST" action="{{route('module.helpdesk.store')}}" enctype="multipart/form-data" class="row g-3">
            @csrf
            <div class="col-md-4">
                <div  id="option-name" class="form-floating">
                    <input autocomplete="off" type="text" name="name" id="name" value="{{old('name')}}" class="form-control {{$errors->first('name') ? "is-invalid" : ""}}" placeholder="Silahkan Isi Nama" >
                    <div id="name_list"></div>
                    <label for="id">Nama</label>
                    <div class="invalid-feedback">
                        {{$errors->first('name')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <input autocomplete="off" type="text" name="nip" id="nip" value="{{old('nip')}}"  class="form-control {{$errors->first('nip') ? "is-invalid" : ""}}" placeholder="NIP">
                    <label for="nip">NIP</label>
                    <div class="invalid-feedback">
                        {{$errors->first('nip')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <input autocomplete="off" type="text" name="location" id="location" value="{{old('location')}}" class="form-control {{$errors->first('location') ? "is-invalid" : ""}}" placeholder="Bagian/Wilayah">
                    <label for="id">Bagian/Wilayah</label>
                    <div class="invalid-feedback">
                        {{$errors->first('location')}}
                    </div>
                </div>
            </div>            
            <div class="form-floating">
                <input autocomplete="off" type="text" id="subject" class="form-control" value="{{old('subject')}}" name="subject" >
                <label for="subject">Judul</label>
                <div class="invalid-feedback">
                    {{$errors->first('subject')}}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <select required name="category" id="category"  class="form-control select2 {{$errors->first('category') ? "is-invalid" : ""}}">
                        <option value="">Pilih Category</option>
                            @foreach ($category as $item)
                        <option value="{{$item->category_id}}" @if(old('category') == $item->category_id) {{'selected'}} @endif>{{$item->name}}</option>
                            @endforeach
                    </select>
                    <label>Pilih Kategory</label>
                    <div class="invalid-feedback">
                        {{$errors->first('category')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <select class="form-control {{$errors->first('subcategory') ? "is-invalid" : ""}}" name="subcategory" id="subcategory">
                        @if(old('subcategory'))
                            @foreach ($subcategory as $item)
                                @if($item->category_id == old('category'))
                                    <option value="{{$item->subcategory_id}}" @if(old('subcategory') == $item->subcategory_id) {{'selected'}} @endif>{{$item->name}}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                    <label>SubKategory</label>
                    <div class="invalid-feedback">
                        {{$errors->first('subcategory')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <select required name="priority" id="priority"  class="form-control select2 {{$errors->first('priority') ? "is-invalid" : ""}}">
                        <option value="">Pilih Prioritas</option>
                        @foreach ($priority as $item)
                            <option value="{{$item->priority_id}}" @if(old('priority') == $item->priority_id ) {{'selected'}} @endif>{{$item->name}}</option>
                        @endforeach
                    </select>
                    <label>Prioritas</label>
                    <div class="invalid-feedback">
                        {{$errors->first('priority')}}
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                
                <div class="form-floating">
                    <textarea autocomplete="off" id="description" rows="5" type="text" class="form-control {{$errors->first('description') ? "is-invalid" : ""}} " name="description" style="height: 100px;">{{old('description')}}</textarea>
                    <label for="description">Jelaskan Secara Detail Disini</label>
                    <div class="invalid-feedback">
                        {{$errors->first('description')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <input required autocomplete="off" type="number" id="user_wa" class="form-control" value="{{old('user_wa')}}" name="user_wa" >
                    <label for="user_wa">User Wa </label>
                    <div class="text-success">*contoh penulisan nomor : 081374757586</div>
                    <div class="invalid-feedback">
                        {{$errors->first('user_wa')}}
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-floating">
                    <input autocomplete="off" type="date" id="date" class="form-control" value="{{old('date')}}" name="date" >
                    <label for="subject">Tanggal</label>
                    <div class="invalid-feedback">
                        {{$errors->first('date')}}
                    </div>
                </div>    
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <input type="file" name="img1" autocomplete="off" type="text"  class="form-control {{$errors->first('img1') ? "is-invalid" : ""}}" >
                    <label for="img1">Attach Image 1</label>
                    <div class="text-danger">*Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                    <div class="invalid-feedback">
                        {{$errors->first('img1')}}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                    <input type="file" name="img2" autocomplete="off" type="text"  class="form-control {{$errors->first('img2') ? "is-invalid" : ""}}" >
                    <label for="img2">Attach Image 2</label>
                    <div class="text-danger">*Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                    <div class="invalid-feedback">
                        {{$errors->first('img2')}}
                    </div>
                </div>
            </div>            
            <br>
            
            <div class="col text-center">
                <button type="submit" class="btn btn-info"><i class="bx bx-save"></i> Simpan</button>
                <button type="reset" class="btn btn-warning"><i class="bx bx-reset"></i> Reset</button>
            </div>
        </form>
    </div>
    
</div>
@endsection
@section('js')

<script>
    $(document).ready(function() {
        @include('layouts.nice.partial.clock')
        $('select[name="category"]').on('change', function(){
            var title_id = $(this).val();
            var url =   '{{ route("getsubcategory", ":id") }}';
                if(title_id){
                    $.ajax({
                        //url: '/getsubcategory/'+title_id,
                        url : url.replace(':id', title_id),
                        type : 'GET',
                        dataType : 'Json',
                        success : function(data){
                            $('select[name="subcategory"]').empty();
                            $.each(data, function(key, values)
                            {
                                $('select[name="subcategory"]').append('<option value="'+values+'">'+key+'</option>');
                                
                            });
                        }
                    });
                    
                }
        });
        $('#name').on('keyup',function() {
            // the text typed in the input field is assigned to a variable 
            var query = $(this).val();
            // call to an ajax function
            $.ajax({
                // assign a controller function to perform search action - route name is search
                url:"{{ route('search_name') }}",
                // since we are getting data methos is assigned as GET
                type:"GET",
                // data are sent the server
                data:{'name':query},
                // if search is succcessfully done, this callback function is called
                success:function (data) {
                    // print the search results in the div called name_list(id)
                    $('#name_list').html(data);
                }
            })
            
        });
        $('#option-name').on('click', 'li', function(){
            // declare the value in the input field to a variable
            var value = $(this).text();
            var result = value.split("-");
            
            //var value1 = $(this).value();
            // assign the value to the search box
            $('#name').val(result[0]);
            $('#nip').val(result[1]);
            $('#location').val(result[2]);
            // after click is done, search results segment is made empty
            $('#name_list').html("");
           
        });
    });
</script>   
@endsection