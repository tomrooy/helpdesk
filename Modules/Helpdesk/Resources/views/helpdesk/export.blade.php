<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{asset('nice/css/sb-admin-2.min.css')}}" rel="stylesheet">
<script src="{{asset('nice/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<style>
    @page {
        margin: 0cm 0cm;
    }

    /** Define now the real margins of every page in the PDF **/
    body {
        margin-top: 2cm;
        margin-left: 2cm;
        margin-right: 2cm;
        margin-bottom: 2cm;
    }

    /** Define the header rules **/
    header {
        position: fixed;
        top: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1.5cm;

        /** Extra personal styles **/
        background-color: #03a9f4;
        color: white;
        text-align: center;
        padding-top: 1em;
        line-height: 1.5cm;
    }

    /** Define the footer rules **/
    footer {
        position: fixed; 
        bottom: 0cm; 
        left: 0cm; 
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        background-color: #03a9f4;
        color: white;
        text-align: center;
        line-height: 1.5cm;
    }
</style>
</head>
<body>
    <header>
        <h2><strong>Data Detail Helpdesk</strong></h2>
    </header>
    <main>
        <div class="table table-responsive-md">
            <table class="table table-borderless">
                <tr>
                    <td>Nip</td>
                    <td>:</td>
                    <td>{{$helpdesk->request_by}}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>{{!empty($helpdesk->users->name) ? $helpdesk->users->name : 'User kosong' }}</td>
                </tr>
                <tr>
                    <td>Ticket Number</td>
                    <td>:</td>
                    <td>{{$helpdesk->ticket_number}}</td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td>:</td>
                    <td>{{!empty($helpdesk->categories->name) ? $helpdesk->categories->name : 'category kosong' }}</td>
                </tr>
                <tr>
                    <td>SubCategory</td>
                    <td>:</td>
                    <td>{{!empty($helpdesk->subcategories->name) ? $helpdesk->subcategories->name : 'subcategory kosong' }}</td>
                </tr>
                <tr>
                    <td>Status Terbaru</td>
                    <td>:</td>
                    <td><h3><span class="{{!empty($helpdesk->statuses->badge) ? $helpdesk->statuses->badge : ''}}">{{!empty($helpdesk->statuses->name) ? $helpdesk->statuses->name : 'status kosong'}}</span></h3></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>:</td>
                    <td>
                        @foreach($helpdesk->helpdesk_details as $item)
                        <li>{{!empty($item->description) ? $item->description : 'kosong' }}</li><br>
                        @endforeach
                    </td>
                </tr>
            </table>
        </div>
    </main>
    <footer>
        Copyright Inspektorat Jenderal Kementerian Hukum dan HAM &copy; <?php echo date("Y");?> 
    </footer>   
</body>
</html>
       
