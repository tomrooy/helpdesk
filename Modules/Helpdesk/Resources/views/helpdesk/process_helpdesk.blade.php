@extends('layouts.nice.main')
@section('title','Proses Helpdesk')
@section('content')
@push('main-menu')Helpdesk @endpush
@push('url-main-menu'){{route('module.helpdesk.index')}} @endpush
@push('sub-menu')Proses helpdesk @endpush
<div class="wrapper">
    <div class="card">
        <div class="card-header">
            <h2><b>Proses Helpdesk</b></h2>
        </div>
        <div class="card-body">
            <form method="POST" id="formProcessHelpdesk" enctype="multipart/form-data" class="row g-3">
                @csrf
                <input type="hidden" value="{{$data->id}}" name="id">
                <div class="row">
                    <div class="col-md-6">
                        <hr>
                        <h3 class="flex text-center">Data Permintaan Helpdesk</h3>
                        <hr>
                        <div class="row g-3">
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input disabled type="text" value="{{$data->users->name}}" class="form-control">
                                    <label for="floatingNama">Nama</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input disabled type="text" value="{{$data->request_by}}" class="form-control">
                                    <label for="floatingNip">NIP</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input disabled type="text" class="form-control" value="{{$data->title}}">
                                    <label for="floatingTitle">Judul Helpdesk</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating">                                    
                                    <input disabled type="text" class="form-control" value="{{$data->helpdesk_categories->name}}">
                                    <label>Category</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating">
                                    <input disabled type="text" class="form-control" value="{{$data->helpdesk_subcategories->name}}">
                                    <label>SubCategory</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating">
                                    <input disabled type="text" class="form-control" value="{{!empty($data->priorities->name) ? $data->priorities->name : 'prioritas  kosong'}}">
                                    <label>Prioritas</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <textarea class="form-control" style="height: 100px;" >@foreach ($data->helpdesk_details as $item)@if($item->created_by == $data->request_by)@endif{{$item->description}}@endforeach</textarea>
                                    <label>Keterangan</label>
                                </div>
                            </div>
                        </div><br>                        
                        <div class="row g-3">
                            <div class="col-md-6">
                                <h3>FOTO LAMPIRAN 1</h3>
                                @foreach ($data->helpdesk_details as $item)
                                    @if($item->created_by == $data->request_by)
                                        @if(!empty($item->image_a))
                                            <img src={{asset('storage/uploads/helpdesks/'.$item->image_a)}} width="130px" height="160px">
                                            <a href="{{asset('storage/uploads/helpdesks/'.$item->image_a)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                            @else
                                            <div class="btn btn-warning"> Tidak Ada Foto yang diupload</div>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                            <div class="col-md-6">
                                <h3>FOTO LAMPIRAN 2</h3>
                                @foreach ($data->helpdesk_details as $item)
                                    @if($item->created_by == $data->request_by)
                                        @if(!empty($item->image_a))
                                        <img src={{asset('storage/uploads/helpdesks/'.$item->image_b)}} width="130px" height="160px">
                                        <a href="{{asset('storage/uploads/helpdesks/'.$item->image_b)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                        @else
                                            <div class="btn btn-warning"> Tidak Ada Foto yang diupload</div>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-md-6"><hr>
                        <h3 class="flex text-center">Proses Permintaan Helpdesk</h3>
                        <hr>
                        <div class="row g-3">
                            <div class="form floating">
                                <select class="form-control process-class" name="process" id="process">
                                    <option value="">Pilih Proses</option>
                                    @if($data->status_id == 1)
                                        <option value="2">Sedang Dikejakan</option>
                                        <option value="3">Selesai</option>
                                        <option value="0">Ditolak</option>
                                    @else
                                        <option value="3">Selesai</option>
                                        <option value="0">Ditolak</option>
                                    @endif
                                </select>
                                <strong id="process-error"></strong>
                            </div>
                            <div class="form-floating">
                                <textarea class="form-control description-class" style="height: 100px;" id="description"  name="description" placeholder="silahkan masukan keterangan mengenai pengerjaan"></textarea>
                                <label>Keterangan</label>
                                <strong id="description-error"></strong>
                            </div>
                            <div class="form floating">
                                <select class="form-control" name="handle_by" id="handle_by">
                                    <option value="">Pilih Pegawai Yang Mengerjakan</option>
                                    <option value="Rintaka Rintaka Brata, S.Kom.">Rintaka Rintaka Brata, S.Kom.</option>
                                    <option value="Ari Fachryadi, S.Kom.">Ari Fachryadi, S.Kom.</option>
                                    <option value="Simon Halomoan, A.Md., S.Kom.">Simon Halomoan, A.Md., S.Kom.</option>
                                    <option value="Tommy Roy Sirait, S.Kom.">Tommy Roy Sirait, S.Kom.</option>
                                    <option value="Zaidal Bustomi, S.Kom.">Zaidal Bustomi, S.Kom.</option>
                                </select>
                                <strong id="handle_by-error"></strong>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="file" name="img1" autocomplete="off" type="text"  class="form-control" >
                                    <label for="img1">Attach Image 1</label>
                                    <div class="text-danger">*Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                                    <strong id="img1-error"></strong>
                                </div>
                            </div>   
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="file" name="img2" autocomplete="off" type="text"  class="form-control" >
                                    <label for="img2">Attach Image 2</label>
                                    <div class="text-danger">*Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                                    <strong id="img2-error"></strong>
                                </div>
                            </div>   
                            <div class="col text-center">
                                <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Simpan</button>
                                <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
    
            </form>

        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function() {
        @include('layouts.nice.partial.clock')
        $('#formProcessHelpdesk').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            swal({
                title: "Konfirmasi",
                text: "Proses Helpdesk?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('module.helpdesk.process.action') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                    swal({
                        title: 'Prosessing...',
                        html: 'Please wait...',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showConfirmButton : false,
                          
                        });                
                    },
                    success:function(data) {
                        if(data.errors) {
                            if(data.errors.process){
                                $( '#process-error' ).html( data.errors.process[0] ).addClass('alert-danger');
                                $('.process-class').addClass('is-invalid');
                            }else
                            {
                                $( '#process-error' ).remove();
                                $('.process-class').removeClass('is-invalid');
                            }
                            if(data.errors.description){
                                $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                                $('.description-class').addClass('is-invalid');
                            }else
                            {
                                $( '#description-error' ).remove();
                                $('.description-class').removeClass('is-invalid');
                            }
                            if(data.errors.img1){
                                $( '#img1-error' ).html( data.errors.img1[0] ).addClass('alert-danger');
                                $( '.img1-error' ).addClass('is-invalid');
                            }
                            if(data.errors.img1){
                                $( '#img2-error' ).html( data.errors.img2[0] ).addClass('alert-danger');
                            }
                            
                        }else
                        {
                            document.getElementById("formProcessHelpdesk").reset();
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 3000
                                }).then( function(){
                                    window.location = "{{ route('module.helpdesk.index') }}";
                              });
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
    });
</script>
@endsection