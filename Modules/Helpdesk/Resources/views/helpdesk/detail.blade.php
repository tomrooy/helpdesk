@extends('layouts.nice.main')
@section('title','Detail helpdesk')
@section('content')
@push('main-menu')Helpdesk @endpush
@push('url-main-menu'){{route('module.helpdesk.index')}} @endpush
@push('sub-menu')Detail @endpush
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                <h2><strong>Data Detail Helpdesk</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <a href="{{route('module.helpdesk.index')}}" class="btn btn-secondary"><span class="fa fa-arrow-circle-left"></span> Kembali</a>
                <a href="{{route('module.helpdesk.exportPDF',$helpdesk->id)}}" class="btn btn-info float-right"><i class="fas fa-download"></i> Download</a>
                <div class="row">
                    <div class="col-md-6">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3>Data Main Helpdesk</h3>
                            </div>
                            
                            <div class="card-body">
                                <div class="table table-responsive-md">
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>Nip</td>
                                            <td>:</td>
                                            <td>{{$helpdesk->request_by}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>{{!empty($helpdesk->users->name) ? $helpdesk->users->name : 'User kosong' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Number</td>
                                            <td>:</td>
                                            <td>{{$helpdesk->ticket_number}}</td>
                                        </tr>
                                        <tr>
                                            <td>Judul Permasalahan</td>
                                            <td>:</td>
                                            <td>{{$helpdesk->title}}</td>
                                        </tr>
                                        <tr>
                                            <td>Category</td>
                                            <td>:</td>
                                            <td>{{!empty($helpdesk->helpdesk_categories->name) ? $helpdesk->helpdesk_categories->name : 'category kosong' }}</td>
                                        </tr>
                                        <tr>
                                            <td>SubCategory</td>
                                            <td>:</td>
                                            <td>{{!empty($helpdesk->helpdesk_subcategories->name) ? $helpdesk->helpdesk_subcategories->name : 'subcategory kosong' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Terbaru</td>
                                            <td>:</td>
                                            <td><h3><span class="{{!empty($helpdesk->statuses->badge) ? $helpdesk->statuses->badge : ''}}">{{!empty($helpdesk->statuses->name) ? $helpdesk->statuses->name : 'status kosong'}}</span></h3></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3>Data Detail Helpdesk</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>keterangan Pengerjaan</td>
                                        <td>:</td>
                                        <td>
                                            @foreach($helpdesk->helpdesk_details as $item)
                                                @if($item->status_id == 1)
                                                    <b>Keterangan dari User</b> <br>
                                                    <li>{{!empty($item->description) ? $item->description : 'kosong' }}</li><br>
                                                    <b>Keterangan dari Tim IT</b> <br>
                                                @else
                                                    <li>{{!empty($item->description) ? $item->description : 'kosong' }}</li>
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>
                                </table>
                               
                                
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3>Lampiran Foto</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                   
                                    <div class="col-md-4">
                                        Lampiran Pengajuan<br>
                                            @if($image[0] != NULL)
                                                <img src={{asset('storage/uploads/helpdesks/'.$image[0])}} width="130px" height="160px">
                                                <a target="_blank" href="{{asset('storage/uploads/helpdesks/'.$image[0])}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                            @endif
                                            @if($image[1] != NULL)
                                                <img src={{asset('storage/uploads/helpdesks/'.$image[1])}} width="130px" height="160px">
                                                <a target="_blank" href="{{asset('storage/uploads/helpdesks/'.$image[1])}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                            @endif
                                    </div>
                                    <div class="col-md-4">
                                        Lampiran Pengerjaan<br>
                                            @if(count($image) >= 3)
                                                @if($image[2] != NULL)
                                                    <img src={{asset('storage/uploads/helpdesks/'.$image[2])}} width="130px" height="160px">
                                                    <a target="_blank" href="{{asset('storage/uploads/helpdesks/'.$image[2])}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                                @endif
                                                @if($image[3] != NULL)
                                                    <img src={{asset('storage/uploads/helpdesks/'.$image[3])}} width="130px" height="160px">
                                                    <a target="_blank" href="{{asset('storage/uploads/helpdesks/'.$image[3])}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                                @endif
                                            @endif
                                    </div><div class="col-md-4">
                                        Lampiran Penyelesaian<br>
                                            @if(count($image) >= 5)
                                                @if($image[4] != NULL)
                                                    <img src={{asset('storage/uploads/helpdesks/'.$image[4])}} width="130px" height="160px">
                                                    <a target="_blank" href="{{asset('storage/uploads/helpdesks/'.$image[4])}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                                @endif
                                                @if($image[5] != NULL)
                                                    <img src={{asset('storage/uploads/helpdesks/'.$image[5])}} width="130px" height="160px">
                                                    <a target="_blank" href="{{asset('storage/uploads/helpdesks/'.$image[5])}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                                @endif
                                            @endif
                                    </div>
                                   
                                </div>
                                
                            </div>
                        </div>
                        @if($helpdesk->status == 1 || $helpdesk->status == 2)
                            <a href="{{route('module.helpdesk.process', $helpdesk->id)}}" type="button" class="btn btn-success  float-right" id="process_helpdesk"><i class="fas fa-cogs"></i> Proses</a>
                        @endif
                    </div>
                </div>
               
               
            </div>
        </div>    
    </div>    
@endsection
@section('css')
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
@endsection