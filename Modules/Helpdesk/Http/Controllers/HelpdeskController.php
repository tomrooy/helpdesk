<?php

namespace Modules\Helpdesk\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Helpdesk;
use App\Models\HelpdeskDetail;
use App\Models\HelpdeskPriority;
use App\Models\HelpdeskCategory;
use App\Models\HelpdeskSubCategory;
use PDF;
use SendWa;
use Carbon\Carbon;
use DB;


class HelpdeskController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function getHelpdesk()
    {
        $data   =   Helpdesk::orderBy('created_at','ASC')->get();
        return \DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('request_by',function($data){
                if(!empty($data->users->name))
                {
                    return $data->users->name;
                }else
                {
                    return "NIP KOSONG";
                }
                
            })
            ->editColumn('handle_by',function($data){
                if(!empty($data->handle_by))
                {
                    return $data->handle_by;
                }else
                {
                    return "Belum Dikerjakan";
                }
                
            })
            ->editColumn('status',function($data){
                $badge  = !empty($data->statuses->badge) ? $data->statuses->badge : '';
                $badge_name = !empty($data->statuses->name) ? $data->statuses->name : 'status kosong';
                return '<h3><span class="'.$badge.'">'.$badge_name.'</span> </h3>';
                //return $data->statuses->name;
            })
            ->addColumn('Actions', function($data) {
                $detail     =   '<a href="'.route('module.helpdesk.detail', $data->id).'" class="btn btn-info btn-sm" alt="Lihat Detail" ><i class="bi bi-view-stacked"></i> </a>';
                $process    =   '<a href="'.route('module.helpdesk.process', $data->id).'" type="button" class="btn btn-success btn-sm" id="process_helpdesk" alt="Proses Helpdesk" data-id="'.$data->id.'" ><i class="bx bx-message-edit"></i> </a>';
                $delete     =   '<button type="button" class="btn btn-danger btn-sm" id="deleteHelpdesk" alt="Hapus Helpdesk" data-id="'.$data->id.'" ><i class="bi bi-trash-fill"></i> </button';
                switch($data->status_id)
                {
                    case 1:
                        return $detail.$process.$delete;
                        break;
                    case 2:
                        return $detail.$process.$delete;
                        break;
                    case 3:
                        return $detail.$delete;
                        break;
                    default:
                        return $detail.$delete;
                        break;
                }
                
            })
            ->addColumn('checked',function ($data) {
                return '<input type="checkbox" name="delete_check[]" onclick="checkcheckbox()" class="delete_check" value="'.$data->id.'"/>';
                })
            ->rawColumns(['Actions','status','checked'])
            ->make(true);
    }
    public function index()
    {
        return view('helpdesk::helpdesk.index');
    }
    public function create()
    {
        $category       =   HelpdeskCategory::all();
        $subcategory    =   HelpdeskSubCategory::all();
        $priority       =   HelpdeskPriority::all();
        return view('helpdesk::helpdesk.create',[
            'category'          =>  $category,
            'priority'          =>  $priority,
            'subcategory'       =>  $subcategory
        ]);
    }
    public function store(Request $request)
    {
        
        $helpdesk       =    new  Helpdesk;
        $id             =    IdGenerator::generate(['table' => 'helpdesks','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
        $ticket         =   $request->category.'-'.$id;

            $messages =     $helpdesk->messages();
            $validator  =   \Validator::make($request->all(),[
                'name'          =>  'required',
                'img1'          =>  'nullable|max:400|mimes:jpg,pdf,jpeg',
                'img2'          =>  'nullable|max:400|mimes:jpg,pdf,jpeg',
                'subject'       =>  'required|max:200',
                'category'      =>  'required',
                'description'   =>  'required',
                'subcategory'   =>  'required',
                'user_wa'       =>  'required',
                'priority'      =>  'required',
                'date'          =>  'required',
            ], $messages)->validate();
        try{
            //$ticket     =   $ticket->generateticket($request->category);
                if($request->img1) {
                    $imagePath = $request->img1;
                    $imageName1 = '1'.$ticket.'.'.$request->img1->getClientOriginalExtension();
                
                    $path = $request->img1->storeAs('uploads/helpdesks', $imageName1, 'public');
                }else
                {
                    $imageName1 = null;
                }
                if($request->img2) {
                    $imagePath = $request->img2;
                    $imageName2 = '2'.$ticket.'.'.$request->img2->getClientOriginalExtension();
                    $path = $request->img2->storeAs('uploads/helpdesks', $imageName2, 'public');
                }else
                {
                    $imageName2 = null;
                }
                $userphone = $request->user_wa;
                if($userphone != NULL)
                {
                    $userphone = ltrim($userphone, $userphone[0]);
                    $userphone = '62'.$userphone;
                }
                DB::beginTransaction();
                $helpdesk->index_number     =   $id;
                $helpdesk->ticket_number    =   $ticket;
                $helpdesk->date             =   $request->date;
                $helpdesk->request_by       =   $request->nip;
                $helpdesk->status_id        =   '1';
                $helpdesk->priority_id      =   $request->priority;
                $helpdesk->title            =   $request->subject;
                $helpdesk->category_id      =   $request->category;
                $helpdesk->subcategory_id   =   $request->subcategory;
                $helpdesk->user_wa          =   $request->user_wa;
                $helpdesk->created_by       =   \Auth::User()->nip;
                $helpdesk->save();

                HelpdeskDetail::create([
                    'ticket_number'     =>  $ticket,
                    'status_id'         => '1',
                    'description'       =>  $request->description,
                    'image_a'           =>  $imageName1,
                    'image_b'           =>  $imageName2,
                    'created_by'        =>  \Auth::User()->nip
                ]);
                $user       = $helpdesk->users->name;
                $location   = $helpdesk->users->userLocations->name;
                $text =
"Ada Permintaan bantuan Layanan TI dari.
Nama : $user
Bagian/wilayah : $location
Permintaan : $request->subject
Category/Subcatgory : $request->category $request->subcategory
Prioritas : $request->priority
Detail Penjelasan : $request->description

Untuk Tindaklanjut laporan dapat dilakukan di :  https://ithelpdesk.itjenkumham.id/admin/module/helpdesk/$ticket/process-helpdesk
";
                $text2 = 
"Mohon menunggu tim IT akan segera menindaklanjuti permohonan bantuan IT Anda
Ticket Code anda : $helpdesk->ticket_number
Terima Kasih
2023 Tim IT Inspektorat Jenderal";
                SendWa::send_group($text,$text2,$userphone);
                DB::commit();
                return redirect()->route('module.helpdesk.index')->with('success_message','data Berhasil di proses');
           
        }catch (\Exception $e)
        {
            DB::rollback();
            return redirect()->route('module.helpdesk.index')->with('errors_message',$e->getMessage());
        }
    }
    public function processHelpdesk($id)
    {
        $data   = Helpdesk::findOrFail($id);
       return view('helpdesk::helpdesk.process_helpdesk',[
            'data'  =>  $data
        ]);
    }
    public function actionProcessHelpdesk(Request $request)
    {
        try{
            $message    =   [
                'category_id.required'     =>  'Data ID harus di isi',
            ];
            $validator  =   \Validator::make($request->all(),[
                'process'                               =>  'required',
                'description'                           =>  'required',
            ],$message);
            
            if($validator->passes())
            {   
                $helpdesk = Helpdesk::findOrFail($request->id);
                $ticket     =   $helpdesk->ticket_number.'-'.$request->process;
                if($request->img1) {
                    $imagePath = $request->img1;
                    $imageName1 = '1'.$ticket.'.'.$request->img1->getClientOriginalExtension();
                
                    $path = $request->img1->storeAs('uploads/helpdesks', $imageName1, 'public');
                }else
                {
                    $imageName1 = null;
                }
                if($request->img2) {
                    $imagePath = $request->img2;
                    $imageName2 = '2'.$ticket.'.'.$request->img2->getClientOriginalExtension();
                    $path = $request->img2->storeAs('uploads/helpdesks', $imageName2, 'public');
                }else
                {
                    $imageName2 = null;
                }
                DB::beginTransaction();
                $helpdesk->update([
                    'status_id'             =>  $request->process,
                    'handle_by'             =>  $request->handle_by,
                    'updated_by'            =>  \Auth::User()->nip,
                    
                ]);
                $helpdesk->helpdesk_details()->create([
                    'ticket_number'         => $helpdesk->ticket_number,
                    'status_id'             => $request->process,
                    'description'           => $request->description,
                    'image_a'               =>  $imageName1,
                    'image_b'               =>  $imageName2,
                    'created_by'            =>  \Auth::User()->nip
                ]);
                $userphone = $helpdesk->user_wa;
                switch($request->process)
                {
                    case '2' :
                        $progress = "Sedang Dikerjakan";
                    break;
                    case '3' :
                        $progress = "Selesai di Proses";
                    break;
                    case '0' :
                        $progress = "Ditolak. Untuk info selanjutnya dapat mengubungi Tim IT";
                    break;
            
                    default :
                    $progress = "cek" ;

                }
                $text = 
"Permohonan bantuan IT Anda $progress
Keterangan : $request->description
PIC Permohonan Anda Adalah $request->handle_by
Terima Kasih
2023 Tim IT Inspektorat Jenderal";
                $user  = $helpdesk->users->name;
                $text2 =
"Permohonan bantuan Layanan TI dari $user
$progress oleh $request->handle_by.
Keterangan : $request->description
2023 Tim IT Inspektorat Jenderal
";
                SendWa::send_it($text2);
                SendWa::progress_helpdesk_user($text,$userphone);
                DB::commit();
            }
            else
            {
                DB::rollback();
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function detail($id)
    {
        $data   = Helpdesk::with('helpdesk_details')->findOrFail($id);
        $image = [];
        foreach($data->helpdesk_details as $item)
        {
            array_push($image,$item->image_a,$item->image_b);
        }
       
        return view('helpdesk::helpdesk.detail',[
            'helpdesk' => $data,
            'image'    => $image
        ]);
    }
    public function destroy($id)
    {
        try{
            $data   =   helpdesk::findOrFail($id);
            $data->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
            $data->helpdesk_details()->delete();
            $data->delete();
            
            return response()->json([
                'status'   =>  true,
                'message'  =>  $data->name.' delete'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
    }
    public function destroyAll(Request $request)
    {
        $ids    =   $request->deleteids_arr;
        foreach($ids as $id)
        {
            $helpdesk   =    Helpdesk::findOrFail($id);
            $helpdesk->delete();
            $helpdesk->helpdesk_details()->delete();
        }
        
       return 'sukses';
    }
    public function exportPDF($id)
    {
        $helpdesk   = Helpdesk::findOrFail($id);
        

        $pdf = PDF::loadView('helpdesk::helpdesk.export',[
            'helpdesk' => $helpdesk
        ])->setPaper('A5', 'potrait');
        $now    =   \Carbon\Carbon::now()->format('H:m');
        return $pdf->stream('pdf_file-'.$now.'.pdf');
    }

}
