<?php

namespace Modules\Helpdesk\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\HelpdeskCategory;
use App\Models\HelpdeskSubCategory;
Use DB;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function __construct()
    {
         //$this->middleware('permission:category-list', ['only' => ['index','getCategory']]);
         //$this->middleware('permission:category-create', ['only' => ['store']]);
         //$this->middleware('permission:category-edit', ['only' => ['edit','update']]);
         //$this->middleware('permission:category-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {
        
        return view('helpdesk::category.index');
    }
    public function getCategory()
    {
        $data = HelpdeskCategory::latest()->get();
        return \DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('Actions', function ($data){
                $edit = '<button type="button" class="btn btn-success btn-sm" id="getEditCategoryData" data-id="'.$data->id.'" ><i class="fa fa-edit"></i> Edit</button>';
                $delete = '<button type="button" class="btn btn-danger btn-sm" id="deleteCategory" data-id="'.$data->id.'" ><i class="fa fa-trash"></i> Delete</button>' ;
                //if(\Auth::User()->can('category-edit') && \Auth::User()->can('category-delete'))
                if(\Auth::check())
                {   
                    return $edit.$delete;
                    
                }elseif(\Auth::User()->can('category-edit'))
                {
                    return $edit;
                }elseif(\Auth::User()->can('category-delete'))
                {   
                    return $delete;
                    
                }else{
                    return '';
                }   
               
            })
            ->addColumn('checked',function ($data) {
                return '<input type="checkbox" name="delete_check[]" onclick="checkcheckbox()" class="delete_check" value="'.$data->id.'"/>';
                })
            ->rawColumns(['Actions','checked'])
            ->make(true);
    }
    public function store(Request $request)
    {
        try{
            $message    =   [
                'category_id.required'     =>  'Data ID harus di isi',
            ];
            $validator  =   \Validator::make($request->all(),[
                'category_id'                       =>  'required',
                'name'                              =>  'required',
                'description'                       =>  'required',
            ],$message);
            
            if($validator->passes())
            {   
                
                HelpdeskCategory::create([
                    'category_id'           => $request->category_id,
                    'name'                  => $request->name,
                    'description'           => $request->description,
                    'slug'                  => $request->name,
                    'created_by'            => \Auth::User()->nip,
                ]);
            }
            else
            {
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function edit($id)
    {
        try{
            $data = HelpdeskCategory::findOrFail($id);
            
            return response()->json([
                'result'  =>  $data,
                'status'  =>   true,
            ]);
        }catch (\Exception $e)
        {
            return response()->json([
                'result'  =>  $e->getMessage(),
                'status'  =>   false
            ]);
        }
        
    }
    public function update(Request $request)
    {
      
        $message    =   [
            'category_id.required'     =>  'Data ID harus di isi',
        ];
        $validator  =   \Validator::make($request->all(),[
            'category_id'                      =>  'required',
            'name'                          =>  'required',
            'description'                   =>  'required',
        ],$message);
        if($validator->passes())
        {
            try{
                $category      =   HelpdeskCategory::findOrFail($request->id);
                $category->update([
                    'category_id'           => $request->category_id,
                    'name'                  => $request->name,
                    'description'           => $request->description,
                    'slug'                  => $request->name,
                    'updated_by'            => \Auth::User()->nip,
                ]);
                return response()->json([
                    'status'   =>  true,
                    'message'  =>  $category->name.' delete'
                ]);

            } catch(\Exception $e)
            {
                return response()->json([
                    'status'   =>   false,
                    'message'  =>  $e->getMessage()
                ]);
            }
           
        }
        else
        {
            return response()->json(['errors' => $validator->errors()]);
        }
    }
    public function destroy($id)
    {
        try{
            $data   =   HelpdeskCategory::findOrFail($id);
            $data->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
            $data->delete();
            return response()->json([
                'status'   =>  true,
                'message'  =>  $data->name.' delete'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
    }
    public function getSubcategory($id)
    {
        $data   =   HelpdeskSubCategory::where('category_id',$id)->pluck('subcategory_id','name');
        return json_encode($data);
    }
    public function getEditCategory()
    {
        $data   =   HelpdeskCategory::all();
        $output = '';
        if(!$data->isEmpty())
        {
            $output .= '<option value="'.$brand_id.'" '.(($brand_id == $id) ? 'selected="selected"':"").'>'.$brand_name.'</option>';
        }
        return response()->json([
            
            'result'  =>  $data
        ]);
    }
    public function destroyAll(Request $request)
    {
        $ids    =   $request->deleteids_arr;
        foreach($ids as $id)
        {
            $category   =    HelpdeskCategory::findOrFail($id);
            $category->delete();
        }
       return 'sukses';
    }
}
