<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    
    Route::group(['prefix'=>'admin/module/','middleware'=>'role:Super_Admin'], function() {

        Route::get('/gethelpdesk','HelpdeskController@getHelpdesk')->name('module.helpdesk.data');
        Route::get('/helpdesk','HelpdeskController@index')->name('module.helpdesk.index');
        Route::get('/helpdesk/input','HelpdeskController@create')->name('module.helpdesk.create');
        Route::post('/helpdesk/simpan','HelpdeskController@store')->name('module.helpdesk.store');
        Route::get('/helpdesk/{id}/detail-data','HelpdeskController@detail')->name('module.helpdesk.detail');
        Route::delete('/helpdesk/delete/all-data','HelpdeskController@destroyAll')->name('module.helpdesk.delete.all');
        Route::get('/helpdesk/{id}/process-helpdesk','HelpdeskController@processHelpdesk')->name('module.helpdesk.process');
        Route::post('/helpdesk/{id}/delete','HelpdeskController@destroy')->name('module.helpdesk.delete');
        Route::post('/helpdesk/process-helpdesk/action','HelpdeskController@actionProcessHelpdesk')->name('module.helpdesk.process.action');
        route::get('helpdesk/exportpdf/{id}','HelpdeskController@exportPDF')->name('module.helpdesk.exportPDF');
        


        Route::get('/subcategory','SubCategoryController@index')->name('module.subcategory.index');
        Route::get('/getsubcategory','SubCategoryController@getSubCategory')->name('module.subcategory.data');
        Route::post('/subcategory/simpan','SubCategoryController@store')->name('module.subcategory.store');
        Route::get('/subcategory/{id}/edit','SubCategoryController@edit')->name('module.subcategory.edit');
        Route::post('/subcategory/update','SubCategoryController@update')->name('module.subcategory.update');
        Route::post('/subcategory/{id}/delete','SubCategoryController@destroy')->name('module.subcategory.delete');


        Route::get('/getcategory','CategoryController@getCategory')->name('module.category.data');
        Route::get('/geteditformcategory','CategoryController@EditCategory')->name('module.geteditformcategory.data');
        Route::get('/category','CategoryController@index')->name('module.category.index');
        Route::delete('/category/delete/all-data','CategoryController@destroyAll')->name('module.category.delete.all');
        Route::post('/category/simpan','CategoryController@store')->name('module.category.store');
        Route::get('/category/{id}/edit','CategoryController@edit')->name('module.category.edit');
        Route::post('/category/update','CategoryController@update')->name('module.category.update');
        Route::post('/category/{id}/delete','CategoryController@destroy')->name('module.category.delete');
    
    });

});