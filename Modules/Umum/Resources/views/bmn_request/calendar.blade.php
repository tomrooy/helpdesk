@extends('layouts.nice.main')
@section('title','Calendar View')
@push('nav-umum') show @endpush
@section('css')
<link href="{{asset('nice/calendar/fullcalendar.css')}}" rel='stylesheet' />
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Index @endpush   
        <div class="card">
            <div class="card-header">
                <div style="text-align: center"><h2><strong >Data Kegiatan Perbulan</strong></h2></div>
            </div>    
            <div class="card-body">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="{{asset('nice/calendar/fullcalendar.min.js')}}"></script>
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {        
        var interval = setInterval(function() {
            var momentNow = moment();
            $('#date-part').html(momentNow.format('dddd')
                                .substring(0,9).toUpperCase()+' '+ momentNow.format('DD MMMM YYYY'));
            $('#time-part').html(momentNow.format('HH:mm:ss A'));
        }, 100);
        var today = new Date().toISOString().slice(0,10);
            console.log(today);
            $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,basicWeek,basicDay'
                    },
                    windowResize: function(arg) {
                        alert('The calendar has adjusted to a window resize. Current view: ' + arg.view.type);
                      },
                    handleWindowResize :true,
                    eventStartEditable :false,//fungsi untuk mindahkan event
                    defaultDate: today,
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    selectable: true,
                    multipleselect : false,
                    selectHelper: true,
                    aspectRatio:  3,
                    displayEventEnd: true,
                    select: function(start, end){
                      if(moment(start._d).add(1, 'days').format('YYYY-MM-DD')==moment(end._d).format('YYYY-MM-DD')){
                        //$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                        //$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                        //$('#ModalAdd').modal('show');
                       
                        
                      }
                    },
                    
                    //fungsi untuk menampilkan detail dari event yang di klik
                    eventRender: function(event, element) {
                        element.bind('dblclick', function() {
                            $('#ModalEdit #id').val(event.id);
                            $('#ModalEdit #title').val(event.name);
                            
                        });
                    },
                   
                    eventDrop: function(event, delta, revertFunc) { // si changement de position
        
                        edit(event);
        
                    },
                    eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
        
                        edit(event);
        
                    },
                    //parsing data zoom dari database
                    events: [
                            @foreach($bmn_request as $item)
                                {
                                    id:     '{{$item->id}}',
                                    title:  "{!! $item->bmns->name .'-'. $item->employees->name !!}",
                                    name : '{{$item->bmn_id}}',
                                    start:  '{{\Carbon\Carbon::createFromFormat('Y-m-d', $item->date)->format('Y-m-d')}}',
                                    end:    '{{\Carbon\Carbon::createFromFormat('Y-m-d', $item->date)->format('Y-m-d')}}',
                                    color:  '#6hd6d7',
                                    
                                },
                            @endforeach                      
                    ]
                });
    });
</script>    
@endsection
