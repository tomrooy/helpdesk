@extends('layouts.nice.main')
@section('title','Data Permintaansk')
@push('nav-umum') show @endpush
@push('main-menu')Layanan BMN @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@section('css')

@endsection
@section('content')
<div class="wrapper">
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data Permintaan Helpdesk
        </div>
        <div class="card-body">
            <br>
            <a href="{{route('front.bmn_request.create')}}" class="btn btn-info"><i class="bi bi-file-plus-fill"></i>Tambah Data</a>
            <br><br>
            <div class="table-responsive">
                <table id="bmn_request-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ticket Number</th>
                            <th>Judul/BMN</th>
                            <th>Tanggal/Sesi </th>
                            <th>Pemohon</th>
                            <th>Status</th>
                            <th width="150" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
           
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
$(document).ready(function() {
        var dataTable = $('#bmn_request-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('module.bmn_request.getData') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'ticket_number', name: 'ticket_number'},
                {data: 'subject', name: 'subject'},
                {data: 'date', name: 'date'},
                {data: 'request_by', name: 'request_by'},
                {data: 'status_id', name:'status_id'},
                {data: 'Aksi', name: 'Aksi',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });
    });
</script>
@endsection