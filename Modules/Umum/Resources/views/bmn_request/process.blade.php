@extends('layouts.nice.main')
@section('title','Proses Layanan BMN')
@section('content')
@push('main-menu')Layanan BMN @endpush
@push('url-main-menu'){{route('module.helpdesk.index')}} @endpush
@push('sub-menu')Proses Permintaan BMN @endpush
<div class="wrapper">
    <div class="card">
        <div class="card-header">
            <h2><b>Proses Helpdesk</b></h2>
        </div>
        <div class="card-body">
            <form method="POST" id="formProcessBmnRequest" enctype="multipart/form-data" class="row g-3">
                @csrf
                <input type="hidden" value="{{$data->id}}" name="id">
                <div class="row">
                    <div class="col-md-6">
                        <hr>
                        <h3 class="flex text-center">Data Permintaan Layanan BMN</h3>
                        <hr>
                        <div class="row g-3">
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input disabled type="text" value="{{$data->employees->name}}" class="form-control">
                                    <label for="floatingNama">Nama</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input disabled type="text" value="{{$data->request_by}}" class="form-control">
                                    <label for="floatingNip">NIP</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input disabled type="text" class="form-control" value="{{$data->subject}}">
                                    <label for="floatingTitle">Judul Helpdesk</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating">                                    
                                    <input disabled type="text" class="form-control" value="{{$data->bmn_id}}">
                                    <label>Nama BMN</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating">                                    
                                    <input disabled type="text" class="form-control" value="{{$data->category_id}}">
                                    <label>Category</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-floating">
                                    <input disabled type="text" class="form-control" value="{{!empty($data->priority_id) ? $data->priority_id : 'prioritas  kosong'}}">
                                    <label>Prioritas</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-floating">
                                    <textarea disabled class="form-control" style="height: 100px;" >@foreach ($data->bmn_request_details as $item)@if($item->created_by == $data->request_by)@endif{{$item->description}}@endforeach</textarea>
                                    <label>Keterangan</label>
                                </div>
                            </div>
                        </div><br>                        
                    </div>
                    <div class="col-md-6"><hr>
                        <h3 class="flex text-center">Proses Permintaan Helpdesk</h3>
                        <hr>
                        @if($data->bmns->type_id == '0')
                        <div>
                            List Kegiatan pada Tanggal {{$data->date}}
                            <br>
                            <ul>
                                @forelse($list as $item)
                                    <li>{{$item->subject}}</li>
                                @empty
                                    <li>Belum ada Kegiatan</li>
                                @endforelse    
                            </ul>
                        </div>
                        @else
                            <div>
                                <p>Berikut informasi ketersediaan Barang :</p>
                                <li>Stok Barang {{$data->bmns->name}} tersedia ada {{$data->bmns->qty}} Buah</li>
                            </div>
                        @endif
                        <br>
                        <div class="row g-3">
                            <div class="form floating">
                                <select class="form-control process-class" name="process" id="process">
                                    <option value="">Pilih Proses</option>
                                    <option value="2">Diterima</option>
                                    <option value="0">Ditolak</option>
                                </select>
                                <strong id="process-error"></strong>
                            </div>
                            @if($data->bmns->type_id == '1')
                                <div class="form-floating">
                                    <input type="number" min='1' max{{$data->bmns->qty}} class="form-control" id="qty"  name="qty" placeholder="silahkan jumlah Barang yang disetujui">
                                    <label>Jumlah Stok Yang Diberikan</label>
                                    <strong id="description-error"></strong>
                                </div>
                                <div class="text-danger">*wajib di isi dibawh </div>
                            @endif
                            <div class="form-floating">
                                <textarea class="form-control description-class" style="height: 100px;" id="description"  name="description" placeholder="silahkan masukan keterangan mengenai pengerjaan"></textarea>
                                <label>Keterangan</label>
                                <strong id="description-error"></strong>
                            </div>
                            <div class="col text-center">
                                <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Simpan</button>
                                <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
    
            </form>

        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function() {
        $('#qty').on('change', function(){
            if($('#qty').val() > 0 && $('#qty').val() > {{$data->bmns->qty}})
            {
                alert('Jumlah Stok tidak Benar dinput');
                document.getElementById("formProcessBmnRequest").reset();
            }
        });
        $('#formProcessBmnRequest').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            swal({
                title: "Konfirmasi",
                text: "Proses Permintaan?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('module.bmn_request.action_processData') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                    swal({
                        title: 'Prosessing...',
                        html: 'Please wait...',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showConfirmButton : false,
                          
                        });                
                    },
                    success:function(data) {
                        if(data.errors) {
                            if(data.errors.process){
                                $( '#process-error' ).html( data.errors.process[0] ).addClass('alert-danger');
                                $('.process-class').addClass('is-invalid');
                            }else
                            {
                                $( '#process-error' ).remove();
                                $('.process-class').removeClass('is-invalid');
                            }
                            if(data.errors.description){
                                $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                                $('.description-class').addClass('is-invalid');
                            }else
                            {
                                $( '#description-error' ).remove();
                                $('.description-class').removeClass('is-invalid');
                            }
                           
                            
                        }else
                        {
                            document.getElementById("formProcessBmnRequest").reset();
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 3000
                                }).then( function(){
                                    window.location = "{{ route('module.bmn_request.index') }}";
                              });
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
    });
</script>
@endsection