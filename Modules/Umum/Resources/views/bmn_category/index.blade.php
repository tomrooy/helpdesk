@extends('layouts.nice.main')
@section('title','Data Kategory')
@push('nav-umum') show @endpush
@push('main-menu')Kategory BMN @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@section('css')

@endsection
@section('content')
<div class="wrapper">
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data Kategory BMN
        </div>
        <div class="card-body">
            <br>
            <a href="#" class="btn btn-info"><i class="bi bi-file-plus-fill"></i>Tambah Data</a>
            <br><br>
            <div class="table-responsive">
                <table id="bmn_category-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Nama</th>
                            <th>Description</th>
                            <th width="150" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
           
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
$(document).ready(function() {
        var dataTable = $('#bmn_category-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('module.bmn_category.getData') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'category_id', name: 'category_id'},
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'Aksi', name: 'Aksi',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });
    });
</script>
@endsection