@extends('layouts.nice.main')
@section('title','Data Kategory')
@push('nav-umum') show @endpush
@push('main-menu') BMN @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush
@section('css')

@endsection
@section('content')
<div class="wrapper">
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data BMN yang dikelola Inspektorat Jenderal
        </div>
        <div class="card-body">
            <br>
            <a href="#" id="createData" class="btn btn-info"><i class="bi bi-file-plus-fill"></i>Tambah Data</a>
            <div class="modal fade" id="createBmnModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Data BMN</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" id="createBmnForm"  enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="bmn_id" name="bmn_id" placeholder="Silahkan Isi BMN ID">
                                        <label for="floatingTitle">ID BMN</label>
                                    </div>
                                    <strong id="bmn_id-error"></strong><br>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="bmn_name" name="bmn_name" placeholder="Silahkan Isi Nama BMN ">
                                        <label for="floatingTitle">Nama BMN</label>
                                    </div>
                                    <strong id="bmn_name-error"></strong><br>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <select class="form-control" id="bmn_category" name="bmn_category" placeholder="Silahkan Isi Category BMN ">
                                            <option value="">--Pilih Category--</option>
                                            @foreach($category as $item)
                                                <option value="{{$item->category_id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        <label for="floatingTitle">Category BMN</label>
                                        <strong id="bmn_category-error"></strong>
                                    </div>
                                    
                                </div>
                                <div class="col-md-12">
                                    <fieldset class="row mb-3">
                                        <legend class="col-form-label col-sm-4 pt-0">Pilihan Jenis BMN</legend>
                                        <div class="col-sm-8">
                                          <div class="form-check">
                                            <input class="form-check-input" type="radio" name="bmn_type" id="bmn_type1" value="0">
                                            <label class="form-check-label" for="gridRadios1">
                                                BMN Tak Habis Pakai
                                            </label>
                                          </div>
                                          <div class="form-check">
                                            <input class="form-check-input" type="radio" name="bmn_type" id="bmn_type2" value="1">
                                            <label class="form-check-label" for="gridRadios2">
                                                BMN Habis Pakai
                                            </label>
                                          </div>
                                        </div>
                                      </fieldset>
                                      <strong id="bmn_type-error"></strong>
                                </div>
                                <div class="col-md-12" style="display: none" id="formQty">
                                    <div class="form-floating">
                                        <input type="number" class="form-control" id="bmn_qty" name="bmn_qty" placeholder="Silahkan Isi Jumlah BMN ">
                                        <label for="floatingTitle">Stok BMN</label>
                                    </div>
                                    <strong id="bmn_name-error"></strong><br>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <textarea class="form-control" name="bmn_description"  id="bmn_description" style="height: 100px;"></textarea>
                                        <label for="floatingTitle">Deskripsi BMN</label><br>
                                    </div>
                                    <strong id="bmn_description-error"></strong>
                                </div>

                            
                        </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-info" >Simpan</button>
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="table-responsive">
                <table id="bmn-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama BMN</th>
                            <th>Category</th>
                            <th>Qty</th>
                            <th>Status</th>
                            <th width="150" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
           
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
$(document).ready(function() {
    var dataTable = $('#bmn-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        pageLength: 5,
        // scrollX: true,
        "order": [[ 0, "desc" ]],
        ajax: '{{ route('module.bmn.getData') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'bmn_id', name: 'bmn_id'},
                {data: 'category_id', name: 'category_id'},
                {data: 'qty', name: 'qty'},
                {data: 'status', name: 'status'},
                {data: 'Aksi', name: 'Aksi',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });
    $('body').on('click', '#createData', function(e) {
        e.preventDefault();
        $('#createBmnModal').modal('show');
    });
    $('#bmn_type2').on('change', function(){
        document.getElementById('formQty').style.display = "block";
    });
    $('#bmn_type1').on('change', function(){
        document.getElementById('formQty').style.display = "none";
    });
    $('#createBmnForm').submit(function(e) {
        e.preventDefault();
        let formData = new FormData(this);
            swal({
                title: "Konfirmasi",
                text: "Proses Permintaan?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('module.bmn.store') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                    swal({
                        title: 'Prosessing...',
                        html: 'Please wait...',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showConfirmButton : false,
                          
                        });                
                    },
                    success:function(data) {
                        if(data.errors) {
                            swal({
                                    title: "Error!",
                                    text: "Mohon Periksa Kembali Form",
                                    type: "warning",
                                    timer: 3000
                                })
                            if(data.errors.bmn_id){
                                $( '#bmn_id-error' ).html( data.errors.bmn_id[0] ).addClass('alert-danger');
                                $('.bmn_id-class').addClass('is-invalid');
                            }else
                            {
                                $( '#subject-error' ).remove();
                                $('.subject-class').removeClass('is-invalid');
                            }
                            if(data.errors.bmn_name){
                                $( '#bmn_name-error' ).html( data.errors.bmn_name[0] ).addClass('alert-danger');
                                $('.bmn_name-class').addClass('is-invalid');
                            }else
                            {
                                $( '#bmn_name-error' ).remove();
                                $('.bmn_name-class').removeClass('is-invalid');
                            }
                            if(data.errors.bmn_category){
                                $( '#bmn_category-error' ).html( data.errors.bmn_category[0] ).addClass('alert-danger');
                                $('.bmn_category-class').addClass('is-invalid');
                            }else
                            {
                                $( '#bmn_category-error' ).remove();
                                $('.bmn_category-class').removeClass('is-invalid');
                            }
                            if(data.errors.bmn_description){
                                $( '#bmn_description-error' ).html( data.errors.bmn_description[0] ).addClass('alert-danger');
                                $('.bmn_description-class').addClass('is-invalid');
                            }else
                            {
                                $( '#bmn_description-error' ).remove();
                                $('.bmn_description-class').removeClass('is-invalid');
                            }
                            
                        }else
                        {
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 3000
                                }).then( function(){
                                    $('#createBmnModal').hide();
                                    document.getElementById("createBmnForm").reset();
                                    $('#bmn-table').DataTable().ajax.reload();
                              });
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });

});
</script>
@endsection