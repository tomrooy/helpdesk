<?php

namespace Modules\Umum\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\BmnCategory;

class BmnCategoryController extends Controller
{
   public function index()
   {
        return view('umum::bmn_category.index');
   }
   public function getData()
   {
        $data       =   BmnCategory::latest()->get();
        return \DataTables::of($data)
                        ->addIndexColumn()
                        ->addColumn('Aksi', function($data)
                        {
                            return "aksi";
                        })
                        ->rawColumns(['Aksi'])
                        ->make(true);
   }            
}
