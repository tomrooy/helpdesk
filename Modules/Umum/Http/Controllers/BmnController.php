<?php

namespace Modules\Umum\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Bmn;
use App\Models\BmnCategory;
use DB;

class BmnController extends Controller
{
    public function index()
    {
        $category       =   BmnCategory::all();
        return view('umum::bmn.index',[
            'category'      =>  $category
        ]);
    }
    public function getData()
    {
        $data       =   Bmn::latest()->get();
        return \DataTables::of($data)
                    ->addIndexColumn()
                    ->editColumn('bmn_id', function ($data)
                    {
                        return $data->name.'<br><b>'.$data->bmn_id.'</b>';
                    })
                    ->addColumn('status', function($data)
                        {
                            return "Tersedia";
                        })
                    ->addColumn('Aksi', function($data)
                    {
                        $update =    '<a href="#" type="button" class="btn btn-success btn-sm" id="edit_helpdesk" alt="Proses bmn_request" ><i class="bi bi-eye-fill"></i> Lihat </a>';   
                        $hapus  =    '<a href="#" type="button" class="btn btn-danger btn-sm" id="delete_helpdesk" alt="Hapus bmn" ><i class="bi bi-trash"></i> Hapus </a>';
                        return $update.$hapus;
                    })
                    ->rawColumns(['Aksi','status','bmn_id'])
                    ->make(true);
    }
    public function store(Request $request)
    {
        try{
            $validator  =   \Validator::make($request->all(),[
                'bmn_id'                       =>  'required|unique:bmns',
                'bmn_name'                     =>  'required',
                'bmn_description'              =>  'required',
                'bmn_category'                 =>  'required', 
            ]);
            
            if($validator->passes())
            {   
                $request->bmn_type == '0' ? $bmn_qty = '1' : $bmn_qty = $request->bmn_qty;
                $bmn                =   New Bmn;
                $bmn->bmn_id        =   $request->bmn_id;
                $bmn->name          =   $request->bmn_name;
                $bmn->type_id       =   $request->bmn_type;
                $bmn->category_id       =   $request->bmn_category;
                $bmn->description   =   $request->bmn_description;
                $bmn->qty           =   $bmn_qty;
                $bmn->created_by    =   \Auth::User()->nip;
                $bmn->save();
                
            }
            else
            {
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
}
