<?php

namespace Modules\Umum\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Bmn;
use App\Models\BmnRequest;
use DB;
use SendWa;

class BmnRequestController extends Controller
{
    public function index()
    {
        
        return view('umum::bmn_request.index');
    }
    public function getData()
    {
        $data    =   BmnRequest::latest()->get();
        return \DataTables::of($data)
                        ->addIndexColumn()
                        ->editColumn('date', function($data){
                            if($data->sesi == '1')
                            {
                                $sesi = "Pagi";
                            }elseif($data->sesi == '2' )
                            {
                                $sesi = "Siang";
                            }else
                            {
                                $sesi = "Sepanjang Hari";
                            }
                            return $data->date.'<br><b>'.$sesi.'</b>';
                        })
                        ->editColumn('subject', function($data){
                            return $data->subject.'<br><b>'.$data->bmns->name.'</b>';
                        })
                        ->editColumn('request_by', function($data){
                            return $data->employees->name;
                        })
                        ->editColumn('status_id',function($data){
                            if($data->status_id == '1')
                            {
                                $status = '<button class="btn btn-info">Permohonan Baru</button>';
                            }elseif($data->status_id == '2' )
                            {
                                $status = '<button class="btn btn-success">Permohonan Disetujui</button>';
                            }else
                            {
                                $status = '<button class="btn btn-danger">Permohonan Ditolak</button>';
                            }
                            return $status;
                        })
                        ->addColumn('Aksi', function($data)
                        {
                            if($data->status_id == '1')
                            {
                                return $process    =   '<a href="'.route('module.bmn_request.processData', $data->id).'" type="button" class="btn btn-success btn-sm" id="process_helpdesk" alt="Proses bmn_request" ><i class="bx bx-message-edit"></i> Proses</a>';
                            }else
                            {
                                return $view    =   '<a href="#" type="button" class="btn btn-success btn-sm" id="process_helpdesk" alt="Proses bmn_request" ><i class="bi bi-eye-fill"></i> Lihat </a>';
                            }
                            
                        })
                        ->rawColumns(['Aksi','subject','date','status_id','request_by'])
                        ->make(true);
    }
    public function processData($id)
    {
        $bmn_request        =   BmnRequest::findOrFail($id);
        $bmn_request_list   =   BmnRequest::where('date',$bmn_request->date)->where('status_id','2')->get();
        return view('umum::bmn_request.process',[
            'data'          =>  $bmn_request,
            'list'          =>  $bmn_request_list    
        ]);
    }
    public function actionProcessData(Request $request)
    {
        try{
            $message    =   [
                'process.required'     =>  'Data ID harus di isi',
            ];
            $validator  =   \Validator::make($request->all(),[
                'process'                               =>  'required',
                'description'                           =>  'required',
            ],$message);
            
            if($validator->passes())
            {   
                $bmn_request        =   BmnRequest::findOrFail($request->id);
               
                DB::beginTransaction();
                $bmn_request->update([
                    'status_id'             =>  $request->process,
                    'handle_by'             =>  \Auth::User()->nip,
                    'updated_by'            =>  \Auth::User()->nip,
                ]);
                $qty = NULL;
                if($bmn_request->bmns->type_id == '1')
                {
                    $qty        = $request->qty;  
                    $bmn        = Bmn::findOrFail($bmn_request->bmns->id);
                    $bmn->qty   = $bmn->qty - $request->qty;
                    $bmn->save();
                }
                $bmn_request->bmn_request_details()->create([
                    'ticket_number'         => $bmn_request->ticket_number,
                    'status_id'             => $request->process,
                    'qty'                   =>  $qty,
                    'description'           => $request->description,
                    'created_by'            =>  \Auth::User()->nip
                ]);
                $userphone = $bmn_request->user_wa;
                $nama_pegawai   =   $bmn_request->employees->name;
                $bagian         =   $bmn_request->employees->location;
                $permintaan     =   $bmn_request->bmns->name;
                $tanggal        =   $bmn_request->date;
                $text = 
"
Permohonan Anda Disetujui dengan Data :
Nama Pegawai    : $nama_pegawai
Bagian          : $bagian
Permintaan      : $permintaan
Tanggal         : $tanggal
";
                $user  = $bmn_request->employees->name;
                $text2 =
"
Permohonan Berikut :
Nama Pegawai    : $nama_pegawai
Bagian          : $bagian
Permintaan      : $permintaan
Tanggal         : $tanggal
Telah Disetujui Admin
";
                SendWa::send_umum($text2);
                SendWa::progress_bmn_request($text,$userphone);
                DB::commit();
            }
            else
            {
                DB::rollback();
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            
            return response()->json(['errors' => $e->getMessage()]);
        }

    }

    
}
