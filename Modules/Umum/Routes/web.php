<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    Route::prefix('umum')->group(function() {
        Route::get('/bmn-request', 'BmnRequestController@index')->name('module.bmn_request.index');
        Route::get('/bmn-request/get-data', 'BmnRequestController@getData')->name('module.bmn_request.getData');
        Route::get('/bmn-request/{id}/proses-data', 'BmnRequestController@processData')->name('module.bmn_request.processData');
        Route::post('/bmn-request/proses-data', 'BmnRequestController@actionProcessData')->name('module.bmn_request.action_processData');


        Route::get('/bmn', 'BmnController@index')->name('module.bmn.index');
        Route::get('/bmn/get-data', 'BmnController@getData')->name('module.bmn.getData');
        Route::post('/bmn/store', 'BmnController@store')->name('module.bmn.store');

        Route::get('/bmn-category', 'BmnCategoryController@index')->name('module.bmn_category.index');
        Route::get('/bmn-category/get-data', 'BmnCategoryController@getData')->name('module.bmn_category.getData');
    });
});
