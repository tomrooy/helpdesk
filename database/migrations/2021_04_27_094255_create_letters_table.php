<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letters', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('letter_type')->nullable();
            $table->string('agenda_number')->nullable();
            $table->string('letter_index')->nullable();
            $table->text('title')->nullable();
            $table->string('from')->nullable();
            $table->string('letter_number')->nullable();
            $table->string('letter_date')->nullable();
            $table->text('description')->nullable();
            $table->string('file')->nullable();
            $table->string('disposition_1')->nullable();
            $table->string('disposition_2')->nullable();
            $table->string('disposition_3')->nullable();
            $table->string('disposition_4')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->string('deleted_by',50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letters');
    }
}
