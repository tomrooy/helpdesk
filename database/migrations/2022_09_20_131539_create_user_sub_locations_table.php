<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSubLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sub_locations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('location_id',30);
            $table->string('sublocation_id',30);
            $table->string('name',60);
            $table->string('eselon',3)->nullable();
            $table->enum('status',['0','1'])->default('1');
            $table->text('description')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->string('deleted_by',50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sub_locations');
    }
}
