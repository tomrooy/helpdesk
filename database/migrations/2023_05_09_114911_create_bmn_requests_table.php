<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBmnRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bmn_requests', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('index_number',7)->nullable();
            $table->string('ticket_number')->nullable();
            $table->string('subject')->nullable();
            $table->string('date')->nullable();
            $table->string('priority_id')->nullable();
            $table->string('status_id')->nullable();
            $table->string('request_by')->nullable();
            $table->string('category_id');
            $table->string('bmn_id');
            $table->string('user_wa');
            $table->string('handle_by')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->string('deleted_by',50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bmn_requests');
    }
}
