<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEkspedisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ekspedisis', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code_id')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->text('name')->nullable();
            $table->string('user')->nullable();
            $table->string('date')->nullable();
            $table->string('uploaded')->nullable();
            $table->string('created_by')->nullable();
            $table->string('update_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ekspedisis');
    }
}
