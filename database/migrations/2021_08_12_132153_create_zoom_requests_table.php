<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZoomRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zoom_requests', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('index_number',7)->nullable();
            $table->string('ticket_number',20)->unique();
            $table->text('name');
            $table->string('participant')->nullable();
            $table->string('request_by',50)->nullable();
            $table->string('date',50)->nullable();
            $table->string('start',50)->nullable();
            $table->string('end',50)->nullable();
            $table->string('category',50)->nullable();
            $table->text('link')->nullable();
            $table->string('zoom_id', 20)->nullable();
            $table->string('password',20)->nullable();
            $table->text('link_record')->nullable();
            $table->string('password_record',20)->nullable();
            $table->string('status',50);
            $table->string('attachment',198)->nullable();
            $table->string('user_wa',50)->nullable();
            $table->text('description')->nullable();
            $table->text('created_by_account',50)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->string('deleted_by',50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zoom_requests');
    }
}
