<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApelActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apel_activities', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('index_number',7)->nullable();
            $table->string('activity_id')->nullable();
            $table->string('name')->nullable();
            $table->string('date')->nullable();
            $table->string('category_id')->nullable();
            $table->enum('status',['0','1']);
            $table->string('qr_code')->nullable();
            $table->string('link')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apel_activities');
    }
}
