<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyPresentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_presents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('date')->nullable();
            $table->string('nip')->nullable();
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->string('unit')->nullable();
            $table->string('status_id')->nullable();
            $table->string('signature')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_presents');
    }
}
