<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('index_number',7)->nullable();
            $table->string('activity_id')->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('date')->nullable();
            $table->string('category_id')->nullable();
            $table->string('start')->nullable();
            $table->string('end')->nullable();
            $table->string('url')->nullable();
            $table->string('location')->nullable();
            $table->enum('status',['0','1']);
            $table->enum('alpha_check',['0','1']);
            $table->enum('photo_upload',['0','1']);
            $table->text('description')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->string('deleted_by',50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
