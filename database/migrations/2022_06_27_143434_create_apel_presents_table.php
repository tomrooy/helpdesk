<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApelPresentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apel_presents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('activity_id')->nullable();
            $table->string('date')->nullable();
            $table->string('nip')->nullable();
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->text('location')->nullable();
            $table->string('signature')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apel_presents');
    }
}
