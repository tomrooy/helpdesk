<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('username',20)->nullable();
            $table->string('nip',20)->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('handphone',20)->nullable();
            $table->string('email_office')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('grade_id',10)->nullable();
            $table->string('group_id',10)->nullable();
            $table->string('title_id',10)->nullable();
            $table->string('location',60)->nullable();
            $table->string('main_location',10)->nullable();
            $table->string('middle_location',10)->nullable();
            $table->string('sub_location',10)->nullable();
            $table->string('photo',10)->nullable();
            $table->string('password');
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->string('deleted_by',50)->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
