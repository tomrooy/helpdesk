<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zooms', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('zoom_id', 20)->nullable();
            $table->string('name', 200)->nullable();
            $table->string('date',50)->nullable();
            $table->string('start',50)->nullable();
            $table->string('end',50)->nullable();
            $table->string('participant',50)->nullable();
            $table->string('host',50)->nullable();
            $table->string('request_by',50)->nullable();
            $table->string('category',50)->nullable();
            $table->string('subcategory',50)->nullable();
            $table->string('request_by_name',50)->nullable();
            $table->text('link')->nullable();
            $table->string('password',20)->nullable();
            $table->text('link_record')->nullable();
            $table->string('password_record',20)->nullable();
            $table->string('attachment',198)->nullable();
            $table->text('description')->nullable();
            $table->string('color')->nullable();
            $table->string('created_by_account',50)->nullable();
            $table->string('method',20)->nullable(); 
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->string('deleted_by',50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zooms');
    }
}
