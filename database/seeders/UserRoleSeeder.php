<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\HelpdeskStatus;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            DB::beginTransaction();
            Role::create([
                'name'  =>  'Super_Admin',
            ]);
            Role::create([
                'name'  =>  'User',
            ]);
            $user = User::create([
                'username'  =>  'tomroy',
                'nip'       =>  '199412152020121001',
                'name'      =>  'Tommy Roy Sirait',
                'email'     =>  'tommy.roy@kemenkumham.go.id',
                'location'  =>  'SIP',
                'password'  =>  '199412152020121001',
                'handphone' =>  '082364464977'
            ]);
            $user->assignRole('Super_Admin');

            $status     =   [
                [
                    'status_id'     =>  1,
                    'name'          => 'Baru',
                    'badge'         => 'badge badge-info',
                    'description'   => 'Baru'
                ],
                [
                    'status_id'     =>  2,
                    'name'          => 'Sedang Dikerjakan',
                    'badge'         => 'badge badge-primary',
                    'description'   => 'Sedang Dikerjakan'
                ],
                [
                    'status_id'     =>  3,
                    'name'          => 'Selesai',
                    'badge'         => 'badge badge-success',
                    'description'   => 'Selesai'
                ],
                [
                    'status_id'     =>  0,
                    'name'          => 'Ditolak',
                    'badge'         => 'badge badge-danger',
                    'description'   => 'Ditolak'
                ]
                
            ];
            foreach($status as $key=> $row)
            {
                HelpdeskStatus::create($row);
            }
            
            DB::commit();
        }catch (\Exception $e)
        {
            DB::rollback();
            echo "\e[31mError Problem: ".$e->getMessage(); 
        }
        
         

    }
}
