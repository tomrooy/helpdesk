<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserLocation;
use DB;

class UserLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            DB::beginTransaction();
            $userlocation = [
                [
                    'location_id'   =>  'Kepegawaian',
                    'name'          =>  'Bagian Kepegawaian',
                    'eselon'        =>  3,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'Umum',
                    'name'          =>  'Bagian Umum',
                    'eselon'        =>  3,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'PHP',
                    'name'          =>  'Bagian Program, Hubungan Masyarakat dan Pelaporan',
                    'eselon'        =>  3,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'SIP',
                    'name'          =>  'Bagian Sistem Informasi Pengawasan',
                    'eselon'        =>  3,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'Keuangan',
                    'name'          =>  'Bagian Keuangan',
                    'eselon'        =>  3,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'ITWIL_I',
                    'name'          =>  'Inspektorat Wilayah I',
                    'eselon'        =>  2,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'ITWIL_II',
                    'name'          =>  'Inspektorat Wilayah II',
                    'eselon'        =>  2,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'ITWIL_III',
                    'name'          =>  'Inspektorat Wilayah III',
                    'eselon'        =>  2,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'ITWIL_IV',
                    'name'          =>  'Inspektorat Wilayah IV',
                    'eselon'        =>  2,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'ITWIL_V',
                    'name'          =>  'Inspektorat Wilayah V',
                    'eselon'        =>  2,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'ITWIL_VI',
                    'name'          =>  'Inspektorat Wilayah VI',
                    'eselon'        =>  2,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'IRJEN',
                    'name'          =>  'Inspektur Jenderal',
                    'eselon'        =>  1,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'location_id'   =>  'SES',
                    'name'          =>  'Sekretaris Inspektorat',
                    'eselon'        =>  2,
                    'status'        =>  1,
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
            ];
            foreach($userlocation as $key=> $row)
            {
                UserLocation::create($row);
            }
            
            DB::commit();
            

        }catch (\Exception $e)
        {
            DB::rollback();
            echo "\e[31mError Problem: ".$e->getMessage(); 
        }
    }
}
