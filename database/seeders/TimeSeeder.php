<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Time;
use DB;

class TimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            DB::beginTransaction();
            $time = [
                [
                    'name'  =>  '07:00'
                ],
                [
                    'name'  =>  '07:30'
                ],
                [
                    'name'  =>  '08:00'
                ],
                [
                    'name'  =>  '08:30'
                ],
                [
                    'name'  =>  '09:00'
                ],
                [
                    'name'  =>  '09:30'
                ],
                [
                    'name'  =>  '10:00'
                ],
                [
                    'name'  =>  '10:30'
                ],
                [
                    'name'  =>  '11:00'
                ],
                [
                    'name'  =>  '11:30'
                ],
                [
                    'name'  =>  '12:00'
                ],
                [
                    'name'  =>  '12:30'
                ],
                [
                    'name'  =>  '13:00'
                ],
                [
                    'name'  =>  '13:30'
                ],
                [
                    'name'  =>  '14:00'
                ],
                [
                    'name'  =>  '14:30'
                ],
                [
                    'name'  =>  '15:00'
                ],
                [
                    'name'  =>  '15:30'
                ],
                [
                    'name'  =>  '16:00'
                ],
                [
                    'name'  =>  '16:30'
                ],
                [
                    'name'  =>  '17:00'
                ],
                [
                    'name'  =>  '17:30'
                ],
                [
                    'name'  =>  '18:00'
                ],
                [
                    'name'  =>  '18:30'
                ],
                [
                    'name'  =>  '19:00'
                ],
                [
                    'name'  =>  '19:30'
                ],
                [
                    'name'  =>  '20:00'
                ],
                [
                    'name'  =>  '20:30'
                ],
                [
                    'name'  =>  '21:00'
                ],
                [
                    'name'  =>  '21:30'
                ],
                

            ];
            foreach($time as $key=> $row)
            {
                Time::create($row);
            }
            
            DB::commit();
        }catch (\Exception $e)
        {
            DB::rollback();
            echo "\e[31mError Problem: ".$e->getMessage(); 
        }
    }
}
