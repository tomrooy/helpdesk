<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ZoomAccount;
use DB;

class ZoomAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            DB::beginTransaction();
            $zoomaccount = [
                [
                    'username'      =>  'Utama',
                    'name'          =>  'Akun Utama',
                    'color'         =>  '#bd1f88',
                    'status'        =>  1,
                    'handphone'     =>  '081333826759',
                    'handle_by'     =>  'Rifaldy',
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'username'      =>  'Rifaldy_A',
                    'name'          =>  'Akun Rifaldi A',
                    'color'         =>  '#2d07d8',
                    'status'        =>  1,
                    'handphone'     =>  '081333826759',
                    'handle_by'     =>  'Rifaldy',
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'username'      =>  'Rohman',
                    'name'          =>  'Akun Rohman',
                    'color'         =>  '#cdee58',
                    'status'        =>  1,
                    'handphone'     =>  '085894581080',
                    'handle_by'     =>  'Rohman',
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'username'      =>  'Rifaldy_B',
                    'name'          =>  'Akun Rifaldy B',
                    'color'         =>  '#a8db4a',
                    'status'        =>  1,
                    'handphone'     =>  '#077a24',
                    'handle_by'     =>  '081333826759',
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'username'      =>  'Ari',
                    'name'          =>  'Akun Ari',
                    'color'         =>  '#ff9100',
                    'status'        =>  1,
                    'handphone'     =>  '081310585961',
                    'handle_by'     =>  'Ari',
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                [
                    'username'      =>  'Tommy',
                    'name'          =>  'Akun Tommy',
                    'color'         =>  '#106976',
                    'status'        =>  1,
                    'handphone'     =>  '082364464977',
                    'handle_by'     =>  'Tommy',
                    'created_by'    =>  'Tommy Roy Sirait',
                ],
                
            ];
            foreach($zoomaccount as $key=> $row)
            {
                ZoomAccount::create($row);
            }
            
            DB::commit();
            

        }catch (\Exception $e)
        {
            DB::rollback();
            echo "\e[31mError Problem: ".$e->getMessage(); 
        }
    }
}
