<script>  
    $(document).ready(function() {
        //data jam dan tanggal
        @include('layouts.sbadmin.partial.clock')
        //menampilkan tabel severside
        var dataTable = $('#category-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('category.data') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'category_id', name: 'category_id'},
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });

        $('#category_id').blur(function(){
            var nik = $('#category_id').val();
            if(nik == '')
            {   
                $('#category_id').addClass('is-invalid');
                $( '#id-error' ).html('data wajib di isi').addClass('alert-danger');
            }else
            {
                $('#category_id').removeClass('is-invalid');
                $( '#id-error' ).html('').removeClass('alert-danger');
            }
           });

        //fungsi simpan data
        $('#formCategory').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            
            $( '#id-error' ).html( "" );
            $( '#name-error' ).html( "" );
            $( '#description-error' ).html( "" );
            

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url:"{{ route('category.store') }}",
                data:formData,
                contentType: false,
                processData: false,
                success:function(data) {
                    if(data.errors) {
                        if(data.errors.category_id){
                            $( '#id-error' ).html( data.errors.category_id[0] ).addClass('alert-danger');
                            $('#category_id').addClass('is-invalid');
                        }
                        if(data.errors.name){
                            $( '#name-error' ).html( data.errors.name[0] );
                        }
                        if(data.errors.description){
                            $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                            
                        }
                        
                    }else
                    {
                        swal({
                            title: "Success!",
                            text: "Berhasil Ditambah",
                            type: "success",
                            timer: 2000
                         });
                        $('#category_id').removeClass('is-invalid');
                        $('#addCategory').modal('hide');
                        document.getElementById("formCategory").reset();
                        $('#category-table').DataTable().ajax.reload();
                        
                        
                        
                    }
                },
                error: function(errors) {
                    console.log('Error:', errors);
                }
            });
        });

        //fungsi close modal
        $('.modelClose').on('click', function(){
            $('#editCategoryData').hide();
        });

        //fungsi nampilkan data di modal
        var id;
        $('body').on('click', '#getEditCategoryData', function(e) {
            // e.preventDefault();
            $('.alert-danger').html('');
            $('.alert-danger').hide();
            id = $(this).data('id');
            $.ajax({
                url: 'category/'+id+'/edit',
                method: 'GET',
                success: function(data) {
                    if(data.status == true)
                    {
                        $('#namaCategory').html(data.result.name);
                        document.getElementById('idCategory').value    =   data.result.id;
                        document.getElementById('formEditNama').value = data.result.name;
                        document.getElementById('formEditCategoryId').value = data.result.category_id;
                        document.getElementById('formEditDescription').value = data.result.description;
                        $('#editCategoryData').show();
                    }else
                    {
                        swal("Error!", "Mohon Hubungi Pihak IT", "error");
                    }
                   
                },
                
            });
        });

        $('#editFormCategory').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $( '#id-error' ).html( "" );
            $( '#name-error' ).html( "" );
            $( '#description-error' ).html( "" );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('category.update') }}",
                type: 'POST',
                data:formData,
                contentType: false,
                processData: false,
                success:function(data) {
                    if(data.errors) {
                        if(data.errors.category_id){
                            $( '#id-error' ).html( data.errors.category_id[0] ).addClass('alert-danger');
                            $('#category_id').addClass('is-invalid');
                        }
                        if(data.errors.name){
                            $( '#name-error' ).html( data.errors.name[0] );
                        }
                        if(data.errors.description){
                            $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                            
                        }
                    }else
                    {
                        if(data.status == true)
                        {
                            swal({
                                title: "Success!",
                                text: "Berhasil Diupdate",
                                type: "success",
                                timer: 2000
                            });
                        }else
                        {
                            swal({
                                title: "Success!",
                                text: data.message,
                                type: "error",
                            });
                        }
                    
                        $('#category_id').removeClass('is-invalid');
                        $('#editCategorypData').modal('hide');
                        document.getElementById("formCategory").reset();
                        $('#category-table').DataTable().ajax.reload();
                    }
                },
                error : function(xhr, status, errorThrown)
                {
                     //Here the status code can be retrieved like;
                    xhr.status;
            
                    //The message added to Response object in Controller can be retrieved as following.
                    xhr.responseText;
                }
             
            });
        });

        //fungsi hapus data
        $(document).on('click', '#deleteCategory', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: "Hapus?",
                text: "Apakah Yakin Menghapus data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {                    
                $.ajax({
                    type: 'POST',
                    url: 'category/'+id+'/delete',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        },
                    dataType: 'JSON',
                    success: function (data) {
                        if(data.status == true)
                        {
                            swal("Done!", data.message, "success");
                            $('#category-table').DataTable().ajax.reload();
                        }else
                        {
                            swal("Error!", data.message, "error");
                        }
                        
                    },
                    error : function(xhr, status, errorThrown)
                    {
                         //Here the status code can be retrieved like;
                        xhr.status;
                
                        //The message added to Response object in Controller can be retrieved as following.
                        xhr.responseText;
                    }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
        

    });  
</script>