@can('category-edit')
    <button type="button" class="btn btn-success btn-sm" id="getEditCategoryData" data-id={{$data->id}} ><i class="fa fa-edit"></i> Edit</button>
@endcan

@can('category-delete')
    <button type="button" class="btn btn-danger btn-sm" id="deleteCategory" data-id={{$data->id}} ><i class="fa fa-trash"></i> Delete</button
@endcan