@extends('layouts.sbadmin.main')
@section('title','Index category')
@section('content')
@push('main-menu')Category @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush    
<div class="wrapper">    
    <div class="card">
        <div class="card-header bg-primary text-white">
            DATA CATEGORY
        </div>
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            <button data-toggle="modal" data-target="#addCategory" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</button>
            <br><br>
            <div class="table-responsive">
                <table id="category-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th width="150" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            @include('category.modal')
        </div>
    </div>
</div>
@endsection
@section('js')
@include('layouts.sbadmin.partial.indexjs')
@include('category.javascript')
@endsection