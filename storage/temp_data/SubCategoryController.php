<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SubCategory;
use App\Models\Category;

use DB;
use Carbon\Carbon;

class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:subcategory-list', ['only' => ['index','getSubCategory']]);
        $this->middleware('permission:subcategory-create', ['only' => ['store']]);
        $this->middleware('permission:subcategory-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:subcategory-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $category    =   Category::all();
        return view('subcategory.index',[
            'category'   =>  $category
        ]);
    }
    public function getSubCategory()
    {
        $data = SubCategory::latest()->get();
        return \DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('Actions', function($data) {
                return '<button type="button" class="btn btn-success btn-sm" id="getEditSubCategoryData" data-id="'.$data->id.'" >Edit</button>
                        <button type="button" class="btn btn-danger btn-sm" id="deleteSubCategory" data-id="'.$data->id.'" ><i class="fa fa-trash"></i> Delete</button';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
    public function store(Request $request)
    {
        $message    =   [
            'subcategory.required'       =>    'Sub Category harus di isi',
            'name.required'                 =>    'Nama Harus di isi',
            'category.required'          =>    'Category ID harus di isi',

        ];
        $validator  =   \Validator::make($request->all(),[
            'subcategory'        =>  'required',
            'category'           =>  'required',
            'name'                  =>  'required',
            'description'           =>  'required',
        ],$message);
        
        if($validator->passes())
        {
            SubCategory::create([
                'subcategory_id'         => $request->subcategory,
                'category_id'            => $request->category,
                'name'                   => $request->name,
                'description'            => $request->description,
                'created_by'             => \Auth::User()->email,
            ]);
        }
        else
        {
            return response()->json(['errors' => $validator->errors()]);
        }
    }
    public function edit($id)
    {
        try{
            $data       =   SubCategory::findOrfail($id);
            $category   =   Category::all();

            if(!$category->isEmpty())
            {
                foreach($category as $item)
                {
                    $output[] = '<option value="'.$item->category_id.'" '.(($item->category_id == $data->category_id) ? 'selected="selected"':"").'>'.$item->name.'</option>';
                }
                
            }
            return response()->json([
                'result'  =>  $data,
                'status'  =>   true,
                'output'  => $output
            ]);
        }catch (\Exception $e)
        {
            return response()->json([
                'result'  =>  $e->getMessage(),
                'status'  =>   false,
                'output'  => $output
            ]);
        }
    }
    public function update(Request $request)
    {
        $message    =   [
            'subcategory_id.required'       =>  'Data ID harus di isi',
        ];
        $validator  =   \Validator::make($request->all(),[
            'subcategory_id'                        =>  'required',
            'name'                                  =>  'required',
            'description'                           =>  'required',
        ],$message);
        if($validator->passes())
        {
            try{
                $subcategory      =   SubCategory::findOrFail($request->id);
                $subcategory->update([
                    'category_id'           => $request->category_id,
                    'subcategory_id'           => $request->subcategory_id,
                    'name'                  => $request->name,
                    'description'           => $request->description,
                    'slug'                  => $request->name,
                    'updated_by'            => \Auth::User()->email,
                ]);
                return response()->json([
                    'status'   =>  true,
                    'message'  =>  $subcategory->name.' updated'
                ]);

            } catch(\Exception $e)
            {
                return response()->json([
                    'status'   =>   false,
                    'message'  =>  $e->getMessage()
                ]);
            }
           
        }
        else
        {
            return response()->json(['errors' => $validator->errors()]);
        }

    }
    public function destroy($id)
    {
        try{
            $data   =   SubCategory::findOrFail($id);
            $data->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
            $data->delete();
            return response()->json([
                'status'   =>  true,
                'message'  =>  $data->name.' delete'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
    }
}
