@extends('layouts.sbadmin.main')
@section('title','Subcategory Main-Page')
@section('content')
    <div class="wrapper">
    @push('main-menu')SubCategory @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Index @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Data SubCategory</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <br>
                @include('subcategory.modal')
                <button data-toggle="modal" data-target="#addSubCategory" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</button>
                <br><br>
                <div class="table-responsive">
                    <table id="subcategory-table" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Sub Category ID</th>
                                <th>Name</th>
                                <th>Category ID</th>
                                <th>Description</th>
                                <th width="150" class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('js')
@include('layouts.sbadmin.partial.indexjs')
@include('subcategory.javascript')
@endsection