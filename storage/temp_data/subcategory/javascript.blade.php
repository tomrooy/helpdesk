<script>
    $(document).ready(function() {        
        var interval = setInterval(function() {
            var momentNow = moment();
            $('#date-part').html(momentNow.format('dddd')
                                .substring(0,9).toUpperCase()+' '+ momentNow.format('DD MMMM YYYY'));
            $('#time-part').html(momentNow.format('HH:mm:ss A'));
        }, 100);
    
        var dataTable = $('#subcategory-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('subcategory.data') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'subcategory_id', name :'subcategory'},
                {data: 'name', name: 'Nama Sub Category'},
                {data: 'category_id', name: 'Parent Category'},
                {data: 'description', name: 'description'},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });
       
        $('.modelClose').on('click', function(){
            $('#addSubCategory').hide();
            $('#editSubCategoryData').hide();

        });
        $('#formSubCategory').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            
            //$( '#id-error' ).html( "" );
            // $( '#name-error' ).html( "" );
            //$( '#description-error' ).html( "" );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url:"{{ route('subcategory.store') }}",
                data:formData,
                contentType: false,
                processData: false,
                success:function(data) {
                    if(data.errors) {
                        if(data.errors.category_id){
                            $( '#id-error' ).html( data.errors.category_id[0] ).addClass('alert-danger');
                            $('#category_id').addClass('is-invalid');
                        }
                        if(data.errors.subcategory_id){
                            $( '#id-error' ).html( data.errors.subcategory_id[0] ).addClass('alert-danger');
                            $('#subcategory_id').addClass('is-invalid');
                        }
                        if(data.errors.name){
                            $( '#name-error' ).html( data.errors.name[0] );
                        }
                        if(data.errors.description){
                            $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                            
                        }
                        
                    }else
                    {
                        swal({
                            title: "Success!",
                            text: "Berhasil Ditambah",
                            type: "success",
                            timer: 2000
                         });
                         $('#subcategory-table').DataTable().ajax.reload();
                        $('#addSubCategory').modal('hide');
                        document.getElementById("formCategory").reset();
                        
                    }
                },
                error: function(errors) {
                    console.log('Error:', errors);
                }
            });
        });
        $('body').on('click', '#getEditSubCategoryData', function(e) {
            // e.preventDefault();
            $('.alert-danger').html('');
            $('.alert-danger').hide();
            id = $(this).data('id');
            $.ajax({
                url: 'subcategory/'+id+'/edit',
                method: 'GET',
                success: function(data) {
                    if(data.status == true)
                    {
                        //console.log(data.output);
                        $('#namaCategory').html(data.result.name);
                        document.getElementById('idSubCategory').value    =   data.result.id;
                        document.getElementById('formEditSubCategoryId').value = data.result.subcategory_id;
                        document.getElementById('formEditNama').value = data.result.name;
                        document.getElementById('formEditDescription').value = data.result.description;
                        $('select[name="category_id"]').empty();
                        var category    = data.output;
                        for(c in category)
                        {
                            $('select[name="category_id"]').append(category[c])
                        }
                        
                        $('#editSubCategoryData').show();
                    }else
                    {
                        swal("Error!", "Mohon Hubungi Pihak IT", "error");
                    }
                   
                },
                
            });
        });
        $('#editFormSubCategory').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('subcategory.update') }}",
                type: 'POST',
                data:formData,
                contentType: false,
                processData: false,
                success:function(data) {
                    if(data.errors) {
                        if(data.errors.category_id){
                            $( '#id-error' ).html( data.errors.category_id[0] ).addClass('alert-danger');
                            $('#category_id').addClass('is-invalid');
                        }
                        if(data.errors.subcategory_id){
                            $( '#id-error' ).html( data.errors.subcategory_id[0] ).addClass('alert-danger');
                            $('#subcategory_id').addClass('is-invalid');
                        }
                        if(data.errors.name){
                            $( '#name-error' ).html( data.errors.name[0] );
                        }
                        if(data.errors.description){
                            $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                            
                        }
                    }else
                    {
                        if(data.status == true)
                        {
                            swal({
                                title: "Success!",
                                text: data.message,
                                type: "success",
                                timer: 2000
                            });
                        }else
                        {
                            swal({
                                title: "Success!",
                                text: data.message,
                                type: "error",
                            });
                        }
                        $('#editSubCategoryData').modal('hide');
                        document.getElementById("editFormSubCategory").reset();
                        $('#subcategory-table').DataTable().ajax.reload();
                    }
                }
             
            });
        });
        $(document).on('click', '#deleteSubCategory', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: "Hapus?",
                text: "Apakah Yakin Menghapus data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {                    
                $.ajax({
                    type: 'POST',
                    url: 'subcategory/'+id+'/delete',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        },
                    dataType: 'JSON',
                    success: function (data) {
                        if(data.status == true)
                        {
                            swal("Done!", data.message, "success");
                            $('#subcategory-table').DataTable().ajax.reload();
                        }else
                        {
                            swal("Error!", data.message, "error");
                        }
                        
                    },
                    error : function(xhr, status, errorThrown)
                    {
                         //Here the status code can be retrieved like;
                        xhr.status;
                
                        //The message added to Response object in Controller can be retrieved as following.
                        xhr.responseText;
                    }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
            
    
    });  
    </script>