<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\GroupController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SubCategoryController;
use App\Http\Controllers\Admin\HelpdeskController;
use App\Http\Controllers\Admin\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/helpdesk',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'create'])->name('front.helpdeskalpha.index');
Route::get('/getsubcategory/{id}',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'getSubcategory']);
Route::post('/helpdesk/simpan',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'store'])->name('front.helpdeskalpha.store');
Route::get('/reload-captcha', [App\Http\Controllers\Front\HelpdeskAlphaController::class, 'reloadCaptcha']);
Route::get('/search-name',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'searchName'])->name('search_name');
Route::get('/search-ticket',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'searchTicket'])->name('search_ticket');
Route::get('/get_ticket/{id}',[App\Http\Controllers\Front\HelpdeskAlphaController::class, 'getTicket'])->name('get_ticket');

Route::get('/', function(){
    return redirect('/login');
})->name('home');
Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {
    
    Route::get('/',[HomeController::class, 'index'])->name('home');
    Route::resource('/user', UserController::class);


    Route::get('/golongan',[GroupController::class, 'index'])->name('group.index');
    Route::post('/golongan/simpan',[GroupController::class, 'store'])->name('group.store');
    Route::get('/golongan/{id}/edit',[GroupController::class,'edit'])->name('group.edit');
    Route::post('/golongan/update',[GroupController::class,'update'])->name('group.update');
    Route::post('/golongan/{id}/delete',[GroupController::class,'destroy'])->name('group.delete');

    Route::get('/getcategory',[CategoryController::class, 'getCategory'])->name('category.data');
    Route::get('/geteditformcategory',[CategoryController::class, 'getEditCategory'])->name('geteditformcategory.data');
    Route::get('/category',[CategoryController::class, 'index'])->name('category.index');
    Route::post('/category/simpan',[CategoryController::class,'store'])->name('category.store');
    Route::get('/category/{id}/edit',[CategoryController::class,'edit'])->name('category.edit');
    Route::post('/category/update',[CategoryController::class,'update'])->name('category.update');
    Route::post('/category/{id}/delete',[CategoryController::class,'destroy'])->name('category.delete');
    

    
    Route::get('/gethelpdesk',[HelpdeskController::class, 'getHelpdesk'])->name('helpdesk.data');
    Route::get('/helpdesk',[HelpdeskController::class, 'index'])->name('helpdesk.index');
    Route::get('/helpdesk/{id}/detail-data',[HelpdeskController::class, 'detail'])->name('helpdesk.detail');
    Route::get('/helpdesk/{id}/process-helpdesk',[HelpdeskController::class, 'processHelpdesk'])->name('helpdesk.process');
    Route::post('/helpdesk/{id}/delete',[HelpdeskController::class,'destroy'])->name('helpdesk.delete');
    Route::post('/helpdesk/process-helpdesk/action',[HelpdeskController::class, 'actionProcessHelpdesk'])->name('helpdesk.process.action');
    route::get('helpdesk/exportpdf/{id}',[HelpdeskController::class, 'exportPDF'])->name('helpdesk.exportPDF');

    route::get('/home/chart/helpdesk',[HomeController::class,'helpdeskChart'])->name('helpdesk.chart');

    Route::get('/subcategory',[SubCategoryController::class, 'index'])->name('subcategory.index');
    Route::get('/getsubcategory',[ SubCategoryController::class, 'getSubCategory'])->name('subcategory.data');
    Route::post('/subcategory/simpan',[SubCategoryController::class, 'store'])->name('subcategory.store');
    Route::get('/subcategory/{id}/edit',[SubCategoryController::class,'edit'])->name('subcategory.edit');
    Route::post('/subcategory/update',[SubCategoryController::class,'update'])->name('subcategory.update');
    Route::post('/subcategory/{id}/delete',[SubCategoryController::class,'destroy'])->name('subcategory.delete');


    route::get('/roles',[RoleController::class,'index'])->name('role.index');
    route::post('/roles/simpan',[RoleController::class,'store'])->name('role.store');
    route::delete('/roles/{id}/delete',[RoleController::class,'destroy'])->name('role.destroy');

    Route::post('/role/permission', [RoleController::class,'addPermission'])->name('role.add_permission');
    Route::get('/role/role-permission', [RoleController::class,'rolePermission'])->name('role.roles_permission');
    Route::put('/role/permission/{role}', [RoleController::class,'setRolePermission'])->name('role.setRolePermission');
    Route::delete('/role/permission/{id}/delete', [RoleController::class,'deletePermission'])->name('role.setpermission.destroy');


});
Route::get('/personal/gethelpdesk',[App\Http\Controllers\Front\HelpdeskController::class, 'getHelpdesk'])->name('front.helpdesk.data');
Route::group(['prefix' => 'personal','middleware' => 'auth'], function() {

    Route::get('/helpdesk',[App\Http\Controllers\Front\HelpdeskController::class, 'index'])->name('front.helpdesk.index');
    Route::get('/helpdesk/detail/{id}',[App\Http\Controllers\Front\HelpdeskController::class, 'detail'])->name('front.helpdesk.detail');
    Route::get('/helpdesk/create',[App\Http\Controllers\Front\HelpdeskController::class, 'create'])->name('front.helpdesk.create');
    Route::post('/helpdesk/simpan',[App\Http\Controllers\Front\HelpdeskController::class, 'store'])->name('front.helpdesk.store');
    Route::get('/getsubcategory/{id}',[CategoryController::class, 'getSubcategory'])->name('getsubcategory');
});
