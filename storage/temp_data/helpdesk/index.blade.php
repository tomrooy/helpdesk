@extends('layouts.sbadmin.main')
@section('title','Data Helpdesk')
@section('content')
<div class="wrapper">
    @push('main-menu')Helpdesk @endpush
    @push('url-main-menu') {{route('helpdesk.index')}} @endpush
    @push('sub-menu')Index @endpush       
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data Permintaan Helpdesk
        </div>
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            <button data-toggle="modal" data-target="#addHelpdesk" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</button>
            <br><br>
            <div class="table-responsive">
                <table id="helpdesk-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ticket Number</th>
                            <th>Permasalahan</th>
                            <th>Direquest Oleh</th>
                            <th>Status</th>
                            <th width="150" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            @include('category.modal')
        </div>
    </div>
</div>
@endsection
@section('js')
@include('layouts.sbadmin.partial.indexjs')
@include('helpdesk.javascript')

@endsection