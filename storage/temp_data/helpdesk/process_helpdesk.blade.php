@extends('layouts.sbadmin.main')
@section('title','Proses Helpdesk')
@section('content')
@push('main-menu')Helpdesk @endpush
@push('url-main-menu'){{route('helpdesk.index')}} @endpush
@push('sub-menu')Proses helpdesk @endpush
<div class="wrapper">
    <div class="card">
        <div class="card-header">
            Proses Helpdesk
        </div>
        <div class="card-body">
            @include('layouts.sbadmin.components.clock')
            <form method="POST" id="formProcessHelpdesk" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$data->id}}" name="id">
                <div class="row">
                    <div class="col-md-6"><hr>
                        <h3 class="flex text-center">Data Permintaan Helpdesk</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input disabled type="text" value="{{$data->users->name}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>NIP</label>
                                    <input disabled type="text" value="{{$data->request_by}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Judul</label>
                            <input disabled type="text" class="form-control" value="{{$data->title}}">
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Category</label>
                                    <input disabled type="text" class="form-control" value="{{$data->categories->name}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>SubCategory</label>
                                    <input disabled type="text" class="form-control" value="{{$data->subcategories->name}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Prioritas</label>
                                    <input disabled type="text" class="form-control" value="{{$data->priority_id}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Keteraangan Lengkap</label>
                            <textarea class="form-control" disabled >
                                @foreach ($data->helpdesk_details as $item)
                                    @if($item->created_by == $data->request_by)
                                        {{$item->description}}
                                    @endif
                                @endforeach
                            </textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h3>FOTO LAMPIRAN 1</h3>
                                @foreach ($data->helpdesk_details as $item)
                                    @if($item->created_by == $data->request_by)
                                        @if(!empty($item->image_a))
                                            <img src={{asset('storage/uploads/helpdesks/'.$item->image_a)}} width="130px" height="160px">
                                            <a href="{{asset('storage/uploads/helpdesks/'.$item->image_a)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                            @else
                                            <button class="btn btn-warning"> Tidak Ada Foto yang diupload</button>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                            <div class="col-md-6">
                                <h3>FOTO LAMPIRAN 2</h3>
                                @foreach ($data->helpdesk_details as $item)
                                    @if($item->created_by == $data->request_by)
                                        @if(!empty($item->image_a))
                                        <img src={{asset('storage/uploads/helpdesks/'.$item->image_b)}} width="130px" height="160px">
                                        <a href="{{asset('storage/uploads/helpdesks/'.$item->image_b)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a>
                                        @else
                                            <button class="btn btn-warning"> Tidak Ada Foto yang diupload</button>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-md-6"><hr>
                        <h3 class="flex text-center">Proses Permintaan Helpdesk</h3>
                        <hr>
                        <div class="form group">
                            <label>Pilih Proses pengerjaan</label>
                            <select class="form-control" name="process" id="process">
                                <option value="">Pilih Proses</option>
                                <option value="2">Sedang Dikejakan</option>
                                <option value="3">Selesai</option>
                                <option value="0">Ditolak</option>
                            </select>
                            <strong id="process-error"></strong>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea class="form-control" id="description"  name="description" placeholder="silahkan masukan keterangan mengenai pengerjaan"></textarea>
                            <strong id="description-error"></strong>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="img1">Attach Image 1</label>
                                    <input type="file" name="img1" autocomplete="off" type="text"  class="form-control" >
                                    <div class="text-danger">*Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                                    <strong id="img1-error"></strong>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="img2">Attach Image 2</label>
                                    <input type="file" name="img2" autocomplete="off" type="text"  class="form-control" >
                                    <div class="text-danger">*Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                                    <strong id="img2-error"></strong>
                                </div>
                            </div>
                        </div>
                        <div class="col text-center">
                            <button type="submit" class="btn btn-info"><i class="fas fa-save"></i> Simpan</button>
                            <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                        </div>
                    </div>
                    
                </div>
    
            </form>

        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function() {
        @include('layouts.sbadmin.partial.clock')
        $('#formProcessHelpdesk').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            swal({
                title: "Konfirmasi",
                text: "Proses Helpdesk?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('helpdesk.process.action') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    success:function(data) {
                        if(data.errors) {
                            if(data.errors.name){
                                $( '#process' ).html( data.errors.process[0] ).addClass('alert-danger');
                            }
                            if(data.errors.description){
                                $( '#process' ).html( data.errors.description[0] ).addClass('alert-danger');
                            }
                            if(data.errors.img1){
                                $( '#img1-error' ).html( data.errors.img1[0] ).addClass('alert-danger');
                                $( '.img1-error' ).addClass('is-invalid');
                            }
                            if(data.errors.img1){
                                $( '#img2-error' ).html( data.errors.img2[0] ).addClass('alert-danger');
                            }
                            
                        }else
                        {
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 2000
                                });
                                document.getElementById("formProcessHelpdesk").reset();
                                //window.open("{{ route('front.helpdesk.index') }}");
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
    });
</script>
@endsection