@extends('layouts.sbadmin.main')
@section('title','Detail helpdesk')
@section('content')
@push('main-menu')Helpdesk @endpush
@push('url-main-menu'){{route('helpdesk.index')}} @endpush
@push('sub-menu')Detail @endpush
    <div class="wrapper">
        <div class="card">
            <div class="card-header">
                <h2><strong>Data Detail Helpdesk</strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <br>
               
                <a href="{{route('helpdesk.index')}}" class="btn btn-secondary"><span class="fa fa-arrow-circle-left"></span> Kembali</a>
                <a href="{{route('helpdesk.exportPDF',$helpdesk->id)}}" class="btn btn-success float-right"><i class="far fa-file-excel"></i> Export</a>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                        
                        <div class="card">
                            <div class="card-header">
                                <h3>Data Main Helpdesk</h3>
                            </div>
                            
                            <div class="card-body">
                                <div class="table table-responsive-md">
                                    <table class="table table-borderless">
                                        <tr>
                                            <td>Nip</td>
                                            <td>:</td>
                                            <td>{{$helpdesk->request_by}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>{{!empty($helpdesk->users->name) ? $helpdesk->users->name : 'User kosong' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Ticket Number</td>
                                            <td>:</td>
                                            <td>{{$helpdesk->ticket_number}}</td>
                                        </tr>
                                        <tr>
                                            <td>Category</td>
                                            <td>:</td>
                                            <td>{{!empty($helpdesk->categories->name) ? $helpdesk->categories->name : 'category kosong' }}</td>
                                        </tr>
                                        <tr>
                                            <td>SubCategory</td>
                                            <td>:</td>
                                            <td>{{!empty($helpdesk->subcategories->name) ? $helpdesk->subcategories->name : 'subcategory kosong' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Terbaru</td>
                                            <td>:</td>
                                            <td><h3><span class="{{!empty($helpdesk->statuses->badge) ? $helpdesk->statuses->badge : ''}}">{{!empty($helpdesk->statuses->name) ? $helpdesk->statuses->name : 'status kosong'}}</span></h3></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3>Data Detail Helpdesk</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Description</td>
                                        <td>:</td>
                                        <td>
                                            @foreach($helpdesk->helpdesk_details as $item)
                                            <li>{{!empty($item->description) ? $item->description : 'kosong' }}</li><br>
                                            @endforeach
                                        </td>
                                    </tr>
                                </table>
                               <div class="row">
                                @foreach($helpdesk->helpdesk_details as $item)
                                    @if($item->created_by == $helpdesk->request_by)
                                        @if(!empty($item->image_a) || !empty($item->image_b))
                                            <div class="col-md-6">
                                                <h3>Lampiran dari Pengguna</h3>
                                            <li><img src={{asset('storage/uploads/helpdesks/'.$item->image_a)}} width="130px" height="160px">
                                                <a href="{{asset('storage/uploads/helpdesks/'.$item->image_a)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a></li><br>
                                            <li><img src={{asset('storage/uploads/helpdesks/'.$item->image_b)}} width="130px" height="160px">
                                                <a href="{{asset('storage/uploads/helpdesks/'.$item->image_b)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a></li>
                                            </div>
                                        @endif
                                    @else
                                        @if($helpdesk->handle_by == $item->created_by && (!empty($item->image_a) || !empty($item->image_b)))
                                            <div class="col-md-6">
                                                <h3>Lampiran dari IT Team</h3>
                                            <li><img src={{asset('storage/uploads/helpdesks/'.$item->image_a)}} width="130px" height="160px">
                                                <a href="{{asset('storage/uploads/helpdesks/'.$item->image_a)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a></li><br>
                                            <li><img src={{asset('storage/uploads/helpdesks/'.$item->image_b)}} width="130px" height="160px">
                                                <a href="{{asset('storage/uploads/helpdesks/'.$item->image_b)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a></li>
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
               
               
            </div>
        </div>    
    </div>    
@endsection
@section('css')
@endsection
@section('js')
@include('layouts.sbadmin.partial.indexjs')
@endsection