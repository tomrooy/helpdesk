<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Helpdesk;
use App\Models\HelpdeskDetail;
use DB;
use PDF;

class HelpdeskController extends Controller
{
    public function __construct()
    {
         $this->middleware('permission:admin-helpdesk-list', ['only' => ['index','getHelpdesk']]);
         $this->middleware('permission:helpdesk-create', ['only' => ['store']]);
         $this->middleware('permission:helpdesk-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:admin-helpdesk-delete', ['only' => ['destroy']]);
    }
    public function getHelpdesk()
    {
        $data   =   Helpdesk::all();
        return \DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('request_by',function($data){
                return $data->users->name;
            })
            ->editColumn('status',function($data){
                $badge  = !empty($data->statuses->badge) ? $data->statuses->badge : '';
                $badge_name = !empty($data->statuses->name) ? $data->statuses->name : 'status kosong';
                return '<h3><span class="'.$badge.'">'.$badge_name.'</span> </h3>';
                //return $data->statuses->name;
            })
            ->addColumn('Actions', function($data) {
                $detail     =   '<a href="'.route('helpdesk.detail', $data->id).'" class="btn btn-info btn-sm"><i class="fas fa-eye"></i> Detail</a>';
                $process    =   '<a href="'.route('helpdesk.process', $data->id).'" type="button" class="btn btn-success btn-sm" id="process_helpdesk" data-id="'.$data->id.'" ><i class="fas fa-cogs"></i> Proses</a>';
                $delete     =   '<button type="button" class="btn btn-danger btn-sm" id="deleteHelpdesk" data-id="'.$data->id.'" ><i class="fa fa-trash"></i> Delete</button';
                switch($data->status)
                {
                    case 1:
                        return $detail.$process.$delete;
                        break;
                    case 2:
                        return $detail.$process.$delete;
                        break;
                    case 3:
                        return $detail.$delete;
                        break;
                    default:
                        return $detail;
                        break;
                }
                
            })
            ->rawColumns(['Actions','status'])
            ->make(true);
    }
    public function index()
    {
        return view('helpdesk.index');
    }
    public function processHelpdesk($id)
    {
        $data   = Helpdesk::findOrFail($id);
       return view('helpdesk.process_helpdesk',[
            'data'  =>  $data
        ]);
    }
    public function actionProcessHelpdesk(Request $request)
    {
        try{
            $message    =   [
                'category_id.required'     =>  'Data ID harus di isi',
            ];
            $validator  =   \Validator::make($request->all(),[
                'process'                               =>  'required',
                'description'                           =>  'required',
            ],$message);
            
            if($validator->passes())
            {   
                $helpdesk = Helpdesk::findOrFail($request->id);
                $ticket     =   $helpdesk->ticket_number.'-'.$request->process;
                if($request->img1) {
                    $imagePath = $request->img1;
                    $imageName1 = '1'.$ticket.'.'.$request->img1->getClientOriginalExtension();
                
                    $path = $request->img1->storeAs('uploads/helpdesks', $imageName1, 'public');
                }else
                {
                    $imageName1 = null;
                }
                if($request->img2) {
                    $imagePath = $request->img2;
                    $imageName2 = '2'.$ticket.'.'.$request->img2->getClientOriginalExtension();
                    $path = $request->img2->storeAs('uploads/helpdesks', $imageName2, 'public');
                }else
                {
                    $imageName2 = null;
                }
                DB::beginTransaction();
                $helpdesk->update([
                    'status'    =>  $request->process
                ]);
                $helpdesk->helpdesk_details()->create([
                    'status_processes_id'   => $request->process,
                    'description'           => $request->description,
                    'image_a'               =>  $imageName1,
                    'image_b'               =>  $imageName2,
                    'created_by'            =>  \Auth::User()->nip
                ]);
                DB::commit();
            }
            else
            {
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function detail($id)
    {
        $data   = Helpdesk::findOrFail($id);
        return view('helpdesk.detail',[
            'helpdesk' => $data
        ]);
    }
    public function destroy($id)
    {
        try{
            $data   =   helpdesk::findOrFail($id);
            $data->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
            $data->helpdesk_details()->delete();
            $data->delete();
            
            return response()->json([
                'status'   =>  true,
                'message'  =>  $data->name.' delete'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
    }
    public function exportPDF($id)
    {
        $helpdesk   = Helpdesk::findOrFail($id);
        

        $pdf = PDF::loadView('helpdesk.export',[
            'helpdesk' => $helpdesk
        ])->setPaper('A5', 'potrait');
        $now    =   \Carbon\Carbon::now()->format('H:m');
        return $pdf->stream('pdf_file-'.$now.'.pdf');
    }
}
