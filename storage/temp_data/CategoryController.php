<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
Use DB;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function __construct()
    {
         $this->middleware('permission:category-list', ['only' => ['index','getCategory']]);
         $this->middleware('permission:category-create', ['only' => ['store']]);
         $this->middleware('permission:category-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:category-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        return view('category.index');
    }
    public function getCategory()
    {
        $data = Category::latest()->get();
        return \DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('Actions', function ($data){
                $edit = '<button type="button" class="btn btn-success btn-sm" id="getEditCategoryData" data-id="'.$data->id.'" ><i class="fa fa-edit"></i> Edit</button>';
                $delete = '<button type="button" class="btn btn-danger btn-sm" id="deleteCategory" data-id="'.$data->id.'" ><i class="fa fa-trash"></i> Delete</button>' ;
                if(\Auth::User()->can('category-edit') && \Auth::User()->can('category-delete'))
                {   
                    return $edit.$delete;
                    
                }elseif(\Auth::User()->can('category-edit'))
                {
                    return $edit;
                }elseif(\Auth::User()->can('category-delete'))
                {   
                    return $delete;
                    
                }else{
                    return '';
                }   
               
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
    public function store(Request $request)
    {
        try{
            $message    =   [
                'category_id.required'     =>  'Data ID harus di isi',
            ];
            $validator  =   \Validator::make($request->all(),[
                'category_id'                       =>  'required',
                'name'                              =>  'required',
                'description'                       =>  'required',
            ],$message);
            
            if($validator->passes())
            {   
                
                Category::create([
                    'category_id'           => $request->category_id,
                    'name'                  => $request->name,
                    'description'           => $request->description,
                    'slug'                  => $request->name,
                    'created_by'            => \Auth::User()->email,
                ]);
            }
            else
            {
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function edit($id)
    {
        try{
            $data = Category::findOrFail($id);
            
            return response()->json([
                'result'  =>  $data,
                'status'  =>   true,
            ]);
        }catch (\Exception $e)
        {
            return response()->json([
                'result'  =>  $e->getMessage(),
                'status'  =>   false
            ]);
        }
        
    }
    public function update(Request $request)
    {
      
        $message    =   [
            'category_id.required'     =>  'Data ID harus di isi',
        ];
        $validator  =   \Validator::make($request->all(),[
            'category_id'                      =>  'required',
            'name'                          =>  'required',
            'description'                   =>  'required',
        ],$message);
        if($validator->passes())
        {
            try{
                $category      =   Category::findOrFail($request->id);
                $category->update([
                    'category_id'           => $request->category_id,
                    'name'                  => $request->name,
                    'description'           => $request->description,
                    'slug'                  => $request->name,
                    'updated_by'            => \Auth::User()->email,
                ]);
                return response()->json([
                    'status'   =>  true,
                    'message'  =>  $category->name.' delete'
                ]);

            } catch(\Exception $e)
            {
                return response()->json([
                    'status'   =>   false,
                    'message'  =>  $e->getMessage()
                ]);
            }
           
        }
        else
        {
            return response()->json(['errors' => $validator->errors()]);
        }

    }
    public function destroy($id)
    {
        try{
            $data   =   Category::findOrFail($id);
            $data->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
            $data->delete();
            return response()->json([
                'status'   =>  true,
                'message'  =>  $data->name.' delete'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
    }
    public function getSubcategory($id)
    {
        $data   =   SubCategory::where('category_id',$id)->pluck('subcategory_id','name');
        return json_encode($data);
    }
    public function getEditCategory()
    {
        $data   =   Category::all();
        $output = '';
        if(!$data->isEmpty())
        {
            $output .= '<option value="'.$brand_id.'" '.(($brand_id == $id) ? 'selected="selected"':"").'>'.$brand_name.'</option>';
        }
        return response()->json([
            
            'result'  =>  $data
        ]);
    }
}
