@extends('layouts.nice.main')
@section('title','Download Report Page')
@section('css')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<style>
    .select2{
        width:100%!important;
        
        } 
</style>
@endsection
@section('content')
    <div class="wrapper">
    @push('main-menu')Admin @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Download Report @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Form Download Report </strong></h2>
            </div>    
            <div class="card-body">
                <div>
                    <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                    <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
                </div>
                <form method="POST" action="{{route('helpdesk.report.process')}}" enctype="multipart/form-data" class="row g-3">
                    @csrf
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input autocomplete="off" type="date" name="start" value="{{old('start')}}" id="start" class="form-control {{$errors->first('start') ? 'is-invalid': ''}}" >
                                <label>Tanggal Awal</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('start')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input autocomplete="off" type="date" name="end" value="{{old('end')}}" id="end" class="form-control {{$errors->first('end') ? 'is-invalid': ''}}" >
                                <label>Tanggal Akhir</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('end')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-floating">
                                <select name="type" class="form-control {{$errors->first('type') ? "is-invalid" : ""}}">
                                    <option value="all">--Pilih Data --</option>
                                    <option value="helpdesk" @if(old('type') == 'helpdesk') {{'selected'}} @endif>Data Helpdesk</option>
                                    <option value="zoom" @if(old('type') == 'zoom') {{'selected'}} @endif>Data Zoom</option>
                                </select>
                                <label><b>Data Yang di export</b></label>
                                <div class="invalid-feedback">
                                    {{$errors->first('type')}}
                                </div>
                            </div>
                        </div>
                    <br>
                    <div class="col text-center">
                        <button type="submit" class="btn btn-info"><span class="bx bx-search"></span> Cari</button>
                        <button type="reset" class="btn btn-warning"><span class="bx bx-recycle"></span> Reset</button>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
@endsection