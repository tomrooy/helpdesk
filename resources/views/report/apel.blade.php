<table>
    <tr>
        <td colspan="6"> Daftar Pegawai yang Hadir Kegiatan </td>
    </tr>
    <tr>
        <td>NO</td>
        <td>NIP</td>
        <td>Nama</td>
        <td>Unit Kerja</td>
        <td>Jabatan</td>
        <td>Timestamp</td>
    </tr>
    @php
        $no = 1 ;
    @endphp
    @foreach ($data as $item)
    <tr>
        <td>{{$no}}</td>
        <td>'{{$item->nip}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->users->location}}</td>
        <td>{{$item->title}}</td>
    </tr>
    @php
        $no++;
    @endphp
    @endforeach
</table>