<table>
    <tr>
        <td colspan="6"> Daftar Pegawai yang Hadir Kegiatan </td>
    </tr>
    <tr>
        <td>NO</td>
        <td>NIP</td>
        <td>Nama</td>
        <td>Unit Kerja</td>
        <td>Jabatan</td>
        <td>Timestamp</td>
    </tr>
    @php
        $no = 1 ;
    @endphp
    @foreach ($data as $item)
    <tr>
        <td>{{$no}}</td>
        <td>'{{$item->nip}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->location}}</td>
        <td>{{$item->title}}</td>
        <td>{{$item->date}}</td>
    </tr>
    @php
        $no++;
    @endphp
    @endforeach
    <tr>
        <td colspan="6"> Daftar Pegawai yang Tidak Hadir Kegiatan </td>
    </tr>
    <tr>
        <td>NO</td>
        <td>NIP</td>
        <td>Nama</td>
        <td>Unit Kerja</td>       
    </tr>
    @php
        $no = 1 ;
    @endphp
    @foreach (json_decode($not_present) as $item)
    <tr>
        <td>{{$no}}</td>
        <td>'{{$item->nip}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->location}}</td>        
    </tr>
    @php
        $no++;
    @endphp
    @endforeach
</table>