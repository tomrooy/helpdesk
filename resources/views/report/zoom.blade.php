<table>
    <tr>
        <td>NO</td>
        <td>Nama Kegiatan</td>
        <td>Tanggal</td>
        <td>Jam</td>
        <td>Permintaan Dari</td>
        <td>Category</td>
        <td>Zoom Id</td>
        <td>Link</td>
        <td>Password</td>
        <td>Link Record</td>
        <td>Password Record</td>
        <td>Akun</td>
        <td>Dibuat</td>
    </tr>
    @php
        $no = 1 ;
    @endphp
    @foreach ($data as $item)
    <tr>
        <td>{{$no}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->date}}</td>
        <td>{{$item->start.'-'.$item->end}}</td>
        <td>{{$item->request_by}}</td>
        <td>{{$item->category}}</td>
        <td>{{$item->zoom_id}}</td>
        <td>{{$item->link}}</td>
        <td>{{$item->password}}</td>
        <td>{{$item->link_record}}</td>
        <td>{{$item->password_record}}</td>
        <td>{{$item->created_by_account}}</td>
        <td>{{$item->created_at}}</td>
    </tr>
        @php
            $no++;
        @endphp
    @endforeach
</table>