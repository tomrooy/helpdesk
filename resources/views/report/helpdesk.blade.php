<table>
    <tr>
        <td>NO</td>
        <td>Nomor Tiket</td>
        <td>Tanggal</td>
        <td>Nama</td>
        <td>NIP</td>
        <td>Judul</td>
        <td>Category</td>
        <td>SubCategory</td>
        <td>Ditangani Oleh</td>
        <td>Deskripsi</td>
    </tr>
    @php
        $no = 1 ;
    @endphp
    @foreach ($data as $item)
    <tr>
        <td>{{$no}}</td>
        <td>{{$item->title}}</td>
        <td>{{$item->date}}</td>
        <td>{{$item->ticket_number}}</td>
        <td>{{$item->request_by}}</td>
        <td>{{$item->title}}</td>
        <td>{{$item->category}}</td>
        <td>{{$item->subcategory}}</td>
        <td>{{$item->handle_by}}</td>
        <td>{{$item->description}}</td>         
    </tr>
    @php
        $no++;
    @endphp
    @endforeach
</table>