@extends('layouts.nice.main')
@section('title','Data Helpdesk')
@push('nav-helpdesk') show @endpush
@section('content')
<div class="wrapper">
    @push('main-menu')Helpdesk @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Index @endpush   
    
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data Permintaan Helpdesk
        </div>
        <div class="card-body">
            <br>
            <a href="{{route('front.helpdesk.create')}}" class="btn btn-info"><i class="bi bi-file-plus-fill"></i>Tambah Data</a>
            <br><br>
            <div class="table-responsive">
                <table id="helpdesk-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ticket Number</th>
                            <th>Judul</th>
                            <th>Kategory/SubKategory </th>
                            <th>Status</th>
                            <th>Dikerjakan Oleh</th>
                            <th width="150" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
           
        </div>
    </div>
</div>
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
@include('front.helpdesk.javascript')
@endsection