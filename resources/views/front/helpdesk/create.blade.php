@extends('layouts.nice.main')
@section('title','Input Helpdesk Baru')
@push('nav-helpdesk') show @endpush
@section('content')
@push('main-menu')Helpdesk @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Input Data @endpush   
<div class="card shadow mb-12">
    <div class="card-header py-3 bg-primary">
        <h6 class="m-0 font-weight-bold text-light">Form Layanan Helpedesk IT</h6>
    </div>      
    <div class="card-body">
        <br>
        <form method="POST" id="formHelpdesk" enctype="multipart/form-data" class="row g-3">
            @csrf
            <div class="col-md-4">
                <div class="form-floating">
                  <input type="text" class="form-control" id="floatingName" placeholder="Your Name" value="{{\Auth::User()->name}}" disabled>
                  <label for="floatingName">Nama</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                  <input type="text" class="form-control" id="floatingName" placeholder="Your Name" value="{{\Auth::User()->nip}}" disabled>
                  <label for="floatingNip">NIP</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                  <input type="text" class="form-control" id="floatingName" placeholder="Your Name" value="{{\Auth::User()->userLocations->name}}" disabled>
                  <label for="floatingUnit">Unit Kerja</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-floating">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Silahkan Isi Judul helpdesk anda">
                  <label for="floatingTitle">Judul Helpdesk</label>
                </div>
                <strong id="subject-error"></strong>
            </div>
            <div class="col-md-4">
                <div class="form-floating mb-3">
                  <select class="form-select select2" name="category" id="category" aria-label="State">
                    <option value="">Pilih Category</option>
                                @foreach ($category as $item)
                            <option value="{{$item->category_id}}">{{$item->name}}</option>
                                @endforeach
                  </select>
                  <label for="floatingCategory">Pilih Kategory</label>
                </div>
                <strong id="category-error"></strong>
            </div>
            <div class="col-md-4">
                <div class="form-floating mb-3">
                  <select class="form-select select2" name="subcategory" id="subcategory" aria-label="State">
                
                  </select>
                  <label for="floatingSubCategory">SubKategory</label>
                </div>
                <strong id="category-error"></strong>
            </div>
            <div class="col-md-4">
                <div class="form-floating mb-3">
                    <select required name="priority" id="priority"  class="form-control select2 " aria-label="State">
                        @foreach ($priority as $item)
                            <option value="{{$item->priority_id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                  <label for="floatingCategory">Pilih Prioritas</label>
                </div>
                <strong id="priority-error"></strong>
            </div>
            <div class="col-8">
                <div class="form-floating">
                  <textarea class="form-control" name="description" placeholder="Address" id="floatingDescription" style="height: 100px;"></textarea>
                  <label for="floatingDescription">Jelaskan Secara Detail Disini</label>
                </div>
                <strong id="description-error"></strong> 
            </div>
            <div class="col-4">
                <div class="form-floating">
                    <input required autocomplete="off" type="number" id="user_wa" class="form-control" value="{{old('user_wa')}}" name="user_wa" >
                    <label for="user_wa">User Wa </label>
                    <div class="text-success">*contoh penulisan nomor : 081374757586</div>
                    <div class="invalid-feedback">
                        {{$errors->first('user_wa')}}
                    </div>
                </div>
            </div>
        
                <div class="col-6">
                    <div class="form-floating">
                        <input type="file" name="img1" autocomplete="off" type="text"  class="form-control" >
                        <label for="floatingImg1">Upload Lampiran 1</label>
                        <div class="text-danger">*Opsional dan  Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                        <strong id="img1-error"></strong>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-floating">
                        <input type="file" name="img2" autocomplete="off" type="text"  class="form-control" >
                        <label for="floatingImg2">Upload Lampiran 2</label>
                        <div class="text-danger">*Opsional dan Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                        <strong id="img2-error"></strong>
                    </div>
                </div>
            
            <br>
            <br>
            <div class="col text-center">
                <button type="submit" class="btn btn-info"><i class="bx bxs-save"></i> Simpan</button>
                <button type="reset" class="btn btn-warning"><i class="bx bx-reset"></i> Reset</button>
            </div>
        </form>
    </div>
    
</div>
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
@include('front.helpdesk.javascript')
@endsection