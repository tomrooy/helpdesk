<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{asset('sbadmin/css/sb-admin-2.min.css')}}" rel="stylesheet">
<script src="{{asset('sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<style>
    @page {
        margin: 0cm 0cm;
    }
    
    /** Define now the real margins of every page in the PDF **/
    body {
        margin-top: 2cm;
        margin-left: 2cm;
        margin-right: 2cm;
        margin-bottom: 2cm;

        background-image: url("kumhamlogo.png");
        background-position: center;
        background-repeat: no-repeat;
        background-size: 100% 100%;
        position: fixed;
    }

    /** Define the header rules **/
    header {
        position: fixed;
        top: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        background-color: #1a35cf;
        color: white;
        text-align: center;
       
        line-height: 1.5cm;
    }

    /** Define the footer rules **/
    footer {
        position: fixed; 
        bottom: 0cm; 
        left: 0cm; 
        right: 0cm;
        height: 1cm;

        /** Extra personal styles **/
        background-color: #1a35cf;
        color: white;
        text-align: center;
        line-height: 1.5cm;
    }
    @media print {
    .noPrint{
        display:none;
    }
}
</style>
</head>
<body>
    <header>
        <h2><strong>Data Detail Helpdesk</strong></h2>
    </header>
    <main>
        <div class="table table-responsive-md">
            <table class="table table-borderless">
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>{{!empty($ticket->users->name) ? $ticket->users->name : 'User kosong' }}</td>
                </tr>
                <tr>
                    <td>NIP</td>
                    <td>:</td>
                    <td>{{$ticket->request_by}}</td>
                </tr>
                <tr>
                    <td>Ticket Number</td>
                    <td>:</td>
                    <td>{{$ticket->ticket_number}}</td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td>:</td>
                    <td>{{!empty($ticket->helpdesk_categories->name) ? $ticket->helpdesk_categories->name : 'category kosong' }}</td>
                </tr>
                <tr>
                    <td>SubCategory</td>
                    <td>:</td>
                    <td>{{!empty($ticket->helpdesk_subcategories->name) ? $ticket->helpdesk_subcategories->name : 'subcategory kosong' }}</td>
                </tr>
                <tr>
                    <td>Status Pengerjaan</td>
                    <td>:</td>
                    <td><h3><span class="{{$ticket->statuses->badge}}">{{$ticket->statuses->name}}</span></h3></td>
                </tr>
                
                <tr>
                    <td>keterangan Pengerjaan</td>
                    <td>:</td>
                    <td>
                        @foreach($ticket->helpdesk_details as $item)
                            @if($item->status_id == 1)
                                <b>Keterangan dari User</b> <br>
                                <li>{{!empty($item->description) ? $item->description : 'kosong' }}</li><br>
                                <b>Keterangan dari Tim IT</b> <br>
                            @else
                                <li>{{!empty($item->description) ? $item->description : 'kosong' }}</li>
                            @endif
                        @endforeach
                    </td>
                </tr>
                
            </table>
        </div>
        @if($status == true)
            <div class="col-text-center">
                <!--
                <a href="{{route('export_ticket', $ticket->ticket_number)}}" class="btn btn-success"><span class="fas fa-download"></span> Download Ticket</a>
                -->
                <button onclick="window.print();" class="btn btn-success noPrint"><span class="fas fa-print"></span> Print Ticket</button>
            </div>
        @endif
    </main>
    <footer>
        Tim IT Inspektorat Jenderal Kementerian Hukum dan HAM &copy; <?php echo date("Y");?> 
    </footer>   
</body>
</html>
       
