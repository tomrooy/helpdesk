<script>
    //$("#txtEditor").Editor();  
    $(document).ready(function() {
        var dataTable = $('#helpdesk-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('front.helpdesk.data') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'ticket_number', name: 'ticket_number'},
                {data: 'title', name: 'title'},
                {data: 'category', name: 'category'},
                {data: 'status', name:'status'},
                {data: 'handle_by', name:'handle_by'},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });

        $('#name').blur(function(){
            var nik = $('#name').val();
            if(nik == '')
            {   
                $('#name').addClass('is-invalid');
                $( '#subject-error' ).html('data wajib di isi').addClass('alert-danger');
            }else
            {
                $('#name').removeClass('is-invalid');
                $( '#subject-error' ).html('').removeClass('alert-danger');
            }
        });
        $('select[name="category"]').on('change', function(){
            var title_id = $(this).val();
                if(title_id){
                    $.ajax({
                        url: '../../getsubcategory/'+title_id,
                        type : 'GET',
                        dataType : 'Json',
                        success : function(data){
                            console.log(data.name);
                            $('select[name="subcategory"]').empty();
                            $.each(data, function(key, values)
                            {
                                $('select[name="subcategory"]').append('<option value="'+values+'">'+key+'</option>');
                            });
                        }
                    });
                    
                }
        });

        $('#formHelpdesk').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            var url =   '{{ route("get_ticket", ":id") }}';
            swal({
                title: "Kirim?",
                text: "Apakah Yakin Mengajukan data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('front.helpdesk.store') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                        swal({
                            title: 'Prosessing...',
                            html: 'Please wait...',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            showConfirmButton : false,
                            
                            });                   
                    },
                    success:function(data) {
                        if(data.errors) {
                            swal({
                                    title: "Gagal!",
                                    text: "Cek Kembali Form",
                                    type: "error",
                                    timer: 2000
                                });
                            if(data.errors.name){
                                $( '#name-error' ).html( data.errors.name[0] ).addClass('alert-danger');
                                $( '#name' ).addClass('is-invalid');
                            }
                            if(data.errors.user_wa){
                                $( '#user_wa-error' ).html( data.errors.user_wa[0] ).addClass('alert-danger');
                                $( '#user_wa' ).addClass('is-invalid');
                            }
                            if(data.errors.img1){
                                $( '#img1-error' ).html( data.errors.img1[0] ).addClass('alert-danger');
                                $( '#img1' ).addClass('is-invalid');
                            }
                            if(data.errors.img2){
                                $( '#img2-error' ).html( data.errors.img2[0] ).addClass('alert-danger');
                                $( '#img2' ).addClass('is-invalid');
                            }
                            
                        }else
                        {
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 2000
                                }).then(function() {
                                    window.open( url.replace(':id', data) , '_blank');
                                    window.location = "{{ route('front.helpdesk.index') }}"; 
                                    });
                                //document.getElementById("formHelpdesk").reset();
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });

    });
</script>