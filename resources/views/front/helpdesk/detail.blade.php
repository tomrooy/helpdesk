@extends('layouts.nice.main')
@section('title','data detail helpdesk')
@push('nav-helpdesk') show @endpush
@section('content')   
<div class="wrapper">
    @push('main-menu')Helpdesk @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Detail @endpush   
    <div class="card">
        <div class="card-header">
            <h2><strong>Data Detail Helpdesk</strong></h2>
        </div>    
        <div class="card-body">
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3>Data Main Helpdesk</h3>
                        </div>
                        <div class="card-body">
                            <div class="table table-responsive-md">
                                <table class="table table-borderless">
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{$helpdesk->users->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>NIP</td>
                                        <td>:</td>
                                        <td>{{!empty($helpdesk->users->name) ? $helpdesk->users->name : 'User kosong' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Ticket Number</td>
                                        <td>:</td>
                                        <td>{{$helpdesk->ticket_number}}</td>
                                    </tr>
                                    <tr>
                                        <td>Judul </td>
                                        <td>:</td>
                                        <td>{{$helpdesk->title}}</td>
                                    </tr>
                                    <tr>
                                        <td>Category</td>
                                        <td>:</td>
                                        <td>{{$helpdesk->helpdesk_categories->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>SubCategory</td>
                                        <td>:</td>
                                        <td>{{$helpdesk->helpdesk_subcategories->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status Terbaru</td>
                                        <td>:</td>
                                        <td><h3><span class="{{$helpdesk->statuses->badge}}">{{$helpdesk->statuses->name}}</span></h3></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3>Data Detail Helpdesk</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>keterangan Pengerjaan</td>
                                    <td>:</td>
                                    <td>
                                        @foreach($helpdesk->helpdesk_details as $item)
                                            @if($item->status_id == 1)
                                                <b>Keterangan dari User</b> <br>
                                                <li>{{!empty($item->description) ? $item->description : 'kosong' }}</li><br>
                                                <b>Keterangan dari Tim IT</b> <br>
                                            @else
                                                <li>{{!empty($item->description) ? $item->description : 'kosong' }}</li>
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            </table>
                           <div class="row">
                            @foreach($helpdesk->helpdesk_details as $item)
                                @if($item->created_by == $helpdesk->request_by)
                                    @if(!empty($item->image_a) || !empty($item->image_b))
                                        <div class="col-md-6">
                                            <h3>Lampiran dari Pengguna</h3>
                                        <li><img src={{asset('storage/uploads/helpdesks/'.$item->image_a)}} width="130px" height="160px">
                                            <a href="{{asset('storage/uploads/helpdesks/'.$item->image_a)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a></li><br>
                                        <li><img src={{asset('storage/uploads/helpdesks/'.$item->image_b)}} width="130px" height="160px">
                                            <a href="{{asset('storage/uploads/helpdesks/'.$item->image_b)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a></li>
                                        </div>
                                    @endif
                                @else
                                    @if($helpdesk->handle_by == $item->created_by && (!empty($item->image_a) || !empty($item->image_b)))
                                        <div class="col-md-6">
                                            <h3>Lampiran dari IT Team</h3>
                                        <li><img src={{asset('storage/uploads/helpdesks/'.$item->image_a)}} width="130px" height="160px">
                                            <a href="{{asset('storage/uploads/helpdesks/'.$item->image_a)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a></li><br>
                                        <li><img src={{asset('storage/uploads/helpdesks/'.$item->image_b)}} width="130px" height="160px">
                                            <a href="{{asset('storage/uploads/helpdesks/'.$item->image_b)}}" class="btn btn-info"><i class="fas fa-eye"></i> Perbesar</a></li>
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                            </div>
                            @if($helpdesk->status == 1 || $helpdesk->status == 2)
                                <a href="{{route('module.helpdesk.process', $helpdesk->id)}}" type="button" class="btn btn-success  float-right" id="process_helpdesk"><i class="fas fa-cogs"></i> Proses</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
           
           
        </div>
    </div>
</div>  
@endsection
@section('js')
@include('layouts.nice.partial.indexjs')
<script>
    $(document).ready(function() {        
        var interval = setInterval(function() {
            var momentNow = moment();
            $('#date-part').html(momentNow.format('dddd')
                                .substring(0,9).toUpperCase()+' '+ momentNow.format('DD MMMM YYYY'));
            $('#time-part').html(momentNow.format('HH:mm:ss A'));
        }, 100);
    });
</script>
@endsection