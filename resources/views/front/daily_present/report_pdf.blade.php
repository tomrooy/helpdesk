<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Absensi Virtual</title>
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css" rel="stylesheet"> 
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
            div.wrapper 
            {
                  background-color: #999;
                  border: 1px solid #333;
                  float: left;
                  margin: 10px;
                  width: 200px;
                  height: 250px;
            }
            
        </style>
    </head>
    <body class="bg-custom" onload="window.print()">
    <div class="card">
        <div class="card-header">
            Rekap Laporan Kehadiran Pegawai Substansi Humas dan SIP
        </div>
        <div class="card-body">
            {{$date}}
            <table class="table table-responsive-md">
                <thead>
                    <th>NO</th>
                    <th>Kegiatan</th>
                    <th>Jumlah Pegawai</th>
                </thead>
                <tbody>
                    @php 
                        $no= 1;
                    @endphp
                        @foreach($rekap as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->daily_presents_count}}</td> 
                                @php
                                    $no++;
                                @endphp
                            </tr>
                        @endforeach                    
                </tbody>
            </table>
            <table class="table table-responsive-md" style='font-size:80%'>
                <thead> 
                    <th>NO</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>Unit Kerja</th>
                    <th>Jabatan</th>
                    <th>Status</th>
                    <th>Tanda Tangan</th>
                </thead>
                <tbody>
                    @php 
                        $no= 1;
                    @endphp
                        @foreach($data as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->nip}}</td>
                                <td>{{$item->unit}}</td>
                                <td>{{$item->title}}</td>
                                <td>{{$item->daily_present_status->name}}</td>
                                <td><img src="{{asset('storage/uploads/present_signature/'. $item->signature)}}" width="150px" height="100px"></td> 
                                @php
                                    $no++;
                                @endphp
                            </tr>
                        @endforeach                    
                </tbody>
            </table>
        </div>
    </div>
        
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</body>
</html>
