<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('nice/assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('nice/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    
    <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('nice/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('nice/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    
    <!-- Template Main CSS File -->
    <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
    <title>Rekap Absensi</title>  
</head>
<body>
<main id="main" class="main">
    <div class="pagetitle">
        <section class="section dashboard">
            <div class="card mt-2">
                <div class="card-header">
                <h3 style="text-align: center"><b>Rekap Harian Daftar Hadir di Substansi Humas dan SIP</b></h3>
                </div>
                <div class="card-body">
                    @if(session('errors_message'))
                        <div class="alert alert-danger btn-sm alert-small" >
                            {{session('errors_message')}}
                        </div> 
                    @endif
                    <form method="POST" action="{{route('daily.present.rekap.store')}}" enctype="multipart/form-data" class="row g-3">
                        @csrf
                        <div class="col-md-4">
                            <div class="form-floating">
                                <input autocomplete="off" type="date" name="date" value="{{old('date')}}" class=" form-control {{$errors->first('date') ? 'is-invalid': ''}}" >
                                <label>Tanggal Yang Dipilih</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('date')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating">
                                <input type="password" name="passcode" value="{{old('passcode')}}" class="form-control {{$errors->first('passcode') ? 'is-invalid': ''}}" placeholder="Silahkan Isi Passcode yang diberikan admin">
                                <label>Passcode</label>
                                <div class="invalid-feedback">
                                    {{$errors->first('passcode')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-info btn-lg"><i class="bx bxs-download"></i> Download Rekap</button>
                        </div>
                    </form>             
                </div>
            </div>
            <div class="col-12">
                <div class="card recent-sales">
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                    <li class="dropdown-header text-start">
                        <h6>Filter</h6>
                    </li>
        
                    <li><a class="dropdown-item" href="#">Today</a></li>
                    <li><a class="dropdown-item" href="#">This Month</a></li>
                    <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
        
                <div class="card-body">
                    <h5 class="card-title">Rekapitulasi Hari ini <span>| Hari ini</span></h5>
                    <div id="container"></div>
                    <b>Yang belum Absen Harian</b>
                    <table class="table table-responsive-md">
                        <thead>
                            <th>NO</th>
                            <th>Nama</th>
                        </thead>
                        <tbody>
                            @php 
                                $no= 1;
                               
                            @endphp
                            @foreach($alpha as $item)
                                <tr>
                                <td>{{$no}}</td>
                                <td>{{$item}}</td>
                                    @php
                                        $no++;
                                    @endphp
                                </tr>
                            @endforeach               
                        </tbody>
                    </table>
                </div>
        
                </div>
            </div><!-- End Recent Sales -->
        </section>
    </div> 
</main>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="{{asset('nice/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Template Main JS File -->
    <script src="{{asset('nice/assets/js/main.js')}}"></script>
    <script>
        Highcharts.setOptions({
            colors: ['#8cc211', '#12476f', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#990808', '#054259']
        });
       const chart = Highcharts.chart('container', {
        chart: {
            inverted: true,
            polar: false
            },
        title: {
            text: 'Grafik Kehadiran'
        },
        //subtitle: {
        //    text: 'Inverted'
        //},
        xAxis: {
            categories: {!!json_encode($kehadiran, JSON_NUMERIC_CHECK)!!},
        },
        id: {
            name: {!!json_encode($id, JSON_NUMERIC_CHECK)!!},
        },
        tooltip: {
            formatter: function () {
                return 'Total pegawai '+ this.point.category + 'sekitar ' + this.y +' orang' ;
            }
        },
        series: [{
            type: 'column',
            colorByPoint: true,
            data: {!!json_encode($count, JSON_NUMERIC_CHECK)!!},
            showInLegend: false,
            cursor: 'pointer',
            point: {
                events: {
                    click: 
                    function (x) {
                        console.log(x);
                        window.open( '/admin/employee/filter_department/'+x.point.category, '_blank'); 
                    }
                }
            }
        }],
        credits: {
        enabled: false
        }
        });

        document.getElementById('inverted').addEventListener('click', () => {
            chart.update({
                chart: {
                inverted: true,
                polar: false
                },
                subtitle: {
                text: 'Inverted'
                }
            });
        });
        
        document.getElementById('plain').addEventListener('click', () => {
            chart.update({
                chart: {
                inverted: false,
                polar: false
                },
                subtitle: {
                text: 'Plain'
                }
            });
        });

        document.getElementById('polar').addEventListener('click', () => {
            chart.update({
                chart: {
                inverted: false,
                polar: true
                },
                subtitle: {
                text: 'Polar'
                }
            });
        });
        
    </script>
</body>
</html>