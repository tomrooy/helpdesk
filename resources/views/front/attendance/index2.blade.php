<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Absensi Virtual</title>
        <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
        </style>
    </head>
    <body class="bg-custom">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-3">
                                    <div class="card-header">
                                        <div class="text-center">
                                            <img src="{{asset('kumham.png')}}" class="my-2" height="150px" weight="150px" class="img-fluid" alt="Responsive image">
                                        </div>
                                        @php
                                            $date = \Carbon\Carbon::parse($data->date)->locale('id');
                                            $date->settings(['formatFunction' => 'translatedFormat']);
                                        @endphp
                                    <h5>{{$data->name}}</h5> {{$date->format('l, j F Y')}} Jam {{ $data->start. ' - '. $data->end }}</div>
                                    <div class="card-body">
                                        <div id="form_display">
                                            <form method="POST"  id="formAttendance" enctype="multipart/form-data" class="row g-3">
                                                @csrf
                                                <input type="hidden" name="activity_id"  id="activity_id" value="{{$data->activity_id}}">
                                                <div class="col-md-12">
                                                    <div class="form-floating">
                                                        <input type="text" name="name" id="name" class="form-control">
                                                        <label>Nama</label>
                                                        <strong id="name-error"></strong>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-floating">
                                                        <input type="number" name="nip" id="nip"   class="form-control " >
                                                        <label>NIP</label>
                                                        <strong id="nip-error"></strong>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-floating">
                                                        <input type="text" name="jabatan" id="jabatan" class="form-control" >
                                                        <label>Jabatan</label>
                                                        <strong id="jabatan-error"></strong>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-floating">
                                                        <input type="text" name="unit_kerja" id="unit_kerja" class="form-control">
                                                        <label>Unit Kerja</label>
                                                        <strong id="unit_kerja-error"></strong>
                                                    </div>
                                                </div>
                                                <div id="signature-pad" class="signature-pad">
                                                    <div class="signature-pad--body">
                                                        <label>Tanda Tangan</label>
                                                        <canvas id="signature-canvas" style="width:650px;height:320px;max-width:100%;border:8px #CCC solid;background-color: white;"></canvas>
                                                    </div>
                                                    <div class="signature-pad-footer">
                                                      <div class="signature-pad-actions">
                                                        <div>
                                                        <button id="clear" class="btn btn-danger btn-sm">Reset Tanda Tangan</button>
                                                        <input type="hidden" id="signed" name="signed" id="signatureImage">
                                                        <strong id="signed-error"></strong>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    
                                                  </div>
                                                <div class="col text-center">
                                                    <button type="submit" id="store" class="btn btn-primary"><span class="fas fa-save"></span> Simpan</button>
                                                    <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div id="notification_display" style="display: none">
                                        <div class="alert alert-success btn-sm alert-small" >
                                            Presensi Anda Berhasil Di Input
                                        </div>
                                        <form action="{{route('attendance.print.personal')}}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="print_activity_id" id="print_activity_id">
                                            <input type="hidden" name="print_nip" id="print_nip">
                                            <button type="submit" class="btn btn-info"><i class="bi bi-printer"></i> Cetak Absen Pribadi</button>
                                        </form>
                                        <br><!--
                                        <form action="{{route('attendance.print.group')}}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="print_activity_id2" id="print_activity_id2">
                                            <button type="submit" class="btn btn-success"><i class="bi bi-printer"></i> Cetak Absen Keseluruhan</button>
                                        </form> -->
                                    </div>
                                    <div class="card-footer text-center py-3">
                                        <div class="text-muted">Tim IT Inspektorat Jenderal Kementerian Hukum dan HAM 2022</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        
        </div>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
<script src="{{asset('sbadmin/js/jquery/jquery.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script src="{{asset('nice/assets/js/main.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var form_display =  document.getElementById("form_display");
        var notification_display =  document.getElementById("notification_display");
        var wrapper = document.getElementById("signature-pad");
        var clearButton = wrapper.querySelector("[data-action=clear]");
        var canvas = document.getElementById('signature-canvas');

        // Adjust canvas coordinate space taking into account pixel ratio,
        // to make it look crisp on mobile devices.
        // This also causes canvas to be cleared.
        function resizeCanvas() {
            var ratio =  Math.max(window.devicePixelRatio || 1, 1);
            canvas.width = canvas.offsetWidth * ratio;
            canvas.height = canvas.offsetHeight * ratio;
            canvas.getContext("2d").scale(ratio, ratio);
        }
        // On mobile devices it might make more sense to listen to orientation change,
        // rather than window resize events.
        window.onresize = resizeCanvas;
        resizeCanvas();
        var signaturePad = new SignaturePad(canvas);
        // One could simply use Canvas#toBlob method instead, but it's just to show
        // that it can be done using result of SignaturePad#toDataURL.
        function dataURLToBlob(dataURL) {
          // Code taken from https://github.com/ebidel/filer.js
          var parts = dataURL.split(';base64,');
          var contentType = parts[0].split(":")[1];
          var raw = window.atob(parts[1]);
          var rawLength = raw.length;
          var uInt8Array = new Uint8Array(rawLength);

          for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
          }
          
          return new Blob([uInt8Array], { type: contentType });
        }
      
        $('#clear').click(function(e) {
            e.preventDefault();
            signaturePad.clear();
        });
        $('#formAttendance').submit(function(e) {
            e.preventDefault();
            var signed  =   signaturePad.toDataURL('image/png');
            document.getElementById("signed").value = signed;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }
            });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('attendance.store') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        signed      : document.getElementById("signed").value,
                        nip         : document.getElementById("nip").value,
                        name        : document.getElementById("name").value,
                        unit_kerja  : document.getElementById("unit_kerja").value,
                        jabatan     : document.getElementById("jabatan").value,
                        activity_id  : document.getElementById("activity_id").value,
                    },
                    success:function(data) {
                        if(data.errors) {
                            swal({
                                    title: "Gagal!",
                                    text: data.errors,
                                    type: "error",
                                    timer: 2000
                                });
                            if(data.errors.nip){
                                $( '#nip-error' ).html( data.errors.nip[0] ).addClass('alert-danger');
                                $( '#nip' ).addClass('is-invalid');
                            }
                            if(data.errors.name){
                                $( '#name-error' ).html( data.errors.name[0] ).addClass('alert-danger');
                                $( '#name' ).addClass('is-invalid');
                            }
                            if(data.errors.unit_kerja){
                                $( '#unit_kerja-error' ).html( data.errors.unit_kerja[0] ).addClass('alert-danger');
                                $( '#unit_kerja' ).addClass('is-invalid');
                            }
                            if(data.errors.signed){
                                $( '#signed-error' ).html( data.errors.signed[0] ).addClass('alert-danger');
                                
                            }
                        }else
                        {
                                swal({
                                    title: "Success!",
                                    text: "Absen Berhasil Di Input",
                                    type: "success",
                                    timer: 2000
                                }).then(function() {
                                    document.getElementById("formAttendance").reset();
                                    signaturePad.clear();
                                    form_display.style.display = "none";
                                    document.getElementById("print_nip").value = data.nip;
                                    document.getElementById("print_activity_id").value = data.activity_id;
                                    document.getElementById("print_activity_id2").value = data.activity_id;
                                    notification_display.style.display = "block";
                                    });
                                
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
        });               
    });
</script>
    </body>
</html>
