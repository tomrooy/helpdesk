<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Input Absensi Virtual</title>
        <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
        </style>
    </head>
    <body class="bg-custom">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="card shadow-lg border-0 rounded-lg mt-3">
                                    <div class="card-header">
                                        <div class="text-center">
                                            <h2 class="text-center">Presensi Apel Input Admin</h2>
                                            <img src="{{asset('kumham.png')}}" class="my-2" height="150px" weight="150px" class="img-fluid" alt="Responsive image">
                                        </div>
                                        @php
                                            $date = \Carbon\Carbon::parse($data->date)->locale('id');
                                            $date->settings(['formatFunction' => 'translatedFormat']);
                                        @endphp
                                    <h4>{{$data->name}}</h4> {{$date->format('l, j F Y')}} Jam {{ $data->start. ' - '. $data->end }}</div>
                                    <div class="card-body">
                                        <div id="form_display">
                                            <form method="POST"  id="formAttendance" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="activity_id"  id="activity_id" value="{{$data->activity_id}}">
                                                <p>Silahkan Masukkan Nip Pegawai</p>
                                                <div class="form-floating mb-3">
                                                    <input autocomplete="off" class="form-control"  type="text" id="nip" name="nip" placeholder="name@example.com"  required/>
                                                    <label for="inputNip">Masukkan NIP</label>
                                                    <div class="col text-center">
                                                        <center><div id="loading_page"></div></center><br>
                                                        <button id="button_1" class="btn btn-info"><i class="fas fa-search"></i> Cek data Pegawai</button>
                                                    </div> 
                                                    <div id="name_list"></div>
                                                    <strong id="nip-error"></strong>
                                                </div>
                                                <div id="foto" class="col text-center" style="display: none" >
                                                   <img id="fotopegawai"  referrerpolicy="no-referrer" height="140" width="90">
                                                </div>
                                                <div class="form-floating mb-3">
                                                    <input disabled autocomplete="off" class="form-control" id="named"  type="text" name="named" placeholder="name@example.com" />
                                                    <input  autocomplete="off" class="form-control" id="name"  type="hidden" name="name" placeholder="name@example.com" />
                                                    <label for="inputName">Nama Lengkap </label>
                                                    <strong id="name-error"></strong>
                                                </div>
                                                <div class="form-floating mb-3">
                                                    <input autocomplete="off" class="form-control" id="unit_kerjad"  type="text" name="unit_kerjad" placeholder="name@example.com" disabled/>
                                                    <input autocomplete="off" class="form-control" id="unit_kerja"  type="hidden" name="unit_kerja" placeholder="name@example.com" />
                                                    <label for="inputUnit_kerja">Unit Kerja </label>
                                                    <strong id="unit_kerja-error"></strong>
                                                </div>
                                                <div class="form-floating mb-3">
                                                    <input autocomplete="off" class="form-control" id="jabatand"  type="text" name="jabatand" placeholder="name@example.com" disabled/>
                                                    <input autocomplete="off" class="form-control" id="jabatan"  type="hidden" name="jabatan" placeholder="name@example.com"/>
                                                    <label for="inputjabatan">Jabatan Pegawai </label>
                                                    <strong id="jabatan-error"></strong>
                                                </div>                                       
                                                <input type="checkbox" id="cek"> Dengan ini saya menyatakan hadir sebenarnya dalam acara tersebut
                                                <div class="col text-center">
                                                    <button type="submit" id="store" class="btn btn-primary " disabled><span class="fas fa-save"></span> Simpan</button>
                                                    <button type="reset" class="btn btn-warning"><i class="fas fa-recycle"></i> Reset</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="notification_display" style="display: none">
                                            <div class="alert alert-success btn-sm alert-small" >
                                                Presensi Anda Berhasil Di Input
                                            </div>
                                            <form action="{{route('attendance.print.personal')}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="print_activity_id" id="print_activity_id">
                                                <input type="hidden" name="print_nip" id="print_nip">
                                                <button type="submit" class="btn btn-info"><i class="bi bi-printer"></i> Cetak Absen Pribadi</button>
                                            </form>
                                            <br>                                             
                                        </div>
                                    </div>
                                    
                                    <div class="card-footer text-center py-3">
                                        <div class="text-muted">Tim Pengelola Teknologi Informasi, Inspektorat Jenderal  2022</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        
        </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
<script src="{{asset('sbadmin/js/jquery/jquery.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script src="{{asset('nice/assets/js/main.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var interval = setInterval(function() {
            var momentNow = moment();
            $('#time-part').html(momentNow.format('HH:mm:ss A'));
        }, 100);

        var form_display =  document.getElementById("form_display");
        var notification_display =  document.getElementById("notification_display");
        var cekPegawai = 0;
        $("#button_1").click(function(e) {
            e.preventDefault();
            var query = document.getElementById("nip").value;
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                    type: "POST",
                    url:"{{ route('search.simpeg') }}",
                    data:{
                        "_token": "{{ csrf_token() }}",
                        'nip':query
                    },
                    beforeSend:function(){
                        $("#loading_page").show(1000).html("<img src='https://cdn.dribbble.com/users/5484/screenshots/2145786/for_dribbble.gif' height='200' > ");                   
                    },
                      success: function(data) {
                        data = data[0];
                        if(data.hasil == 0)
                        {
                            $("#loading_page").hide();
                            swal({
                                title: "Gagal!",
                                text: "Mohon Isi Form terlebih dahulu",
                                type: "error",
                                timer: 2000
                            });
                        }else if(data.hasil == 1){
                            $("#loading_page").hide();
                            swal({
                                title: "Gagal!",
                                text: "Pegawai Tidak Terdaftar Mohon Hubungi Tim IT",
                                type: "error",
                                timer: 2000
                            });
                        }else{
                            $("#loading_page").hide();
                            swal({
                                title: "Success!",
                                text: "Data Pegawai Ditemukan",
                                type: "success",
                                timer: 2000
                            })
                            cekPegawai = 1;
                            document.getElementById("nip").value = query;
                            document.getElementById("name").value = data.nama;
                            document.getElementById("jabatan").value = data.jabatan;
                            document.getElementById("unit_kerja").value = data.unitkerja;
                            document.getElementById("named").value = data.nama;
                            document.getElementById("jabatand").value = data.jabatan;
                            document.getElementById("unit_kerjad").value = data.unitkerja;
                            //$("#fotopegawai").html("<img src='data.data.NIP'>");
                            document.getElementById("foto").style.display = "block";
                            document.getElementById("fotopegawai").src = "https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/"+query+".jpg";
                            //console.log(data.data.Foto);
                        }
                      },
                      error:function(data)
                      {
                        $("#loading_page").hide();
                        alert('tidak ditemukan');
                      },
                    });
        });
        $('#formAttendance').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    }
                });
            $.ajax({
                type: 'POST',
                url:"{{ route('apel.manual.simpan') }}",
                data: formData,
                contentType: false,
                processData: false,
                success:function(data) {
                    if(data.errors) {
                        swal({
                            title: "Gagal!",
                            text: data.errors,
                            type: "error",
                            timer: 2000
                        });
                        if(data.errors.nip){
                                $( '#nip-error' ).html( data.errors.nip[0] ).addClass('alert-danger');
                                $( '#nip' ).addClass('is-invalid');
                            }
                            if(data.errors.name){
                                $( '#name-error' ).html( data.errors.name[0] ).addClass('alert-danger');
                                $( '#name' ).addClass('is-invalid');
                            }
                            if(data.errors.unit_kerja){
                                $( '#unit_kerja-error' ).html( data.errors.unit_kerja[0] ).addClass('alert-danger');
                                $( '#unit_kerja' ).addClass('is-invalid');
                            }
                        }else
                        {
                            swal({
                                title: "Success!",
                                text: "Absen Berhasil Di Input",
                                type: "success",
                                timer: 2000
                                }).then(function() {
                                document.getElementById("formAttendance").reset();
                                form_display.style.display = "none";
                                document.getElementById("print_nip").value = data.nip;
                                document.getElementById("print_activity_id").value = data.activity_id;
                                notification_display.style.display = "block";
                                });
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
        });
        $('#cek').click(function () {
            //check if checkbox is checked
            if ($(this).is(':checked') && cekPegawai == 1) {
                console.log(cekPegawai);
                $('#store').removeAttr('disabled'); //enable input

            }else
            {
                $('#store').attr('disabled', true); //disable input
            }
        });
    });
</script>
    </body>
</html>
