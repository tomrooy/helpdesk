<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Absensi Virtual</title>
        <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
        </style>
    </head>
    <body class="bg-custom" onload="window.print()">
        <main>
            <div>
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h1 class="h3 mb-0 text-gray-800">
                                    <div class="page-header" id="banner">
                                        <div class="row">
                                            <div class="" style="padding: 15px 15px 0 15px;">
                                                <div class="well well-sm">
                                                    <img src="{{asset('kumham.png')}}" class="thumbnail span3" style="display: inline; float: left; margin-right: 20px; width: 100px; height: 100px">
                                                    <h2 style="margin: 15px 0 10px 0; color: #000;">Inspektorat Jenderal</h2>
                                                    <div style="color: #000; font-size: 16px; font-family: Tahoma" class="clearfix"><b>Alamat :JL. HR. Rasuna Said Kav. 8, Setiabudi, Kuningan Jakarta Selatan</b>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </h1>
                            </div>
                            <div class="card-body">
                                <br>
                                <table class="table table-borderless">
                                    <tr>
                                      <td width="200px">Nama Kegiatan</td>
                                      <td>{{$attendance->activities->name}}</td>
                                    </tr>
                                    @php
                                        $date = \Carbon\Carbon::parse($attendance->date)->locale('id');
                                        $date->settings(['formatFunction' => 'translatedFormat']);
                                    @endphp
                                    <tr>
                                      <td>Jadwal</td>
                                      <td>{{$date->format('l, j F Y')}} Pukul {{$attendance->activities->start. ' - '. $attendance->activities->end }} WIB</td>
                                    </tr>
                                    <tr>
                                        <td>Lokasi</td>
                                        <td>Zoom Virtual Meeting</td>
                                      </tr>
                                    <tr>
                                      <td>Nama</td>
                                      <td>{{$attendance->name}}</td>
                                    </tr>
                                    <tr>
                                      <td>NIP</td>
                                      <td>{{$attendance->nip}}</td>
                                    </tr>
                                    <tr>
                                      <td>Unit Kerja</td>
                                      <td>{{$attendance->location}}</td>
                                    </tr>
                                    <tr>
                                      <td>Jabatan</td>
                                      <td>{{$attendance->title}}</td>
                                    </tr>
                                    <tr>
                                      <td>Foto</td>
                                      <td><img src="https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/{{$attendance->nip}}.jpg" width="90" height="120" alt="Profile"></td>
                                    </tr>
                                    <tr>
                                      <td>Tanda Tangan</td>
                                      <td><img src="{{asset('storage/uploads/signature/'. $attendance->signature)}}" width="200px" height="100px"></td>
                                    </tr>
                                    @if(!empty($attendance->photo))
                                        <tr>
                                            <td>Bukti Kegiatan</td>
                                            <td><img src="{{asset('storage/uploads/activity_photo/'. $attendance->photo)}}" width="300px" height="150px"></td>
                                        </tr>
                                    @endif
                                        <!--
                                    <tr>
                                        <td>Link Absensi</td>
                                        <td>https://helpdesk.itjenkumham.site/presensi-rekap/{{$attendance->activity_id}}</td>
                                      </tr>
                                    -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>
