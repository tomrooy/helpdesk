<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Absensi Virtual</title>
        <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
        </style>
</head>
<body>
    <main>
        <div class="container my-3">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="card top-selling">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                            <li class="dropdown-header text-start">
                                <h6>Filter</h6>
                            </li>

                            <li><a class="dropdown-item" href="#">Today</a></li>
                            <li><a class="dropdown-item" href="#">This Month</a></li>
                            <li><a class="dropdown-item" href="#">This Year</a></li>
                            </ul>
                        </div>
                        <div class="card-body pb-0">
                            @php
                                $date = \Carbon\Carbon::now()->locale('id');
                                $date->settings(['formatFunction' => 'translatedFormat']);
                            @endphp
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="{{asset('kumham.png')}}" class="my-2">
                                </div>
                                <div class="col-md-8">
                                    <h3 class="text-center my-5" >Kegiatan Zoom Virtual <span>| Hari ini </span>
                                        <br>{{$date->format('l, j F Y')}}</h3>
                                </div>
                                
                            </div>
                           
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Agenda</th>
                                        <th>Jam</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;    
                                    @endphp
                                    @foreach($data as $item)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->start.'-'.$item->end}} </td>
                                        <td><a href="{{$item->url}}" class="btn btn-info"><i class="bi bi-vector-pen"></i> Hadiri</a></td>
                                    </tr>
                                    @php
                                        $no++;
                                    @endphp    
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{asset('nice/assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
<script src="{{asset('nice/assets/js/main.js')}}"></script>
</body>
</html>