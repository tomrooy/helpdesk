@extends('layouts.nice.main')
@section('title','Zoom Form Request Page')
@push('nav-zoom') show @endpush
@section('content')
    <div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Form Request @endpush   
        <div class="card">
            <div class="card-header">
                <h2><strong>Form Permintaan Link ZOOM</strong></h2>
            </div>    
            <div class="card-body">
                <form method="POST" id="formRequestZoom" enctype="multipart/form-data" class="row g-3">
                    @csrf
                    <div class="col-md-4">
                        <div class="form-floating">
                            <input type="text" value="{{\Auth::User()->name}}" class="form-control" disabled>
                            <label>Nama</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating">
                            <input type="text" value="{{\Auth::User()->nip}}" class="form-control" disabled>
                            <label>NIP</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-floating">
                            <input type="text" value="{{\Auth::User()->userLocations->name}}" class="form-control" disabled>
                            <label>Bagian / Wilayah</label>
                        </div>
                    </div>
                    <div class="form-floating">
                        <input autocomplete="off" type="text" id="name" class="form-control" name="name" placeholder="Silahkan Isi Judul Agenda kegiatan anda">
                        <label for="subject">Judul Kegiatan</label>
                        <strong id="name-error"></strong>
                    </div>
                    <center><div id="loading_page"></div></center><br>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <select name="participant" class="form-control {{$errors->first('participant') ? "is-invalid" : ""}}">
                                <option value="">--Pilih Jumlah Peserta --</option>
                                <option value="100" @if(old('participant') == '100') {{'selected'}} @endif>Maksimal 100 Peserta</option>
                                <option value="100-300" @if(old('participant') == '100-300') {{'selected'}} @endif>100-300 Peserta</option>
                                <option value="300-500" @if(old('participant') == '300-500') {{'selected'}} @endif>300-500 Peserta</option>
                                <option value="500-1000" @if(old('participant') == '500-1000') {{'selected'}} @endif>500-1000 Peserta</option>
                            </select>
                            <label><b>Jumlah Peserta</b></label>
                            <div class="invalid-feedback">
                                {{$errors->first('participant')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <select name="category" class="form-control {{$errors->first('category') ? "is-invalid" : ""}}">
                                <option value="">--kategory meeting --</option>
                                <option value="Rapat Kordinasi" @if(old('category') == 'Rapat Kordinasi') {{'selected'}} @endif>Rapat Kordinasi</option>
                                <option value="Rapat Internal" @if(old('category') == 'Rapat Internal') {{'selected'}} @endif>Rapat Internal</option>
                                <option value="Acara Workshop" @if(old('category') == 'Acara Workshop') {{'selected'}} @endif>Acara Workshop</option>
                                <option value="Acara Gemar Belajar" @if(old('category') == 'Acara Gemar Belajar') {{'selected'}} @endif>Acara Gemar Belajar</option>
                            </select>
                            <label><b>Kategory Kegiatan</b></label>
                            <div class="invalid-feedback">
                                {{$errors->first('category')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input required autocomplete="off" type="number" id="user_wa" class="form-control" value="{{old('user_wa')}}" name="user_wa" >
                            <label for="user_wa">User Wa </label>
                            <div class="text-success">*contoh penulisan nomor : 081374757586</div>
                            <strong id="user_wa-error"></strong>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-floating">
                            <textarea autocomplete="off" style="height: 100px;" type="text" class="form-control {{$errors->first('description') ? "is-invalid" : ""}} " name="description" placeholder="description">{{old('description')}}</textarea>
                                <div class="invalid-feedback">
                                    {{$errors->first('description')}}
                                </div>
                            <label><b>Deskripsi ZOOM</b></label>
                        </div> 
                    </div>   
                    <br><br>                 
                    <div class="col-md-3">
                        <div class="form-floating">
                            <input autocomplete="off" type="date" id="date" name="date" class="form-control">
                            <label>Tanggal Agenda Meeting</label>
                            <strong id="date-error"></strong>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-floating">
                            <select name="start" class="select2 form-control"  id="start">
                                @foreach($time as $item)
                                    <option value="{{$item->name}}" @if(old('start') == '{{$item->name}}') 'selected' @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                            <label>Jam Mulai</label>
                            <div class="invalid-feedback">
                                test
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-floating">
                            <select name="end" class="form-control"  id="end">
                                @foreach($time as $item)
                                    <option value="{{$item->name}}" @if(old('end') == '{{$item->name}}') 'selected' @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                            <label>Jam Berakhir</label>
                            <div class="invalid-feedback">
                                {{$errors->first('end')}}
                            </div>
                        </div>
                    </div>    
                        <div class="col-md-5">
                            <div class="form-floating">
                                <input type="file" name="attachment"  class="form-control {{$errors->first('attachment') ? 'is-invalid': ''}}" >
                                <div class="invalid-feedback">
                                    {{$errors->first('attachment')}}
                                </div>
                                <label>Nodin Atau Surat Undangan(opsional)</label>
                                *format yang diterima adalah PDF max : 1 MB
                            </div>
                        </div>
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" name="activity" id="flexSwitchCheckDefault">
                            <label class="form-check-label" for="flexSwitchCheckDefault">Request Presensi Online Kegiatan</label>
                        </div>
                    <br><br>
                    <div class="col text-center">
                        <button type="submit" class="btn btn-info"><i class="bx bx-save"></i> Simpan</button>
                        <button type="reset" class="btn btn-warning"><i class="bx bx-reset"></i> Reset</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
@include('front.zoom_request.javascript')
@endsection