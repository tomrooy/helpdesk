<script> 
    $(document).ready(function() {       
        $('#name').blur(function(){
            var nik = $('#name').val();
            if(nik == '')
            {   
                $('#name').addClass('is-invalid');
                $( '#name-error' ).html('data wajib di isi').addClass('alert-danger');
            }else
            {
                $('#name').removeClass('is-invalid');
                $( '#name-error' ).html('').removeClass('alert-danger');
            }
        });

        $('#formRequestZoom').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            var url =   '{{ route("front.zoom_request.index") }}';
            swal({
                title: "Kirim?",
                text: "Apakah Yakin Mengajukan data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('front.zoom_request.store') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                        swal({
                            title: 'Prosessing...',
                            html: 'Please wait...',
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                            showConfirmButton : false,
                            
                            });                   
                    },
                    success:function(data) {
                        $("#loading_page").hide();
                        if(data.errors) {
                            swal({
                                title: "Gagal!",
                                text: 'Cek Kembali Form Pengisian',
                                type: "warning",
                                timer: 4000
                            });
                            if(data.errors.name){
                                $('#subject-error').html( data.errors.name[0] ).addClass('alert-danger');
                            }
                            if(data.errors.date){
                                $('#date').addClass('is-invalid');
                                $('#date-error').html( data.errors.date[0] ).addClass('alert-danger');
                            }
                        }else
                        {   if(data)
                            {
                                swal({
                                    title: "Gagal!",
                                    text: data,
                                    type: "warning",
                                    timer: 4000
                                });
                            }else
                            {
                                swal({
                                    title: "SUKSES!",
                                    text: "Data Berhasil Diajukan",
                                    type: "success",
                                    timer: 2000
                                }).then(function() {
                                    window.location = url; 
                                    });
                               
                            }
                                
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });

    });
</script>