@extends('layouts.nice.main')
@section('title','data detail helpdesk')
@push('nav-zoom') show @endpush
@section('content')
@section('css')
<style>
    .hide_zoom{
        display:none;
    }
</style>    
@endsection   
<div class="wrapper">
    @push('main-menu')Zoom @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Detail @endpush   
    <div class="card">
        <div class="card-header">
            <h2><strong>Data Detail ZOOM {{$zoom->name}}</strong></h2>
        </div>    
        <div class="card-body">
            <br>
            <a href="{{route('front.zoom_request.index')}}" class="btn btn-success float-end"><i class="bx bx-arrow-back"></i> Kembali </a>
            <div class="row">
                <div class="col-md-5">
                    <h3><b>Data Kegiatan</b></h3>
                        <table class="table table-responsive text-gray-900">
                            <tr>
                                <td>Nama Agenda Rapat</td>
                                <td>{{$zoom->name}}</td>
                            </tr>
                            <tr>
                                <td>Diajukan Oleh</td>
                                <td>{{$zoom->request_by}}</td>
                            </tr>
                            <tr>
                                <td>Jenis Rapat</td>
                                <td>{{$zoom->category}}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Rapat</td>
                                <td>{{$zoom->date.' '.$zoom->start.'-'.$zoom->end}}</td>
                            </tr>
                            <tr>
                                <td>File Nodin</td>
                                <td>{{$zoom->attachment}}</td>
                            </tr>
                            <tr>
                                <td>Keterangan Lain</td>
                                <td>{{$zoom->description}}</td>
                            </tr>
                            <tr>
                                <td>Tim IT Yang handle</td>
                                <td>{{!empty($zoom->create_zoom->name) ? $zoom->create_zoom->name : ''}}</td>
                            </tr>
                            <tr>
                                <td>Contact Person</td>
                                <td>{{!empty($zoom->create_zoom->handphone) ? $zoom->create_zoom->handphone : ''}}</td>
                            </tr>
                        </table>
                </div>
                <div class="col-md-3">
                    <h4><b>Link ZOOM Meeting</b></h4>
                        <table class="table table-responsive text-gray-900">
                            <tr>
                                <td>ID ZOOM</td>
                                <td>{{$zoom->zoom_id}}</td>
                            </tr>
                            <tr>
                                <td>Password ZOOM</td>
                                <td>{{$zoom->password}}</td>
                            </tr>
                        </table>
                    <button type="button" id="modal_zoom" class="btn btn-info"><span class="bx bx-copy"></span> Copy Link</button>
                </div>
                <div class="col-md-4">
                    <h4><b>Link Record ZOOM</b></h4>
                    <table class="table table-responsive text-gray-900" id="link_record">
                        <tr>
                            <td>LINK Record ZOOM</td>
                            <td><a href="{{$zoom->link_record}}">{{$zoom->link_record}}</a></td>
                        </tr>
                        <tr>
                            <td>Password Record ZOOM</td>
                            <td>{{$zoom->password_record}}</td>
                        </tr>
                       
                    </table>
                    <button onclick="copy('#link_record')" type="button" class="btn btn-info"><span class="bx bx-copy"></span> Copy Link</button>
                </div>
            </div>
            
           
           
        </div>
    </div>
</div>  
<div class="modal fade" id="verticalycentered" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Data Lengkap ZOOM</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-responsive text-gray-900" id="link_zoom">
                    <tr id="name_zoom">
                        <td>Nama Agenda Rapat</td>
                        <td>{{$zoom->name}}</td>
                    </tr>
                    <tr id="tanggal_zoom">
                        <td>Tanggal Rapat</td>
                        <td>{{$zoom->date.' '.$zoom->start.' '.$zoom->end}}</td>
                    </tr>
                    <tr>
                        <td>ID ZOOM</td>
                        <td>{{$zoom->zoom_id}}</td>
                    </tr>
                    <tr>
                        <td>Password ZOOM</td>
                        <td>{{$zoom->password}}</td>
                    </tr>
                    <tr>
                        <td>LINK ZOOM</td>
                        <td><a href="{{$zoom->link}}">{{$zoom->link}}</a></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button onclick="copy('#link_zoom')" type="button" class="btn btn-info"><span class="bx bx-copy"></span> Copy Link</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $('body').on('click', '#modal_zoom', function(e) {
        e.preventDefault();
        $('#verticalycentered').modal('show');
        });
    function selectText(el){
            var doc = document;
            var element = document.getElementById(el);
            if (doc.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(element);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();        
                var range = document.createRange();
                range.selectNodeContents(element);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
        function copy(selector) {
            var x = document.getElementById("name_zoom");
            var y = document.getElementById("tanggal_zoom");
            x.style.display = "block";
            y.style.display = "block";
            var $temp = $("<div id='toCopy'>");
            $("body").append($temp);
            $temp.attr("contenteditable", true)
                .html($(selector).html());
            selectText("toCopy");
            document.execCommand("copy");
            $temp.remove();
            x.style.display = "none";
            x.style.display = "none";
            alert('sukses copy data');
        }
</script>
@endsection