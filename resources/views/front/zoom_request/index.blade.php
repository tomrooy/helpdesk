@extends('layouts.nice.main')
@section('title','Data Permintaan ZOOM')
@section('css')
<link href="{{asset('nice/assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">
@endsection
@push('nav-zoom') show @endpush
@section('content')
<div class="wrapper">
    @push('main-menu')ZOOM @endpush
    @push('url-main-menu') # @endpush
    @push('sub-menu')Index @endpush   
    
    <div class="card">
        <div class="card-header bg-primary text-white">
            Data History Permintaan ZOOM Yang Diajukan Oleh {{\Auth::User()->location}}
        </div>
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            <a href="{{route('front.zoom_request.create')}}" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</a>
            <br><br>
            <div class="table-responsive">
                <table id="myTable" class="table table-bordered datatable">
                    <thead class="bg bg-primary text-white">
                        <th>#</th>
                        <th>Nama Agenda</th>
                        <th>Tanggal</th>
                        <th>Peserta</th>
                        <th>Jenis Acara</th>
                        <th>Status</th>
                        <th width="150" class="text-center">Action</th>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;    
                        @endphp
                        @foreach ($zoomrequest as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{!! 'Tanggal '.\Carbon\Carbon::createFromFormat('Y-m-d',$item->date)->format('d M Y'). "<br>Jam ". $item->start.'-'.$item->end !!}</td>
                                <td>{{$item->participant}} Orang</td>
                                <td>{{$item->category}}</td>
                                <td><span class="badge bg-success">{{$item->status}}</span></td>
                                <td><a href="{{route('front.zoom_request.show',$item->id)}}" class="btn btn-info"><span class="bx bx-detail"></span> Detail</a></td>
                            </tr>
                            @php
                                $no++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
           
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('nice/assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
@endsection