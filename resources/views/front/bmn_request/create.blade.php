@extends('layouts.nice.main')
@push('nav-umum') show @endpush
@section('title','Form Peminjaman Baru')
@section('content')
@push('main-menu')BMN @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Input Data @endpush   
<div class="card shadow mb-12">
    <div class="card-header py-3 bg-primary">
        <h6 class="m-0 font-weight-bold text-light">Form Permintaan Layanan Rumah Tangga</h6>
    </div>      
    <div class="card-body">
        <br>
        <form method="POST" id="formBmnService" enctype="multipart/form-data" class="row g-3">
            @csrf
            <div class="col-md-4">
                <div class="form-floating">
                  <input type="text" class="form-control" id="floatingName" placeholder="Your Name" value="{{\Auth::User()->name}}" disabled>
                  <label for="floatingName">Nama</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                  <input type="text" class="form-control" id="floatingName" placeholder="Your Name" value="{{\Auth::User()->nip}}" disabled>
                  <label for="floatingNip">NIP</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-floating">
                  <input type="text" class="form-control" id="floatingName" placeholder="Your Name" value="{{\Auth::User()->userLocations->name}}" disabled>
                  <label for="floatingUnit">Unit Kerja</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-floating">
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Silahkan Isi Judul helpdesk anda">
                  <label for="floatingTitle">Nama Kegiatan / Perihal</label>
                </div>
                <strong id="subject-error"></strong>
            </div>
            <div class="col-md-4">
                <div class="form-floating mb-3">
                  <select class="form-select select2" name="category" id="category" aria-label="State">
                    <option value="">Pilih Category</option>
                                @foreach ($bmn_category as $item)
                            <option value="{{$item->category_id}}">{{$item->name}}</option>
                                @endforeach
                  </select>
                  <label for="floatingCategory">Pilih Kategory</label>
                </div>
                <strong id="category-error"></strong>
            </div>
            <div class="col-md-4">
                <div class="form-floating mb-3">
                  <select class="form-select select2" name="bmn" id="bmn" aria-label="State">
                
                  </select>
                  <label for="floatingbmn">Pilih BMN</label>
                </div>
                <strong id="bmn-error"></strong>
            </div>
            <div class="col-md-4">
                <div class="form-floating mb-3">
                    <select required name="priority" id="priority"  class="form-control select2 " aria-label="State">
                            <option value="">--Pilih Priotitas</option>
                        @foreach($priority as $item)   
                            <option value="{{$item->priority_id}}">{{$item->name}}</option>
                        @endforeach    
                    </select>
                  <label for="floatingCategory">Pilih Prioritas</label>
                </div>
                <strong id="priority-error"></strong>
            </div>
            <div class="col-4">
                <div class="form-floating">
                    <input required autocomplete="off" type="date" id="date_request" class="form-control"  name="date_request" >
                    <label for="user_wa">Tanggal Peminjaman </label>
                </div>
                <strong id="date_request-error"></strong>
            </div>
            <div class="col-4">
                <div class="form-floating mb-3">
                    <select required name="sesi" id="sesi"  class="form-control select2 " aria-label="State">
                        <option value="">--Pilih Sesi</option>
                        @foreach($sesi as $item)
                            <option value="{{$item->sesi_id}}">{{$item->name}}</option>
                        @endforeach    
                    </select>
                  <label for="floatingCategory">Pilih Sesi</label>
                </div>
                <div class="text-success">Jika Permintaan Barang Habis Pakai Pilih opsi Seharian</div>
                <strong id="sesi-error"></strong>
            </div>
            <div class="col-4">
                <div class="form-floating">
                    <input required autocomplete="off" type="number" id="user_wa" class="form-control" value="{{old('user_wa')}}" name="user_wa" >
                    <label for="user_wa">User Wa </label>
                    <div class="text-success">*contoh penulisan nomor : 081374757586</div>
                    <div class="invalid-feedback">
                        {{$errors->first('user_wa')}}
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-floating">
                  <textarea class="form-control" name="description" placeholder="Address" id="floatingDescription" style="height: 100px;"></textarea>
                  <label for="floatingDescription">Jelaskan Secara Detail Disini</label>
                </div>
                <strong id="description-error"></strong> 
            </div>        
            <!--<div class="col-6">
                    <div class="form-floating">
                        <input type="file" name="img1" autocomplete="off" type="text"  class="form-control" >
                        <label for="floatingImg1">Upload Lampiran 1</label>
                        <div class="text-danger">*Opsional dan  Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                        <strong id="img1-error"></strong>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-floating">
                        <input type="file" name="img2" autocomplete="off" type="text"  class="form-control" >
                        <label for="floatingImg2">Upload Lampiran 2</label>
                        <div class="text-danger">*Opsional dan Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                        <strong id="img2-error"></strong>
                    </div>
                </div>
            -->
            <br>
            <br>
            <div class="col text-center">
                <button type="submit" class="btn btn-info"><i class="bx bxs-save"></i> Simpan</button>
                <button type="reset" class="btn btn-warning"><i class="bx bx-reset"></i> Reset</button>
            </div>
        </form>
    </div>
    
</div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function() {
        $('select[name="category"]').on('change', function(){
            var title_id = $(this).val();
            var url_get = '{{ route("front.bmn_request.getdata_bmn", ":id") }}';
            url_get = url_get.replace(':id', title_id);
                if(title_id){
                    $.ajax({
                        url: url_get,
                        type : 'GET',
                        dataType : 'Json',
                        success : function(data){
                            console.log(data.name);
                            $('select[name="bmn"]').empty();
                            $.each(data, function(key, values)
                            {
                                $('select[name="bmn"]').append('<option value="'+values+'">'+key+'</option>');
                            });
                        }
                    });
                    
                }
        });
        $('#formBmnService').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            swal({
                title: "Konfirmasi",
                text: "Proses Permintaan?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('front.bmn_request.store') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                    swal({
                        title: 'Prosessing...',
                        html: 'Please wait...',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showConfirmButton : false,
                          
                        });                
                    },
                    success:function(data) {
                        if(data.errors) {
                            if(data.errors.subject){
                                $( '#subject-error' ).html( data.errors.subject[0] ).addClass('alert-danger');
                                $('.subject-class').addClass('is-invalid');
                            }else
                            {
                                $( '#subject-error' ).remove();
                                $('.subject-class').removeClass('is-invalid');
                            }
                            if(data.errors.date){
                                $( '#date-error' ).html( data.errors.date[0] ).addClass('alert-danger');
                                $('.date-class').addClass('is-invalid');
                            }else
                            {
                                $( '#date-error' ).remove();
                                $('.date-class').removeClass('is-invalid');
                            }
                            if(data.errors.sesi){
                                $( '#sesi-error' ).html( data.errors.sesi[0] ).addClass('alert-danger');
                                $('.sesi-class').addClass('is-invalid');
                            }else
                            {
                                $( '#sesi-error' ).remove();
                                $('.sesi-class').removeClass('is-invalid');
                            }
                            if(data.errors.priority){
                                $( '#priority-error' ).html( data.errors.priority[0] ).addClass('alert-danger');
                                $('.priority-class').addClass('is-invalid');
                            }else
                            {
                                $( '#priority-error' ).remove();
                                $('.priority-class').removeClass('is-invalid');
                            }
                            if(data.errors.category){
                                $( '#category-error' ).html( data.errors.category[0] ).addClass('alert-danger');
                                $('.category-class').addClass('is-invalid');
                            }else
                            {
                                $( '#process-error' ).remove();
                                $('.process-class').removeClass('is-invalid');
                            }
                            if(data.errors.bmn){
                                $( '#process-error' ).html( data.errors.bmn[0] ).addClass('alert-danger');
                                $('.bmn-class').addClass('is-invalid');
                            }else
                            {
                                $( '#bmn-error' ).remove();
                                $('.bmn-class').removeClass('is-invalid');
                            }
                        }else
                        {
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 3000
                                }).then( function(){
                                    window.location = "{{ route('front.bmn_request.index') }}";
                              });
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
        
    });
</script>
@endsection