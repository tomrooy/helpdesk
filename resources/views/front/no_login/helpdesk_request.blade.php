<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link href="{{asset('nice/assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('nice/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">
        <title>Form Permintaan Zoom</title>
        <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
            #loading {
                width: 100%;
                height: 100%;
                position: fixed;
                z-index: 9999;
                background: url({{asset('loading2.gif')}}) no-repeat center center rgba(227, 227, 227, 0.8)
            }
        </style>
    </head>
<body class="bg-custom">
    <div id="loading" style="display: none"></div> 
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card shadow-lg border-0 rounded-lg mt-3">
                    <div class="card-header">
                        <h1 style="text-align :center;">Form Bantuan Layanan IT</h1>
                    </div>
                    <div class="card-body"><br>
                        <div id="form_display">
                            <form method="POST" id="formRequestHelpdesk" enctype="multipart/form-data" class="row g-3">
                                @csrf
                                <div class="col-md-4">
                                    <div  id="option-name" class="form-floating">
                                        <input autocomplete="off" type="text" name="name" id="name" value="{{old('name')}}" class="form-control {{$errors->first('name') ? "is-invalid" : ""}}" placeholder="Silahkan Isi Nama" >
                                        <div id="name_list"></div>
                                        <label for="id">Nama</label>
                                        <strong id="name-error"></strong>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <input autocomplete="off" type="hidden" name="nip" id="nip" value="{{old('nip')}}"  class="form-control {{$errors->first('nip') ? "is-invalid" : ""}}" placeholder="NIP">
                                        <input disabled autocomplete="off" type="text"  id="nip_disabled" value="{{old('nip')}}"  class="form-control {{$errors->first('nip') ? "is-invalid" : ""}}" placeholder="NIP">
                                        <label for="nip">NIP</label>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating">
                                        <input autocomplete="off" type="hidden" name="location" id="location" value="{{old('location')}}" class="form-control {{$errors->first('location') ? "is-invalid" : ""}}" placeholder="Bagian/Wilayah">
                                        <input disabled autocomplete="off" type="text"  id="location_disabled" value="{{old('location')}}" class="form-control {{$errors->first('location') ? "is-invalid" : ""}}" placeholder="Bagian/Wilayah">
                                        <label for="id">Bagian/Wilayah</label>
                                     
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                      <input type="text" class="form-control" id="subject" name="subject" placeholder="Silahkan Isi Judul helpdesk anda">
                                      <label for="floatingTitle">Judul Helpdesk</label>
                                    </div>
                                    <strong id="subject-error"></strong>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating mb-3">
                                      <select class="form-select select2" name="category" id="category" aria-label="State">
                                        <option value="">Pilih Category</option>
                                                    @foreach ($category as $item)
                                                <option value="{{$item->category_id}}">{{$item->name}}</option>
                                                    @endforeach
                                      </select>
                                      <label for="floatingCategory">Pilih Kategory</label>
                                    </div>
                                    <strong id="category-error"></strong>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating mb-3">
                                      <select class="form-select select2" name="subcategory" id="subcategory" aria-label="State">
                                    
                                      </select>
                                      <label for="floatingSubCategory">SubKategory</label>
                                    </div>
                                    <strong id="subcategory-error"></strong>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating mb-3">
                                        <select required name="priority" id="priority"  class="form-control select2 " aria-label="State">
                                            @foreach ($priority as $item)
                                                <option value="{{$item->priority_id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                      <label for="floatingCategory">Pilih Prioritas</label>
                                    </div>
                                    <strong id="priority-error"></strong>
                                </div>
                                <div class="col-12">
                                    <div class="form-floating">
                                      <textarea class="form-control" name="description" placeholder="Address" id="description" style="height: 100px;"></textarea>
                                      <label for="description">Jelaskan Secara Detail Disini</label>
                                    </div>
                                    <strong id="description-error"></strong> 
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input required autocomplete="off" type="number" id="handphone" name="handphone" class="form-control">
                                        <label>NO WA yang bisa dihubungi</label>
                                        <strong id="handphone-error"></strong>
                                        *contoh : 085222212333
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-floating">
                                        <input type="file" id="img1" name="img1" autocomplete="off" type="text"  class="form-control" >
                                        <label for="floatingImg1">Upload Lampiran 1</label>
                                        <div class="text-danger">*Opsional dan  Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                                        <strong id="img1-error"></strong>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-floating">
                                        <input type="file" id="img2" name="img2" autocomplete="off" type="text"  class="form-control" >
                                        <label for="floatingImg2">Upload Lampiran 2</label>
                                        <div class="text-danger">*Opsional dan Ukuran Max: 400 KB. jenis File: jpg.png.jpeg</div>
                                        <strong id="img2-error"></strong>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="captcha">
                                        <table>
                                            <tr>
                                                <td>
                                                    <span>{!! captcha_img() !!}</span>&nbsp&nbsp&nbsp
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger" class="reload" id="reload">Reload Captcha</button>
                                                </td>
                                                <td>
                                                    <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                                                    <strong id="captcha-error"></strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col text-center">
                                    <button type="submit" id="store" class="btn btn-info"><i class="fa fa-save"></i> Simpan</button>
                                    <button type="reset" class="btn btn-warning"><i class="fa fa-reset"></i> Reset</button>
                                </div>                                 
                            </form>
                        </div>
                        <div id="form_notif" style="display: none">
                            <h4><p>Terima kasih. Tim IT akan menindaklanjuti permintaan anda</p></h4>
                        </div>
                    </div>
                    <div class="card-footer text-center py-3" bg-info>
                        <div class="text-muted">Tim IT Inspektorat Jenderal Kementerian Hukum dan HAM 2022</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@include('front.no_login.js')
<script>
    $(document).ready(function() {
        $('#name').on('keyup',function() {
            // the text typed in the input field is assigned to a variable 
            var query = $(this).val();
            // call to an ajax function
            $.ajax({
                // assign a controller function to perform search action - route name is search
                url:"{{ route('search_name') }}",
                // since we are getting data methos is assigned as GET
                type:"GET",
                // data are sent the server
                data:{'name':query},
                // if search is succcessfully done, this callback function is called
                success:function (data) {
                    // print the search results in the div called name_list(id)
                    $('#name_list').html(data);
                }
            })
            
        });
        $('#option-name').on('click', 'li', function(){
            // declare the value in the input field to a variable
            var value = $(this).text();
            var result = value.split("-");
            
            //var value1 = $(this).value();
            // assign the value to the search box
            $('#name').val(result[0]);
            $('#nip').val(result[1]);
            $('#nip_disabled').val(result[1]);
            $('#location').val(result[2]);
            $('#location_disabled ').val(result[2]);
            // after click is done, search results segment is made empty
            $('#name_list').html("");
           
        });
        $('select[name="category"]').on('change', function(){
            var title_id = $(this).val();
                if(title_id){
                    $.ajax({
                        url: '../../getsubcategory/'+title_id,
                        type : 'GET',
                        dataType : 'Json',
                        success : function(data){
                            console.log(data.name);
                            $('select[name="subcategory"]').empty();
                            $.each(data, function(key, values)
                            {
                                $('select[name="subcategory"]').append('<option value="'+values+'">'+key+'</option>');
                            });
                        }
                    });
                    
                }
        });
        function reloadCapcta()
        {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function (data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        }
        $('#reload').click(function () 
        {
            reloadCapcta();
        });
        $('#formRequestHelpdesk').submit(function(e) {
            e.preventDefault();
            $('#store').attr('disabled', true);
            let formData = new FormData(this);
            swal({
                title: "Kirim?",
                text: "Apakah Yakin Mengajukan data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('nologin.store.Helpdesk') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                        loading.style.display = "block";                  
                    },
                    success:function(data) {
                        loading.style.display = "none";
                        if(data.errors) {
                            swal({
                                title: "Gagal!",
                                text: 'Cek Kembali Form Pengisian',
                                type: "warning",
                                timer: 4000
                            });
                            $('#store').attr('disabled', false);
                            if(data.errors.name){
                                $( '#name-error' ).html( data.errors.name[0] ).addClass('alert-danger');
                                $( '#name' ).addClass('is-invalid');
                            }
                            if(data.errors.subject){
                                $( '#subject-error' ).html( data.errors.subject[0] ).addClass('alert-danger');
                                $( '#subject' ).addClass('is-invalid');
                            }
                            if(data.errors.category){
                                $( '#category-error' ).html( data.errors.category[0] ).addClass('alert-danger');
                                $( '#category' ).addClass('is-invalid');
                            }
                            if(data.errors.subcategory){
                                $( '#subcategory-error' ).html( data.errors.subcategory[0] ).addClass('alert-danger');
                                $( '#subcategory' ).addClass('is-invalid');
                            }
                            if(data.errors.priority){
                                $( '#priority-error' ).html( data.errors.priority[0] ).addClass('alert-danger');
                                $( '#priority' ).addClass('is-invalid');
                            }
                            if(data.errors.description){
                                $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                                $( '#description' ).addClass('is-invalid');
                            }
                            if(data.errors.handphone){
                                $( '#handphone-error' ).html( data.errors.handphone[0] ).addClass('alert-danger');
                                $( '#handphone' ).addClass('is-invalid');
                            }
                            if(data.errors.img1){
                                $( '#img1-error' ).html( data.errors.img1[0] ).addClass('alert-danger');
                                $( '#img1' ).addClass('is-invalid');
                            }
                            if(data.errors.img2){
                                $( '#img2-error' ).html( data.errors.img2[0] ).addClass('alert-danger');
                                $( '#img2' ).addClass('is-invalid');
                            }
                            if(data.errors.captcha){
                                $( '#captcha-error' ).html( data.errors.captcha[0] ).addClass('alert-danger');
                                $( '#captcha' ).addClass('is-invalid');
                                reloadCapcta();
                            }
                        }else
                        {   
                            
                            swal({
                                    title: "SUKSES!",
                                    text: "Data Berhasil Diajukan",
                                    type: "success",
                                    timer: 2000
                                }).then(function() 
                                {
                                    document.getElementById("formRequestHelpdesk").reset();
                                    form_display.style.display = "none";
                                    form_notif.style.display = "block";
                                });
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
    });
</script>
</html>
