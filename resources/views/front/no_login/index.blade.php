<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('nice/assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
    <style>
        .bg-custom{
            background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
            background-repeat: repeat;
        }
        .status {
            height: 200px;
            width: 100%;
            background: #13a2ce;
            float: left;
            margin-top: 2px;
            margin-left: 2px;
            border: 1px solid #ccc;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-box-pack: center;
            -webkit-box-align: center;
            font-size: 100px

            display: -moz-box;
            -moz-box-orient: vertical;
            -moz-box-pack: center;
            -moz-box-align: center;
            
            display: box;
            box-orient: vertical;
            box-pack: center;
            box-align: center; 
            text-align:center;
        }
        .container{
            height: 100%;
        }
    </style>
    <title>Portal Layanan IT</title>
</head>
<body class="bg-custom">
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{asset('kumham.png')}}" height="100%" width="100%">
                            </div>
                            <div class="col-md-8">
                                <h1 style="text-align :center; padding :40px;">PORTAL LAYANAN IT INSPEKTORAT JENDERAL</h1>
                            </div>
                            <div class="col-md-2">
                                <!--<img src="{{asset('kumhsam.png')}}" style="float: right" height="50%" width="50%">  -->
                            </div>                          
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <a href="{{route('nologin.view.requestZoom')}}" style="font-size: 30px"><div class="status">Layanan Permintaan Link Zoom</div></a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{route('nologin.view.Helpdesk')}}" style="font-size: 30px"><div class="status">Layanan Bantuan IT</div></a>   
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <p style="font-weight: bold; text-align :center;">
                            &copy; 2022. Copyright <strong><span>IT Inspektorat Jenderal</span></strong>. All Rights Reserved
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>