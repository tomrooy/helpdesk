<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link href="{{asset('nice/assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('nice/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">
        <title>Form Permintaan Zoom</title>
        <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
        <style>
            .bg-custom{
                background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
                background-repeat: repeat;
            }
            #loading {
                width: 100%;
                height: 100%;
                position: fixed;
                z-index: 9999;
                background: url({{asset('loading2.gif')}}) no-repeat center center rgba(227, 227, 227, 0.8)
            }
        </style>
    </head>
<body class="bg-custom">
    <div id="loading" style="display: none"></div>
    <main>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="card shadow-lg border-0 rounded-lg mt-3">
                        <div class="card-header">
                            <h1 style="text-align :center;">Form Permohonan Link Zoom</h1>
                        </div>
                        <div class="card-body"><br>
                            <div id="form_display">
                                <form method="POST" id="formRequestZoom" enctype="multipart/form-data" class="row g-3">
                                    @csrf
                                    <div class="col-md-4">
                                        <div  id="option-name" class="form-floating">
                                            <input autocomplete="off" type="text" name="name" id="name" value="{{old('name')}}" class="form-control {{$errors->first('name') ? "is-invalid" : ""}}" placeholder="Silahkan Isi Nama" >
                                            <div id="name_list"></div>
                                            <label for="id">Nama</label>
                                            <strong id="name-error"></strong>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-floating">
                                            <input autocomplete="off" type="hidden" name="nip" id="nip" value="{{old('nip')}}"  class="form-control {{$errors->first('nip') ? "is-invalid" : ""}}" placeholder="NIP">
                                            <input disabled autocomplete="off" type="text"  id="nip_disabled" value="{{old('nip')}}"  class="form-control {{$errors->first('nip') ? "is-invalid" : ""}}" placeholder="NIP">
                                            <label for="nip">NIP</label>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-floating">
                                            <input autocomplete="off" type="hidden" name="location" id="location" value="{{old('location')}}" class="form-control {{$errors->first('location') ? "is-invalid" : ""}}" placeholder="Bagian/Wilayah">
                                            <input disabled autocomplete="off" type="text"  id="location_disabled" value="{{old('location')}}" class="form-control {{$errors->first('location') ? "is-invalid" : ""}}" placeholder="Bagian/Wilayah">
                                            <label for="id">Bagian/Wilayah</label>
                                         
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <textarea autocomplete="off" type="text" id="subject" class="form-control" value="{{old('subject')}}" name="subject" placeholder="Input Nama Kegiatan" ></textarea>
                                            <label for="subject">Judul Kegiatan</label>
                                            <strong id="subject-error"></strong>
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-floating">
                                            <select name="participant" id="participant" class="form-control {{$errors->first('participant') ? "is-invalid" : ""}}">
                                                <option value="">--Pilih Jumlah Peserta --</option>
                                                <option value="100" @if(old('participant') == '100') {{'selected'}} @endif>Maksimal 100 Peserta</option>
                                                <option value="100-300" @if(old('participant') == '100-300') {{'selected'}} @endif>100-300 Peserta</option>
                                                <option value="300-500" @if(old('participant') == '300-500') {{'selected'}} @endif>300-500 Peserta</option>
                                                <option value="500-1000" @if(old('participant') == '500-1000') {{'selected'}} @endif>500-1000 Peserta</option>
                                            </select>
                                            <label><b>Jumlah Peserta</b></label>
                                            <strong id="participant-error"></strong>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-floating">
                                            <select name="category" id="category" class="form-control {{$errors->first('category') ? "is-invalid" : ""}}">
                                                <option value="">--kategory meeting --</option>
                                                <option value="Rapat Kordinasi" @if(old('category') == 'Rapat Kordinasi') {{'selected'}} @endif>Rapat Kordinasi</option>
                                                <option value="Rapat Internal" @if(old('category') == 'Rapat Internal') {{'selected'}} @endif>Rapat Internal</option>
                                                <option value="Acara Workshop" @if(old('category') == 'Acara Workshop') {{'selected'}} @endif>Acara Workshop</option>
                                                <option value="Acara Gemar Belajar" @if(old('category') == 'Acara Gemar Belajar') {{'selected'}} @endif>Acara Gemar Belajar</option>
                                            </select>
                                            <label><b>Kategory Kegiatan</b></label>
                                            <strong id="category-error"></strong>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-floating">
                                            <input autocomplete="off" type="date" id="date" name="date" class="form-control">
                                            <label>Tanggal Agenda Meeting</label>
                                            <strong id="date-error"></strong>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-floating">
                                            <select name="start" class="select2 form-control"  id="start">
                                                    <option value="">----</option>
                                                @foreach($time as $item)
                                                    <option value="{{$item->name}}" @if(old('start') == '{{$item->name}}') 'selected' @endif>{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            <label>Jam Mulai</label>
                                            <strong id="start-error"></strong>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-floating">
                                            <select name="end" class="form-control"  id="end">
                                                <option value="">----</option>
                                                @foreach($time as $item)
                                                    <option value="{{$item->name}}" @if(old('end') == '{{$item->name}}') 'selected' @endif>{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            <label>Jam Berakhir</label>
                                            <strong id="end-error"></strong>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-floating">
                                            <input autocomplete="off" required type="number" id="handphone" name="handphone" class="form-control">
                                            <label>NO WA yang bisa dihubungi</label>
                                            <strong id="handphone-error"></strong>
                                            *contoh : 085222212333
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-floating">
                                            <input type="file" name="attachment" id="attachment"  class="form-control {{$errors->first('attachment') ? 'is-invalid': ''}}" >
                                            <strong id="subject-error"></strong>
                                            <label>Nodin Atau Surat Undangan(opsional)</label>
                                            *format yang diterima adalah PDF max : 1 MB
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-8">
                                        <div class="captcha">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <span>{!! captcha_img() !!}</span>&nbsp&nbsp&nbsp
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger" class="reload" id="reload">Reload Captcha</button>
                                                    </td>
                                                    <td>
                                                        <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                                                        <strong id="captcha-error"></strong>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" name="activity" id="flexSwitchCheckDefault">
                                        <label class="form-check-label" for="flexSwitchCheckDefault">Request Presensi Online Kegiatan</label>
                                    </div>
                                    <div class="col text-center">
                                        <button type="submit" class="btn btn-info" id="store"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="reset" class="btn btn-warning"><i class="fa fa-reset"></i> Reset</button>
                                    </div>
                                     
                                </form>
                            </div>
                            <div id="form_notif" style="display: none">
                                <h4><p>Terima kasih. Tim IT akan menindaklanjuti permintaan anda</p></h4>
                            </div>
                        </div>
                        <div class="card-footer text-center py-3" bg-info>
                            <div class="text-muted">Tim IT Inspektorat Jenderal Kementerian Hukum dan HAM 2022</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
@include('front.no_login.js')
<script>
    $(document).ready(function() {
        $('#name').on('keyup',function() {
            // the text typed in the input field is assigned to a variable 
            var query = $(this).val();
            // call to an ajax function
            $.ajax({
                // assign a controller function to perform search action - route name is search
                url:"{{ route('search_name') }}",
                // since we are getting data methos is assigned as GET
                type:"GET",
                // data are sent the server
                data:{'name':query},
                // if search is succcessfully done, this callback function is called
                success:function (data) {
                    // print the search results in the div called name_list(id)
                    $('#name_list').html(data);
                }
            })
            
        });
        $('#option-name').on('click', 'li', function(){
            // declare the value in the input field to a variable
            var value = $(this).text();
            var result = value.split("-");
            
            //var value1 = $(this).value();
            // assign the value to the search box
            $('#name').val(result[0]);
            $('#nip').val(result[1]);
            $('#nip_disabled').val(result[1]);
            $('#location').val(result[2]);
            $('#location_disabled ').val(result[2]);
            // after click is done, search results segment is made empty
            $('#name_list').html("");
           
        });
        function reloadCapcta()
        {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function (data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        }
        $('#reload').click(function () 
        {
            reloadCapcta();
        });
        $('#formRequestZoom').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $('#store').attr('disabled', true);
            swal({
                title: "Kirim?",
                text: "Apakah Yakin Mengajukan data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url:"{{ route('nologin.store.requestZoom') }}",
                    data:formData,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                        loading.style.display = "block";                  
                    },
                    success:function(data) {
                        loading.style.display = "none";
                        if(data.errors) {
                            swal({
                                title: "Gagal!",
                                text: 'Cek Kembali Form Pengisian',
                                type: "warning",
                                timer: 4000
                            });
                            $('#store').attr('disabled', false);
                            if(data.errors.name){
                                $( '#name-error' ).html( data.errors.name[0] ).addClass('alert-danger');
                                $( '#name' ).addClass('is-invalid');
                            }
                            if(data.errors.subject){
                                $( '#subject-error' ).html( data.errors.subject[0] ).addClass('alert-danger');
                                $( '#subject' ).addClass('is-invalid');
                            }
                            if(data.errors.participant){
                                $( '#participant-error' ).html( data.errors.participant[0] ).addClass('alert-danger');
                                $( '#participant' ).addClass('is-invalid');
                            }
                            if(data.errors.category){
                                $( '#category-error' ).html( data.errors.category[0] ).addClass('alert-danger');
                                $( '#category' ).addClass('is-invalid');
                            }
                            if(data.errors.date){
                                $( '#date-error' ).html( data.errors.date[0] ).addClass('alert-danger');
                                $( '#date' ).addClass('is-invalid');
                            }
                            if(data.errors.start){
                                $( '#start-error' ).html( data.errors.start[0] ).addClass('alert-danger');
                                $( '#start' ).addClass('is-invalid');
                            }
                            if(data.errors.end){
                                $( '#handphone-error' ).html( data.errors.handphone[0] ).addClass('alert-danger');
                                $( '#handphone' ).addClass('is-invalid');
                            }
                            if(data.errors.handphone){
                                $( '#end-error' ).html( data.errors.handphone[0] ).addClass('alert-danger');
                                $( '#end' ).addClass('is-invalid');
                            }
                            if(data.errors.attachment){
                                $( '#attachment-error' ).html( data.errors.attachment[0] ).addClass('alert-danger');
                                $( '#attachment' ).addClass('is-invalid');
                            }
                            if(data.errors.captcha){
                                $( '#captcha-error' ).html( data.errors.captcha[0] ).addClass('alert-danger');
                                $( '#captcha' ).addClass('is-invalid');
                                reloadCapcta();
                            }
                            

                        }else
                        {   
                            
                            swal({
                                    title: "SUKSES!",
                                    text: "Data Berhasil Diajukan",
                                    type: "success",
                                    timer: 2000
                                }).then(function() {
                                    document.getElementById("formRequestZoom").reset();
                                    form_display.style.display = "none";
                                    form_notif.style.display = "block";
                                    });
                        }
                    },
                    error : function(xhr, status, errorThrown)
                        {
                            //Here the status code can be retrieved like;
                            xhr.status;
                    
                            //The message added to Response object in Controller can be retrieved as following.
                            xhr.responseText;
                        }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
    });
</script>
</html>
