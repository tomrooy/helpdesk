<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>@yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('nice/assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('nice/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('nice/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('nice/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('nice/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
   
    <!-- Template Main CSS File -->
    <link href="{{asset('nice/assets/css/style.css')}}" rel="stylesheet">
    @yield('css')
    <!-- =======================================================
        * Template Name: NiceAdmin - v2.2.0
        * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
        * Author: BootstrapMade.com
        * License: https://bootstrapmade.com/license/ !--->

    <style>
    .modal-backdrop {
      z-index: -1;
        }
        #loading {
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 9999;
            background: url({{asset('loading2.gif')}}) no-repeat center center rgba(227, 227, 227, 0.8)
        }
    #statusjaringan.online{
    background: green;
    color: #cbd5e0;
    padding: 20px;
    text-align: center;
    }
    #statusjaringan.offline{
    background: tomato;
    color: cornsilk;
    padding: 20px;
    }
    </style>
</head>
<body>
    <div id="loading"></div> 
    <!-- ======= Header ======= -->
    <header id="header" class="header fixed-top d-flex align-items-center">
        
        @include('layouts.nice.components.navbar')
    </header>
    <!-- End Header -->

    <!-- ======= Sidebar ======= -->
    <aside id="sidebar" class="sidebar">
      @include('layouts.nice.components.sidebar')
    </aside><!-- End Sidebar-->

    <main id="main" class="main">
        <div class="pagetitle">
            <h1 class="h3 mb-0 text-gray-800">
                <div class="page-header" id="banner">
                    <div class="row">
                        <div class="" style="padding: 15px 15px 0 15px;">
                            <div class="well well-sm">
                                <img src="{{asset('kumham.png')}}" class="thumbnail span3" style="display: inline; float: left; margin-right: 20px; width: 100px; height: 100px">
                                <h2 style="margin: 15px 0 10px 0; color: #000;">Inspektorat Jenderal</h2>
                                <div style="color: #000; font-size: 16px; font-family: Tahoma" class="clearfix"><b>Alamat :JL. HR. Rasuna Said Kav. 8, Setiabudi, Kuningan Jakarta Selatan</b>
                                    @php
                                        $date = \Carbon\Carbon::now()->locale('id');
                                        $date->settings(['formatFunction' => 'translatedFormat']);
                                    @endphp
                                    <div class="m-0 font-weight-bold">{{$date->format('l, j F Y')}}</div>
                                    <div id='time-part' class="m-0 font-weight-bold"></div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </h1>
            <section id="statusjaringan" class=""></section>
            <nav>
                <ol class="breadcrumb justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href=@stack('url-main-menu')>@stack('main-menu')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@stack('sub-menu')</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->

        <section class="section dashboard">
            @yield('content')

        </section>
    </main>
    <!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">
        @include('layouts.nice.components.footer')
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="{{asset('nice/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    @yield('js')
    
    <!-- Template Main JS File -->
    <script src="{{asset('nice/assets/js/main.js')}}"></script>
    <script>
        var load = document.getElementById("loading"); 
        window.addEventListener('load', function () {
        load.style.display = "none";
        var statusJaringan = document.getElementById('statusjaringan');
        statusJaringan.innerHTML = '';

        var statusOnlineOffline = function statusOnlineOffline() {
            statusJaringan.classList.remove('offline');
            statusJaringan.classList.remove('online');
            var kondisi = navigator.onLine;

            if (!kondisi) {
            statusJaringan.innerHTML = 'Kamu sedang offline, cek internet koneksi kamu ya';
            statusJaringan.classList.add('offline');
            statusJaringan.style.display = 'block';
            return;
            }
            statusJaringan.innerHTML = 'Kamu sudah online';
            statusJaringan.classList.add('online');
           
            setTimeout(function () {
            statusJaringan.style.display = 'none';
            console.log('setTimeout');
            }, 3000);
        };

        window.addEventListener('online', statusOnlineOffline);
        window.addEventListener('offline', statusOnlineOffline);
        }); 
        $(document).ready(function() {
            var interval = setInterval(function() {
                var momentNow = moment();
                $('#time-part').html(momentNow.format('HH:mm:ss A'));
            }, 100);
            $('body').on('click', '#button_sign_out', function(e) {
                e.preventDefault();
                $('#exampleModal').modal('show');
                });
        });
    </script>
</body>

</html>