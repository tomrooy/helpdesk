<ul class="sidebar-nav" id="sidebar-nav">
  <li class="nav-item">
    <a class="nav-link " href="{{route('home')}}">
      <i class="bi bi-grid"></i>
      <span>Dashboard</span>
    </a>
  </li><!-- End Dashboard Nav -->
    @role('Super_Admin')
      <li class="nav-heading">Data master</li>
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-layout-text-window-reverse"></i><span>Users</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="tables-nav" class="nav-content collapse @stack('nav-user')" data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('user.index')}}" class="{{NavbarLinkActive::set_active('user.index') }}">
              <i class="bi bi-circle"></i><span>Data User</span>
            </a>
          </li>
          <li>
            <a href="{{route('role.index')}}" class="{{NavbarLinkActive::set_active('role.index') }}">
              <i class="bi bi-circle"></i><span>Role</span>
            </a>
          </li>
          <li>
            <a href="{{route('role.roles_permission')}}" class="{{NavbarLinkActive::set_active('role.roles_permission') }}">
              <i class="bi bi-circle"></i><span>Permission</span>
            </a>
          </li>
        </ul>
      </li><!-- End User Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#charts-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-bar-chart"></i><span>Helpdesk</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="charts-nav" class="nav-content collapse @stack('nav-helpdesk')" data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('module.helpdesk.index')}}" class="{{NavbarLinkActive::set_active('module.helpdesk.index') }}">
              <i class="bi bi-circle"></i><span>Helpdesk Data</span>
            </a>
          </li>
          <li>
            <a href="{{route('module.category.index')}}" class="{{NavbarLinkActive::set_active('module.category.index') }}">
              <i class="bi bi-circle"></i><span>Kategory</span>
            </a>
          </li>
          <li>
            <a href="{{route('module.subcategory.index')}}" class="{{NavbarLinkActive::set_active('module.subcategory.index') }}">
              <i class="bi bi-circle"></i><span>Sub Kategory</span>
            </a>
          </li>
        </ul>
      </li><!-- End Helpdesk Nav -->
      
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#icons-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-gem"></i><span>ZOOM</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="icons-nav" class="nav-content collapse @stack('nav-zoom')" data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('module.zoom.index')}}" class="{{NavbarLinkActive::set_active('module.zoom.index') }}">
              <i class="bi bi-circle "></i><span>Data Kegiatan Zoom</span>
            </a>
          </li>
          <li>
            <a href="{{route('module.zoom_request.index')}}" class="{{NavbarLinkActive::set_active('module.zoom_request.index') }}">
              <i class="bi bi-circle"></i><span>Data Request Zoom</span>
            </a>
          </li>
          <li>
            <a href="{{route('module.zoom.calendar.view')}}" class="{{NavbarLinkActive::set_active('module.zoom.calendar.view') }}">
              <i class="bi bi-circle"></i><span>Data Kalendar Zoom</span>
            </a>
          </li>
          <li>
            <a href="{{route('module.zoom.search')}}" class="{{NavbarLinkActive::set_active('module.zoom.search') }}">
              <i class="bi bi-circle"></i><span>Cari Kegiatan ZOOM</span>
            </a>
          </li>
        </ul>
      </li><!-- End Zoom Nav -->
    @endrole

      @role('Super_Admin|User_Bmn|Admin_Bmn')
          <li class="nav-item">
            <a class="nav-link collapsed" data-bs-target="#umum-nav" data-bs-toggle="collapse" href="#">
              <i class="bi bi-bar-chart"></i><span>Layanan Umum</span><i class="bi bi-chevron-down ms-auto"></i>
            </a>
            <ul id="umum-nav" class="nav-content collapse @stack('nav-umum')" data-bs-parent="#sidebar-nav">
              @role('User_Bmn')
                  <li>
                    <a href="{{route('front.bmn_request.create')}}" class="{{NavbarLinkActive::set_active('front.bmn_request.create') }}">
                      <i class="bi bi-circle"></i><span>Form Pemintaan Layanan</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{route('front.bmn_request.calendar')}}" class="{{NavbarLinkActive::set_active('front.bmn_request.calendar') }}">
                      <i class="bi bi-circle"></i><span>Calendar Kegiatan</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{route('front.bmn_request.index')}}" class="{{NavbarLinkActive::set_active('front.bmn_request.index') }}">
                      <i class="bi bi-circle"></i><span>Data Pemintaan Layanan</span>
                    </a>
                  </li>
              @endrole
              @role('Super_Admin|Admin_Bmn')
                  <li>
                    <a href="{{route('module.bmn_request.index')}}" class="{{NavbarLinkActive::set_active('module.bmn_request.index') }}">
                      <i class="bi bi-circle"></i><span>Data Pemintaan Layanan</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{route('front.bmn_request.calendar')}}" class="{{NavbarLinkActive::set_active('front.bmn_request.calendar') }}">
                      <i class="bi bi-circle"></i><span>Calendar Kegiatan</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{route('module.bmn.index')}}" class="{{NavbarLinkActive::set_active('module.bmn.index') }}">
                      <i class="bi bi-circle"></i><span>Master BMN</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{route('module.bmn_category.index')}}" class="{{NavbarLinkActive::set_active('module.bmn_category.index') }}">
                      <i class="bi bi-circle"></i><span>Kategory</span>
                    </a>
                  </li>
              @endrole
            </ul>
          </li><!-- End Helpdesk Nav -->
      @endrole
    
    @role('Super_Admin')
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#activity-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-list-check"></i><span>Data Kegiatan</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="activity-nav" class="nav-content collapse @stack('nav-activity')" data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('module.activity.index')}}" class="{{NavbarLinkActive::set_active('module.activity.index') }}">
              <i class="bi bi-circle "></i><span>Data Kegiatan Zoom</span>
            </a>
          </li>
          <!--<li>
            <a href="{{route('module.daily_present.index')}}" class="{{NavbarLinkActive::set_active('module.daily_present.index') }}">
              <i class="bi bi-circle"></i><span>Data Kehadiran SIP</span>
            </a>
          </li> -->
          <li>
            <a href="{{route('module.apel.index')}}" class="{{NavbarLinkActive::set_active('module.apel.index') }}">
              <i class="bi bi-circle"></i><span>Data Apel Itjen</span>
            </a>
          </li>
        </ul>
      </li><!-- End Activity Nav -->

      <li class="nav-heading">Rekap Laporan</li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('helpdesk.report')}}">
          <i class="bi bi-person"></i>
          <span>Data Laporan</span>
        </a>
      </li><!-- End Laporan Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('user.faq')}}">
          <i class="bi bi-question-circle"></i>
          <span>F.A.Q</span>
        </a>
      </li><!-- End F.A.Q Page Nav -->
    @endrole
    @role('User|Kepegawaian|User_Bmn')
    <li class="nav-item">
      <a class="nav-link collapsed" data-bs-target="#charts-nav" data-bs-toggle="collapse" href="#">
        <i class="bi bi-bar-chart"></i><span>IT Helpdesk</span><i class="bi bi-chevron-down ms-auto"></i>
      </a>
      <ul id="charts-nav" class="nav-content collapse @stack('nav-helpdesk')" data-bs-parent="#sidebar-nav">
        <li>
          <a href="{{route('front.helpdesk.create')}}" class="{{NavbarLinkActive::set_active('front.helpdesk.create') }}">
            <i class="bi bi-circle"></i><span>Form Helpdesk</span>
          </a>
        </li>
        <li>
          <a href="{{route('front.helpdesk.index')}}" class="{{NavbarLinkActive::set_active('front.helpdesk.index') }}">
            <i class="bi bi-circle"></i><span>Data Helpdesk</span>
          </a>
        </li>
      </ul>
    </li><!-- End Helpdesk Nav -->

    <li class="nav-item">
      <a class="nav-link collapsed" data-bs-target="#icons-nav" data-bs-toggle="collapse" href="#">
        <i class="bi bi-gem"></i><span>ZOOM</span><i class="bi bi-chevron-down ms-auto"></i>
      </a>
      <ul id="icons-nav" class="nav-content collapse @stack('nav-zoom')" data-bs-parent="#sidebar-nav">
        <li>
          <a href="{{route('front.zoom_request.create')}}" class="{{NavbarLinkActive::set_active('front.zoom_request.create') }}">
            <i class="bi bi-circle "></i><span>Form Permintaan ZOOM </span>
          </a>
        </li>
        <li>
          <a href="{{route('front.zoom_request.index')}}" class="{{NavbarLinkActive::set_active('front.zoom_request.index') }}">
            <i class="bi bi-circle"></i><span>Data Request Zoom</span>
          </a>
        </li>
      </ul>
    </li><!-- End Zoom Nav -->
    @endrole
    @role('Kepegawaian')
    <li class="nav-item">
      <a class="nav-link collapsed" data-bs-target="#activity-nav" data-bs-toggle="collapse" href="#">
        <i class="bi bi-list-check"></i><span>Data Kegiatan</span><i class="bi bi-chevron-down ms-auto"></i>
      </a>
      <ul id="activity-nav" class="nav-content collapse @stack('nav-activity')" data-bs-parent="#sidebar-nav">
        <li>
          <a href="{{route('module.apel.index')}}" class="{{NavbarLinkActive::set_active('module.apel.index') }}">
            <i class="bi bi-circle"></i><span>Data Apel Itjen</span>
          </a>
        </li>
      </ul>
    </li><!-- End Activity Nav -->
    @endrole
    
</ul>

