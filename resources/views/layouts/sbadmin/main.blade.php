<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>@yield('title')</title>
        <link rel="icon" href="{{asset('sbadmin/favicon-32x32.png')}}" type="image/gif" sizes="16x16">
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="{{asset('sbadmin/css/styles.css')}}" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        @yield('css')
        <style>
            #loading { 
                width: 50px; height: 50px; border-radius: 100%; border: 5px solid #ccc; border-top-color: #1328e4; position: fixed; top: 0; left: 0; right: 0; bottom: 0; margin: auto; z-index: 99; animation: putar 2s ease infinite; } @keyframes putar { from { transform: rotate(0deg); } to { transform: rotate(360deg); } 
            }
            .modal-backdrop {
                z-index: -1;
              }
              form label{
                font-weight:bold;
              }
        </style>
    </head>
    <body class="sb-nav-fixed">
        <div id="loading"></div>
        @include('layouts.sbadmin.components.navbar')
        <div id="layoutSidenav">
            @include('layouts.sbadmin.components.sidebar')
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="h3 mb-0 text-gray-800">
                            <div class="page-header" id="banner">
                                <div class="row">
                                  <div class="" style="padding: 15px 15px 0 15px;">
                                    <div class="well well-sm">
                                        <img src="{{asset('kumham.png')}}" class="thumbnail span3" style="display: inline; float: left; margin-right: 20px; width: 100px; height: 100px">
                                        <h2 style="margin: 15px 0 10px 0; color: #000;">Inspektorat Jenderal</h2>
                                        <div style="color: #000; font-size: 16px; font-family: Tahoma" class="clearfix"><b>Alamat :JL. HR. Rasuna Said Kav. 8, Setiabudi, Kuningan Jakarta Selatan</b></div>
                                     </div>
                                  </div>
                                </div>
                              </div>
                        </h1>
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb justify-content-end">
                                <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                                <li class="breadcrumb-item"><a href=@stack('url-main-menu')>@stack('main-menu')</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@stack('sub-menu')</li>
                            </ol>
                        </nav>
                        @yield('content')
                    </div>
                </main>
                
                @include('layouts.sbadmin.components.footer')
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="{{asset('sbadmin/js/jquery/jquery.min.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{asset('sbadmin/js/scripts.js')}}"></script>
        @yield('js')
        <script>
            var load = document.getElementById("loading"); 
            window.addEventListener('load', function(){ load.style.display = "none"; }); 
         
        </script>
    </body>
</html>
