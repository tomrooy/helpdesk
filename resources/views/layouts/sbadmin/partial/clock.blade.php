var interval = setInterval(function() {
    var momentNow = moment();
    $('#date-part').html(momentNow.format('dddd')
                        .substring(0,9).toUpperCase()+' '+ momentNow.format('DD MMMM YYYY'));
    $('#time-part').html(momentNow.format('HH:mm:ss A'));
}, 100);