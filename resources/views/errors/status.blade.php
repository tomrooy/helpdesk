@if(session('status_success'))
    <div class="alert alert-success btn-md alert-success">
        {{session('status_success')}}
    </div>
@endif
@if(session('status_error'))
    <div class="alert alert-danger btn-md alert-danger">
        {{session('status_error')}}
    </div>
@endif                       