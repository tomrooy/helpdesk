Highcharts.chart('highcharts', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        },
        backgroundColor: null,
        //plotBackgroundImage: 'https://pengaduan.kemenkumham.go.id/img/background.png'
    },
    title: {
        text: ''
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        },
        series : {
          cursor: 'pointer',
          point: {
              events: {
                  click: 
                      function () {
                      window.open('/admin/employee/filter_religion/'+ this.options.name, '_blank'); 
                                  }
                      }
              }
      }
       
    },
    series: [{
        name: 'Helpdesk',
        colorByPoint: true,
        data: {!!json_encode($highcharts, JSON_NUMERIC_CHECK)!!}
    }],
    credits: {
      enabled: false
      },
});