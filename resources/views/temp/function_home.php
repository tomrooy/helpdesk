//$highcharts   =   Letter::SelectRaw('COUNT(id) as y,letter_type as name')
        //                        ->groupBy('letter_type')->get();
        //$highcharts     =   LetterType::withCount('letters')->get();
        //foreach($highcharts as $item)
        //{
        //    $highcharts[] = [
        //        'y'  => $item->letters_count,
        //        'name'=> $item->name,
        //   ];
        //}
        $data2  =   Status::withCount('helpdesks')->get();
        $data       =   $data2->flatten(1)->pluck('helpdesks_count');
        $labels     =   $data2->flatten(1)->pluck('name');

        $colors = $labels->map(function($item) {
            return $rand_color = '#' . substr(md5(mt_rand()), 0, 6);
        });

        $chart = new HelpdeskChart;
        $chart->labels($labels);
        $chart->dataset('Jumlah helpdesk', 'pie', $data)->backgroundColor($colors);
        