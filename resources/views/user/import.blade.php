@extends('layouts.nice.main')
@section('title','User Dashboard')
@section('css')
@livewireStyles
@endsection
@section('content')
@push('main-menu')User @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Import Data @endpush   
    <div class="card shadow mb-12">
        <div class="card-header py-3 bg-primary">
            <h6 class="m-0 font-weight-bold text-light">Import User</h6>
           
        </div>      
        <div class="card-body">
            <br>
            @if(session('status'))
                <div class="alert alert-success btn-sm alert-small" >
                    {{session('status')}}
                </div>
            @endif
            <div class="form-group row">
                <div class="col-md-6">
                    <form method="POST" id="importUser" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Input File Excel</label>
                            <input type="file" class="form-control {{$errors->first('file_excel') ? "is-invalid" : ""}}" id="file_excel" name="file_excel">
                            <strong id="file_excel-error"></strong>
                            <div class="text-info">*Input File Excel dengan format .xls xlsx dengan ukuran maksimum 2 MB</div><br>
                        </div>
                        <div>
                            <div class="col text-center">
                                <button type="submit" id="btn-submit" class="btn bg-success btn-lg text-white"><i class="fas fa-save"></i> IMPORT</button>
                                <button type="reset" class="btn btn-warning btn-lg "><i class="fas fa-recycle"></i> RESET</button>
                            </div> 
                        </div>
                    </form>
                </div>
       
                <div class="col-md-6">
                    <p>petunjuk pengisian excel</p>
                    <table class="table table-responsive-md">
                        <tr>
                            <td>Kolom A</td>
                            <td>Nama</td>
                        </tr>
                        <tr>
                            <td>Kolom B</td>
                            <td>Nip</td>
                        </tr>
                        <tr>
                            <td>Kolom A</td>
                            <td>Lokasi</td>
                        </tr>
                    </table>
                    <br><br>
                    
                </div>
            </div>
        </div>
        
    </div>
@endsection
@section('js')
@livewireScripts
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function() {
        $('#importUser').submit(function(e){
            e.preventDefault();
            let formData    =   new FormData(this);
            swal({
                title: "Proses?",
                text: "Apakah Yakin Import User!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e){
                if (e.value === true) {
                    swal({
                        title: 'Uploading...',
                        html: 'Please wait...',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showConfirmButton : false,
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url:"{{ route('user.import.process') }}",
                        type: 'POST',
                        data:formData,
                        contentType: false,
                        processData: false,
                        success:function(data) {
                            if(data.errors){
                                if(data.errors.file_excel)
                                {
                                    $('#file_excel-error' ).html( data.errors.file_excel[0] ).addClass('invalid-feedback');
                                    $('#file_excel').addClass('is-invalid')
                                    swal({
                                    title: "GAGAL!",
                                    text: "Cek Form Kembali",
                                    type: "error",
                                    timer: 2000
                                 });
                                }                                
                            }else
                            {
                                swal.close();
                                swal({
                                    title: "Success!",
                                    text: "Berhasil Ditambah",
                                    type: "success",
                                    timer: 2000
                                 });
                               
                                document.getElementById("importUser").reset();
                                
                            }
                        }
                    });
                }
            });
        });

    });
  
  
</script>
@endsection