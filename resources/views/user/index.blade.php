@extends('layouts.nice.main')
@section('title','User Dashboard')
@section('css')
@livewireStyles
@endsection
@section('content')
@push('nav-user') show @endpush 
@push('main-menu')User @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush   
    <div class="card shadow mb-12">
        <div class="card-header py-3 bg-primary">
            <h6 class="m-0 font-weight-bold text-light">Master User</h6>
           
        </div>      
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            @livewire('admin.user-component')
        </div>
        
    </div>
@endsection
@section('js')
@livewireScripts
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
    });
    window.livewire.on('userStore', () => {
        $('#exampleModal').modal('hide');
    });
  
</script>
<x-livewire-alert::scripts />
@endsection