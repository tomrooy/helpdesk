<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="{{asset('sbadmin/css/styles.css')}}" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <title>Home Inspektorat Jenderal</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
<style>
    
    body{
		background-image: url("kumhamlogo.png");
        background-repeat: repeat;
        
	}
    .bg-content-image{
        background-image: url("https://pengaduan.kemenkumham.go.id/img/background.png");
        background-repeat: repeat;
    }
	.contact{
		padding: 6%;
		height: 400px;
	}
    .img{
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
    
    @foreach ($zoomaccount as $item)
        .{{$item->username}}  
        {
            background-color: {{$item->color}};
            color: #ffffff;
            padding: 5px;
        }
    @endforeach

</style>
</head>
<body>
<div class="wrapper contact">
    <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title col text-center" id="exampleModalLabel"><b>Data Lengkap ZOOM</b></h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="overflow: hidden;">
                    <form>
                        <div class="row mb-3">
                            <label for="name" class="col-sm-2 col-form-label">Nama Kegiatan</label>
                            <div class="col-sm-10">
                              <textarea type="text" class="form-control" id="title"></textarea>
                            </div>
                          </div>
                          <div class="row mb-3">
                            <label for="name" class="col-sm-2 col-form-label">Jadwal Kegiatan</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="schedule"></input>
                            </div>
                          </div>
                          <div class="row mb-3">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">ID dan Password</label>
                            <div class="col-sm-3">
                              <input type="text" class="form-control" id="zoom_id">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="password">
                              </div>
                              <div class="col-sm-5">
                                <button id="something" class="btn btn-info"><i class="fas fa-video"></i> Start Meeting</button>
                              </div>
                          </div>
                          <div class="row mb-3">
                            <label for="name" class="col-sm-2 col-form-label">Link ZOOM</label>
                            <div class="col-sm-10">
                              <textarea type="text" class="form-control" id="link"></textarea>
                            </div>
                          </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close-btn " data-bs-dismiss="modal">Close</button>
                </div>
                                
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title col text-center" id="exampleModalLabel"><b>Pilih Layanan TI Yang Dibutuhkan</b></h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="overflow: hidden;">
                    <div class="col text-center">
                        <a href="{{route('front.zoom_request.create')}}" class="btn btn-info btn-lg"><span class="fas fa-video"></span> Layanan Permintaan Link ZOOM</a><br><br>
                        <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal"><span class="fas fa-calendar"></span> Kalender Kegiatan ZOOM</button><br><br>
                        <a href="{{route('front.helpdesk.create')}}" class="btn btn-success btn-lg"><span class="fas fa-tools"></span> Layanan Helpdesk IT</a><br><br>
                        <a href="/manualbook.pdf" class="btn btn-primary btn-lg"><span class="fas fa-book"></span> Download Manual Book Aplikasi</a><br><br>
                        <a href="/login" class="btn btn-success btn-lg"><span class="fas fa-key"></span> Login Admin</a>
                    </div>
                </div>
                                
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-header">
               <h3 style="text-align: center"><b>Kalender Jadwal Kegiatan ZOOM Meeting Inspektorat Jenderal</b></h3>
            </div>
            <div class="card-body">
                <a href="" class="btn btn-info float-end">Pilih Fitur Aplikasi</a><br>
                <table class="table-dark">
                    <tr>
                        @foreach ($zoomaccount as $item)
                            <td class="{{$item->username}}">{{$item->username}}</td>
                        @endforeach
                    </tr>
                </table>
                <br><div id='calendar'></div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('sbadmin/js/jquery/jquery.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{asset('sbadmin/js/scripts.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>


<script> 
    window.mobilecheck = function() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
      };
    $(document).ready(function() {
        $('#myModal').modal('show');
        var today = new Date().toISOString().slice(0,10);
            console.log(today);
            $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,basicWeek,basicDay'
                    },
                    handleWindowResize :true,
                    eventStartEditable :false,//fungsi untuk mindahkan event
                    defaultDate: today,
                    defaultView: window.mobilecheck() ? "basicWeek" : "month",
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    selectable: true,
                    multipleselect : false,
                    selectHelper: true,
                    aspectRatio:  3,
                    displayEventEnd: true,
                    select: function(start, end){
                      if(moment(start._d).add(1, 'days').format('YYYY-MM-DD')==moment(end._d).format('YYYY-MM-DD')){
                        //$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                        //$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                        //$('#ModalAdd').modal('show');
                       
                        
                      }
                    },
                    
                    //fungsi untuk menampilkan detail dari event yang di klik
                    eventRender: function(event, element) {
                        element.bind('dblclick', function() {
                            $('#ModalEdit #id').val(event.id);
                            $('#ModalEdit #title').val(event.name);
                            $('#ModalEdit #zoom_id').val(event.zoom_id); 
                            $('#ModalEdit #link').val(event.link);
                            $('#ModalEdit #password').val(event.password);
                            $('#ModalEdit #schedule').val(event.schedule);
                            $('#ModalEdit #color').val(event.color);
                            $('#something').click(function() { 
                                window.open(event.link,'_blank');
                            } );
                            //var deleteMsg = alert("Nama Kegiatan : "+ event.name +
                            //                    "\n ID : "+event.zoom_id+ 
                            //                    "\n Password : "+event.password+
                            //                    "\n Link : " +event.link);
                            $('#ModalEdit').modal('show');
                        
                        });
                    },
                   
                    eventDrop: function(event, delta, revertFunc) { // si changement de position
        
                        edit(event);
        
                    },
                    eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
        
                        edit(event);
        
                    },
                    //parsing data zoom dari database
                    events: [
                            @foreach($zoom as $item)
                                {
                                    id:     '{{$item->id}}',
                                    title:  '{{$item->request_by}}',
                                    name : '{{$item->name}}',
                                    link : '{{$item->link}}',
                                    zoom_id: '{{$item->zoom_id}}',
                                    password: '{{$item->password}}',
                                    start:  '{{$item->date.' '.\Carbon\Carbon::createFromFormat('H:i', $item->start)->format('H:i:s')}}',
                                    end:    '{{$item->date.' '.\Carbon\Carbon::createFromFormat('H:i', $item->end)->format('H:i:s')}}',
                                    color:  '{{$item->colors->color}}',
                                    schedule : '{{$item->date.' '.$item->start.'-'.$item->end}}' 
                                },
                            @endforeach                      
                    ]
                });
    });
</script>

</body>
</html>