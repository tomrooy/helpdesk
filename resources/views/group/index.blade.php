@extends('layouts.sbadmin.main')
@section('title','Golongan Dashboard')
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data User</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Golongan</a></li>
                <li class="breadcrumb-item active" aria-current="page">Index</li>
            </ol>
        </nav>
    </div>
    <div class="card shadow mb-12">
        <div class="card-header py-3 bg-primary">
            <h6 class="m-0 font-weight-bold text-light">Master Group</h6>
           
        </div>      
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            <button data-toggle="modal" data-target="#addGroup" class="btn btn-info"><span class="fa fa-plus"></span> Tambah Data</button>
            <br><br>
            <div class="table-responsive">
                <table id="group-table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th width="150" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        @include('group.modal')
    </div>
@endsection
@section('js')
@include('layouts.sbadmin.partial.indexjs')
@include('group.javascript')
@endsection