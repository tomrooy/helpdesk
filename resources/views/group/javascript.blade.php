<script>  
    $(document).ready(function() {
        //menampilkan tabel severside
        var dataTable = $('#group-table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            pageLength: 5,
            // scrollX: true,
            "order": [[ 0, "desc" ]],
            ajax: '{{ route('group.data') }}',
            columns: [
                {data: 'DT_RowIndex', name : 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });

        function checkId()
        {
            var id  = document.getElementById('group_id').value
            if(id == null)
            {
                $('#group_id').addClass('is-invalid');
                console.log('test')
            }
        }
        $('#group_id').blur(function(){
            var nik = $('#group_id').val();
            if(nik == '')
            {   
                $('#group_id').addClass('is-invalid');
                $( '#id-error' ).html('data wajib di isi').addClass('alert-danger');
            }else
            {
                $('#group_id').removeClass('is-invalid');
                $( '#id-error' ).html('').removeClass('alert-danger');
            }
           });

        //fungsi simpan data
        $('#formGroup').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            //var registerForm = $("#formGroup");
            //var formData = registerForm.serialize();
            $( '#id-error' ).html( "" );
            $( '#name-error' ).html( "" );
            $( '#description-error' ).html( "" );
            //$( '#image-error' ).html( "" );

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url:"{{ route('group.store') }}",
                data:formData,
                contentType: false,
                processData: false,
                success:function(data) {
                    if(data.errors) {
                        if(data.errors.group_id){
                            $( '#id-error' ).html( data.errors.group_id[0] ).addClass('alert-danger');
                            $('#group_id').addClass('is-invalid');
                        }
                        if(data.errors.name){
                            $( '#name-error' ).html( data.errors.name[0] );
                        }
                        if(data.errors.description){
                            $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                            
                        }
                        //if(data.errors.image_file){
                        //    $( '#image-error' ).html( data.errors.image_file[0] );
                        //    $('#image_id').addClass('is-invalid');
                        //}
                        
                    }else
                    {
                        swal({
                            title: "Success!",
                            text: "Berhasil Ditambah",
                            type: "success",
                            timer: 2000
                         });
                        //$('#image_id').removeClass('is-invalid');
                        $('#group_id').removeClass('is-invalid');
                        $('#addGroup').modal('hide');
                        document.getElementById("formGroup").reset();
                        $('#group-table').DataTable().ajax.reload();
                        
                        
                        
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });

        //fungsi close modal
        $('.modelClose').on('click', function(){
            $('#EditGroupData').hide();
        });

        //fungsi nampilkan data di modal
        var id;
        $('body').on('click', '#getEditGroupData', function(e) {
            // e.preventDefault();
            $('.alert-danger').html('');
            $('.alert-danger').hide();
            id = $(this).data('id');
            $.ajax({
                url: 'golongan/'+id+'/edit',
                method: 'GET',
                success: function(data) {
                    //console.log(data.result.name);
                    $('#namaJabatan').html(data.result.name);
                    document.getElementById('idgroup').value    =   data.result.id;
                    document.getElementById('formEditNama').value = data.result.name;
                    document.getElementById('formEditGroupId').value = data.result.group_id;
                    document.getElementById('formEditDescription').value = data.result.description;
                    $('#EditGroupData').show();
                }
            });
        });

        $('#editFormGroup11').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $( '#id-error' ).html( "" );
            $( '#name-error' ).html( "" );
            $( '#description-error' ).html( "" );
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{ route('group.update') }}',
                data:formData,
                contentType: false,
                processData: false,
                success:function(data) {
                    if(data.errors) {
                        if(data.errors.group_id){
                            $( '#id-error' ).html( data.errors.group_id[0] ).addClass('alert-danger');
                            $('#group_id').addClass('is-invalid');
                        }
                        if(data.errors.name){
                            $( '#name-error' ).html( data.errors.name[0] );
                        }
                        if(data.errors.description){
                            $( '#description-error' ).html( data.errors.description[0] ).addClass('alert-danger');
                            
                        }
                    }else
                    {
                        if(data.status == true)
                        {
                            swal({
                                title: "Success!",
                                text: "Berhasil Diupdate",
                                type: "success",
                                timer: 2000
                            });
                        }else
                        {
                            swal({
                                title: "Success!",
                                text: data.message,
                                type: "error",
                            });
                        }
                    
                        $('#group_id').removeClass('is-invalid');
                        $('#EditGroupData').modal('hide');
                        document.getElementById("formGroup").reset();
                        $('#group-table').DataTable().ajax.reload();
                    }
                }
             
            });
        });

        //fungsi hapus data
        $(document).on('click', '#deleteGroup', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: "Hapus?",
                text: "Apakah Yakin Menghapus data ini!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak!",
                reverseButtons: false
            }).then(function (e) {
                if (e.value === true) {                    
                $.ajax({
                    type: 'POST',
                    url: 'golongan/'+id+'/delete',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        },
                    dataType: 'JSON',
                    success: function (data) {
                        if(data.status == true)
                        {
                            swal("Done!", data.message, "success");
                            $('#group-table').DataTable().ajax.reload();
                        }else
                        {
                            swal("Error!", data.message, "error");
                        }
                        
                    },
                    error : function(xhr, status, errorThrown)
                    {
                         //Here the status code can be retrieved like;
                        xhr.status;
                
                        //The message added to Response object in Controller can be retrieved as following.
                        xhr.responseText;
                    }
                });
                }else
                {
                    swal("Pemberitahuan!","Sukses dibatalkan", "success");
                }
            },function (dismiss) {
                return false;
            })

        });
        

    });  
</script>