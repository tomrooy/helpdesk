<div class="modal fade" id="addGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Jabatan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <form method="POST" id="formGroup" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="id">ID</label>
                        <input autocomplete="off" type="text"  class="form-control" id="group_id" name="group_id" placeholder="Silahkan Isi Id">
                        <strong id="id-error"></strong>
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input autocomplete="off" type="text" class="form-control" name="name" placeholder="Silahkan Isi Nama">
                        <strong id="name-error"></strong>
                    </div>
                    <!-- 
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input autocomplete="off" type="file" class="form-control" name="image_file" id="image_id">
                        <strong id="image-error"></strong>
                    </div> -->
                    <div class="form-group">
                        <label>Description</label>
                            <textarea autocomplete="off" type="text" class="form-control {{$errors->first('description') ? "is-invalid" : ""}} " name="description" placeholder="Silahkan Isi deskripsi singkat">{{old('description')}}</textarea>
                            <strong id="description-error"></strong>       
                    </div>
                    
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success"><span class="fa fa-save"></span> Simpan</button>
                <button type="reset" class="btn btn-warning"><span class="fa fa-recycle"></span> Reset</button>
            </div>
                </form>
        </div>
    </div>
</div>

<div class="modal" id="EditGroupData">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit Jabatan <span id="namaJabatan"></span></h4>
                <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div id="EditArticleModalBody">
                        <form method="POST" id="EditFormGroup" enctype="multipart/form-data">
                            @csrf
                        <div class="form-group">
                            <input type="hidden" name="id" id="idgroup">
                            <label for="id">ID</label>
                            <input autocomplete="off" type="text" class="form-control" id="formEditGroupId" name="group_id" placeholder="Silahkan Isi Id">
                            <strong id="id-error"></strong>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input autocomplete="off" id="formEditNama" type="text" class="form-control" name="name" placeholder="Silahkan Isi Nama">
                            <strong id="name-error"></strong>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                                <textarea autocomplete="off" type="text" class="form-control {{$errors->first('description') ? "is-invalid" : ""}} " name="description" placeholder="Silanhkan isi Deskripsi singkat" id="formEditDescription"></textarea>
                                <strong id="description-error"></strong>       
                        </div>
                    </div>
            </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" >Update</button>
                    <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>