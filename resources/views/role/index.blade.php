@extends('layouts.nice.main')
@section('title','Role Index')
@push('nav-user') show @endpush 
@section('content')
<div class="wrapper">
    <div class="card">
        <div class="card-header">
            <h2><strong>Data Role Permission</strong></h2>
        </div>    
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            @if(session('status'))
                <div class="alert alert-success btn-sm alert-small" >
                    {{session('status')}}
                </div> 
            @endif
                    <div class="row">
                        <div class="col-md-4">
                            <form role="form" action="{{ route('role.store') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="name">Role</label>
                                <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" id="name" required>
                                <div class="invalid-feedback">
                                    {{$errors->first('name')}}
                                </div>
                            </div>
                                <button class="btn btn-primary">Simpan</button>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Role</td>
                                        <td>Guard</td>
                                        <td>Created At</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                      $no = 1;  
                                    @endphp
                                    @foreach ($role as $item)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->guard_name }}</td>
                                            <td>{{ $item->created_at->diffForHumans() }}</td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="$(this).find('form').submit()" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete
                                                <form method="POST" action={{route('role.destroy',$item->id) }}  onsubmit="return confirm('Delete this category temporarily?')">
                                                @csrf
                                                @method('DELETE')
                                                </form>
                                                 </a>
                                            </td>
                                    @endforeach
                                </tbody>
                            </table>
                                
                        </div>
                    </div>
        </div>
    </div>
</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
@endsection
