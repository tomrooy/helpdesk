@extends('layouts.nice.main')
@section('title','Role Index')
@push('nav-user') show @endpush 
@section('content')
<div class="wrapper">
    <div class="card">
        <div class="card-header">
            <h2><strong>Data Role Permission</strong></h2>
        </div>    
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <br>
            @if(session('status'))
                <div class="alert alert-success btn-sm alert-small" >
                    {{session('status')}}
                </div> 
            @endif
            <div class="row">
                <div class="col-md-4">
                    <form action="{{ route('role.add_permission') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" required>
                            <p class="text-danger">{{ $errors->first('name') }}</p>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-sm">
                                Add New
                            </button>
                        </div>
                    </form>
                    <br>
                    <table class="table table-stripped">
                        <thead>                              
                            <th>Nama Permission</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($permissions_list as $item)
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td><a href="javascript:void(0)" onclick="$(this).find('form').submit()" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> Delete
                                        <form method="POST" action={{route('role.setpermission.destroy',$item->id) }}  onsubmit="return confirm('Delete this permission permanently?')">
                                      @csrf
                                      @method('DELETE')
                                    </form>
                                </a></td>
                                </tr>
                            @endforeach
                        </tbody>
                        
                        
                    </table>
                </div>
                <div class="col-md-8">
                    <form action="{{ route('role.roles_permission') }}" method="GET">
                        <div class="form-group">
                            <label for="">Roles</label>
                            <div class="input-group">
                                <select name="role" class="form-control">
                                    @foreach ($roles as $value)
                                        <option value="{{ $value }}" {{ request()->get('role') == $value ? 'selected':'' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-danger">Check!</button>
                                </span>
                            </div>
                        </div>
                    </form>
                    {{-- jika $permission tidak bernilai kosong --}}
                    @if (!empty($permissions))  
                    <form action="{{ route('role.setRolePermission', request()->get('role')) }}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1" data-toggle="tab">Permissions</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            @php $no = 1; @endphp
                                            @foreach ($permissions as $key => $row)
                                                <input type="checkbox" 
                                                    name="permission[]" 
                                                    class="minimal-red" 
                                                    value="{{ $row }}"
                                                    {{--  CHECK, JIKA PERMISSION TERSEBUT SUDAH DI SET, MAKA CHECKED --}}
                                                    {{ in_array($row, $hasPermission) ? 'checked':'' }}
                                                    > {{ $row }} <br>
                                                @if ($no++%4 == 0)
                                                <br>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="pull-right">
                                <button class="btn btn-primary btn-sm">
                                    <i class="fa fa-send"></i> Set Permission
                                </button>
                            </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
@endsection
