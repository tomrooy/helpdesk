@extends('layouts.sbadmin.main')
@section('title','Dashboard')
@section('css')
    <link href="{{asset('sbadmin/calendar/fullcalendar.css')}}" rel='stylesheet' />
    <style>
        .tommy{
            background-color: #106976;
            color: #ffffff;
            padding: 5px;
        }
        .rifaldy{
            background-color: #2d07d8;
            color: #ffffff;
            padding: 5px;
        }
        .ari{
            background-color: #ff9100;
            color: #ffffff;
            padding: 5px;
        }
        .utama{
            background-color: #bd1f88;
            color: #ffffff;
            padding: 5px;
        }
    </style>
@endsection
@push('main-menu')Dashboard @endpush
@push('url-main-menu') # @endpush
@push('sub-menu')Index @endpush    
@section('content')
<div class="wrapper">
    @role('Super_Admin')
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{ __('Grafik Pengerjaan Helpdesk') }}</div>
    
                    <div class="card-body">
                        {!! $chart->container() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{ __('Grafik Jenis Permintaan Masuk Helpdesk') }}</div>
    
                    <div class="card-body">
                       <div id="noname">

                       </div>
                    </div>
                </div>
            </div>
        </div>
    @endrole    
    @role('User')
    <div class="card">
        <div class="card-header">
            <div style="text-align: center"><h2><strong >Kalender Kegiatan ZOOM Inspektorat Jenderal</strong></h2></div>
        </div>    
        <div class="card-body">
            <div>
                <div id='date-part' class="m-0 font-weight-bold text-primary"></div>
                <div id='time-part' class="m-0 font-weight-bold text-primary"></div>
            </div>
            <table>
                <tr>
                    <td class="utama">Warna Akun Utama</td>
                    <td class="tommy">Warna Akun Tommy</td>
                    <td class="ari">Warna Akun Ari</td>
                    <td class="rifaldy">Warna Akun Rifaldy</td>
                </tr>
            </table><br>
            <div id='calendar'></div>
              
        </div>
    </div>
    @endrole
</div>

@endsection
@section('js')
@role('Super_Admin')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
{!! $chart->script() !!}
@endrole
@role('User')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{asset('sbadmin/calendar/fullcalendar.min.js')}}"></script>
<script>
    $(document).ready(function() {        
        var today = new Date().toISOString().slice(0,10);
            console.log(today);
            $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,basicWeek,basicDay'
                    },
                    handleWindowResize :true,
                    eventStartEditable :false,//fungsi untuk mindahkan event
                    defaultDate: today,
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    selectable: true,
                    multipleselect : false,
                    selectHelper: true,
                    aspectRatio:  3,
                    displayEventEnd: true,
                    select: function(start, end){
                      if(moment(start._d).add(1, 'days').format('YYYY-MM-DD')==moment(end._d).format('YYYY-MM-DD')){
                        //$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                        //$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                        //$('#ModalAdd').modal('show');
                       
                        
                      }
                    },
                    
                    //fungsi untuk menampilkan detail dari event yang di klik
                    eventRender: function(event, element) {
                        element.bind('dblclick', function() {
                            $('#ModalEdit #id').val(event.id);
                            $('#ModalEdit #title').val(event.title);
                            $('#ModalEdit #color').val(event.color);
                            var deleteMsg = alert("Nama Kegiatan : "+ event.name +
                                                "\n ID : "+event.zoom_id+ 
                                                "\n Password : "+event.password+
                                                "\n Link : " +event.link);
                        });
                    },
                   
                    eventDrop: function(event, delta, revertFunc) { // si changement de position
        
                        edit(event);
        
                    },
                    eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
        
                        edit(event);
        
                    },
                    //parsing data zoom dari database
                    events: [
                            @foreach($zoom as $item)
                                {
                                    id:     '{{$item->id}}',
                                    title:  '{{$item->request_by}}',
                                    name : '{{$item->name}}',
                                    link : '{{$item->link}}',
                                    zoom_id: '{{$item->zoom_id}}',
                                    password: '{{$item->password}}',
                                    start:  '{{$item->date.' '.\Carbon\Carbon::createFromFormat('H:i', $item->start)->format('H:i:s')}}',
                                    end:    '{{$item->date.' '.\Carbon\Carbon::createFromFormat('H:i', $item->end)->format('H:i:s')}}',
                                    color:  '{{$item->color}}', 
                                },
                            @endforeach                      
                    ]
                });
                
     
                
                
            
    });
</script> 
@endrole
<script>
    
</script>
@endsection
