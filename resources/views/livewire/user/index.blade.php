<div>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{session('message')}}
        </div>
    @endif
    @include('livewire.user.edit')
    @include('livewire.user.create')

    <br><br>
    <div class="row mb-4">
        <div class="col-sm-1 form-inline float-start">
            Per Page: &nbsp;
            <select wire:model="perPage" class="form-control">
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>25</option>
            </select>
        </div>

        <div class="col-sm-6 float-end">
            Percarian: &nbsp;
            <input wire:model.debounce.300ms="search" class="form-control" type="text" placeholder="Search Data...">
        </div>

    </div>
    <div class="table-responsive">
        <table class="table table-responsive-xl table-striped">
            <thead>
                <th>NO</th>
                <th>NIP</th>
                <th wire:click="sorting('name')" style="cursor : pointer">Nama Lengkap @include('layouts.sbadmin.partial.sort_icon',['field'=>'name'])</th>
                <th wire:click="sorting('email')" style="cursor : pointer">Email @include('layouts.sbadmin.partial.sort_icon',['field'=>'email'])</th>
                <th wire:click="sorting('location')" style="cursor : pointer">Bagian @include('layouts.sbadmin.partial.sort_icon',['field'=>'location'])</th> 
                <th>Role</th>
                <th>Photo</th>
                <th>Action</th>
            </thead>
            <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach ($user as $item)
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$item->nip}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->userLocations->name}}</td>
                    <td>@foreach ($item->getRoleNames() as $items)
                            {{$items}}
                        @endforeach
                    </td>
                    <td>@if($item->photo != null)
                        <img src="{{asset('storage/uploads/users/'. $item->photo)}}" width="130px" height="200px">
                        @else
                        <img src={{asset('default.png')}} width="80px" height="90px">
                        @endif
                    </td>
                    <td>
                        <button class="btn btn-info" wire:click="edit('{{$item->id}}')" data-bs-toggle="modal" data-bs-target="#updateModal" ><span class="fa fa-edit"></span> Edit Data</button>
                        <button class="btn btn-danger" wire:click="destroy('{{$item->id}}')"><span class="fa fa-trash"></span> Hapus </button>
                    </td>
                </tr>
                @php
                    $no++;
                @endphp
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="float-left">
        Showing {{$user->firstItem()}} to {{$user->lastItem()}} out of {{$user->total()}} items
    </div>
    <div class="float-right">
        {{$user->links()}}
    </div>
</div>