<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createModal">
	Create User
</button>
<a href="{{route('user.import')}}" class="btn btn-info float-right"><span class="fas fa-file-excel"></span> Import Excel</a>

<!-- Modal create -->
<div wire:ignore.self class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </button>
            </div>
           <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>NIP</label>
                        <input type="text" class="form-control"  placeholder="Enter Nip" wire:model="nip">
                        @error('nip') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label >Name</label>
                        <input type="text" class="form-control"  placeholder="Enter Name" wire:model="name">
                        @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label>Bagian</label>
                        <select  class="form-control" wire:model="location">
                            <option value="">PILIH Bagian/Wilayah</option>
                            @foreach($userlocation as $item)
                                <option value="{{$item->location_id}}" @if(old('location') == '{{$item->location_id}}') {{'selected'}} @endif>{{$item->name}}</option>
                            @endforeach    
                        </select> 
                        @error('location') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">Email address</label>
                        <input type="email" class="form-control" id="exampleFormControlInput2" wire:model="email" placeholder="Enter Email">
                        @error('email') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select  class="form-control" wire:model="role">
                            <option value="">Pilih Role</option>
                            @foreach($roledata as $item)
                                <option value="{{$item->name}}" >{{$item->name}}</option>
                            @endforeach    
                        </select> 
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput3">foto Profil</label>
                        <input type="file" class="form-control" id="exampleFormControlInput3" wire:model="photo_create" placeholder="upload Foto">
                        <span class="alet alert-danger">*file max 300 Kb dan jenis File harus .jpg atau .jpeg</span>
                        @error('photo') <span class="text-danger">{{ $message }}</span>@enderror
                        @if ($photo_create)
                            Photo Preview:
                            <img src="{{ $photo_create->temporaryUrl() }}" width="300px" height="400px">
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" wire:model="password" placeholder="enter password" class="form-control">
                        @error('password')<span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" wire:model="c_password" placeholder="enter password" class="form-control">
                        @error('c_password')<span class="text-danger"><b>{{$message}}</b></span> @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary close-btn" data-bs-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">Save changes</button>
            </div>
        </div>
    </div>
</div>