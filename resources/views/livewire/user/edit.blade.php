
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="hidden" wire:model="userId">
                        <label >Name</label>
                        <input type="text" class="form-control" wire:model="name"  placeholder="Enter Name">
                        @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label >NIP</label>
                        <input type="nip" class="form-control" wire:model="nip"  placeholder="Enter nip">
                        @error('nip') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label >Email address</label>
                        <input type="email" class="form-control" wire:model="email"  placeholder="Enter Email">
                        @error('email') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label>Bagian</label>
                        <select  class="form-control" wire:model="location">
                            <option value="">PILIH Bagian/Wilayah</option>
                            @foreach($userlocation as $item)
                                <option value="{{$item->location_id}}" @if(old('location') == '{{$item->location_id}}') {{'selected'}} @endif>{{$item->name}}</option>
                            @endforeach    
                        </select> 
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select  class="form-control" wire:model="role">
                            <option value="">Pilih Role</option>
                            @foreach($roledata as $item)
                                <option value="{{$item->name}}" >{{$item->name}}</option>
                            @endforeach    
                        </select> 
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" wire:model="password" placeholder="enter password" class="form-control">
                        @error('password')<span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" wire:model="c_password" placeholder="enter password" class="form-control">
                        @error('c_password')<span class="text-danger"><b>{{$message}}</b></span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormContronput3">foto Profil</label>
                        <input type="file" class="form-control" id="exampleFormrolInput3" wire:model="photo_update" placeholder="upload Foto">
                        <span class="alet alert-danger">*file max 300 Kb dan jenis File harus .jpg atau .jpeg</span>
                        @error('photo') <span class="text-danger">{{ $message }}</span>@enderror
                        @if ($photo_update)
                            Photo Preview:
                            <img src="{{ $photo_update->temporaryUrl() }}" width="300px" height="400px">
                        @endif
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
            </div>
       </div>
    </div>
</div>