@extends('layouts.nice.main')
@section('title','Ganti Password')
@push('main-menu')User @endpush
@push('url-main-menu') {{route('user.index')}} @endpush
@push('sub-menu')Ganti Password @endpush
@section('content')
    <div class="card bg-content-image ">
        <div class="card-header text-center">
            Ganti Password
        </div>
        <div class="card-body">
            @if(session('status'))
                <div class="alert alert-success btn-sm alert-small" >
                    {{session('status')}}
                </div>
            @endif
            <form action={{ route('user.change.password.process') }} enctype="multipart/form-data" method="POST" onsubmit="return (data berhasil ditambahkan")>
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Password Lama</label>
                        <input autocomplete="off" id="current_password" value="{{old('current_password')}}" type="password" class="form-control {{$errors->first('current_password') ? "is-invalid" : ""}} " name="current_password" placeholder="Password Lama">
                    <div class="invalid-feedback">
                        {{$errors->first('current_password')}}
                    </div>
                </div>
                <div class="form-group">
                    <label>Password Baru</label>
                        <input autocomplete="off" value="{{old('password')}}" type="password" class="form-control {{$errors->first('password') ? "is-invalid" : ""}} " name="password" placeholder="password">
                    <div class="invalid-feedback">
                        {{$errors->first('password')}}
                    </div>
                </div>
                <div class="form-group">
                    <label>Confirmasi Password Baru</label>
                        <input id="password_confirmation" autocomplete="off" value="{{old('password_confirmation')}}" type="password" class="form-control {{$errors->first('password_confirmation') ? "is-invalid" : ""}} " name="password_confirmation" placeholder="Konfirmasi Password">
                    <div class="invalid-feedback">
                        {{$errors->first('password_confirmation')}}
                    </div>
                </div><br>
                <div class="col text-center">
                    <button type="submit" class="btn btn-primary">Simpan</button> 
                    <button type="reset" class="btn btn-warning">Reset</button> 
                </div>    
                   
            </form>
        </div>
        
    </div>
@endsection