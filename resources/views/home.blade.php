@extends('layouts.nice.main')
@section('title','Dashboard')
@section('css')
    <link href="{{asset('nice/assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">
@endsection
@push('main-menu')Dashboard @endpush
@push('url-main-menu') # @endpush
@push('sub-menu') @endpush  
@section('content')
<div class="row">
    @role('Super_Admin')
    <div class="col-md-12">
        <div class="row">
            <!-- Helpdesk Card -->
            <div class="col-xxl-3 col-md-6">
                <div class="card info-card sales-card">
                    <div class="filter">
                        <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                        <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                        <li class="dropdown-header text-start">
                            <h6>Filter</h6>
                        </li>
                        <li><a class="dropdown-item" href="#">Today</a></li>
                        <li><a class="dropdown-item" href="#">This Month</a></li>
                        <li><a class="dropdown-item" href="#">This Year</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Helpdesk <span>| 2023</span></h5>
            
                        <div class="d-flex align-items-center">
                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                            <i class="bi bi-tools"></i>
                        </div>
                        <div class="ps-3">
                            <h6>{{$helpdesk_count}}</h6>
                            <!--<span class="text-success small pt-1 fw-bold">12%</span> <span class="text-muted small pt-2 ps-1">increase</span> -->
            
                        </div>
                        </div>
                    </div>
                </div>
            </div><!-- End Helpdesk Card -->
            
            <!-- ZOOM Request Card -->
            <div class="col-xxl-3 col-md-6">
                <div class="card info-card sales-card">
        
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                    <li class="dropdown-header text-start">
                        <h6>Filter</h6>
                    </li>
                    <li><a class="dropdown-item" href="#">Today</a></li>
                    <li><a class="dropdown-item" href="#">This Month</a></li>
                    <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
        
                <div class="card-body">
                    <h5 class="card-title">Apel Present <span>| 2023</span></h5>
        
                    <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-card-list"></i>
                    </div>
                    <div class="ps-3">
                        <h6>{{$apel_count}}</h6>
                        <!--<span class="text-success small pt-1 fw-bold">12%</span> <span class="text-muted small pt-2 ps-1">increase</span> -->
        
                    </div>
                    </div>
                </div>
        
                </div>
            </div><!-- ZOOM Request Card -->
    
            <!-- Zoom Link Card -->
            <div class="col-xxl-3 col-md-6">
                <div class="card info-card revenue-card">
        
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                    <li class="dropdown-header text-start">
                        <h6>Filter</h6>
                    </li>
        
                    <li><a class="dropdown-item" href="#">Today</a></li>
                    <li><a class="dropdown-item" href="#">This Month</a></li>
                    <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
        
                <div class="card-body">
                    <h5 class="card-title">Zoom Link <span>| 2023</span></h5>
        
                    <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-camera-reels-fill"></i>
                    </div>
                    <div class="ps-3">
                        <h6>{{$zoom_count}}</h6>
                        <span class="text-success small pt-1 fw-bold">8%</span> <span class="text-muted small pt-2 ps-1">increase</span>
        
                    </div>
                    </div>
                </div>
        
                </div>
            </div><!-- End Zoom Link Card -->
        
            <!-- Activity Card -->
            <div class="col-xxl-3 col-md-6">
        
                <div class="card info-card customers-card">
        
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                    <li class="dropdown-header text-start">
                        <h6>Filter</h6>
                    </li>
        
                    <li><a class="dropdown-item" href="#">Today</a></li>
                    <li><a class="dropdown-item" href="#">This Month</a></li>
                    <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
        
                <div class="card-body">
                    <h5 class="card-title">Link Absensi <span>| 2023</span></h5>
        
                    <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-people-fill"></i>
                    </div>
                    <div class="ps-3">
                        <h6>{{$activity_count}}</h6>
                        <!-- <span class="text-danger small pt-1 fw-bold">12%</span> <span class="text-muted small pt-2 ps-1">decrease</span> -->
        
                    </div>
                    </div>
        
                </div>
                </div>
        
            </div><!-- End Activity Card -->
            
          <!-- Recent Sales -->
          <div class="col-12">
            <div class="card recent-sales">
    
              <div class="filter">
                <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                  <li class="dropdown-header text-start">
                    <h6>Filter</h6>
                  </li>
    
                  <li><a class="dropdown-item" href="#">Today</a></li>
                  <li><a class="dropdown-item" href="#">This Month</a></li>
                  <li><a class="dropdown-item" href="#">This Year</a></li>
                </ul>
              </div>
    
              <div class="card-body">
                <h5 class="card-title">Helpdesk <span>| Hari ini</span></h5>
    
                <table class="table table-borderless datatable">
                    <thead>
                        <tr>
                            <th width="30"> 
                               #
                            </th>
                            <th>Ticket Number</th>
                            <th>Permasalahan</th>
                            <th>Direquest Oleh</th>
                            <th>Dikerjakan Oleh</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;    
                        @endphp
                       
                        @foreach ($helpdesk_today as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->ticket_number}}</td>
                                <td>{{$item->title}}</td>
                                <td>{{$item->request_by}}</td>
                                <td>{{!empty($item->handle_by) ? $item->handle_by : 'Belum Dikerjakan' }}</td>
                                <td> <span class="{{$item->statuses->badge}}" > {{$item->statuses->name}}</span></td>
                                <td><a href="{{route('module.helpdesk.detail', $item->id)}}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i> Detail</a></td>
                                @php
                                    $no++;    
                                @endphp
                            </tr>
                        @endforeach
                    </tbody>
                </table>
    
              </div>
    
            </div>
          </div><!-- End Recent Sales -->
    
          <!-- Top Selling -->
          <div class="col-12">
            <div class="card top-selling">
    
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                    <li class="dropdown-header text-start">
                        <h6>Filter</h6>
                    </li>
        
                    <li><a class="dropdown-item" href="#">Today</a></li>
                    <li><a class="dropdown-item" href="#">This Month</a></li>
                    <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
                <div class="card-body pb-0">
                    <h5 class="card-title">ZOOM <span>| Hari ini</span></h5>
        
                    <table class="table table-borderless datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Agenda</th>
                                <th>Direquest Oleh</th>
                                <th>Tanggal</th>
                                <th>Link</th>
                                <th>ID</th>
                                <th>Password</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;    
                            @endphp
                            @foreach ($zoom_today as $item)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->request_by}}</td>
                                <td>{!! 'Tanggal '.\Carbon\Carbon::createFromFormat('Y-m-d',$item->date)->format('d M Y'). "<br>Jam ". $item->start.'-'.$item->end !!}</td>
                                <td><a href="{{$item->link}}" >{{$item->link}}</a></td>
                                <td>{{$item->zoom_id}}</td>
                                <td>{{$item->password}}</td>
                                <td>
                                    <a href="{{route('module.zoom.show',$item->id)}}" class="btn btn-info btn-sm"><span class="bx bx-detail"></span></a>
                                </td>
                            </tr>
                            @php
                                $no++;
                            @endphp    
                            @endforeach
                        </tbody>
                    </table>
        
                </div>
    
            </div>
          </div><!-- End Top Selling -->
    
        </div>
    </div>
    @endrole
    @role('User')
    <div class="col-md-12">
        <div class="row">
            <div class="card">
                <div class="card-header">
                    Anda Berhasil Login Ke Dashboard {{\Auth::User()->name}} !
                </div>
            </div>
        </div>
    </div>
    
    @endrole
</div>

@endsection
@section('js')
<script src="{{asset('nice/assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
@endsection