<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Zoom extends Model
{
    use HasFactory,UuidTrait,SoftDeletes,LogsActivity;

    protected $guarded = [];
    protected static $logUnguarded = true;
    protected static $logName = 'Log Tabel Zoom';
    protected static $logOnlyDirty = true;
    public function scopeYear($query,$year)
    {
        return $query->whereYear('date', '=', $year);
    }



    public function create_zoom()
    {
        return $this->belongsTo('App\Models\User','created_by','nip');
    }
    public function created_by_users()
    {
        return $this->belongsTo('App\Models\User','created_by','nip');
    }
    public function updated_by_users()
    {
        return $this->belongsTo('App\Models\User','updated_by','nip');
    }
    public function deleted_by_users()
    {
        return $this->belongsTo('App\Models\User','deleted_by','nip');
    }
    public function colors()
    {
        return $this->belongsTo('App\Models\ZoomAccount','created_by_account','username');
    }

}
