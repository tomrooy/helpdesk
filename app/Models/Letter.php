<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;
use Illuminate\Support\Str;


class Letter extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded  =   [];

    public function users()
    {
        return $this->belongsTo('App\Models\User','created_by','nip');
    }
    public function messages()
    {
        $messages = [
            'nomor_agenda.required'        => 'Agenda Wajib Di isi',
            'asal_surat.required'        => 'Asal Surat Wajib di isi',
            'nomor_surat.required'         => 'No surat Wajib di isi',
            'kode_klasifikasi.required' => 'kode klasifikasi wajib di isi',
            'isi_ringkas.required'      => 'Isi ringkas wajib di isi',
            'index_berkas.required'     =>  'index berkas wajib di isi',
            'tanggal_surat.required'    =>  'tanggal surat wajib di isi',
            'file_surat.mimes'          =>  'file harus berformat pdf',
        ];
        return $messages;
    }

    
    public function letter_types()
    {
        return $this->belongsTo('App\Models\LetterType', 'letter_type', 'letter_id');
    }
    
}
