<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class HelpdeskStatus extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded = [];
    
    public function helpdesks()
    {
        return $this->belongsTo('App\Models\Helpdesk', 'status_id', 'status_id');
    }
    
}
