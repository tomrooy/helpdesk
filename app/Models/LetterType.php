<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;
use Illuminate\Support\Str;


class LetterType extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded  =   [];


    
    public function letters()
    {
        return $this->belongsTo('App\Models\Letter', 'letter_id', 'letter_type');
    }
    
}
