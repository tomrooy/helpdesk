<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;
use Carbon\Carbon;
use Spatie\Activitylog\Traits\LogsActivity;

class Helpdesk extends Model
{
    use HasFactory,UuidTrait,SoftDeletes,LogsActivity;

    protected $guarded = [];
    protected static $logUnguarded = true;
    protected static $logName = 'Log Tabel Helpdesk';
    protected static $logOnlyDirty = true;

    public function scopeYear($query,$year)
    {
        return $query->whereYear('date', '=', $year);
    }
    public function helpdesk_details()
    {
        return $this->hasMany('App\Models\HelpdeskDetail', 'ticket_number', 'ticket_number');
    }
    public function generateticket($category)
    {
        $id         =   'ITJ';
        $date       =   Carbon::today()->format('Ymd');
        $number     =   rand(1001,9999);
        $ticket_number  =   $id.'-'.$date.'-'.$category.'-'.$number;
        return $ticket_number;
    }
    public function messages()
    {
        $messages = [
            'name.required' => 'Nama Wajib Di isi',
            'img1.max'      => 'Foto pertama harus lebih kecil dari 400 KB',
            'img2.max'      => 'Foto kedua harus lebih kecil dari 400 KB',
            'captcha.required' => 'captcha harus di isi',
            'captcha.captcha' => 'captcha salah',
        ];
        return $messages;
    }
    public function users()
    {
        return $this->belongsTo('App\Models\User','request_by','nip');
    }
    public function helpdesk_categories()
    {
        return $this->belongsTo('App\Models\HelpdeskCategory','category_id','category_id')->withDefault();
    }public function helpdesk_subcategories()
    {
        return $this->belongsTo('App\Models\HelpdeskSubCategory','subcategory_id','subcategory_id')->withDefault();
    }
    public function statuses()
    {
        return $this->belongsTo('App\Models\HelpdeskStatus','status_id','status_id');
    }
    public function priorities()
    {
        return $this->belongsTo('App\Models\HelpdeskPriority','priority_id','priority_id')->withDefault();
    }

    public function created_by_users()
    {
        return $this->belongsTo('App\Models\User','created_by','nip');
    }
    public function updated_by_users()
    {
        return $this->belongsTo('App\Models\User','updated_by','nip');
    }
    public function deleted_by_users()
    {
        return $this->belongsTo('App\Models\User','deleted_by','nip');
    }
    
}
