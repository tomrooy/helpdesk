<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class BmnRequest extends Model
{
    use HasFactory,SoftDeletes,UuidTrait;

    protected $guarded  =   [];

    /**
     * Get the user that owns the BmnRequest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bmns()
    {
        return $this->belongsTo('App\Models\Bmn', 'bmn_id', 'bmn_id')->withDefault();
    }
    public function employees()
    {
        return $this->belongsTo('App\Models\User', 'request_by', 'nip')->withDefault();
    }
    public function bmn_request_details()
    {
        return $this->hasMany('App\Models\BmnRequestDetail', 'ticket_number', 'ticket_number');
    }
}
