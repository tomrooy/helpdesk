<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;


class NotPresent extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded  =   [];

    public function activities()
    {
        return $this->belongsTo(Activity::class, 'activity_id', 'activity_id');
    }

}
