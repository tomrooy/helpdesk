<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;
use GuzzleHttp\Client;

class ApelActivity extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    public function short_link($id)
    {
        try {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => 'https://api-ssl.bitly.com/',
            ]);
              
            $response = $client->request('POST', 'v4/bitlinks', [
                'json' => [
                    'long_url' => 'https://ithelpdesk.itjenkumham.id/presensi-apel/'.$id,
                ],
                'headers' => [
                    'Authorization' => 'Bearer 1f4b6bdfd7603c504f027515d54ec98d0e36206b'
                ],
                'verify' => false,
            ]);
         
            if(in_array($response->getStatusCode(), [200, 201])) {
                $body = $response->getBody();
                $arr_body = json_decode($body);
                return $arr_body->link;
            }
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }
    public function scopeYear($query,$year)
    {
        return $query->whereYear('date', '=', $year);
    }
    public function apel_presents()
    {
        return $this->belongsTo('App\Models\ApelPresent','activity_id','activity_id')->withDefault();
    }
    public function apel_presentss()
    {
        return $this->belongsToMany('App\Models\ApelPresent','activity_id','activity_id')->withDefault();
    }
}
