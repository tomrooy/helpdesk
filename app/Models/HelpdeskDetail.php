<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class HelpdeskDetail extends Model
{
    use HasFactory,UuidTrait,SoftDeletes,LogsActivity;
    protected $guarded = [];
    protected static $logUnguarded = true;
    protected static $logName = 'Log Tabel Heldesk Detail';
    protected static $logOnlyDirty = true;
  
    //public function helpdesk()
    //{
    //    return $this->belongsTo('App\Models\Helpdesk', 'ticket_number', 'ticket_number');
    //}
    public function users()
    {
        return $this->belongsTo('App\Models\User','created_by','nip');
    }
    public function created_by_users()
    {
        return $this->belongsTo('App\Models\User','created_by','nip');
    }
    public function updated_by_users()
    {
        return $this->belongsTo('App\Models\User','updated_by','nip');
    }
    public function deleted_by_users()
    {
        return $this->belongsTo('App\Models\User','deleted_by','nip');
    }
}
