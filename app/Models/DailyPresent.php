<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class DailyPresent extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded  =   [];

    public function daily_present_status()
    {
        return $this->belongsTo('App\Models\DailyPresentStatus','status_id','status_id')->withDefault('Data Kosong');
    }
    /**
     * The roles that belong to the DailyPresent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsTo('App\Models\User', 'nip', 'nip')->withDefault('Data Kosong');
    }
}
