<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Concerns\UuidTrait;

class WaLog extends Model
{
    use HasFactory,UuidTrait;
    protected $guarded = [];
}
