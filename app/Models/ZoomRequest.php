<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;
use Spatie\Activitylog\Traits\LogsActivity;


class ZoomRequest extends Model
{
    use HasFactory,UuidTrait,SoftDeletes,LogsActivity;

    protected $guarded = [];
    protected static $logUnguarded = true;
    protected static $logName = 'Log Tabel Zoom Request';
    protected static $logOnlyDirty = true;
    public function created_by_users()
    {
        return $this->belongsTo('App\Models\User','created_by','nip')->withDefault([
            'name' => 'nip kosong atau tidak terbaca',
        ]);
    }
    public function updated_by_users()
    {
        return $this->belongsTo('App\Models\User','updated_by','nip')->withDefault([
            'name' => 'nip kosong atau tidak terbaca',
        ]);
    }
    public function deleted_by_users()
    {
        return $this->belongsTo('App\Models\User','deleted_by','nip')->withDefault([
            'name' => 'nip kosong atau tidak terbaca',
        ]);
    }
    public function create_zoom()
    {
        return $this->belongsTo('App\Models\User','updated_by','nip')->withDefault([
            'name' => 'nip kosong atau tidak terbaca',
        ]);
    }
}
