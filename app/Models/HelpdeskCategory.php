<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;
use Illuminate\Support\Str;

class HelpdeskCategory extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded  =   [];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value,"-");
    }
    /**
     * Get all of the comments for the Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function helpdesk_subcategories()
    {
        return $this->hasMany('\App\Models\HelpdeskSubCategory', 'category_id', 'category_id')->withDefault('Data Kosong');
    }
}
