<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class ZoomAccount extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded = [];

    public function zooms()
    {
        return $this->belongsTo('App\Models\Zoom','username','created_by_account');
    }
}
