<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Concerns\UuidTrait;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use HasFactory, Notifiable, UuidTrait,SoftDeletes,HasRoles,LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nip',
        'name',
        'email',
        'password',
        'photo',
        'location',
        'handphone',
        'created_by',
    ];
    protected $keyType = 'string';
    public $incrementing = false; 

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function scopeSearch($query, $val)
    {
        return $query
        ->where('name','like','%'.$val.'%')
        ->Orwhere('nip','like','%'.$val.'%')
        ->Orwhere('location','like','%'.$val.'%')
        ->OrwhereHas("roles", function($q) use($val)
        {
             $q->where('name', 'like','%'.$val.'%'); 
        })
        ->OrwhereHas("userLocations", function($z) use($val)
        {
             $z->where('name', 'like','%'.$val.'%'); 
        })
        ;
    }
    public function groups()
    {
        return $this->belongsTo('App\Models\Group', 'group_id', 'group_id');
    }
    public function userLocations()
    {
        return $this->belongsTo('App\Models\UserLocation', 'location', 'location_id');
    }
    public function helpdesks()
    {
        return $this->hasMany('App\Models\Helpdesk','nip','created_by');
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
   
    public function apel_presents()
    {
        return $this->belongsTo('App\Models\ApelPresent', 'nip', 'nip');
    }
    
}
