<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class Group extends Model
{
    use HasFactory, UuidTrait,SoftDeletes;

    protected $guarded = [];

    
    public function users()
    {
        return $this->belongsTo('App\Model\User', 'group_id', 'group_id');
    }
    
}
