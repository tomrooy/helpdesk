<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class ApelPresent extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    
    public function scopeYear($query,$year)
    {
        return $query->whereYear('date', '=', $year);
    }
    public function users()
    {
        return $this->belongsTo('App\Models\User', 'nip', 'nip')->withDefault();
    }
    public function apel_activities()
    {
        return $this->belongsToMany('App\Models\ApelActivity', 'activity_id', 'activity_id');
    }
    public function getPeople($id)
    {
        return ApelPresent::where('activity_id', $id)->get();
    }
    
}
