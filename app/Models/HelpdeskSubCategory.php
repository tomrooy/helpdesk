<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class HelpdeskSubCategory extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded = [];

    public function helpdesk_categories()
    {
        return $this->belongsTo('App\Models\HelpdeskCategory', 'category_id', 'category_id')->withDefault('Data Kosong');
    }
    
}
