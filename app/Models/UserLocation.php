<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class UserLocation extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded = [];

    
    public function users()
    {
        return $this->hasMany('App\Models\User', 'location', 'location_id');
    }
}
