<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Concerns\UuidTrait;

class DailyPresentStatus extends Model
{
    use HasFactory,UuidTrait,SoftDeletes;
    protected $guarded  =   [];

    public function daily_presents()
    {
        return $this->hasMany('App\Models\DailyPresent', 'status_id', 'status_id');
    }
}
