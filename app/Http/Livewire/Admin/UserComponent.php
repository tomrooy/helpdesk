<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\UserLocation;
use DB,Storage;
use Carbon\Carbon;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class UserComponent extends Component
{
    use WithPagination,WithFileUploads,LivewireAlert;
    public $pagemode    = 'index',
    $sortby             =   'name',
    $typesort           =   'ASC',
    $search             =   '',
    $perPage            =   10,
    $userId,$name,$nip,$email,$role,$location,$password,$c_password,$photo_create,$photo_update,$modal_destroy;
    protected $paginationTheme = 'bootstrap';
    
    protected $messages    =   [
        'c_password.same'       => 'konfirmasi password harus sama dengan password',
        'email.required'        =>  'email wajib di isi',
        'nip.required'          =>  'nip wajib di isi',
        'location.required'     =>  'Bagian atau wilayah wajib di isi',
        'name.required'         =>  'nama wajib di isi',
        'c_password.required'   =>  'Konfirmasi Password Harus di isi',
        'password.required'     =>  'password harus di isi'
    ];
    protected $listeners = [
        'confirmed',
        'cancelled',
        
    ];

    public function resetInput()
    {
        $this->name             =   '';
        $this->email            =   '';
        $this->photo_create     =   '';
        $this->password         =   '';
        $this->c_password       =   '';
        $this->nip              =   '';
        $this->location         =   '';
    }
    public function formValidation()
    {
        
        return $this->validate([
            'email'     =>  'required',
            'nip'     =>  'required',
            'location'     =>  'required',
            'name'      =>  'required',
            'password'  =>  'required',
            'c_password'=>  'required|same:password',
            'photo_create'     =>  'nullable|mage|max:300|min:50|mimes:jpg,jpeg'
        ]);
    }
    public function savingSuccess()
    {
        $this->alert(
            'success',
            'Data Berhasil Disimpan, jika tidak berubah mohon di refresh pagenya'
        );
    }
    public function updatingSuccess()
    {
        $this->alert(
            'success',
            'Data Berhasil Diupdate'
        );
    }
    public function render()
    {
        $user   =   User::query()->search($this->search)
                                ->orderBy($this->sortby,$this->typesort)
                                ->paginate($this->perPage);
        $roledata   =   Role::all();
        $userlocation   =   UserLocation::all();
        return view('livewire.user.index',[
            'user'  =>  $user,
            'roledata'  =>  $roledata,
            'userlocation'  =>  $userlocation,
        ]);
    }
    public function sorting($field)
    {
        if($this->typesort == 'ASC')
        {
            $this->typesort = 'DESC';
        }else
        {
            $this->typesort = 'ASC';
        }
        return $this->sortby = $field;
    }
    public function store()
    {
        $this->formValidation();

        if ($this->photo_create) {
            $imagePath = $this->photo_create;
            $imageName = $this->nip.'.'.$this->photo_create->getClientOriginalExtension();
        
            $path = $this->photo_create->storeAs('uploads', $imageName, 'public');
        }else
        {
            $imageName = null;
        }

        try{
            DB::begintransaction();
                $user   =   new User;
                $user->nip      =   $this->nip;
                $user->location =   $this->location;
                $user->name     =   $this->name;
                $user->email    =   $this->email;
                $user->photo    =   $imageName;
                $user->password =   bcrypt($this->password);
                $user->save();
                $user->assignRole($this->role);
            DB::commit();
            //session()->flash('message','sukses ditambah');
            $this->resetInput();
            $this->emit('userStore');
            $this->savingSuccess();
            
        }catch (\Exception $e)
        {
            DB::rollback();
            session()->flash('message',$e->getMessage());
        }
    }
    public function edit($id)
    {
        $data               =   User::where('id',$id)->first();
        $this->userId       =   $data->id;
        $this->email        =   $data->email;
        $this->name         =   $data->name;
        $this->nip          =   $data->nip;
        $this->location     =   $data->location;
        
    }
    public function update()
    {
        
        $data           =   User::findOrfail($this->userId);
        //$this->formValidation();
        if ($this->photo_update) {
            $imagePath = $this->photo_update;
            $imageName = $data->nip.'.'.$this->photo_update->getClientOriginalExtension();

            $photo_exist  = Storage::Exists('public/storage/uploads/users/'.$data->photo);
            if($photo_exist)
            {
                //Storage::copy('public/candidate/'.$candidate->photo, 'public/employee/'.$name);
                Storage::delete('public/storage/uploads/users/'.$data->photo);
            }
            $path = $this->photo_update->storeAs('uploads/users/', $imageName, 'public');
            //$this->deletePreview($imageName);
        }else{
            $imageName = $data->photo;
        }
        if($this->password == $this->c_password)
        {
            if($this->password != "")
            {
                $data->password     =   $this->password;
            }   
            $data->name         =   $this->name;
            $data->nip          =   $this->nip;
            $data->location     =   $this->location;
            $data->email        =   $this->email;
            $data->photo        =   $imageName;
            $data->save();
        
            $data->assignRole($this->role);
            $data->syncRoles($this->role);
            //session()->flash('message','sukses diedit');
            $this->resetInput();
            $this->updatingSuccess();

        }else{
            $this->error_password();
        }
    }
    public function error_password()
    {
        $this->alert(
            'error',
            'Password dan konfirmasi password tidak sama'
        );
    }
    public function cancel()
    {
        $this->resetInput();

    }
    public function destroy($id)
    {
        $this->confirm('Apakah yakin menghapus data ini?', [
            'toast' => false,
            'position' => 'center',
            'showConfirmButton' => true,
            'cancelButtonText' => 'NO',
            'onConfirmed' => 'confirmed',
            'onCancelled' => 'cancelled'
        ]);
        $this->modal_destroy = User::findOrfail($id);
    }
    public function cancelled()
    {
        $this->alert(
            'success',
            'Data Berhasil DIBATALKAN'
        );
    }
    public function confirmed()
    {
        if($this->modal_destroy->id == \Auth::User()->id)
        {
            $this->alert(
                'error',
                'Akun yang sedang login tidak bisa dihapus'
            );
        }else{
            $this->modal_destroy->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
            $this->modal_destroy->delete();
        }
       
        
    }
    public function deletePreview($data)
    {
        unlink(storage_path('app/livewire-tmp/'.$data));
    }
}
