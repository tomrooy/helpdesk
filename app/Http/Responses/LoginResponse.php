<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{
    /**
     * @param  $request
     * @return mixed
     */
    public function toResponse($request)
    {
        if(auth()->user()->hasRole('Super_Admin'))
        {
            return redirect('/dashboard');
        } 

        return redirect()->intended('/dashboard');
    }
}