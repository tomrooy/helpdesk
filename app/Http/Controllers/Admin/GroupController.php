<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Group;

class GroupController extends Controller
{
    public function index()
    {
        return view('group.index');
    }
    public function getGroup()
    {
        
        $data = Group::latest()->get();
        return \DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('Actions', function($data) {
                return 
                '<button type="button" class="btn btn-success btn-sm" id="getEditGroupData" data-id="'.$data->id.'" ><i class="fa fa-edit"></i> Edit</button>        
                <button type="button" class="btn btn-danger btn-sm" id="deleteGroup" data-id="'.$data->id.'" ><i class="fa fa-trash"></i> Delete</button';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
    public function store(Request $request)
    {
        $message    =   [
            'group_id.required'     =>  'Data ID harus di isi',
        ];
        $validator  =   \Validator::make($request->all(),[
            'group_id'                      =>  'required',
            'name'                          =>  'required',
            'description'                   =>  'required',
        ],$message);

        if($validator->passes())
        {
            Group::create([
                'group_id'         => $request->group_id,
                'name'              => $request->name,
                'description'       => $request->description,
                'created_by'        => \Auth::User()->email,
            ]);
        }
        else
        {
            return response()->json(['errors' => $validator->errors()]);
        }
    }
    public function edit($id)
    {
        $data = Group::findOrFail($id);
        return response()->json([
            'result'  =>  $data
        ]);
    }
    public function update(Request $request)
    {
      
        $message    =   [
            'group_id.required'     =>  'Data ID harus di isi',
        ];
        $validator  =   \Validator::make($request->all(),[
            'group_id'                      =>  'required',
            'name'                          =>  'required',
            'description'                   =>  'required',
        ],$message);
        if($validator->passes())
        {
            try{
                $group      =   Group::findOrFail($request->id);
                $group->update([
                    'group_id'         => $request->group_id,
                    'name'              => $request->name,
                    'description'       => $request->description,
                    'updated_by'        => \Auth::User()->email,
                ]);
                return response()->json([
                    'status'   =>  true,
                    'message'  =>  $group->name.' delete'
                ]);

            } catch(\Exception $e)
            {
                return response()->json([
                    'status'   =>   false,
                    'message'  =>  $e->getMessage()
                ]);
            }
           
        }
        else
        {
            return response()->json(['errors' => $validator->errors()]);
        }

    }
    public function destroy($id)
    {
        try{
            $data   =   Group::findOrFail($id);
            $data->update([
                'deleted_by'    =>  \Auth::User()->nip,
            ]);
            $data->delete();
            return response()->json([
                'status'   =>  true,
                'message'  =>  $data->name.' delete'
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'   =>   false,
                'message'  =>  $e->getMessage()
            ]);
        }
        
    }
    

}
