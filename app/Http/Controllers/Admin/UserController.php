<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Imports\UserImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$data = User::whereHas("roles", function($q){ $q->where("name", "Super_Admin"); })->get();
        //return $data;
        return view('user.index');
    }
    public function import()
    {
        return view('user.import');
    }
    public function importProcess(Request $request)
    {
        $messages   =   [
            'file_excel.required'   =>  'tolong file di input'
        ];
        $validator =    \Validator::make($request->all(),[
            'file_excel'    =>  'required|max:2024|mimes:xlsx,xls'
        ], $messages);

        if($validator->passes())
        {
            $file = $request->file('file_excel');
            $proses_import = Excel::Import(new UserImport, $file);
            return response()->json(['success' => 'sukses']); 
        }else
        {
            return response()->json(['errors' => $validator->errors()]);
        }
        
    }
    public function importProcessAlpha(Request $request)
    {
        $messages   =   [
            'file_excel.required'   =>  'tolong file di input'
        ];
        \Validator::make($request->all(),[
            'file_excel'    =>  'required|max:2024|mimes:xlsx,xls'
        ], $messages)->validate();
        $file = $request->file('file_excel');
        $proses_import = Excel::Import(new UserImport, $file);
        return redirect()->back()->with([
            'status'  => 'success di import'
        ]);

    }
    public function profile($id)
    {
        $data   =   User::where('nip',$id)->first();
        if(!$data){
            return abort(404);
        }
        $data = $data->nip;
        if(Gate::allows('isUser',$data)) {
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );  
            $response = json_decode(file_get_contents('https://emawas.kemenkumham.go.id/penugasan/ajax/get_user_simpeg/'. $id, false, stream_context_create($arrContextOptions)), true);
            //return $response;
            return view('user.profile',[
                'data'  =>  $response
            ]);
         } else {
            return abort(403);
         } 
        
       
    }
    public function viewChangePassword()
    {
        return view('auth.change_password');
    }
    public function actionChangePassword(Request $request)
    {
        \Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, \Auth::user()->password);
        });
 
        
        $messages = [
            'password' => 'Invalid current password.',
        ];
 
        // validate form
        $validator = \Validator::make(request()->all(), [
            'current_password'      => 'required|password',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
 
        ], $messages);
 
        // if validation fails
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->errors());
        }
 
        
        $user = User::find(\Auth::id());
 
        $user->password = $request->password;
        $user->save();
        return redirect()->back()->with('status','password User berhasil diedit');
    }
    public function syncSimpeg()
    {
        $userall   =   User::where('nip','LIKE','%1995%')->get();
        foreach($userall as $value)
        {
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );  
            $response = json_decode(file_get_contents('https://emawas.kemenkumham.go.id/penugasan/ajax/get_user_simpeg/'. $value->nip, false, stream_context_create($arrContextOptions)), true);
            foreach($response as $item)
            {
                $telepon[] =  $item['NIP'];
                      
            }
            //$user = User::findOrFail($value->id);
            //$user->update([
            //    'handphone'   =>  NULL
            //]);
        }
        return $telepon;
    }
    public  function actionFaq()
    {
        return view('faq');
    }
  
}
