<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Helpdesk;
use App\Models\Status;
use App\Models\Zoom;
use App\Models\ApelActivity;
use App\Models\Activity;
use App\Models\ZoomAccount;
use App\Charts\HelpdeskChart;
use App\Exports\ReportExport;

Use Carbon\Carbon;

class HomeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('permission:dashboard', ['only' => ['index']]);
    }
    public function index()
    {
        $zoom_count             =   Zoom::year(2023)->count();
        $apel_count             =   ApelActivity::year(2023)->count();
        $activity_count         =   Activity::year(2023)->count();
        $helpdesk_count         =   Helpdesk::year(2023)->count();
        $zoom_today             =   Zoom::whereDate('date',Carbon::today())->get();
        $helpdesk_today         =   Helpdesk::whereDate('date',Carbon::today())->get();
        
        return view('home', [
            'activity_count'        =>  $activity_count,
            'apel_count'            =>  $apel_count,
            'zoom_count'            =>  $zoom_count,
            'helpdesk_count'        =>  $helpdesk_count,
            'zoom_today'            =>  $zoom_today,
            'helpdesk_today'        =>  $helpdesk_today,
            
        ]);
    }
    public function helpdeskChart()
    {
        $data1   =   Helpdesk::SelectRaw('COUNT(id) as jumlah,status_id')
                                ->groupBy('status_id')->get();
        $data2  =   Status::withCount('helpdesks')->get();
        $data       =   $data2->flatten(1)->pluck('helpdesks_count');
        $labels     =   $data2->flatten(1)->pluck('name');

        $colors = $labels->map(function($item) {
            return $rand_color = '#' . substr(md5(mt_rand()), 0, 6);
        });

        $chart = new HelpdeskChart;
        $chart->labels($labels);
        $chart->dataset('Jumlah helpdesk', 'pie', $data)->backgroundColor($colors);

        return view('helpdesk.chart', [
            'chart' => $chart,
        ]);
    }
    public function report()
    {
        return view('report.index');
    }
    public function processReport(Request $request)
    {
        \Validator::make($request->all(),[
            'start'     =>  'required',
            'end'       =>  'required',
            'type'      =>  'required'
        ])->validate();
            $start      =   $request->start;
            $end        =   $request->end;
            $type       =   $request->type;
            if($request->type == 'helpdesk')
            {
                return Excel::download(new ReportExport($start,$end,$type),$start.'-'.$end.'-Report_Helpdesk.xlsx');
            }else
            {
                return Excel::download(new ReportExport($start,$end,$type),$start.'-'.$end.'-Report_Zoom.xlsx');
            }
    }
   
}
