<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class RoleController extends Controller
{
    public function index()
    {
        $role       =   Role::all();
        return view('role.index',[
            'role'      =>  $role
        ]);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50'
        ]); 
        //$activity = Activity::all()->last();
        $role = Role::firstOrCreate(['name' => $request->name]);
        return redirect()->route('role.index')->with('status','role berhasil ditambah');
    }
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        //$activity = Activity::all()->last();
        return redirect()->route('role.index')->with('status','role berhasil dihapus');
    }
    public function rolePermission(Request $request)
    {
        $role = $request->get('role');
        
        //Default, set dua buah variable dengan nilai null
        $permissions = null;
        $hasPermission = null;
        
        //Mengambil data role
        $roles = Role::all()->pluck('name');
        
        //apabila parameter role terpenuhi
        if (!empty($role)) {
            //select role berdasarkan namenya, ini sejenis dengan method find()
            $getRole = Role::findByName($role);
            
            //Query untuk mengambil permission yang telah dimiliki oleh role terkait
            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();
            
            //Mengambil data permission
            $permissions = Permission::all()->pluck('name');
        }
        $permissions_list = Permission::all();
        //return $permissions_list;
        //$activity = Activity::all()->last();
        return view('role/role_permission', compact('roles', 'permissions', 'hasPermission','permissions_list'));
    }
    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:permissions'
        ]);

        $permission = Permission::firstOrCreate([
            'name' => $request->name
        ]);
        return redirect()->route('role.roles_permission')->with('status','permission berhasil ditambah');
    }
    public function setRolePermission(Request $request, $role)
    {
        //select role berdasarkan namanya
        $role = Role::findByName($role);
        
        //fungsi syncPermission akan menghapus semua permissio yg dimiliki role tersebut
        //kemudian di-assign kembali sehingga tidak terjadi duplicate data
        $role->syncPermissions($request->permission);
        return redirect()->back()->with(['status' => 'Permission to Role Saved!']);
    }
    public function deletePermission($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
        //$activity = Activity::all()->last();
        return redirect()->route('role.roles_permission')->with('status','permission berhasil dihapus');
    }
    
}
