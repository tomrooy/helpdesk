<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Helpdesk;
use App\Models\User;
use App\Models\HelpdeskCategory;
use App\Models\HelpdeskSubCategory;
use App\Models\HelpdeskDetail;
use PDF;


class HelpdeskAlphaController extends Controller
{
    public function create()
    {
        $category   =   Category::all();
        return view('front.helpdesk_alpha.create',[
            'category'  =>  $category
        ]);
    }
    public function getSubcategory($id)
    {
        $data   =   HelpdeskSubCategory::where('category_id',$id)->pluck('subcategory_id','name');
        return json_encode($data);
    }
    public function searchName(Request $request)
    {
        // check if ajax request is coming or not
        if($request->ajax()) {
            // select country name from database
            $data = User::where('name', 'LIKE', $request->name.'%')->limit(4)
                ->get();
            // declare an empty array for output
            $output = '';
            // if searched countries count is larager than zero
            if (count($data)>0) {
                // concatenate output to the array
                $output = '<ul class="list-group" style="display: block; position: relative; z-index: 1">';
                // loop through the result array
                foreach ($data as $row){
                    // concatenate output to the array
                    $output .= '<li class="list-group-item">'.$row->name.'-'.$row->nip.'-'.$row->location. '</li>';
                }
                // end of output
                $output .= '</ul>';
            }
            else {
                // if there's no matching results according to the input
                $output .= '<li class="list-group-item" value="">'.'No results'.'</li>';
            }
            // return output result array
            return $output;
        }
    }
    public function searchTicket(Request $request)
    {
        // check if ajax request is coming or not
        if($request->ajax()) {
            // select country name from database
            $data = Helpdesk::where('ticket_number', 'LIKE', $request->name.'%')
                ->limit(4)->get();
            // declare an empty array for output
            $output = '';
            // if searched countries count is larager than zero
            if (count($data)>0) {
                // concatenate output to the array
                $output = '<ul  class="list-group text-info" style="display: block; position: relative; z-index: 1">';
                // loop through the result array
                foreach ($data as $row){
                    // concatenate output to the array
                    $output .= '<li class="list-group-item text-info">'.$row->ticket_number.'</li>';
                }
                // end of output
                $output .= '</ul>';
            }
            else {
                // if there's no matching results according to the input
                $output .= '<li class="list-group-item text-info" value="">'.'No results'.'</li>';
            }
            // return output result array
            return $output;
        }
    }
    public function store(Request $request)
    {
        $ticket     =    new  Helpdesk;
        try{
            $messages =     $ticket->messages();
            $validator  =   \Validator::make($request->all(),[
                'name'          =>  'required',
                'img1'          =>  'nullable|max:400',
                'img2'          =>  'nullable|max:400',
                'captcha'       => 'required|captcha'   
            ], $messages);
            if($validator->passes())
            {
                $ticket     =   $ticket->generateticket($request->category);
                if($request->img1) {
                    $imagePath = $request->img1;
                    $imageName1 = '1'.$ticket.'.'.$request->img1->getClientOriginalExtension();
                
                    $path = $request->img1->storeAs('uploads/helpdesks', $imageName1, 'public');
                }else
                {
                    $imageName1 = null;
                }
                if($request->img2) {
                    $imagePath = $request->img2;
                    $imageName2 = '2'.$ticket.'.'.$request->img2->getClientOriginalExtension();
                    $path = $request->img2->storeAs('uploads/helpdesks', $imageName2, 'public');
                }else
                {
                    $imageName2 = null;
                }
               
                Helpdesk::create([
                    'ticket_number'     =>  $ticket,
                    'request_by'        =>  $request->nip,
                    'status_id'         =>  '1',
                    'priority_id'       =>  $request->priority,
                    'title'             =>  $request->name,
                    'category_id'       =>  $request->category,
                    'subcategory_id'    =>  $request->subcategory,
                    'created_by'        =>  $request->nip,
                ]);

                HelpdeskDetail::create([
                    'ticket_number'     =>  $ticket,
                    'status_id'         => '1',
                    'description'       =>  $request->description,
                    'image_a'           =>  $imageName1,
                    'image_b'           =>  $imageName2,
                    'created_by'        =>  $request->nip
                ]);
            
            return $ticket;
            }else{
                return response()->json(['errors' => $validator->errors()]);
            }

        }catch (\Exception $e)
        {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    
    
    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
