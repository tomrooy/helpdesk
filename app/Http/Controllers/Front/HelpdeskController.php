<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use App\Models\Helpdesk;
use App\Models\HelpdeskCategory;
use App\Models\HelpdeskPriority;
use App\Models\HelpdeskDetail;
use DB;
use Carbon\Carbon;
use PDF;
use SendWa;


class HelpdeskController extends Controller
{
    public function __construct()
    {
        //$this->middleware('permission:user-helpdesk-list',['only'=>['index','getHelpdesk','detail']]);
        //$this->middleware('permission:user-helpdesk-create',['only'=>['create','store']]);
    }
    public function index()
    {
        return view('front.helpdesk.index');
    }
    public function getHelpdesk()
    {
        $data   =   Helpdesk::where('request_by',\Auth::User()->nip)->get();
        return \DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('handle_by',function($data){
                if(!empty($data->handle_by))
                {
                    return $data->handle_by;
                }else
                {
                    return "Belum Dikerjakan";
                }
                
            })
            ->editColumn('status',function($data){
                $badge  = !empty($data->statuses->badge) ? $data->statuses->badge : '';
                $badge_name = !empty($data->statuses->name) ? $data->statuses->name : 'status kosong';
                return '<h3><span class="'.$badge.'">'.$badge_name.'</span> </h3>';
                //return $data->statuses->name;
            })
            ->editColumn('category',function($data){
                return $data->helpdesk_categories->name. '<br>'. $data->helpdesk_subcategories->name;
            })
            ->addColumn('Actions', function($data) {
                return 
                '<a href="'.route('front.helpdesk.detail', $data->id).'" class="btn btn-info btn-sm"><i class="bi bi-eye-fill"></i> </a>
                <a href="'.route('export_ticket', $data->ticket_number).'" class="btn btn-success btn-sm" id="getEditCategoryData" data-id="'.$data->id.'" ><i class="bi bi-download"></i></a>';        
                //<button type="button" class="btn btn-danger btn-sm" id="deleteCategory" data-id="'.$data->id.'" ><i class="fa fa-trash"></i> Delete</button
            })
            ->rawColumns(['Actions','status','category'])
            ->make(true);
    }
    public function create()
    {
        $priority   =   HelpdeskPriority::all();
        $category   =   HelpdeskCategory::all();
        return view('front.helpdesk.create',[
            'category'  =>  $category,
            'priority'  =>  $priority
        ]);
    }
    public function store(Request $request)
    {
        $helpdesk       =    new  Helpdesk;
        $id             =    IdGenerator::generate(['table' => 'helpdesks','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
        $ticket         =   $request->category.'-'.$id;
        try{
            $messages =     $helpdesk->messages();
            $validator  =   \Validator::make($request->all(),[
                'name'          =>  'required',
                'img1'          =>  'nullable|max:400',
                'img2'          =>  'nullable|max:400',
                'category'      =>  'required',
                'subcategory'   =>  'required',
                'priority'      =>  'required',
                'user_wa'       =>  'required',
                'description'   =>  'required',
            ], $messages);
            if($validator->passes())
            {
                //$ticket     =   $ticket->generateticket($request->category);
                if($request->img1) {
                    $imagePath = $request->img1;
                    $imageName1 = '1'.$ticket.'.'.$request->img1->getClientOriginalExtension();
                
                    $path = $request->img1->storeAs('uploads/helpdesks', $imageName1, 'public');
                }else
                {
                    $imageName1 = null;
                }
                if($request->img2) {
                    $imagePath = $request->img2;
                    $imageName2 = '2'.$ticket.'.'.$request->img2->getClientOriginalExtension();
                    $path = $request->img2->storeAs('uploads/helpdesks', $imageName2, 'public');
                }else
                {
                    $imageName2 = null;
                }
                $date   =   Carbon::now()->format('Y-m-d');
                $userphone = $request->user_wa;
                if($userphone != NULL)
                {
                    $userphone = ltrim($userphone, $userphone[0]);
                    $userphone = '62'.$userphone;
                }
                DB::beginTransaction();
                $helpdesk->index_number     =   $id;
                $helpdesk->ticket_number    =   $ticket;
                $helpdesk->date             =   $date;
                $helpdesk->request_by       =   \Auth::User()->nip;
                $helpdesk->status_id        =   '1';
                $helpdesk->priority_id      =   $request->priority;
                $helpdesk->title            =   $request->name;
                $helpdesk->category_id      =   $request->category;
                $helpdesk->subcategory_id   =   $request->subcategory;
                $helpdesk->user_wa          =   $request->user_wa;
                $helpdesk->created_by       =   \Auth::User()->nip;
                $helpdesk->save();

                HelpdeskDetail::create([
                    'ticket_number'     =>  $ticket,
                    'status_id'=> '1',
                    'description'       =>  $request->description,
                    'image_a'           =>  $imageName1,
                    'image_b'           =>  $imageName2,
                    'created_by'        =>  \Auth::User()->nip
                ]);
                
                $user       = $helpdesk->users->name;
                $location   = $helpdesk->users->userLocations->name;
                $text =
"Ada Permintaan bantuan Layanan TI dari.
Nama : $user
Bagian/wilayah : $location
Permintaan : $request->name
Category/Subcatgory : $request->category $request->subcategory
Prioritas : $request->priority
Detail Penjelasan : $request->description
Untuk Tindaklanjut laporan dapat dilakukan di :  https://ithelpdesk.itjenkumham.id/admin/module/helpdesk/$ticket/process-helpdesk
";
                $text2 = 
"Mohon menunggu tim IT akan segera menindaklanjuti permohonan bantuan IT Anda
Ticket Code anda : $helpdesk->ticket_number
Terima Kasih
2023 Tim IT Inspektorat Jenderal";
                SendWa::send_group($text,$text2,$userphone);
                DB::commit();
                return $ticket;
            }else{
                return response()->json(['errors' => $validator->errors()]);
            }

        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function detail($id)
    {
        $data   =   Helpdesk::with('helpdesk_details')->findOrfail($id);
        return view('front.helpdesk.detail',[
            'helpdesk'  =>  $data
        ]);
        
    }
    public function getTicket($id)
    {
        $ticket =   Helpdesk::with('helpdesk_details')->where('ticket_number',$id)->first();
        $ticket ? $status = true : $status = false;
        
        return view('front.helpdesk.get_ticket',[
            'ticket'    => $ticket,
            'status'    => $status
        ]);
    }
    public function exportTicket($id)
    {
        $ticket =   Helpdesk::with('helpdesk_details')->where('ticket_number',$id)->first();
        $status  = false;
        $pdf = PDF::loadView('front.helpdesk.get_ticket',[
            'ticket' => $ticket,
            'status'    => $status
        ])->setPaper('A5', 'potrait');
        $now    =   \Carbon\Carbon::now()->format('H:m');
        return $pdf->stream('pdf_file-'.$now.'.pdf');
    }
}
