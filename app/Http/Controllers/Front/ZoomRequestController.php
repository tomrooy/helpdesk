<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\Jobs\SendEmailZoom;
use App\Models\ZoomRequest;
use App\Models\Zoom;
use App\Models\Time;
use DB;
use SendWa;
use Carbon\Carbon;

class ZoomRequestController extends Controller
{
    public function __construct()
    {

    }
    public function index()
    {
        $data   =   ZoomRequest::where('request_by',\Auth::User()->location)->get();
        return view('front.zoom_request.index',[
            'zoomrequest'  =>  $data
        ]);
    }
    public function create()
    {
        $time   =  Time::all();
        return view('front.zoom_request.create',[
            'time'  =>  $time
        ]);
    }
    public function store(Request $request)
    {
        $message =[
            'name.required' =>  'judul harus di isi',

        ];
        $validator = \Validator::make($request->all(),[
            'name'          =>  'required',
            'participant'   =>  'required',
            'category'      =>  'required',
            'description'   =>  'nullable',
            'date'          =>  'required',
            'start'         =>  'required',
            'end'           =>  'required',
            'attachment'    =>  'nullable',
            'user_wa'       =>  'required'
        ],$message);
        if($validator->passes())
        {
            if($request->participant == '100')
            {          
                $zaidal     = Zoom::where('created_by_account', 'Akun Zaidal')->where('date', $request->date)->where('start',$request->start)->get();
                $tommy      = Zoom::where('created_by_account', 'Akun Tommy')->where('date', $request->date)->where('start',$request->start)->get();
                $ari        = Zoom::where('created_by_account', 'Akun Ari')->where('date', $request->date)->where('start',$request->start)->get();
                $rintaka    = Zoom::where('created_by_account', 'Akun Rintaka')->where('date', $request->date)->where('start',$request->start)->get();
                $simon      = Zoom::where('created_by_account', 'Akun Simon')->where('date', $request->date)->where('start',$request->start)->get();
                if(count($zaidal) > 0 && count($tommy) > 0 && count($ari) > 0 && count($rintaka) > 0 && count($simon) > 0)
                {
                   $data = 'Gagal Tambah Data.Semua Akun Zoom sudah digunakan untuk kegiatan di tanggal dan jam yang sama dengan akun yang sama';
                   return $data;
                }
            }elseif($request->participant == '500-1000')
            {

            }else{
                $cek_data1 = Zoom::where('created_by_account', 'Akun Utama')->where('date', $request->date)->where('start',$request->start)->get();
                if(count($cek_data1) > 0)
                {
                    $data = 'Gagal Tambah Data. Akun Zoom sudah digunakan untuk kegiatan di tanggal dan jam yang sama dengan akun yang sama';
                    return $data;
                }
            }
            $id         =    IdGenerator::generate(['table' => 'zoom_requests','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
            $ticket     = 'ZM-'.$id;
            $userphone = $request->user_wa;
                if($userphone != NULL)
                {
                    $userphone = ltrim($userphone, $userphone[0]);
                    $userphone = '62'.$userphone;
                }
            try{
                DB::beginTransaction();
                $zoom   =   new ZoomRequest;
                $zoom->index_number     =   $id;
                $zoom->ticket_number    =   $ticket;
                $zoom->name             =   $request->name;
                $zoom->participant      =   $request->participant;
                $zoom->category         =   $request->category;
                $zoom->presensi         =   $request->activity;
                $zoom->description      =   $request->description;
                $zoom->request_by       =   \Auth::User()->location;
                $zoom->date             =   $request->date;
                $zoom->start            =   $request->start;
                $zoom->end              =   $request->end;
                $zoom->created_by       =   \Auth::User()->nip;
                $zoom->status           =   'Dalam Proses';
                $zoom->save();

                $user       = $zoom->created_by_users->name;
                $location   = $zoom->created_by_users->userLocations->name;
                $text =
"Ada Permintaan Link Zoom dari.
Nama : $user
Kegiatan : $zoom->name
Bagian : $zoom->request_by
Time : $zoom->date $zoom->start
Paserta : $zoom->participant orang

Untuk Pengisian data dapat dilakukan di :  https://ithelpdesk.itjenkumham.id
";
                $text2 = 
"Mohon menunggu tim IT akan segera menindaklanjuti permohonan link Zoom Anda
Terima Kasih
2023 Tim IT Inspektorat Jenderal";
                SendWa::send_group($text,$text2,$userphone);
                //dispatch(new SendEmailZoom());
                DB::commit();
                
            }catch(\exception $e)
            {
                DB::rollback();
                //return response()->json(['errors' => $e->getMessage()]);
                $data   =   $e->getMessage();
                return $data;
            }
        }else{
            return response()->json(['errors' => $validator->errors()]);
        }
        
    }
    public function show($id)
    {
        $zoom   =   ZoomRequest::findOrFail($id);
        return view('front.zoom_request.detail',[
            'zoom'      =>  $zoom
        ]);

    }
}
