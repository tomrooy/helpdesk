<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Helpdesk;
use App\Models\ZoomRequest;
use App\Models\Zoom;
use App\Models\Time;
use App\Models\HelpdeskCategory;
use App\Models\HelpdeskPriority;
use App\Models\HelpdeskDetail;
use App\Jobs\SendEmailZoom;
use App\Mail\TestEmail;
use Mail;
use DB;
use SendWa;
use Carbon\Carbon;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Spatie\Activitylog\Models\Activity;



class NologinController extends Controller
{
    public function index()
    {
        return view('front.no_login.index');
    }
    public function viewRequestZoom()
    {
        $time   =  Time::all();
        return view('front.no_login.zoom_request',[
            'time'      =>  $time
        ]);
    }
    public function storeRequestZoom(Request $request)
    {
        $message =[
            'name.required' =>  'Nama Yang Request harus di isi',

        ];
        $validator = \Validator::make($request->all(),[
            'name'          =>  'required',
            'participant'   =>  'required',
            'subject'       =>  'required',
            'category'      =>  'required',
            'date'          =>  'required',
            'start'         =>  'required',
            'end'           =>  'required',
            'attachment'    =>  'nullable',
            'handphone'     =>  'required|numeric',
            'captcha'       => 'required|captcha'   
        ],$message);
        if($validator->passes())
        {
            if($request->participant == '100')
            {          
                $rifaldi = Zoom::where('created_by_account', 'Akun Rifaldy')->where('date', $request->date)->where('start',$request->start)->get();
                $tommy = Zoom::where('created_by_account', 'Akun Tommy')->where('date', $request->date)->where('start',$request->start)->get();
                $ari = Zoom::where('created_by_account', 'Akun Ari')->where('date', $request->date)->where('start',$request->start)->get();
                if(count($rifaldi) > 0 && count($tommy) > 0 && count($ari) > 0)
                {
                   $data = 'Gagal Tambah Data.Semua Akun Zoom sudah digunakan untuk kegiatan di tanggal dan jam yang sama dengan akun yang sama';
                   return $data;
                }
            }elseif($request->participant == '500-1000')
            {

            }else{
                $cek_data1 = Zoom::where('created_by_account', 'Akun Utama')->where('date', $request->date)->where('start',$request->start)->get();
                if(count($cek_data1) > 0)
                {
                    $data = 'Gagal Tambah Data. Akun Zoom sudah digunakan untuk kegiatan di tanggal dan jam yang sama dengan akun yang sama';
                    return $data;
                }
            }
            $id         =    IdGenerator::generate(['table' => 'zoom_requests','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
            $ticket     = 'ZM-'.$id;
            $userphone = $request->handphone;
            if($userphone != NULL)
            {
                $userphone = ltrim($userphone, $userphone[0]);
                $userphone = '62'.$userphone;
            }
             
            try{
                DB::beginTransaction();
                $zoom                   =   new ZoomRequest;
                $zoom->index_number     =   $id;
                $zoom->ticket_number    =   $ticket;
                $zoom->name             =   $request->subject;
                $zoom->participant      =   $request->participant;
                $zoom->category         =   $request->category;
                $zoom->presensi         =   $request->activity;
                $zoom->description      =   $request->description;
                $zoom->request_by       =   $request->location;
                $zoom->date             =   $request->date;
                $zoom->start            =   $request->start;
                $zoom->user_wa          =   $request->handphone;
                $zoom->end              =   $request->end;
                $zoom->created_by       =   $request->nip;
                $zoom->status           =   'Dalam Proses';
                $zoom->save();
                DB::commit();

                $text =
"Ada Permintaan Link Zoom dari.
Nama : $request->name
Kegiatan : $zoom->name
Bagian : $zoom->request_by
Time : $zoom->date $zoom->start
Paserta : $zoom->participant orang

Untuk Pengisian data dapat dilakukan di :  https://ithelpdesk.itjenkumham.id
";
                $text2 = 
"Mohon menunggu tim IT akan segera menindaklanjuti permohonan link Zoom Anda
Terima Kasih
2023 Tim IT Inspektorat Jenderal";
                SendWa::send_group($text,$text2,$userphone);
                //dispatch(new SendEmailZoom($request->name));
                
                
            }catch(\exception $e)
            {
                DB::rollback();
                //return response()->json(['errors' => $e->getMessage()]);
                $data   =   $e->getMessage();
                return $data;
            }
        }else{
            return response()->json(['errors' => $validator->errors()]);
        }
    }
    public function viewHelpdesk()
    {
        $priority   =   HelpdeskPriority::all();
        $category   =   HelpdeskCategory::all();
        return view('front.no_login.helpdesk_request',[
            'category'  =>  $category,
            'priority'  =>  $priority
        ]);
    }
    public function storeHelpdesk(Request $request)
    {
        $helpdesk       =    new  Helpdesk;
        $id             =    IdGenerator::generate(['table' => 'helpdesks','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
        $ticket         =    $request->category.'-'.$id;
        try{
            $messages =     $helpdesk->messages();
            $validator  =   \Validator::make($request->all(),[
                'name'          =>  'required',
                'subject'       =>  'required',
                'img1'          =>  'nullable|max:400',
                'img2'          =>  'nullable|max:400',
                'category'      =>  'required',
                'subcategory'   =>  'required',
                'priority'      =>  'required',
                'handphone'     =>  'required|numeric',
                'captcha'       => 'required|captcha'
            ], $messages);
            if($validator->passes())
            {
                if($request->img1) {
                    $imagePath = $request->img1;
                    $imageName1 = '1'.$ticket.'.'.$request->img1->getClientOriginalExtension();
                
                    $path = $request->img1->storeAs('uploads/helpdesks', $imageName1, 'public');
                }else
                {
                    $imageName1 = null;
                }
                if($request->img2) {
                    $imagePath = $request->img2;
                    $imageName2 = '2'.$ticket.'.'.$request->img2->getClientOriginalExtension();
                    $path = $request->img2->storeAs('uploads/helpdesks', $imageName2, 'public');
                }else
                {
                    $imageName2 = null;
                }
                $userphone = $request->handphone;
                if($userphone != NULL)
                {
                    $userphone = ltrim($userphone, $userphone[0]);
                    $userphone = '62'.$userphone;
                }
                $date   =   Carbon::now()->format('Y-m-d');
                DB::beginTransaction();
                
                    $helpdesk->index_number      =  $id;
                    $helpdesk->ticket_number     =  $ticket;
                    $helpdesk->date            =  $date;
                    $helpdesk->request_by       =  $request->nip;
                    $helpdesk->status_id       =  '1';
                    $helpdesk->priority_id       =  $request->priority;
                    $helpdesk->title             =  $request->subject;
                    $helpdesk->user_wa           =   $request->handphone;
                    $helpdesk->category_id       =  $request->category;
                    $helpdesk->subcategory_id    =  $request->subcategory;
                    $helpdesk->created_by        =  $request->nip;
                
                $helpdesk->save();
                HelpdeskDetail::create([
                    'ticket_number'     =>  $ticket,
                    'status_id'         => '1',
                    'description'       =>  $request->description,
                    'image_a'           =>  $imageName1,
                    'image_b'           =>  $imageName2,
                    'created_by'        =>  $request->nip
                ]);
                $text =
"Ada Permintaan bantuan Layanan TI dari.
Nama : $request->name
Bagian/wilayah : $request->location
Permintaan : $request->subject
Category/Subcatgory : $request->category $request->subcategory
Prioritas : $request->priority
Detail Penjelasan : $request->description

Untuk Tindaklanjut laporan dapat dilakukan di :  https://ithelpdesk.itjenkumham.id/admin/module/helpdesk/$ticket/process-helpdesk
";
$text2 = 
"Mohon menunggu tim IT akan segera menindaklanjuti permohonan bantuan IT Anda
Ticket Code anda : $helpdesk->ticket_number
Terima Kasih
2023 Tim IT Inspektorat Jenderal";
                SendWa::send_group($text,$text2,$userphone);
                DB::commit();

            return $ticket;
            }else{
                return response()->json(['errors' => $validator->errors()]);
            }

        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    
    public function test_email()
    {
        Mail::to("tommyroysirait@gmail.com")->send(new TestEmail());

		return "Email telah dikirim";
    }
}
