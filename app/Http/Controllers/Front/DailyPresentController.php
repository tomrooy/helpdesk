<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DailyPresentStatus;
use App\Models\DailyPresent;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use DB;
use PDF;
Use Carbon\Carbon;

class DailyPresentController extends Controller
{
    public function getData()
    {
        $now    =   Carbon::now()->format('Y-m-d');
        $rekap          =   DailyPresentStatus::withCount(['daily_presents' => function (Builder $q) use($now){
                            $q->where('date',$now);
        }])->get();
        $not = 0;
        foreach($rekap as $item)
        {
            $kehadiran[]    = $item->name;
            $id[]           = $item->status_id;
            $count[]        = $item->daily_presents_count;
            $not            = $not + $item->daily_presents_count;
        }
            array_push($id,'belum_mengisi','total');
            array_push($kehadiran,'Belum Mengisi','Total Pegawai');
            array_push($count,21 - $not,21);

        return response()->json([
            'count'      => $count,
            'kehadiran'  => $kehadiran,
            'id'         => $id  
        ]);
        
    }
    Public Function index()
    {
        $daily      =   DailyPresentstatus::all();
        return view('front.daily_present.index',[
            'daily'     =>  $daily,
        ]);
    }
    public function store(Request $request)
    {
        //return $request->all();
        try{
            $messages   =   [
                'status.require'     =>  'Pilih Status Presensi Anda'
            ];
            $validator  =   \Validator::make($request->all(),[
                'nip'           =>      'required|max:255',
                'name'          =>      'required|max:255',
                'unit_kerja'    =>      'required|max:255',
                'jabatan'       =>      'required|max:255',
                'status'        =>      'required',
            ],$messages);                
            if($validator->passes())
            {
                $date = Carbon::now()->format('Y-m-d');
                $data   =   DailyPresent::where('date', $date)->where('nip',$request->nip)->get();
                if(count($data) > 0)
                {
                    return response()->json(['errors' => 'Pengisian Absen Sudah dilakukan']);
                }
                $folderPath = public_path('storage/uploads/present_signature/');

                $filename = $request->nip.date('mdYHis') . "-signature.png";
                $data_uri = $request->signed;
                $encoded_image = explode(",", $data_uri)[1];
                $decoded_image = base64_decode($encoded_image);

                $unit_kerja     =   (explode("-",$request->unit_kerja));
                $unit_kerja     = end($unit_kerja);

                DB::beginTransaction();
                file_put_contents($folderPath.$filename, $decoded_image);
                $data                       =   new DailyPresent;
                $data->nip                  =   $request->nip;
                $data->name                 =   $request->name;
                $data->unit                 =   ltrim($unit_kerja);
                $data->status_id            =   $request->status;
                $data->title                =   $request->jabatan;
                $data->signature            =   $filename;
                $data->date                 =   \Carbon\Carbon::now()->format('Y-m-d');
                $data->save();
                DB::commit();

                return $data;
            }else{
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
    public function rekap()
    {
        //$rekap      =   DailyPresentStatus::withCount('daily_presents')->whereHas('daily_presents', function($q) {
        //    $q->where('date','2022-05-25');
        //})->get();
        //$rekap         =    DailyPresent::selectRaw("status_id , COUNT(id) as y")->where('date' ,'2022-05-25')->groupBy('status_id')->orderBy('status_id', 'DESC')->get();
        $now    =   Carbon::now()->format('Y-m-d');
        $rekap          =   DailyPresentStatus::withCount(['daily_presents' => function (Builder $q) use($now){
                            $q->where('date',$now);
        }])->get();
        $not = 0;
        
        $data = DailyPresent::with('users')->whereDate('created_at', '=', Carbon::today())->get();
        $user   =   User::where('location','SIP')->get();
        $alpha = [];
        foreach($user as $item)
        {
            $data = DailyPresent::with('users')->whereDate('created_at', '=', Carbon::today())->where('nip',$item->nip)->first();
            if(!$data)
            {
                $alpha[] = $item->name;
            }
            
            
        }
        foreach($rekap as $item)
        {
            $kehadiran[]    = $item->name;
            $id[]           = $item->status_id;
            $count[]        = $item->daily_presents_count;
            $not            = $not + $item->daily_presents_count;
        }
            array_push($id,'belum_mengisi','total');
            array_push($kehadiran,'Belum Mengisi','Total Pegawai');
            array_push($count,21 - $not,'21');
        
        return view('front.daily_present.rekap',[
            'rekap'      =>  $rekap,
            'count'      => $count,
            'kehadiran'  => $kehadiran,
            'id'         => $id,
            'alpha'       => $alpha   
        ]);

    }
    public function rekap_store(Request $request)
    {
        \Validator::make($request->all(),[
            'date'                  =>  'required|max:200',
            'passcode'              =>  'required|max:50',
        ])->validate();
        $data   =   DailyPresent::where('date',$request->date)->get();
        $tanggal    =   $request->date;


        $date       =   $request->date;
        $date       = \Carbon\Carbon::parse($date)->locale('id');
        $date->settings(['formatFunction' => 'translatedFormat']);
        $date       =  $date->format('l, j F Y');

        if($request->passcode != "sip1966")
        {
            return redirect()->back()->with('errors_message','Passcode Salah');
        }
        if(count($data) < 1)
        {
            return redirect()->back()->with('errors_message','Belum ada pengisian daftar kehadiran hari tersebut');
        }
        $rekap          =   DailyPresentStatus::withCount(['daily_presents' => function (Builder $q) use($tanggal){
                                    $q->where('date', $tanggal);
                            }])->get();
        
        return view('front.daily_present.report_pdf',[
            'data'       =>  $data,
            'date'       =>  $date,
            'rekap'      =>  $rekap,   
        ]);

    }
}
