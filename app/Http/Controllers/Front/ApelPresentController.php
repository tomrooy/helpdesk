<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ApelPresent;
use App\Models\ApelActivity;
use DB;
use Carbon\Carbon;

class ApelPresentController extends Controller
{
    
    public function index($id)
    {
        $activity       =   ApelActivity::where('id',$id)->where('status','1')->first();
        
        if(!$activity)
        {
            return "tidak ada kegiatan berlangsung";
        }
        return view('front.apel_present.index',[
            'data'      =>  $activity
        ]);
    }
    public function store(Request $request)
    {
        try{
                $validator  =   \Validator::make($request->all(),[
                    'nip'           =>      'required|max:255',
                    'name'          =>      'required|max:255',
                    'unit_kerja'    =>      'required|max:255',
                    'jabatan'       =>      'required|max:255',
                    'signed'        =>      'required'
                ]);
        
            if($validator->passes())
            {
                $data   =   ApelPresent::where('activity_id', $request->activity_id)->where('nip',$request->nip)->get();
                if(count($data) > 0)
                {
                    return response()->json(['errors' => 'Pengisian Absen Sudah dilakukan']);
                }

                $folderPath = public_path('storage/uploads/apel/signature/');
                $filename = $request->nip.date('mdYHis') . "-signature.png";
                $data_uri = $request->signed;
                $encoded_image = explode(",", $data_uri)[1];
                $decoded_image = base64_decode($encoded_image);
               
                DB::beginTransaction();
                file_put_contents($folderPath.$filename, $decoded_image);
                $data                       =   new ApelPresent;
                $data->nip                  =   $request->nip;
                $data->date                 =   Carbon::now()->format('Y-m-d');
                $data->activity_id          =   $request->activity_id;
                $data->name                 =   $request->name;
                $data->location             =   $request->unit_kerja;
                $data->title                =   $request->jabatan;
                $data->signature            =   $filename;
                $data->save();
                DB::commit();
                return $data;
            }else{
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
}
