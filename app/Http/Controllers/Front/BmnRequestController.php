<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bmn;
use App\Models\BmnRequest;
use App\Models\BmnRequestDetail;
use App\Models\BmnRequestPriority;
use App\Models\BmnRequestSession;
use App\Models\BmnCategory;
use DB;
use SendWa;
use Carbon\Carbon;
use Haruncpi\LaravelIdGenerator\IdGenerator;


class BmnRequestController extends Controller
{
    public function index()
    {
        return view('front.bmn_request.index');
    }
    public function getData()
    {
        $data    =   BmnRequest::latest()->get();
        return \DataTables::of($data)
                        ->addIndexColumn()
                        ->editColumn('date', function($data){
                            if($data->sesi == '1')
                            {
                                $sesi = "Pagi";
                            }elseif($data->sesi == '2' )
                            {
                                $sesi = "Siang";
                            }else
                            {
                                $sesi = "Sepanjang Hari";
                            }
                            return $data->date.'<br><b>'.$sesi.'</b>';
                        })
                        ->editColumn('subject', function($data){
                            return $data->subject.'<br><b>'.$data->bmns->name.'</b>';
                        })
                        ->editColumn('status_id',function($data){
                            if($data->status_id == '1')
                            {
                                $status = '<button class="btn btn-info">Permohonan Baru</button>';
                            }elseif($data->status_id == '2' )
                            {
                                $status = '<button class="btn btn-success">Permohonan Disetujui</button>';
                            }else
                            {
                                $status = '<button class="btn btn-danger">Permohonan Ditolak</button>';
                            }
                            return $status;
                        })
                        ->addColumn('Aksi', function($data)
                        {
                            $view    =   '<a href="#" type="button" class="btn btn-success btn-sm" id="process_helpdesk" alt="Proses bmn_request" ><i class="bi bi-eye-fill"></i> Lihat </a>';
                            return $view;
                        })
                        ->rawColumns(['Aksi','subject','date','status_id'])
                        ->make(true);
    }
    public function getDataBmn1(Request $request)
    {
        $bmn            =   Bmn::where('category_id',$request->id)->pluck('bmn_id','name');
        if($request->sesi = '3')
        {
            $bmn_request    =   BmnRequest::where('date',$request->date)->get();

            count($bmn_request) > 0 ? $status = "Ruangan Sudah Dipesan" : $status = "Ruangan tersedia" ;

        }else
        {
            $bmn_request    =   BmnRequest::where('date',$request->date)
                                        ->where('sesi',$request->sesi)
                                        ->OrWhere('sesi',3)->get();
            count($bmn_request) > 0 ? $status = "Ruangan Sudah Dipesan" : $status = "Ruangan tersedia" ;
        
        }
        return response()->json([
            'status' =>  $status,
            'bmn'    =>  $bmn
        ]);
    }
    public function getDataBmn($id)
    {
        $bmn            =   Bmn::where('category_id',$id)->pluck('bmn_id','name');
        return json_decode($bmn);
    }
    public function create()
    {
        $sesi           =   BmnRequestSession::all();
        $priority       =   BmnRequestPriority::all();
        $bmn_category   =   BmnCategory::all();
        return view('front.bmn_request.create',[
            'bmn_category'      =>  $bmn_category,
            'sesi'              =>  $sesi,
            'priority'          =>  $priority
        ]);
    } 
    public function store(Request $request)
    {
        $message =[
            'subject.required' =>  'Nama Yang Request harus di isi',

        ];
        $validator = \Validator::make($request->all(),[
            'subject'                =>  'required',
            'category'               =>  'required',
            'bmn'                    =>  'required',
            'priority'               =>  'required',
            
            
        ],$message);
        if($validator->passes())
        {
            $id             =    IdGenerator::generate(['table' => 'bmn_requests','field' => 'index_number', 'length' => 4, 'prefix' =>'0']);
            $ticket         = 'BMN-'.$id;
            $userphone      = $request->user_wa;
            if($userphone != NULL)
            {
                $userphone = ltrim($userphone, $userphone[0]);
                $userphone = '62'.$userphone;
            }
             
            try{
                DB::beginTransaction();
                $bmn_request                            =   new BmnRequest;
                $bmn_request->index_number              =   $id;
                $bmn_request->ticket_number             =   $ticket;
                $bmn_request->subject                   =   $request->subject;
                $bmn_request->date                      =   $request->date_request;
                $bmn_request->sesi                      =   $request->sesi;
                $bmn_request->priority_id               =   $request->priority;
                $bmn_request->user_wa                   =   $request->user_wa;
                $bmn_request->request_by                =   \Auth::User()->nip;
                $bmn_request->category_id               =   $request->category;
                $bmn_request->status_id                 =   '1';
                $bmn_request->bmn_id                    =   $request->bmn;
                $bmn_request->created_by                =   \Auth::User()->nip;
                $bmn_request->save();


                $bmn_request_detail                     =   new BmnRequestDetail;
                $bmn_request_detail->ticket_number      =   $ticket;
                $bmn_request_detail->status_id          =   '1';
                $bmn_request_detail->description        =   $request->description;
                $bmn_request_detail->created_by         =   \Auth::User()->nip;
                $bmn_request_detail->save();
                
                $bmn_name   =   $bmn_request->bmns->name;
                $nama_pegawai   =   $bmn_request->employees->name;
                $bagian         =   $bmn_request->employees->location;
                $permintaan     =   $bmn_request->bmns->name;
                $tanggal        =   $bmn_request->date;
                $text =
"Ada permintaan Layanan BMN :
Nama Pegawai    : $nama_pegawai
Bagian          : $bagian
Permintaan      : $permintaan
Tanggal         : $tanggal
Mohon Konfirmasi pada Aplikasi IT Helpdesk";
                $text2 = 
"Mohon Menunggu Tim Rumah Tangga Akan Merespon Permintaan Anda";
                SendWa::send_group_bmn($text,$text2,$userphone);
                DB::commit();

            }catch(\exception $e)
            {
                DB::rollback();
                $data   =   $e->getMessage();
               
            }
        }else{
            return response()->json(['errors' => $validator->errors()]);
        }
    }
    public function calendar()
    {
        $bmn_request        =   BmnRequest::whereHas('bmns',function($q){
            $q->where('type_id','0');
        })->where('status_id','2')->get();
        return view('umum::bmn_request.calendar',[
            'bmn_request'   =>  $bmn_request
        ]);
    }
}
