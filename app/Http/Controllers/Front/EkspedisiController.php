<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class EkspedisiController extends Controller
{
    public function create()
    {
        return view('front.ekspedisi.create');
    }
}
