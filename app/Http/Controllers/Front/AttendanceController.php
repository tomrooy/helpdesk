<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Exports\AttendanceExport;
use App\Models\User;
use App\Models\Activity;
use App\Models\Attendance;
use App\Models\NotPresent;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;
use PDF;

class AttendanceController extends Controller
{
    public function index($id)
    {
        $data   =   Activity::where('activity_id', $id)->where('status','1')->first();
        if(!$data)
        {
            return 'Tidak Ada kegiatan Berlangsung';
        }
        return view('front.attendance.index',[
            'data'      =>  $data
        ]);
    }
    public function index2($id)
    {
        $data   =   Activity::where('activity_id', $id)->where('status','1')->first();
        if(!$data)
        {
            return 'Tidak Ada kegiatan Berlangsung';
        }
        return view('front.attendance.index2',[
            'data'      =>  $data
        ]);
    }
    public function dashboard()
    {
        $data   =   Activity::whereDate('date',Carbon::today())->get();
        return view('front.attendance.dashboard',[
            'data'      =>  $data
        ]);
    }
    public function store(Request $request)
    {
        try{
            $messages   =   [
                'photo_upload.max'          =>  'Mohon upload dengan file ukuran dibawah 500 KB',
                'photo_upload.required'     =>  'Wajib menyertakan bukti kegiatan'
            ];
            if($request->alpha_check == '1')
            {
                $validator  =   \Validator::make($request->all(),[
                    'nip'           =>      'required|max:255',
                    'name'          =>      'required|max:255',
                    'unit_kerja'    =>      'required|max:255',
                    'jabatan'       =>      'required|max:255',
                    'signed'        =>      'required',
                    'photo_upload'  =>      'required|max:500|mimes:jpg,jpeg,png'
                ], $messages);
            }else
            {
                $validator  =   \Validator::make($request->all(),[
                    'nip'           =>      'required|max:255',
                    'name'          =>      'required|max:255',
                    'unit_kerja'    =>      'required|max:255',
                    'jabatan'       =>      'required|max:255',
                    'signed'        =>      'required',
                    'photo_upload'  =>      'nullable|max:500'
                ], $messages);
            }
                
            if($validator->passes())
            {
                $data   =   Attendance::where('activity_id', $request->activity_id)->where('nip',$request->nip)->get();
                if(count($data) > 0)
                {
                    return response()->json(['errors' => 'Pengisian Absen Sudah dilakukan']);
                }
                $folderPath = public_path('storage/uploads/signature/');
                $filename = $request->nip.date('mdYHis') . "-signature.png";
                $data_uri = $request->signed;
                $encoded_image = explode(",", $data_uri)[1];
                $decoded_image = base64_decode($encoded_image);

                if($request->photo_upload) {
                    $imagePath = $request->photo_upload;
                    $imageName = $request->nip.$request->activity_id.date('mdYHis').'.'.$request->photo_upload->getClientOriginalExtension();
                    $path = $request->photo_upload->storeAs('uploads/activity_photo', $imageName, 'public');
                }else
                {
                    $imageName = null;
                }
                $category   =   $request->category;
                $unit_kerja =   $request->unit_kerja;
                //if($category == 'itjen')
                //{
                //    $unit_kerja     =   (explode("-",$unit_kerja));
                //    $unit_kerja     = end($unit_kerja);
                //}
                DB::beginTransaction();
                file_put_contents($folderPath.$filename, $decoded_image);
                $data                       =   new Attendance;
                $data->nip                  =   $request->nip;
                $data->activity_id          =   $request->activity_id;
                $data->photo                =   $imageName;
                $data->name                 =   $request->name;
                $data->location             =   $unit_kerja;
                $data->title                =   $request->jabatan;
                $data->signature            =   $filename;
                $data->date                 =   \Carbon\Carbon::now();
                $data->save();
                DB::commit();
                return $data;
            }else{
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['errors' => $e->getMessage()]);
        }
        
    }
    public function reportAttendance($id)
    {
        $data           =   Activity::where('activity_id', $id)->first();
        $attendance     =   Attendance::where('activity_id', $id)->get();
        if(!$data)
        {
            return 'Tidak Ada kegiatan Berlangsung';
        }
        $user       =   User::all();
        foreach($user as $item)
        {
            $cek     =   Attendance::where('activity_id', $id)->where('nip', $item->nip)->first();
            if(!$cek)
            {
                $not_present[] = [
                'name'                 =>   $item->name,
                'nip'                  =>   $item->nip,
                //$not_present->activity_id          =>   $item->activity_id,
                'location'             =>   $item->userLocations->name,
                //$not_present->title                =>   NULL,
                ];
               
            }
        }
        $not_present1 = json_encode($not_present);

        return view('front.attendance.report',[
            'data'      =>  $data,
            'attendance'=>  $attendance,
            'not_present'=>  $not_present1
        ]);
    }
    public function reportExcelAttendance($id)
    {
        return Excel::download(new AttendanceExport($id),'Presensi.xlsx');
    }
    public function reportAttendancePersonal(Request $request)
    {
        $attendance     =   Attendance::with('activities')->where('activity_id', $request->print_activity_id)->where('nip', $request->print_nip)->first();
        return view('front.attendance.report_personal',[
            'attendance'=>  $attendance
        ]);
    }
    public function reportAttendanceGroup(Request $request)
    {
        $data           =   Activity::where('activity_id', $request->print_activity_id2)->first();
        $attendance     =   Attendance::where('activity_id', $request->print_activity_id2)->get();

        
        return view('front.attendance.report',[
            'data'       =>  $data,
            'attendance' =>  $attendance,
        ]);
    }
    public function reportAttendance_pdf($id)
    {
        $data           =   Activity::where('activity_id', $id)->first();
    	$attendance     =   Attendance::where('activity_id', $id)->get();
        if(!$data)
        {
            return 'Tidak Ada kegiatan Berlangsung';
        }
        $user       =   User::all();
        foreach($user as $item)
        {
            $cek     =   Attendance::where('activity_id', $id)->where('nip', $item->nip)->first();
            if(!$cek)
            {
                $not_present[] = [
                'name'                 =>   $item->name,
                'nip'                  =>   $item->nip,
                //$not_present->activity_id          =>   $item->activity_id,
                'location'             =>   $item->userLocations->name,
                //$not_present->title                =>   NULL,
                ];
               
            }
        }
        $not_present1 = json_encode($not_present);
 
    	$pdf = PDF::loadview('front.attendance.report_pdf',[
            'attendance'=>$attendance,
            'data'       =>  $data,
            'not_present'=> $not_present1
        ]);
        $pdf->setPaper('A4', 'landscape');
    	return $pdf->download('laporan-pegawai.pdf');
    }
    public function seacrhSimpeg(Request $request)
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $response = json_decode($client->post(
            'https://dse.kemenkumham.go.id/index.php/home/get_ajax_pegawai/',
            array(
                'form_params' => array(
                    'nip' => $request->nip
                    )
                )
        )->getBody(), true);

        $data = $response[0];
        if(count($data) > 1)
        {
            foreach($response as $item)
            {

                $url = "https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/$request->nip.jpg";
                $handle = curl_init($url);
                curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
                $response = curl_exec($handle);
                $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                if ($httpCode == 404) 
                {
                    $url            = "https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/$request->nip.jpeg";
                    $handle = curl_init($url);
                    curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
                    $response = curl_exec($handle);
                    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                }else if($httpCode == 404)
                {
                    $url            = 'https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/' . $nip . '.png';
                }
                curl_close($handle);
                $response = [
                    'hasil'     =>  $item['hasil'],
                    'nama'      =>  $item['nama'],
                    'jabatan'   =>  $item['jabatan'],
                    'unitkerja' =>  $item['unitkerja'],
                    'url'       =>  $url,
                ];
                
            }
        }
        return $response;
       
    }
    public function addurl($id)
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $response = json_decode($client->post(
            'https://dse.kemenkumham.go.id/index.php/home/get_ajax_pegawai/',
            array(
                'form_params' => array(
                    'nip' => $id
                    )
                )
        )->getBody(), true);
        $data = $response[0];
        if(count($data) > 1)
        {
            foreach($response as $item)
            {
                $response = [
                    'hasil'     =>  $item['hasil'],
                    'nama'      =>  $item['nama'],
                    'jabatan'   =>  $item['jabatan'],
                    'unitkerja' =>  $item['unitkerja'],
                    'url'       =>  'https://simpeg.kemenkumham.go.id/siap/client/kumham/uploads/foto/"+query".jpg";'
                ];
                
            }
        }

        return $response;
    }
    public function syncUser()
    {
        $users       =   User::all();
        foreach($users as $items)
        {
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]);
            $response = json_decode($client->post(
                'https://dse.kemenkumham.go.id/index.php/home/get_ajax_pegawai/',
                array(
                    'form_params' => array(
                        'nip' => $items->nip
                        )
                    )
            )->getBody(), true);
            $data = $response[0];
            if(count($data) > 1)
            {
                foreach($response as $item)
                {
                    $unitkerja = $item['unitkerja'];
                    
                }
                //$str = "Hello world. It's a beautiful day.";
                $unitkerja=  (explode("-",$unitkerja));
                $unitkerja=  array_map('trim', $unitkerja);
                
                $user   =   User::where('nip',$items->nip)->first();
            
                switch($unitkerja)
                {
                    case count($unitkerja) == 4:
                        $user->main_location    =   $unitkerja[1];   
                        $user->middle_location  =   $unitkerja[2];
                        $user->sub_location     =   $unitkerja[3];
                        $user->save();
                        break;
                    case count($unitkerja) == 3:
                        $user->main_location    =   $unitkerja[1];   
                        $user->middle_location  =   $unitkerja[2];
                        $user->save();
                        break;
                    case count($unitkerja) == 2:
                        $user->main_location    =   $unitkerja[1];   
                        $user->save();
                        break;
                    default :
                        $user->main_location    =   $unitkerja[0];
                        $user->save();
                }
            }
            
        }
        
        return "sukses";
    }
    public function searchNip(Request $request)
    {
        // check if ajax request is coming or not
        if($request->ajax()) {
            // select country name from database
            $data = User::where('nip', 'LIKE', $request->nip.'%')->limit(4)
                ->get();
            // declare an empty array for output
            $output = '';
            // if searched countries count is larager than zero
            if (count($data)>0) {
                // concatenate output to the array
                $output = '<ul class="list-group" style="display: block; position: relative; z-index: 1">';
                // loop through the result array
                foreach ($data as $row){
                    // concatenate output to the array
                    $output .= '<li class="list-group-item">'.$row->name.'-'.$row->nip.'-'.$row->userLocations->name. '</li>';
                }
                // end of output
                $output .= '</ul>';
            }
            else {
                // if there's no matching results according to the input
                $output .= '<li class="list-group-item" value="">'.'No results'.'</li>';
            }
            // return output result array
            return $output;
        }
    }
    public function apelManual($id)
    {
        $data   =   Activity::where('activity_id', $id)->where('status','1')->first();
        if(!$data)
        {
            return 'Tidak Ada kegiatan Berlangsung';
        }
        return view('front.attendance.apel',[
            'data'      =>  $data
        ]);
    }
    public function Store_apelManual(Request $request)
    {
        try{
            $validator  =   \Validator::make($request->all(),[
                'nip'           =>      'required|max:255',
                'name'          =>      'required|max:255',
                'unit_kerja'    =>      'required|max:255',
                'jabatan'       =>      'required|max:255',
            ]);
        
                
            if($validator->passes())
            {
                $data   =   Attendance::where('activity_id', $request->activity_id)->where('nip',$request->nip)->get();
                if(count($data) > 0)
                {
                    return response()->json(['errors' => 'Pengisian Absen Sudah dilakukan']);
                }
                DB::beginTransaction();
                $data                       =   new Attendance;
                $data->nip                  =   $request->nip;
                $data->activity_id          =   $request->activity_id;
                $data->photo                =   'hadir.jpg';
                $data->name                 =   $request->name;
                $data->location             =   $request->unit_kerja;
                $data->title                =   $request->jabatan;
                $data->signature            =   'hadir.jpg';
                $data->date                 =   \Carbon\Carbon::now();
                $data->save();
                DB::commit();
                return $data;
            }else{
                return response()->json(['errors' => $validator->errors()]);
            }
        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['errors' => $e->getMessage()]);
        }
    }
}
