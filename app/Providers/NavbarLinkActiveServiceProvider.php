<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class NavbarLinkActiveServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/NavbarLinkActive.php';
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
