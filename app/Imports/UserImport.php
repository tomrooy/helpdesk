<?php

namespace App\Imports;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;
use Carbon\Carbon;
use DB;

class UserImport implements WithHeadingRow,ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
       DB::beginTransaction();
       try{
           $email       =   Str::slug($row['nama']);
           $nip         =   str_replace(' ','',$row['nip'] );
            if($nip != 199412152020121001)
            {
                $data = new User([
                    'name'      =>  $row['nama'],
                    'email'     =>  $email.'@itjenkumham.id',
                    'password'  =>  $nip,
                    'location'  =>  $row['lokasi'],
                    'nip'       =>   $nip,
                    'created_by'=>  \Auth::User()->nip
                ]);
                $data->save();
                $role = Role::where('name','User')->first();
                $data->assignRole($role);
            }
            DB::commit();
        
       }catch (\Exception $e)
       {
           DB::rollback();
           dd($e->getMessage());
       }
    }
}
