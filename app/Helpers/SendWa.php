<?php

namespace App\Helpers;
use App\Models\WaLog;

class SendWa
{
    public static function send_group($text,$text2,$userphone)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data1 = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => "6282364464977",
            "message" => $text
        );

        $data2 = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => $userphone,
            "message" => $text2
        );
        $param_post1 = json_encode($data1, JSON_PRETTY_PRINT);
        $param_post2 = json_encode($data2, JSON_PRETTY_PRINT);

        $post1        = curl_init("https://app.alatwa.com/api/send/message/text");
        $post2        = curl_init("https://app.alatwa.com/api/send/message/text");

        curl_setopt($post1, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post1, CURLOPT_POST, 1);
        curl_setopt($post1, CURLOPT_POSTFIELDS, $param_post1);
        curl_setopt($post1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post1, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post1, CURLOPT_TIMEOUT, 5);
        $response1 = curl_exec($post1);
        curl_close($post1);

        if($response1 == NULL)
        {
            $post1        = curl_init("https://app.alatwa.com/api/send/message/text");
            curl_setopt($post1, CURLOPT_HTTPHEADER, $header);
            curl_setopt($post1, CURLOPT_POST, 1);
            curl_setopt($post1, CURLOPT_POSTFIELDS, $param_post1);
            curl_setopt($post1, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($post1, CURLOPT_CONNECTTIMEOUT, 0); 
            curl_setopt($post1, CURLOPT_TIMEOUT, 5);
            $response1 = curl_exec($post1);
            curl_close($post1);
        }else{
            $response1 = json_decode($response1);
            if($response1->status == "ok")
            {
                $wa1 = new WaLog;
                $wa1->jenis_id   =   '1';
                $wa1->message_id =   $response1->message_id;
                $wa1->status     =   $response1->status;
                $wa1->to         =   $response1->to;
                $wa1->from       =   $response1->from;
                $wa1->message    =   $text;
                $wa1->save();
            }
        }



        curl_setopt($post2, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post2, CURLOPT_POST, 1);
        curl_setopt($post2, CURLOPT_POSTFIELDS, $param_post2);
        curl_setopt($post2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post2, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post2, CURLOPT_TIMEOUT, 5);
        $response2 = curl_exec($post2);
        curl_close($post2);

        if($response2 == NULL)
        {
            $post2        = curl_init("https://app.alatwa.com/api/send/message/text");
            curl_setopt($post2, CURLOPT_HTTPHEADER, $header);
            curl_setopt($post2, CURLOPT_POST, 1);
            curl_setopt($post2, CURLOPT_POSTFIELDS, $param_post2);
            curl_setopt($post2, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($post2, CURLOPT_CONNECTTIMEOUT, 0); 
            curl_setopt($post2, CURLOPT_TIMEOUT, 5);
            $response2 = curl_exec($post2);
            curl_close($post2);
        }else
        {
            $response2 = json_decode($response2);
            if($response2->status == 'ok')
            {
                $wa2 = new WaLog;
                $wa2->jenis_id   =   '2';
                $wa2->message_id =   $response2->message_id;
                $wa2->status     =   $response2->status;
                $wa2->to         =   $response2->to;
                $wa2->from       =   $response2->from;
                $wa2->message    =   $text2;
                $wa2->save();
            }
        }
      
        
    }
    public static function send_user($text2,$userphone)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => $userphone,
            "message" => $text2
        );

        $param_post = json_encode($data, JSON_PRETTY_PRINT);
        $post        = curl_init("https://app.alatwa.com/api/send/message/text");
        curl_setopt($post, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post, CURLOPT_POST, 1);
        curl_setopt($post, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($post);
        curl_close($post);
        $response = json_decode($response);

        if($response->status == 'ok')
        {
            $wa = new WaLog;
            $wa->jenis_id   =   '2';
            $wa->message_id =   $response->message_id;
            $wa->status     =   $response->status;
            $wa->to         =   $response->to;
            $wa->from       =   $response->from;
            $wa->message    =   $text2;
            $wa->save();
        }

    }
    public function send_it($text)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => '6282364464977',
            "message" => $text
        );
        $param_post = json_encode($data, JSON_PRETTY_PRINT);
        $post       = curl_init("https://app.alatwa.com/api/send/message/text");
        

        curl_setopt($post, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post, CURLOPT_POST, 1);
        curl_setopt($post, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($post);
        curl_close($post);
        $response = json_decode($response);
        
            if($response->status == "ok")
            {
                $wa = new WaLog;
                $wa->jenis_id   =   '1';
                $wa->message_id =   $response->message_id;
                $wa->status     =   $response->status;
                $wa->to         =   $response->to;
                $wa->from       =   $response->from;
                $wa->message    =   $text;
                $wa->save();
            }
    }
    public function progress_helpdesk_user($text,$userphone)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => $userphone,
            "message" => $text
        );

        $param_post = json_encode($data, JSON_PRETTY_PRINT);
        $post        = curl_init("https://app.alatwa.com/api/send/message/text");
        curl_setopt($post, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post, CURLOPT_POST, 1);
        curl_setopt($post, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($post);
        curl_close($post);
        $response = json_decode($response);

        if($response->status == 'ok')
        {
            $wa = new WaLog;
            $wa->jenis_id   =   '2';
            $wa->message_id =   $response->message_id;
            $wa->status     =   $response->status;
            $wa->to         =   $response->to;
            $wa->from       =   $response->from;
            $wa->message    =   $text;
            $wa->save();
        }
    }
    public function send_group_bmn($text,$text2,$userphone)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data1 = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => "6281314452362",
            "message" => $text
        );

        $data2 = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => $userphone,
            "message" => $text2
        );
        $param_post1 = json_encode($data1, JSON_PRETTY_PRINT);
        $param_post2 = json_encode($data2, JSON_PRETTY_PRINT);

        $post1        = curl_init("https://app.alatwa.com/api/send/message/text");
        $post2        = curl_init("https://app.alatwa.com/api/send/message/text");

        curl_setopt($post1, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post1, CURLOPT_POST, 1);
        curl_setopt($post1, CURLOPT_POSTFIELDS, $param_post1);
        curl_setopt($post1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post1, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post1, CURLOPT_TIMEOUT, 5);
        $response1 = curl_exec($post1);
        curl_close($post1);

        if($response1 == NULL)
        {
            $post1        = curl_init("https://app.alatwa.com/api/send/message/text");
            curl_setopt($post1, CURLOPT_HTTPHEADER, $header);
            curl_setopt($post1, CURLOPT_POST, 1);
            curl_setopt($post1, CURLOPT_POSTFIELDS, $param_post1);
            curl_setopt($post1, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($post1, CURLOPT_CONNECTTIMEOUT, 0); 
            curl_setopt($post1, CURLOPT_TIMEOUT, 5);
            $response1 = curl_exec($post1);
            curl_close($post1);
        }else{
            $response1 = json_decode($response1);
            if($response1->status == "ok")
            {
                $wa1 = new WaLog;
                $wa1->jenis_id   =   '1';
                $wa1->message_id =   $response1->message_id;
                $wa1->status     =   $response1->status;
                $wa1->to         =   $response1->to;
                $wa1->from       =   $response1->from;
                $wa1->message    =   $text;
                $wa1->save();
            }
        }
        curl_setopt($post2, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post2, CURLOPT_POST, 1);
        curl_setopt($post2, CURLOPT_POSTFIELDS, $param_post2);
        curl_setopt($post2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post2, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post2, CURLOPT_TIMEOUT, 5);
        $response2 = curl_exec($post2);
        curl_close($post2);

        if($response2 == NULL)
        {
            $post2        = curl_init("https://app.alatwa.com/api/send/message/text");
            curl_setopt($post2, CURLOPT_HTTPHEADER, $header);
            curl_setopt($post2, CURLOPT_POST, 1);
            curl_setopt($post2, CURLOPT_POSTFIELDS, $param_post2);
            curl_setopt($post2, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($post2, CURLOPT_CONNECTTIMEOUT, 0); 
            curl_setopt($post2, CURLOPT_TIMEOUT, 5);
            $response2 = curl_exec($post2);
            curl_close($post2);
        }else
        {
            $response2 = json_decode($response2);
            if($response2->status == 'ok')
            {
                $wa2 = new WaLog;
                $wa2->jenis_id   =   '2';
                $wa2->message_id =   $response2->message_id;
                $wa2->status     =   $response2->status;
                $wa2->to         =   $response2->to;
                $wa2->from       =   $response2->from;
                $wa2->message    =   $text2;
                $wa2->save();
            }
        }
        
    }
    public function send_umum($text)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => '6281314452362',
            "message" => $text
        );
        $param_post = json_encode($data, JSON_PRETTY_PRINT);
        $post       = curl_init("https://app.alatwa.com/api/send/message/text");
        

        curl_setopt($post, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post, CURLOPT_POST, 1);
        curl_setopt($post, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($post);
        curl_close($post);
        $response = json_decode($response);
        
            if($response->status == "ok")
            {
                $wa = new WaLog;
                $wa->jenis_id   =   '1';
                $wa->message_id =   $response->message_id;
                $wa->status     =   $response->status;
                $wa->to         =   $response->to;
                $wa->from       =   $response->from;
                $wa->message    =   $text;
                $wa->save();
            }
    }
    public function progress_bmn_request($text,$userphone)
    {
        $header = array(
            "Content-Type: application/json",
            "Authorization: d1aecc5f35a8aa39fbb5715c466a26c3"
        );

        $data = array(
            "device" => "085155318887",
            "type" => "text",
            "phone" => $userphone,
            "message" => $text
        );

        $param_post = json_encode($data, JSON_PRETTY_PRINT);
        $post        = curl_init("https://app.alatwa.com/api/send/message/text");
        curl_setopt($post, CURLOPT_HTTPHEADER, $header);
        curl_setopt($post, CURLOPT_POST, 1);
        curl_setopt($post, CURLOPT_POSTFIELDS, $param_post);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($post, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($post, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($post);
        curl_close($post);
        $response = json_decode($response);

        if($response->status == 'ok')
        {
            $wa = new WaLog;
            $wa->jenis_id   =   '2';
            $wa->message_id =   $response->message_id;
            $wa->status     =   $response->status;
            $wa->to         =   $response->to;
            $wa->from       =   $response->from;
            $wa->message    =   $text;
            $wa->save();
        }
    }

}