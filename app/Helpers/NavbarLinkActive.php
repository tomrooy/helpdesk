<?php
namespace App\Helpers;
 
use Illuminate\Support\Facades\DB;
 
class NavbarLinkActive {
      public static function set_active($uri, $output = 'active')
      {
      if( is_array($uri) ) {
        foreach ($uri as $u) {
          if (\Route::is($u)) {
            return $output;
          }
        }
      } else {
        if (\Route::is($uri)){
          return $output;
        }
      }
      }
    }