<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use App\Models\ApelPresent;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ApelExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($id)
    {
        $this->id     =   $id;
    }
    public function view() : view
    {

        $data       =   ApelPresent::with('users')->where('activity_id',$this->id)->get();
        return view('report.apel',[
            'data'          =>  $data,
        ]);
    }
}
