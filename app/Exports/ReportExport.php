<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use App\Models\Helpdesk;
use App\Models\Zoom;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($start,$end,$type)
    {
        $this->start    =   $start;
        $this->end      =   $end;
        $this->type     =   $type;
    }
    public function view() : view
    {
        if($this->type == 'helpdesk')
        {
            $data   =   Helpdesk::whereBetween('date',[$this->start , $this->end])->get();
        }else
        {
            $data   =   Zoom::whereBetween('date',[$this->start , $this->end])->get();
        }
        return view('report.'.$this->type,[
            'data'  =>  $data
        ]);
    }
}
