<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use App\Models\Attendance;
use App\Models\Activity;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class AttendanceExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($id)
    {
        $this->id     =   $id;
    }
    public function view() : view
    {
        $activity   =   Activity::where('activity_id',$this->id)->first();
        $data       =   Attendance::where('activity_id',$this->id)->get();
        $user       =   User::all();
        foreach($user as $item)
        {
            $cek     =   Attendance::where('activity_id', $this->id)->where('nip', $item->nip)->first();
            if(!$cek)
            {
                $not_present[] = [
                'name'                 =>   $item->name,
                'nip'                  =>   $item->nip,
                'location'             =>   $item->userLocations->name,
                ];
               
            }
        }
        $not_present1 = json_encode($not_present);
        return view('report.attendance',[
            'data'          =>  $data,
            'activity'      =>  $activity,
            'not_present'=>  $not_present1
        ]);
    }
    
    
}
